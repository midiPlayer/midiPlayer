#include "fileinput.h"

#include <QFile>

FileInput::FileInput() :
    path(),
    watch(false),
    watcher()
{
    connect(&watcher,SIGNAL(fileChanged(QString)),this,SLOT(read()));
}

void FileInput::setPath(QString pathP)
{
    path = pathP;
    read();
}

QString FileInput::getPath()
{
    return path;
}

bool FileInput::getWatch() const
{
    return watch;
}

void FileInput::setWatch(bool value)
{
    watch = value;
}

void FileInput::read()
{
    QString content;
    if(path.length() != 0){
        QFile f(path);
           if (!f.open(QIODevice::ReadOnly | QIODevice::Text))
               return;

           while (!f.atEnd()) {
               QByteArray line = f.readLine();
               content.append(line);
           }
           emit contentRead(content);
           watcher.addPath(path);
        f.close();
    }
}

