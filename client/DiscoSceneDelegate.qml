import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.2
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import WebSocketConnector 1.1
import Clipboard 1.0

Item {
            id: delegateItem
            width: discolistView.width
            height: dragRect.height

            onFocusChanged: {
                console.log("focus:" + focus);
            }

            Keys.onPressed: {
                if(event.key === Qt.Key_C && event.modifiers === Qt.ControlModifier){
                    console.log("copy");
                    var copy = {"type":"discodelegate","scene":modelData.sceneId};
                    clipboard.clip = JSON.stringify(copy);
                }
            }

            Clipboard{
                id: clipboard;
            }

            property WebSocketConnector parentWsc

            Component.onCompleted: {
                parentWsc.message.connect(function(msg){
                    if(msg.fusionTypeChanged !== undefined){
                         if(modelData.subSceneId === msg.fusionTypeChanged.subSceneId){
                             console.log("new combo box state:");
                             for(var i  =0; i < fusionTypeModel.count;i++){
                                 if(fusionTypeModel.get(i).name === msg.fusionTypeChanged.fusionType){
                                     fusionTypeCombo.currentIndex = i;
                                     break;
                                 }
                             }
                         }
                    }
                    if(msg.muteChanged !== undefined){
                        if(modelData.subSceneId === msg.muteChanged.subSceneId){
                             console.log("new mute state:");
                            if(msg.muteChanged.state)
                                activeBtn.setOff(false)
                            else
                                activeBtn.setOn(false)
                        }
                    }
                    if(msg.soloChanged !== undefined){
                        if(modelData.subSceneId === msg.soloChanged.subSceneId){
                             console.log("new solo state:");
                            if(msg.soloChanged.state){
                                soloBtn.setOn(false);
                                soloBtn.switchOffOthers();
                            }
                            else
                                soloBtn.setOff(false)
                        }
                    }
                });
            }

            Item {
                id: dragRect
                width: discolistView.width
                height: column.height
                //height:60
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter

                Rectangle{
                    id: column
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.right: parent.right
                    width:parent.width
                    height:discoRow.height + optionBox.height
                    color: delegateItem.focus ? "#33333333" : "#33000000"
                    border.color: "#55000000"
                    border.width: 1
                    anchors.margins: 0


                Item{
                    id:discoRow
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    height:60


                RowLayout{
                    z: 2

                    anchors.fill: parent
                    spacing: 1


                    Item{
                        id: dragger
                        height: parent.height
                        Layout.preferredWidth: 100

                        Image {
                            anchors.centerIn: parent
                            source: "icons/move.png"
                            fillMode: scale
                            width: parent.width*0.5
                            height: parent.height * 0.5

                        }

                        MouseArea {
                            id: mouseArea
                            anchors.fill:parent
                            drag.target: dragRect
                            drag.axis: Drag.YAxis
                            onPressed: {
                                dragRect.dragStart = delegateItem.y;
                                console.log(dragRect.dragStart);
                            }
                        }
                    }


                    Item{
                        Layout.fillWidth: true;
                        Layout.fillHeight: true
                    TextEdit {
                        id: nameEdit
                        text: modelData.name
                        color:"#fff"
                        enabled: false
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left

                        Keys.onEscapePressed: {
                            delegateItem.forceActiveFocus()
                        }

                        onTextChanged: {
                            if(text.indexOf("\n") != -1){
                                text = text.replace("\n","");
                                delegateItem.forceActiveFocus();
                            }
                        }
                        onFocusChanged: {
                            if(!focus){//send new name
                                parentWsc.send = JSON.stringify({"nameChanged":
                                                             {"scene":modelData.subSceneId,
                                                             "name":nameEdit.text}});
                            }
                        }
                        Component.onCompleted: {
                            parentWsc.onMessage.connect(function(msg) {
                                if(msg.hasOwnProperty("nameChanged") && msg.nameChanged.scene === modelData.subSceneId){
                                    nameEdit.text = msg.nameChanged.name;
                                }
                            });
                        }
                    }
                    MouseArea{
                        anchors.fill: parent
                        acceptedButtons: Qt.LeftButton | Qt.RightButton
                        onPressed: longClickTimer.go();
                        onReleased: {
                            longClickTimer.stop();
                            if(!longClickTimer.longClick){
                                if(mouse.button & Qt.LeftButton)
                                    optionBox.toggle();
                                delegateItem.forceActiveFocus();
                            }
                        }
                        onExited: longClickTimer.stop();
                        Timer{
                            property bool longClick: false;
                            id: longClickTimer
                            onTriggered: {
                                nameEdit.enabled = true;
                                nameEdit.forceActiveFocus();
                                console.log("yeah!")
                                longClick = true;
                            }
                            function go(){
                                longClick = false
                                restart()
                            }

                            interval: 300
                        }

                    }
                    }

                    ComboBox {
                        id:fusionTypeCombo
                        Layout.preferredWidth: 200
                        model: fusionTypeModel
                        currentIndex: modelData.fusionTypeId
                        onActivated: {
                            var msg = new Object();
                            msg.fusionTypeChanged = new Object();
                            msg.fusionTypeChanged.subSceneId = modelData.subSceneId;
                            msg.fusionTypeChanged.fusionType = model.get(index).name;
                            ws.send = JSON.stringify(msg);
                        }
                    }

                    Button{
                        id: delBtn
                        text:qsTr("Delete")
                        onClicked: {
                            var msg = new Object();
                            msg.deleteScene = modelData.subSceneId;
                            ws.send = JSON.stringify(msg);
                            importer.sendMessage({"msg":msg,"listModel":listModel});
                        }
                    }

                    Item{
                        Layout.preferredWidth: soloBtn.width+5;
                        height: parent.height;
                        PushLockBtn{
                            id: soloBtn
                            text: "S"
                            onStateOn: {
                                soloStateChanged(modelData.subSceneId,true);
                                switchOffOthers();
                            }
                            onStateOff: {
                                soloStateChanged(modelData.subSceneId,false);
                            }

                            function soloStateChanged(id,stateP){
                                var msg = new Object();
                                msg.soloChanged = new Object();
                                msg.soloChanged.subSceneId = id;
                                msg.soloChanged.state = stateP;
                                ws.send = JSON.stringify(msg);
                            }
                            function switchOffOthers(){
                                if(discoScene.currentSolo !== null && discoScene.currentSolo != soloBtn){
                                    discoScene.currentSolo.setOff(false);
                                }
                                discoScene.currentSolo = soloBtn;
                            }

                            anchors.centerIn: parent;
                        }
                    }

                    Item{
                    Layout.preferredWidth: activeBtn.width+5;
                    height: parent.height;
                    PushLockBtn{
                        anchors.centerIn: parent;
                        id:activeBtn
                        mute: true
                        text: "A"
                        onStateOn: {
                            muteStateChanged(modelData.subSceneId,false);
                        }
                        onStateOff: {
                            muteStateChanged(modelData.subSceneId,true);
                        }

                        isOn: !modelData.mute;

                        anchors.right: parent.right;
                        anchors.margins: 10;

                        function muteStateChanged(id,stateP){
                            var msg = new Object();
                            msg.muteChanged = new Object();
                            msg.muteChanged.subSceneId = id;
                            msg.muteChanged.state = stateP;
                            ws.send = JSON.stringify(msg);
                        }
                    }
                    }
                }

                }


                    Item{
                        id:optionBox
                        anchors.top: discoRow.bottom
                        anchors.left: parent.left
                        height:0
                        //implicitHeight: beatScene.height
                        property string link;
                        width:parent.width
                        Behavior on height{
                            id:heightAnim
                            enabled: true
                           NumberAnimation{
                               duration: 200
                               onRunningChanged: {
                                    if (running) {//start
                                        if(height != 0)
                                            loader.item.visible = false;
                                    }
                                    else{//stoped
                                        if(optionBox.height != 0)
                                            loader.item.visible = true;
                                    }
                               }
                           }
                        }

                        function toggle(){
                            if(link != ""){
                                stackView.pushNamedView(modelData.name,Qt.resolvedUrl(link),{"requestId":modelData.providerId});
                            }
                            else{
                                heightAnim.enabled = true;
                                if(optionBox.height != 0)
                                    optionBox.height = 0;
                                else
                                    optionBox.height = optionBox.implicitHeight;
                            }
                        }


                        Loader{
                            id:loader
                            width: parent.width
                            onLoaded: {
                                optionBox.implicitHeight = item.height;
                                item.visible = false;
                                item.requestId = modelData.providerId;

                                item.heightChanged.connect(function(){
                                    optionBox.implicitHeight = item.height;
                                    if(optionBox.height != 0){
                                        heightAnim.enabled = false;
                                        optionBox.height = item.height;
                                    }
                                });
                            }
                        }

                        Component.onCompleted: {
                            var filename = "";
                            if(modelData.requestType === "beatScene1")
                                filename = "scenes/BeatScene.qml";
                            if(modelData.requestType === "falshScene")
                                filename = "scenes/FlashScene.qml";
                            if(modelData.requestType === "musicScene")
                                filename = "MusicScene.qml";
                            if(modelData.requestType === "sphere")
                                filename = "scenes/SphereScene.qml";
                            if(modelData.requestType === "colorWaveScene")
                                filename = "ColorWaveScene.qml";
                            if(modelData.requestType === "loudnessScene")
                                filename = "scenes/LoudnessScene.qml";
                            if(modelData.requestType === "beamerScene")
                                filename = "scenes/BeamerScene.qml";
                            if(modelData.requestType === "lineScene")
                                filename = "scenes/LineScene.qml";
                            if(modelData.requestType === "keyFrameScene")
                                link = "scenes/keyframe/KeyframeScene.qml";
                            if(modelData.requestType === "discoScene")
                                link = "DiscoScene.qml";
                            if(modelData.requestType === "diaScene")
                                link = "DiaScene.qml";
                            if(modelData.requestType === "mouseScene")
                                link = "scenes/MouseScene.qml";
                            if(modelData.requestType === "screenColor")
                                filename = "ScreenColorScene.qml";
                            if(filename != ""){
                                loader.source = filename;
                            }
                        }


                    }

            }

                property int dragStart: 0
                property int jumpedItems: 0
                onYChanged: {
                    if(!dragRect.Drag.active)
                        return;
                    var nextHeight = 100000;
                    if(discolistView.itemAt(dragRect.width/2,y+height+discolistView.contentY) !== null)
                        nextHeight = discolistView.itemAt(dragRect.width/2,y+height+discolistView.contentY).height;

                    var prevHeight = 100000;
                    if(discolistView.itemAt(dragRect.width/2,y+discolistView.contentY) !== null)
                        prevHeight = discolistView.itemAt(dragRect.width/2,y+discolistView.contentY).height;

                   if(dragRect.y + discolistView.contentY - dragStart > nextHeight * 0.75
                           && index < discolistView.count-1
                           && (discolistView.itemAt(dragRect.width/2,y+height+discolistView.contentY) !== delegateItem)){

                        dragStart = dragStart + nextHeight;
                        discolistView.model.move(index,index+1,1);
                        discolistView.model.rowMovedManualy();
                       jumpedItems++;
                   }
                   if(dragRect.y + discolistView.contentY - dragStart < -prevHeight * 0.75
                           && index != 0
                           && (discolistView.itemAt(dragRect.width/2,y+height+discolistView.contentY) !== delegateItem)){
                        discolistView.model.move(index,index-1,1);
                       discolistView.model.rowMovedManualy();
                       dragStart = dragStart - prevHeight;
                       if(index == 0){
                           mouseArea.drag.minimumY = 0;
                       }
                   }
                 }


                states: [
                    State {
                        when: dragRect.Drag.active
                        ParentChange {
                            target: dragRect
                            parent: discoScene
                        }

                        AnchorChanges {
                            target: dragRect
                            anchors.horizontalCenter: undefined
                            anchors.verticalCenter: undefined
                        }
                    }
                ]

                Drag.active: mouseArea.drag.active
                Drag.hotSpot.x: dragRect.width / 2
                Drag.hotSpot.y: dragRect.height / 2
            }
        }
