import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.0
import WebSocketConnector 1.1
import "VirtualDeviceManager"
import "ColorPicker/"
Item{
    width: parent.width;
    height:colLayout.height + 20
    id:main
    property alias requestId : wsc.requestId

    ColumnLayout{
        id:colLayout
        width: parent.width - 2*20
        height: children.height
        x:20

        GridLayout{
            width: parent.width
            columns: 2
            rowSpacing: 20

            Text{
                text:qsTr("Trigger:")
                color:"#fff";
            }
            TriggerSourceBtn{
                id:triggerBtn
            }

            Text{
                text:qsTr("Active radius (in m)")
                color:"#fff";
            }

            RowLayout{
                spacing: 10
                ExponentialSlider{
                    id: activeRadiusSlider
                    maxValue: 500
                    Layout.fillWidth: true
                    Layout.fillHeight: true;
                    onReadValueChanged: {
                        if(pressed){
                            var mesage = new Object();
                            mesage.activeRadiusChanged = readValue;
                            wsc.send = JSON.stringify(mesage);
                        }
                    }
                }

                Text{
                    Layout.preferredWidth: 50
                    font.pointSize: 10
                    color:"#fff"
                    text: qsTr("%L1 m").arg(Math.round(activeRadiusSlider.readValue * 100) / 100);
                }
            }



                Text{
                    text:qsTr("Speed (in m/s)")
                    color:"#fff";
                }
                RowLayout{
                    spacing: 10

                    ExponentialSlider{
                        id: speedSlider
                        maxValue: 2000
                        Layout.fillWidth: true
                        Layout.fillHeight: true;
                        onReadValueChanged: {
                            if(pressed){
                                var mesage = new Object();
                                mesage.speedChanged = readValue;
                                wsc.send = JSON.stringify(mesage);
                            }

                        }
                    }

                    Text{
                        Layout.preferredWidth: 50
                        font.pointSize: 10
                        color:"#fff"
                        text: qsTr("%L1 m/s").arg(Math.round(speedSlider.readValue * 100) / 100);
                    }
                }

        }


            RowLayout{
                width: parent.width
                ColorPickerButton{
                    id: colorPickerButton
                    minColorNum: 1
                }

                SelectHook{
                    id:vDevManager
                }
            }

    }


    WebSocketConnector{
        id: wsc
        onMessage: {
           if(msg.trigger !== undefined)
            triggerBtn.requestId = msg.trigger;
           if(msg.activeRadiusChanged !== undefined)
            activeRadiusSlider.setValue  = msg.activeRadiusChanged;
           if(msg.speedChanged !== undefined)
            speedSlider.setValue = msg.speedChanged;
           if(msg.colorButton!==undefined)
            colorPickerButton.requestId = msg.colorButton;
           if(msg.hasOwnProperty("devManager"))
               vDevManager.requestId = msg.devManager;
        }
    }

}

