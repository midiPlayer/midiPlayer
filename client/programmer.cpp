#include "programmer.h"
#include <QtSerialPort/QSerialPortInfo>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>

#define BAUD 9600

Programmer::Programmer() : serial(this), timer(this)
{
    timer.setSingleShot(true);
    serial.setBaudRate(BAUD);
    connect(&timer,SIGNAL(timeout()),this,SLOT(timeout()));
    connect(&serial,SIGNAL(readyRead()),this,SLOT(read()));
}

QString Programmer::scanPorts()
{
    QJsonArray ret;
    qDebug() << "scan started";
   foreach(QSerialPortInfo info, QSerialPortInfo::availablePorts()){
       qDebug() << info.portName();
       ret.append(info.portName());
   }

   QJsonDocument d;
   d.setArray(ret);
   return d.toJson();
}

void Programmer::programm(QString port, QString data)
{
    qDebug() << "program started";

    serial.setPortName(port);
    if(!serial.open(QIODevice::ReadWrite)){
        emit openFailed();
        return;
    }

    data.append( "\r");
    qDebug() << data;
    QByteArray ba = data.toLocal8Bit();
    serial.write(ba);

    timer.start(10000);//1 sec bsi timeout
}

void Programmer::timeout()
{
    serial.close();
    emit programmTimeout();
}

void Programmer::read()
{
     QByteArray data = serial.readAll();
     qDebug() << data;
     QString message(data);
     if(message.contains("ok")){//finished
         serial.close();
         timer.stop();
         emit programmSucceded();
     }
}
