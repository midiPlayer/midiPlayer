import QtQuick 2.4
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import WebSocketConnector 1.1
import Qt.labs.settings 1.0
import QtQuick.Controls 2.0

Item {
    id:connectView

    property string address
    property string nextView: "/MainMenu.qml"



    width: stackView.width
    height: stackView.height
    ConnectViewForm {
        anchors.fill: parent
        id: mainForm;

        connectBtn.onClicked: startConnect();
        Component.onCompleted: {
            urlEdit.text = settings.lastUrl;
            if(urlEdit.text != ""){
                mainForm.startConnect();
            }
        }
        function startConnect(){
            if(state == "connected")
                wsc.url = "";//disconnect
            else{
                wsc.url = urlEdit.text;
                mainForm.state = "connecting";
            }
        }
    }



    WebSocketConnector{
        id: wsc
        onConnectionSucceded: {
            //reopen = true;
            settings.lastUrl = url;
            mainForm.state = "connected";
            var next = nextView;
            stackView.pushNamedView("Start",Qt.resolvedUrl(next),{});

        }
        onConnectionFailed: {
            mainForm.state = "failed";
            stackView.popAll();
        }
    }
    Settings {
            id: settings
            property string lastUrl: ""
            //property alias overideNext: stackView.overideStartView;
    }
}
