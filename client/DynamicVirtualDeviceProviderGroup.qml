import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import WebSocketConnector 1.1

DeviceSelectionGroup{
    property WebSocketConnector wsc;
    property string type;

    item:  DynamicVirtualDeviceProviderItem{
        onLongPressed: {//delte
            var msg = {"rm"  : devId};
            wsc.send = JSON.stringify(msg);
        }
        onShortPressed: { //open Prefs
            stackView.pushNamedView("Edit",Qt.resolvedUrl("DynamicVirtualDeviceProviderAdd.qml"),{"type":type,"ws":wsc,"devId":devId});
        }
    }

    additionalHeadlineUiLoader.sourceComponent: Button{
        text:"add"
        onClicked: {
             stackView.pushNamedView("Add",Qt.resolvedUrl("DynamicVirtualDeviceProviderAdd.qml"),{"type":type,"ws":wsc});
        }
    }

    Component.onCompleted: {
         wsc.onMessage.connect(function(msg){
             if(msg.hasOwnProperty("add") && msg.add.type === type){
                 addGenericDevice(msg.add.devId);
             }
             if(msg.hasOwnProperty("rm")){
                 rmDevice(msg.rm);
             }
         });
    }

}
