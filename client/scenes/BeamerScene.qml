import QtQuick 2.0
import QtQuick.Layouts 1.3
import "../beamerEffects/"
import "../"
import WebSocketConnector 1.1
import "../VirtualDeviceManager"

Item {
    width: parent.width;
    height: colLayout.height + 20
    property int requestId;
    ColumnLayout{
        id: colLayout
        spacing: 10
        height: children.height;
        width: parent.width - 2* 20
        x:20
        BeamerEffectCombo{
            id: effectCombo
            hasEmptyOption: true
            onCurrentIndexChanged: {
                ws.send = JSON.stringify({"effect":getSelectedName()})
            }
            onFinished: {
                ws.requestId = requestId;
            }
            Component.onCompleted: {
                console.log("show!  show!   show!   show!   show!")
            }
        }

        TriggerSourceBtn{
            id: triggerBtn
        }

        SelectHook{
            id: devMgr
        }

        EffectEditorManagerPreviewContainer {
            id: manager
            width:parent.width
            height: 200
            onSend: {
                ws.send = JSON.stringify({"config":msg});
            }

            property var nextRecieve: null;
            onContentLoaded: {
                if(nextRecieve !== null){
                    recieve(nextRecieve);
                    nextRecieve = null;
                }
            }
        }
    }
    property var component;
    WebSocketConnector{
        id: ws
        onMessage: {
            console.log("=========" + JSON.stringify(msg));
            if(msg.hasOwnProperty("trigger"))
                triggerBtn.requestId = msg.trigger;
            if(msg.hasOwnProperty("effect")){
                manager.effectId = msg.effect
                effectCombo.setSelectedId(msg.effect)
            }
            if(msg.hasOwnProperty("config")){
                manager.nextRecieve = msg.config;
            }
            if(msg.hasOwnProperty("devs")){
                devMgr.requestId = msg.devs;
            }
        }
    }
}
