import QtQuick 2.0
import WebSocketConnector 1.1
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

import "../../VirtualDeviceManager/"
import "../../"
import "../"

Item{
    id:track;
    width: parent.width
    height: 40
    signal deleteRequersted();
    RowLayout{
        anchors.fill: parent
        
        Text {
            text: name
            Layout.fillWidth: true
            color:"#369cb6"
            Layout.margins: 10
        }
        SubSceneHandler{
            id: subScene
            requestId: scene
            Layout.fillHeight: true
        }

        SelectHook{
            requestId: devs
        }

        Button{
            text: qsTr("X")
            onClicked: {
                deleteRequersted();
            }
        }
        
        
        CheckBox{
            onCheckedChanged: {
                if(checked)
                    graphViewer.addDevice(name,requestId);
                else
                    graphViewer.rmDevice(name);
            }
        }
    }
}
