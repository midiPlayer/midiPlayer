import QtQuick 2.0

ListModel {
    ListElement{
        myColor: "#ff9c00" //orange
    }
    ListElement{
        myColor: "#eaff00" //yellow
    }
    ListElement{
        myColor: "#2aff00" //green
    }
    ListElement{
        myColor: "#00fff0" //türkies
    }
    ListElement{
        myColor: "#0072ff" //blue
    }
    ListElement{
        myColor: "#de00ff" //purple
    }
    ListElement{
        myColor: "#ff00a2" //pink
    }
    ListElement{
        myColor: "#8f6500" //brown
    }
    ListElement{
        myColor: "#006c8f" //ark blue
    }
    ListElement{
        myColor: "#8f005b" //ark purple
    }

}
