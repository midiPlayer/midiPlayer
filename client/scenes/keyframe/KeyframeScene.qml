import QtQuick 2.0
import WebSocketConnector 1.1
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import "../../"

Item {

    focus: true;

    property alias requestId : ws.requestId
    width: parent.width
    height: parent.height

    onRequestIdChanged: {
        console.log("rqidset: " + requestId);
    }

    ListModel {
        id:deviceModell
    }

    SplitView{
    anchors.fill: parent
    orientation: Qt.Vertical
    Item{
        id:deviceView
        height: parent.height/2
        Layout.minimumHeight:  150
     //   width: parent.width
     //   height: parent.height/2

        ColumnLayout{
            anchors.fill: parent;
            Item{
                height: musicPlayer.height
                Layout.fillWidth: true;
                Layout.rightMargin: 15
                RowLayout{
                    anchors.fill: parent
                    Item{
                        Layout.fillWidth: true
                    }

                    TriggerSourceBtn{
                        id: trigger
                    }

                    MusicPlayer{
                        id:musicPlayer
                    }

                    TextField{
                        id: newTrackName
                        placeholderText: "new track name"
                    }

                    Button{
                        text: "Add Track"
                        onClicked: {
                            var msg = {"add_track":newTrackName.text};
                            console.log(JSON.stringify(msg));
                            ws.send = JSON.stringify(msg);
                            newTrackName.text = "";
                            newTrackName.focus = false;
                        }
                        enabled: newTrackName.text != ""
                    }
                    Text{
                        text: qsTr("trigger shift in 10 ms")
                        color: "#fff"
                    }

                    NumberField{
                        id:triggerShiftEdit
                        text: "0.1"

                        Settings {
                            id: settings
                            property alias triggerTimeShift: triggerShiftEdit.value

                        }

                    }

                }
            }
            Item{
                Layout.fillWidth: true;
                Layout.fillHeight: true;
                ScrollView{

            anchors.fill: parent
            ListView {
                width: 180; height: 200

                id:lv


                property string copyedDev;
                property string buttonState : "copy";


                model: deviceModell
                delegate: Track {
                    onDeleteRequersted: {
                        var msg = {"rm_track":name};
                        ws.send = JSON.stringify(msg);
                    }
                }
            }

        }
            }
         }
     }


    Item{
        Layout.minimumHeight: parent.height / 4
        GraphViewer{
            id:graphViewer
            requestId: ws.requestId
        }
    }
    }

    WorkerScript {
            id: deviceWorker
            source: "KeyframeSceneDeviceWorker.js"
   }

    WebSocketConnector{
        id: ws
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("tracks"))
                deviceWorker.sendMessage({"model":deviceModell,"tracks":msg.tracks});
            if(msg.hasOwnProperty("musicPlayer")){
                musicPlayer.requestId = msg.musicPlayer;
            }
            if(msg.hasOwnProperty("trigger")){
                trigger.requestId = msg.trigger;
            }
        }
    }
}
