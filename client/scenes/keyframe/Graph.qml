import QtQuick 2.0
import RGBWColor 1.1
import "KeyframeCreator.js" as KeyframCreator
import WebSocketConnector 1.1
import QtQuick.Layouts 1.1

Item{
    id:graph
    //color:"#000";
    property var self: graph;
    //property string deviceRequestId;

    property string name;

    property var viewer;

    property var points: [];
    property var triggers: [{"time":1,"num":"2"}]

    property var zeroPoint : Keyframe{    }
    property Keyframe activePoint: null;

    property bool isLiveEditing: false



    Component.onCompleted: {
        ws.requestId = params.requestId;
        name = params.name;

        viewer.onValueEditingChanged.connect(function(){
            isLiveEditing = viewer.valueEditing;
            if(activePoint !== null){
                activePoint.setLiveEditing(isLiveEditing)
            }
        });
    }

    function repaint(){
        graphCanvas.requestPaint();
    }





    width: parent.width
    onHeightChanged: {
        console.log("height" + height);
    }

    function calcPosY(point){
        return height - height * point.value.brightness
    }

    function getAbsHeight(height){
        return height;
    }


    function sortPoints(){
        points = points.sort(function(a,b){return a.time - b.time});
    }

    Canvas {
        id: graphCanvas
        anchors.fill: parent;
        onPaint: {
            var ctx = graphCanvas.getContext('2d');
             ctx.reset();
            var clacPosx = viewer.calcPosX;

            //draw graph
            var drawedPoints = [];

            ctx.lineWidth = 4
            ctx.strokeStyle = "blue"
            // setup the fill


            ctx.beginPath()
            ctx.moveTo(0,getAbsHeight(height))

            var isFirst = true;
            var isFirstUnprint = true;

            for(var i = 0; i <points.length; i++){
              var point = points[i];

              if(calcPosX(point.time) < 0 && i != points.length - 1)
                  continue;


              if(isFirst){//vorherigen punkt zeichnen
                if(i > 0){
                    ctx.lineTo(calcPosX(points[i-1].time),calcPosY(points[i-1]));
                    drawedPoints.push(points[i-1]);
                }
                else{
                    ctx.lineTo(0,calcPosY(points[i]));
                }



                isFirst = false;
              }

              if(calcPosX(point.time <= width)){//visible points:
                  ctx.lineTo(calcPosX(point.time),calcPosY(point));
                  drawedPoints.push(point);
              }
              else if(isFirstUnprint){
                  ctx.lineTo(calcPosX(point.time),calcPosY(point));
                  drawedPoints.push(point);
                  isFirstUnprint = false;

              }
            }
            if(isFirstUnprint && points.length > 0){
               ctx.lineTo(width,calcPosY(points[i-1]));
                drawedPoints.push(points[i-1]);
            }

            ctx.lineTo(width,getAbsHeight(height));
            ctx.closePath();

            //generate gradient
            if(drawedPoints.length > 1){
              var start = calcPosX(drawedPoints[0].time);
              var end = calcPosX(drawedPoints[drawedPoints.length - 1].time);
              var gradient = ctx.createLinearGradient(start,0,end,0);
              for(var i = 0; i < drawedPoints.length;i++){
                  var point = drawedPoints[i];
                  var pos = 0;
                  if(end-start != 0)
                    pos = (calcPosX(point.time)-start)/(end-start);
                  if(point.value.brightness > 0){
                      gradient.addColorStop(pos,point.value.preview);
                  }
                  else{//workarround für schwarze punkte
                      if(i > 0){
                          gradient.addColorStop(Math.max(Math.min(pos-0.0001,1),0),drawedPoints[i-1].value.preview);
                      }
                      if(i < drawedPoints.length-1){
                          gradient.addColorStop(Math.max(Math.min(pos+0.0001,1),0),drawedPoints[i+1].value.preview);
                      }
                  }

              }
            }

            ctx.fillStyle = gradient;
            ctx.fill()

            //draw points:
            for(var i = 0; i < points.length; i++){
              var point = points[i];

              if(calcPosX(point.time) < 0)
                  continue;

                ctx.beginPath();
                var x = calcPosX(point.time);
                var y = calcPosY(point);
                ctx.fillStyle = "#fff";
                ctx.moveTo(x,y);
                var radius = 3;
                if(point === activePoint)
                    radius = 5;
                ctx.arc(x,y,radius,0,Math.PI*2,false);
                ctx.fill();
            }


            //draw Triggers:
            ctx.lineWidth = 2
            for(var i = 0; i < triggers.length;i++){
                var trigger = triggers[i];
                var x = calcPosX(trigger.time);
                if(0 < x && x < width){
                    ctx.beginPath();
                    ctx.strokeStyle = colorModel.get(trigger.num).myColor;
                    ctx.moveTo(x,0);
                    ctx.lineTo(x,height);
                    ctx.stroke();
                }
            }

        }

        PlannedTriggerColorModel{
            id:colorModel
        }


    MouseArea{
        id: graphMouse
        anchors.fill: parent;
        onWheel: {
            if(viewer.canvas.mouseA.rPressed || viewer.canvas.mouseA.gPressed || viewer.canvas.mouseA.bPressed | viewer.canvas.mouseA.wPressed){
               if(parent.parent.activePoint != null){
                  var deltaP = wheel.angleDelta.y / 5000;
                   if(wheel.modifiers & Qt.MetaModifier){
                       deltaP = deltaP / 10;
                   }
                   console.log("wheel");

                   if(viewer.canvas.mouseA.rPressed){
                       parent.parent.activePoint.value.r = Math.max(Math.min(parent.parent.activePoint.value.r + deltaP,1),0)
                   }
                   if(viewer.canvas.mouseA.gPressed){
                       parent.parent.activePoint.value.g = Math.max(Math.min(parent.parent.activePoint.value.g + deltaP,1),0)
                   }
                   if(viewer.canvas.mouseA.bPressed){
                       parent.parent.activePoint.value.b = Math.max(Math.min(parent.parent.activePoint.value.b + deltaP,1),0)
                   }
                   if(viewer.canvas.mouseA.wPressed){
                       parent.parent.activePoint.value.w = Math.max(Math.min(parent.parent.activePoint.value.w + deltaP,1),0)
                   }
                   parent.requestPaint();
               }
            }
        }

        function getClickedPointIndex(){
            for(var i = 0; i <parent.parent.points.length; i++){
                var point = parent.parent.points[i];
                var x = viewer.calcPosX(point.time);
                var y = calcPosY(point);
                if(Math.abs(mouseX - x) < 20 && Math.abs(mouseY - y) < 20){
                    return i;
                }
            }
            return -1;
        }


        acceptedButtons : Qt.LeftButton | Qt.RightButton | Qt.MiddleButton;
        onPressed: {
          if(pressedButtons & Qt.LeftButton){
            var index = getClickedPointIndex()
            if(parent.parent.activePoint !== null)
               parent.parent.activePoint.setLiveEditing(false);
            parent.parent.activePoint = null;
            if(index !== -1){
                parent.parent.activePoint = parent.parent.points[index];
                if(isLiveEditing)
                    parent.parent.activePoint.setLiveEditing(true);
                parent.requestPaint();
            }
          }
        }

        onClicked: {
            if(mouse.button == Qt.MiddleButton){//delete poiunt
                    var index = getClickedPointIndex();
                    if(index !== -1){
                    var keyframe = parent.parent.points[index]
                    parent.parent.points.splice(index,1);
                    keyframe.requestDelete();
                    keyframe.destroy();
                    parent.requestPaint();
                }
            }
            else if(mouse.button == Qt.RightButton){//add Point
                var t = viewer.calcTime(mouseX);

                var msg = new Object();
                msg.add_keyframe = new Object();
                msg.add_keyframe.devId = graph.devideID;
                msg.add_keyframe.time = t;
                ws.send = JSON.stringify(msg);

                //KeyframCreator.createNewKeyframe(t,prev.value.copy,graph,graphCanvas);
            }
            mouse.accepted = false;
        }

        propagateComposedEvents: true
        onPositionChanged: {
            if(pressedButtons & Qt.LeftButton){
              if(parent.parent.activePoint != null){
                  parent.parent.activePoint.setTime(Math.max(0,viewer.calcTime(mouseX)));
                }
            }
        }


        Component.onCompleted: {
         applicationWindow.keyPress.connect(key);
        }
        Component.onDestruction: {
         applicationWindow.keyPress.disconnect(key);
        }


        hoverEnabled: true;

        function key(event){
            if(!graphMouse.containsMouse)
                return
            var cursorT = viewer.getTimerPos() /1000;
            var timeShift = triggerShiftEdit.value / 100;
            cursorT -= timeShift;
            cursorT = Math.max(0,cursorT);
            if(event.key === Qt.Key_Delete){//remove next
                var bestInd = -1;
                var bestTime = 1000000;
                for(var i = 0; i < triggers.length;i++){
                    var thisT = Math.abs(triggers[i].time - cursorT);
                    if( thisT < bestTime){
                        bestInd = i
                        bestTime = thisT;
                    }
                }
                if(bestInd != -1){
                    triggers.splice(bestInd,1);
                    graphCanvas.requestPaint();
                    sendTriggerframes();
                }
            }else{//add
                var num = event.text * 1;
                if(event.text === "" || isNaN(num))
                    return;
                triggers.push({"time" : cursorT,"num":num});
                graphCanvas.requestPaint();
                sendTriggerframes();
            }
        }

    }

    }


    function sendTriggerframes(){
        console.log(JSON.stringify({"triggerframes": triggers}));
        ws.send = JSON.stringify({"triggerframes": triggers});
    }

     WebSocketConnector{
         id: ws

         onMessage: {
             console.log(JSON.stringify(msg));

             if(msg.hasOwnProperty("keyframes")){
                for(var i = 0; i < msg.keyframes.length; i++){
                    KeyframCreator.createNewKeyframe(msg.keyframes[i],graph,graphCanvas);
                }
             }
             if(msg.hasOwnProperty("new_keyframe")){
                    KeyframCreator.createNewKeyframe(msg.new_keyframe,graph,graphCanvas);
             }

             if(msg.hasOwnProperty("triggerframes")){
                 triggers = msg.triggerframes;
                 graphCanvas.requestPaint();
             }
         }
     }

}

