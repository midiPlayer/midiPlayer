WorkerScript.onMessage = function(message) {
    var model = message.model;
    if(message.action === "add")
        model.append({"params":message.data});
    else if(message.action === "del"){
        for(var i = 0; i < model.count;i++) {
            if(model.get(i).params.name === message.data.name){
                model.remove(i);
                break;
            }
        }
    }

    model.sync();
}
