import QtQuick 2.0
import RGBWColor 1.1
import WebSocketConnector 1.1

Item{
    id: keyframe
    RGBWColor{
        id:zeroColor
        hasR: true
        hasG: true
        hasB: true
        hasW: true
    }
    property RGBWColor value: zeroColor
    property double time: 0
    property alias requestId : ws.requestId;

    signal deleteRequested();

    function setTime(t){
        time = t;
        var msg = new Object();
        msg.time = time;
        ws.send = JSON.stringify(msg);
    }

    function requestDelete() {
        var msg = new Object();
        msg.deleteKeyframe = true;
        ws.send = JSON.stringify(msg);
    }


    Component.onCompleted: {
        value.onColorActiveChanged.connect(function(){
            var msg = new Object();
            msg.color = value.serialized;
            ws.send = JSON.stringify(msg);
        });
    }

    function setLiveEditing(editing) {
        var msg = new Object();
        msg.liveEditing = editing;
        ws.send = JSON.stringify(msg)
    }

    WebSocketConnector{
        id: ws
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("time")){
                time = msg.time;
            }
            if(msg.hasOwnProperty("color")){
                value.serialized = msg.color;
            }
            if(msg.hasOwnProperty("deleteKeyframe")){
                keyframe.deleteRequested();
            }
        }
    }
}


