WorkerScript.onMessage = function(message) {
    var model = message.model;
    var tracks =  message.tracks;
    model.clear();
    for(var i = 0; i < tracks.length;i++){
        model.append(tracks[i]);
    }
    model.sync();
}
