import QtQuick 2.0
import QtQuick.Layouts 1.3
import WebSocketConnector 1.1
import QtQuick.Controls 1.3
import "../../RemoteTypes"
import "../../"

Item{
    width: parent.width;
    height:colLayout.height + 20
    id:main
    property alias requestId : wsc.requestId


    ColumnLayout{
        id: colLayout
        width: parent.width - 2*20
        height: children.height
        x:20

        ScalaInput{
            id:scala
            Layout.fillWidth: true
        }
    }


    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("scala"))
                scala.requestId = msg.scala;
        }
    }

}
