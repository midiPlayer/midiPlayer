import QtQuick 2.0
import QtQuick.Layouts 1.3
import WebSocketConnector 1.1
import QtQuick.Controls 1.0
import "../../VirtualDeviceManager"
import "../../RemoteTypes"
import "../../"

Item{
    width: parent.width;
    height:colLayout.height + 20
    id:main
    property alias requestId : wsc.requestId


    ColumnLayout{
        id: colLayout
        width: parent.width
        height: children.height

        GridLayout{
            width: parent.width
            Layout.fillWidth: true;
            columns: 2
            rowSpacing: 20

            Text{
               color: "#fff"
               text: qsTr("Centers")
            }

            SelectHook{
                id:vDevManagerCenter
            }

            Text{
               color: "#fff"
               text: qsTr("Fading percentage")
            }
            Slider{
                id: fadeOverLength
                Layout.fillWidth: true;
                minimumValue: 0.2
                maximumValue: 10
                //stepSize: 0.1
                RemoteType{
                    parentWsc:wsc
                    remoteId:"fadeOverLength"
                    property alias value : fadeOverLength.value
                }
            }
        }
        ScalaInput{
            id: scala
            Layout.fillWidth: true
        }
    }


    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("centers"))
                vDevManagerCenter.requestId = msg.centers;
            if(msg.hasOwnProperty("scala"))
                scala.requestId = msg.scala;
        }
    }

}
