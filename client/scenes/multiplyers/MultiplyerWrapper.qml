import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import "../../RemoteTypes"
import WebSocketConnector 1.1

Item {
    property string multiplyerName
    property alias multiplyerDelegate: loader.source
    property alias requestId : wsc.requestId

    width: parent.width
    height: colLayout.height

    ColumnLayout{
        id: colLayout
        width: parent.width
        height: childrenRect.height
        CheckBox{
            id: activeCb
            text: multiplyerName
            RemoteType{
                property alias value: activeCb.checked
                remoteId: "active"
                parentWsc: wsc
            }
        }
        Item{
            Layout.fillWidth: true
            height: loader.height
            visible: activeCb.checked
            onVisibleChanged: {
                colLayout.height = colLayout.children.height
            }

            Loader{
                id: loader
                x:20
                width: parent.width - 20
            }
        }
    }

    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("multiplyer")){
                loader.item.requestId = msg.multiplyer;
            }
        }
    }

}
