import QtQuick 2.0
import WebSocketConnector 1.1
import "../RemoteTypes"
import WebSocketConnector 1.1
import SetCursor 1.0

Item {

    property alias requestId : ws.requestId
    MouseArea{
        id: mouseA

        anchors.fill: parent
        property double lastX
        property double lastY

        property int absY: getAbsolutePosition(mouseA).y

        onPressed: {
            lastX = mouseX;
            lastY = mouseY;
            //setCur.setPos(0,0);
        }

        onMouseXChanged: {
            var newx = valX.value + (mouseX-lastX)/5000;
            if(newx > 1) newx = 1;
            if(newx < 0) newx = 0;
            valX.value = newx;
            lastX = mouseX;

            if(mouseX > width){
                setCur.setPos(10,mouseY+absY);
                lastX = 10;
            }
            if(mouseX < 0){
                setCur.setPos(width - 10,mouseY+absY);
                lastX = width - 10;
            }
        }
        onMouseYChanged: {
            var newy = valY.value + (mouseY-lastY)/5000;
            if(newy > 1) newy = 1;
            if(newy < 0) newy = 0;
            valY.value = newy;
            lastY = mouseY;

            if(mouseY > height){
                setCur.setPos(mouseX,10 + absY);
                lastY = 10;
            }
            if(mouseY < 0){
                setCur.setPos(mouseX,height - 10 + absY);
                lastY = height - 10;
            }
        }
    }

    function getAbsolutePosition(node) {
          var returnPos = {};
          returnPos.x = 0;
          returnPos.y = 0;
          if(node !== undefined && node !== null) {
              var parentValue = getAbsolutePosition(node.parent);
              returnPos.x = parentValue.x + node.x;
              returnPos.y = parentValue.y + node.y;
          }
          return returnPos;
      }

    SetCursor{
         id: setCur
    }

    Rectangle{
        width: 20
        height: 20
        radius: width/2
        //x: parent.width*valX.value + width/2;
        //y: parent.height*valY.value + height/2;
        x: parent.width*valX.value - width /2
        y: parent.height*valY.value - height/2
    }

    RemoteType{
        id: valX
        property double value : 0.0
        remoteId: "x"
        parentWsc: ws
    }

    RemoteType{
        id: valY
        property double value : 0.0
        remoteId: "y"
        parentWsc: ws
    }

    WebSocketConnector{
        id: ws
    }
}
