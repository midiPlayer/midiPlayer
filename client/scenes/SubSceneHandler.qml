import QtQuick 2.0
import WebSocketConnector 1.1
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
Item {
    property alias requestId : wsc.requestId

    width: 200

    SceneFunctions{
        id:sceneFunctions
    }


    RowLayout{
        height: parent.height

        Button{
            id: scene
            visible: false

            property int provId;
            property string sceneType;

            onClicked: {
                console.log("push:" + scene.sceneId);
                stackView.pushNamedView(scene.text,Qt.resolvedUrl("../" + sceneFunctions.getFileName(scene.sceneType)),{"requestId":scene.provId});
            }
        }

        Button{
            id: addScene
            text: "+"
            Layout.preferredWidth: 25

            onClicked: {
                stackView.pushNamedView("Add Scene",Qt.resolvedUrl("../AddScene.qml"),{"requestId":requestId, "type":"diaScene"});
            }
        }

        Button{
            id:delScene
            text:"X"
            visible: false;
            Layout.preferredWidth: 25
            onClicked: {
                var msg = {"delScene":""};
                wsc.send = JSON.stringify(msg);
            }
        }

        Item{
            Layout.fillWidth: true;
        }

    }

    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("scene")){
                if(msg.scene === ""){
                    scene.visible = false;
                    delScene.visible = false;
                    addScene.visible = true;
                }
                else{
                    scene.text = msg.scene.name;
                    scene.provId = msg.scene.provId;
                    scene.sceneType = msg.scene.type;

                    scene.visible = true;
                    delScene.visible = true;
                    addScene.visible = false;
                }
            }
        }
    }
}
