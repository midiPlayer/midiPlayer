import QtQuick 2.0
import QtQuick 2.0
import QtQuick.Layouts 1.1
import WebSocketConnector 1.1
import QtQuick.Controls 1.0
import "../VirtualDeviceManager"
import "../RemoteTypes"
import "../ColorPicker/"
import "../"
Item{
    width: parent.width;
    height:colLayout.height + 20
    id:main
    property alias requestId : wsc.requestId

    Column{
        id: colLayout
        width: parent.width - 40
        x:20
        height: children.height
        spacing: 10

        RowLayout{
            width: parent.width
            TriggerSourceBtn{
                id:triggerBtn
            }
            ColorPickerButton{
                id: colorPickerButton
                minColorNum: 1
                onlyOne: false
            }
            SelectHook{
                id:vDevManager
            }
            MyCheckBox{
                id: sameColorCb
                text:qsTr("Same Color");
                onCheckedChanged: {
                    var msg = {"sameColor":checked};
                    wsc.send = JSON.stringify(msg);
                }
                Component.onCompleted: {
                    wsc.onMessage.connect(function(msg){
                        if(msg.hasOwnProperty("sameColor"))
                            sameColorCb.checked = msg.sameColor;
                    });
                }
            }

            MyCheckBox{
                id: interruptCb
                text: qsTr("Interrupt running")
                RemoteType{
                    property alias value : interruptCb.checked
                    remoteId: "interrupt"
                    parentWsc: wsc
                }
            }
        }

        GridLayout{
            width: parent.width
            columns: 2
            rowSpacing: 10

            Text{
                text:qsTr("Duration")
                color:"#fff";
            }
            RowLayout{
                Layout.fillWidth: true;
                Slider{
                    id: durationSlider
                    minimumValue: 0
                    maximumValue: 1
                    Layout.fillWidth: true;
                    RemoteType{
                        property alias value : durationSlider.value
                        remoteId: "duration"
                        parentWsc: wsc
                    }
                }
                MyCheckBox{
                    id: relativeDurationCb
                    text: qsTr("relative")
                    RemoteType{
                        property alias value : relativeDurationCb.checked
                        remoteId: "relativeDuration"
                        parentWsc: wsc
                    }
                }
            }
            Text{
                text:qsTr("Fade in")
                color:"#fff";
            }
            Slider{
                id: fadeInSlider
                minimumValue: 0
                maximumValue:1
                Layout.fillWidth: true;

                RemoteType{
                    property alias value : fadeInSlider.value
                    remoteId: "fadeIn"
                    parentWsc: wsc
                }
            }

            Text{
                text:qsTr("Fade out")
                color:"#fff";
            }
            Slider{
                id: fadeOutSlider
                minimumValue: 0
                maximumValue: 1
                Layout.fillWidth: true;
                onValueChanged: {

                }

                RemoteType{
                    property alias value : fadeOutSlider.value
                    remoteId: "fadeOut"
                    parentWsc: wsc
                }
            }

            Text{
                text:qsTr("Time-Shift")
                color:"#fff";
            }
            Slider{
                id: timeDiffSlider
                minimumValue: 0
                maximumValue: 2000
                Layout.fillWidth: true;
                onValueChanged: {
                    if(pressed){
                        var mesage = new Object();
                        mesage.timeDiff = value;
                        wsc.send = JSON.stringify(mesage);
                    }
                }
                Component.onCompleted: {
                    wsc.onMessage.connect(function(msg){
                        if(msg.hasOwnProperty("timeDiff"))
                            value = msg.timeDiff;
                    });
                }
            }

            Text{
                text: qsTr("changing Devices")
                color:"#fff";
            }
            RowLayout{
                Slider{
                    id:numChangingDevSlider
                    Layout.fillWidth: true;
                    stepSize: 1
                    minimumValue: 0
                    maximumValue: 20

                    onValueChanged: {
                        var msg = {"changingDevices":value};
                        wsc.send = JSON.stringify(msg);
                    }
                    Component.onCompleted: {
                        wsc.onMessage.connect(function(msg){
                            if(msg.hasOwnProperty("numDevs"))
                                numChangingDevSlider.maximumValue = msg.numDevs;
                            if(msg.hasOwnProperty("changingDevices"))
                                numChangingDevSlider.value = msg.changingDevices;
                        });
                    }
                }
                Text{
                    color:"#fff";
                    text: numChangingDevSlider.value == 0 ? qsTr("all") : numChangingDevSlider.value
                    Layout.minimumWidth: 20
                }
            }
         }
    }


    WebSocketConnector{
        id: wsc
        onMessage: {
                    console.log(JSON.stringify(msg));
            if(msg.trigger !== undefined)
                triggerBtn.requestId = msg.trigger;
            if(msg.smoothnessChanged !== undefined)
                smoothnessSlider.value  = msg.smoothnessChanged;
            if(msg.durationChanged !== undefined)
                durationSlider.value  = msg.durationChanged;
            if(msg.color !== undefined)
                colorPickerButton.requestId = msg.color;
            if(msg.hasOwnProperty("devManager"))
                vDevManager.requestId = msg.devManager;
        }
    }

}
