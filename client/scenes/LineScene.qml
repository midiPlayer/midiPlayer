import QtQuick 2.0
import QtQuick.Layouts 1.3
import WebSocketConnector 1.1
import QtQuick.Controls 1.0
import "../VirtualDeviceManager"
import "../RemoteTypes"
import "../"
import "../ColorPicker"

Item{
    width: parent.width;
    height:colLayout.height + 20
    id:main
    property alias requestId : wsc.requestId


    ColumnLayout{
        id: colLayout
        width: parent.width - 2*20
        height: children.height
        x:20

        RowLayout{
            ColorPickerButton{
                id: colorPickerButton
                minColorNum: 1
            }

            TriggerSourceBtn{
                id:trigger
            }
        }

        GridLayout{
            width: parent.width
            Layout.fillWidth: true;
            columns: 2
            rowSpacing: 20

            Text{
               color: "#fff"
               text: qsTr("Devices")
            }
            SelectHook{
                id:vDevManager
            }

            Text{
               color: "#fff"
               text: qsTr("Centers")
            }
            SelectHook{
                id:vDevManagerCenters
            }

            Text{
               color: "#fff"
               text: qsTr("Speed")
            }

            Slider{
                id: speed
                Layout.fillWidth: true;
                minimumValue: 0.01
                maximumValue: 1
                //stepSize: 0.1
                RemoteType{
                    parentWsc:wsc
                    remoteId:"speed"
                    property alias value : speed.value
                }
            }

            Text{
               color: "#fff"
               text: qsTr("Acceleration")
            }

            Slider{
                id: acceleration
                Layout.fillWidth: true;
                minimumValue: 0.01
                maximumValue: 1
                //stepSize: 0.1
                RemoteType{
                    parentWsc:wsc
                    remoteId:"acceleration"
                    property alias value : acceleration.value
                }
            }

            Text{
               color: "#fff"
               text: qsTr("Block size")
            }

            Slider{
                id: blockSize
                Layout.fillWidth: true;
                minimumValue: 1
                maximumValue: 20
                stepSize: 1
                RemoteType{
                    parentWsc:wsc
                    remoteId:"blockSize"
                    property alias value : blockSize.value
                }
            }
        }
    }


    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("devices"))
                vDevManager.requestId = msg.devices;
            if(msg.hasOwnProperty("startDevices"))
                vDevManagerCenters.requestId = msg.startDevices;
            if(msg.hasOwnProperty("colors"))
                colorPickerButton.requestId = msg.colors;
            if(msg.hasOwnProperty("trigger"))
                trigger.requestId = msg.trigger;
        }
    }

}
