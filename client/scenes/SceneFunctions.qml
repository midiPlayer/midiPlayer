import QtQuick 2.0

Item{
    function getFileName(requestType){
        console.log(requestType);
        if(requestType == "discoScene")
            return "DiscoScene.qml"
        else if(requestType == "diaScene")
            return "DiaScene.qml"
        else if(requestType == "colorScene")
            return "ColorScene.qml"
        else if(requestType == "keyFrameScene")
            return "scenes/keyframe/KeyframeScene.qml";
        else if(requestType == "djScene")
            return "scenes/dj/DjScene.qml";
        else if(requestType == "beamerScene")
            return "scenes/BeamerScene.qml";
        else if(requestType == "mouseScene.qml")
            return "scenes/MouseScene.qml";
    }
}
