import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import WebSocketConnector 1.1
import "../"
ColumnLayout{
    spacing: 20
    Slider {
        id: volumeDial
        Layout.alignment: Qt.AlignHCenter;
        Layout.fillWidth: true;
        enabled: false
        from: 0
        to: 1
    }
    ProgressBar {
        id: time
        value: 0.5
        Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom;
        from: 0
        to: 1
        Layout.fillWidth: true;
    }

    Text {
        id: title
        text: deckRequestId
        font.pixelSize: 12
        Layout.alignment: Qt.AlignHCenter;
        wrapMode: Text.WordWrap
        Layout.minimumWidth: 0
        Layout.fillWidth: true;
        color:"#369cb6"
    }


    Button {
        id: scene

        property int requestId;
        property string requestType;

        SceneFunctions{
            id:sceneFunctions
        }

        text:"";
        font.pixelSize: 12
        Layout.alignment: Qt.AlignHCenter;
        Layout.minimumWidth: 0
        Layout.fillWidth: true;
        enabled: text !== ""
        onClicked: {
            stackView.pushNamedView(scene.text,Qt.resolvedUrl("../../" + sceneFunctions.getFileName(scene.requestType)),{"requestId":scene.requestId});
        }
    }

    Button{
        text:qsTr("Save");
        onClicked: {
            deckWs.send = JSON.stringify({"save":true});
        }
    }

    WebSocketConnector{
        id: deckWs
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("volume"))
                volumeDial.value = msg.volume;
            if(msg.hasOwnProperty("pos"))
                time.value = msg.pos;
            if(msg.hasOwnProperty("title"))
                title.text = msg.title;
            if(msg.hasOwnProperty("scene"))
                scene.text = msg.scene;
            if(msg.hasOwnProperty("sceneRequestType"))
                scene.requestType = msg.sceneRequestType;
            if(msg.hasOwnProperty("sceneProviderId"))
                scene.requestId = msg.sceneProviderId;

        }
    }
    Component.onCompleted: {
        deckWs.requestId = deckRequestId;
    }
}
