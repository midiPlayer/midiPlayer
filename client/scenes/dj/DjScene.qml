import QtQuick 2.7
import WebSocketConnector 1.1
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

Item {
    property alias requestId : wsc.requestId



ColumnLayout{
 anchors.fill: parent;
    RowLayout{
        id: deckRow
        Layout.maximumHeight: 150
        Layout.fillWidth: true
        Layout.maximumWidth: parent.width
        Repeater{
            model:ListModel{
                id: deckModel;
                ListElement{
                    deckRequestId: -1;
                }
            }

            delegate: DjDeck{
                width: deckRow.width / deckModel.count;
            }
        }
    }

    ListView{
        Layout.preferredWidth: parent.width - 40;
        Layout.minimumHeight: 200;
        Layout.alignment: "AlignHCenter"
        spacing: 10

        model: ListModel{
            id: songModel
            ListElement{
                song:"Loading ..."
                sceneProv:-1
            }
        }
        delegate: SongDelegate {
            ws:wsc
        }
    }
}

    WorkerScript{
        id: worker
        source: "DjSceneWorker.js"
    }

    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
            worker.sendMessage({"msg":msg, "songModel":songModel, "deckModel": deckModel});
        }
    }
}
