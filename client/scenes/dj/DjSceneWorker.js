
WorkerScript.onMessage = function(params) {
    var msg = params.msg;



    if(msg.songs_scenes !== undefined){
        params.songModel.clear();
        for (var z = 0; z < msg.songs_scenes.length; z++){
            var song = msg.songs_scenes[z];
            params.songModel.append({"song" : song.song, "sceneProv" : song.sceneProv});
        }
        params.songModel.sync();
    }

    if(msg.decks !== undefined){
        params.deckModel.clear();
        for (var z = 0; z < msg.decks.length; z++){
            var deck = msg.decks[z];
            console.log(deck)
            params.deckModel.append({"deckRequestId": deck});
        }
        params.deckModel.sync();
    }



}
