import QtQuick 2.7
import WebSocketConnector 1.1
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import "../"

Rectangle{
    width: parent.width
    height: songRow.height + 2
    color:"#10000000"
    property WebSocketConnector ws;

    RowLayout{
        id: songRow
        width: parent.width - 20
        spacing: 10
        anchors.centerIn: parent;
        Text{
            color: "#fff"
            text: song
            Layout.fillWidth: true
        }

        SubSceneHandler{
            id: handler
            requestId: sceneProv
            Layout.preferredHeight: 30
        }
    }
}
