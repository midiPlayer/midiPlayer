import QtQuick 2.0
import WebSocketConnector 1.1
import "../"
import "../VirtualDeviceManager/"
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1

Item {
    property alias requestId : ws.requestId

    GridLayout{
    columns: 2
    Text {
        text: qsTr("Device for X-Achsis")
        color: "#fff"
    }
    SelectHook{
        id: devX;
    }

    Text {
        text: qsTr("Device for Y-Achsis")
        color: "#fff"
    }
    SelectHook{
        id: devY;
    }
    Text {
        text: qsTr("Perform")
        color: "#fff"
    }
    Button{
        text: qsTr("And Action")
        onClicked:{
            stackView.pushNamedView("Mouse Pad",Qt.resolvedUrl("MouseSceneAction.qml"),{"requestId":requestId});
        }
    }
    Text {
        text: qsTr("Save current Position")
        color: "#fff"
    }
    Button{
        text: qsTr("Save")
        onClicked:{
            ws.send = JSON.stringify({"savePos":true});
        }
    }


    }





    WebSocketConnector{
        id: ws
        onMessage: {
            console.log(JSON.stringify(msg))
            if(msg.hasOwnProperty("xDev")){
                devX.requestId = msg.xDev;
            }
            if(msg.hasOwnProperty("yDev")){
                devY.requestId = msg.yDev;
            }
        }
    }
}
