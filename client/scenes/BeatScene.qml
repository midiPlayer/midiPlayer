import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import WebSocketConnector 1.1

import "../VirtualDeviceManager"
import "../RemoteTypes"
import "../"
import "multiplyers"
import "../ColorPicker"

Item{
    width: parent.width;
    height: col.height + 20
    id:main
    property alias requestId : wsc.requestId

    Column{
        id: col
        width: parent.width - 2*20
        x: 20
        spacing: 0

        Column{
            width: parent.width
            spacing: 10

            RowLayout{
                width: parent.width
                TriggerSourceBtn{
                    id:backroundBtn
                }
                ColorPickerButton{
                    id: colorPickerButton
                    minColorNum: 1
                }

                SelectHook{
                    id:vDevManager
                }

                MyCheckBox{
                    id: sameColorCb
                    text:qsTr("Same Color");
                    onCheckedChanged: {
                        var msg = {"sameColor":checked};
                        wsc.send = JSON.stringify(msg);
                    }
                    Component.onCompleted: {
                        wsc.onMessage.connect(function(msg){
                            if(msg.hasOwnProperty("config") && msg.config.hasOwnProperty("sameColor"))
                                sameColorCb.checked = msg.config.sameColor;
                            if(msg.hasOwnProperty("sameColor"))
                                sameColorCb.checked = msg.sameColor;
                        });
                    }
                }

                MyCheckBox{
                    id: inactiveBlack
                    text: qsTr("inactive black")
                    RemoteType{
                        property alias value : inactiveBlack.checked
                        remoteId: "inactiveBlack"
                        parentWsc: wsc
                    }
                }
            }
            GridLayout{
                width: parent.width
                columns: 2
                rowSpacing: 10
                    Text {
                        text: qsTr("changing Devices")
                        color:"#fff";
                    }
                    RowLayout{
                        Slider{
                            id:numChangingDevSlider
                            Layout.fillWidth: true;
                            stepSize: 1
                            minimumValue: 0
                            maximumValue: 20

                            onValueChanged: {
                                var msg = {"changingDevices":value};
                                wsc.send = JSON.stringify(msg);
                            }
                            Component.onCompleted: {
                                wsc.onMessage.connect(function(msg){
                                    if(msg.hasOwnProperty("config") && msg.config.hasOwnProperty("numDevs"))
                                        numChangingDevSlider.maximumValue = msg.config.numDevs;
                                    if(msg.hasOwnProperty("config") && msg.config.hasOwnProperty("changingDevices"))
                                        numChangingDevSlider.value = msg.config.changingDevices;
                                    if(msg.hasOwnProperty("changingDevices"))
                                        numChangingDevSlider.value = msg.changingDevices;
                                });
                            }
                        }
                        Text{
                            color:"#fff";
                            text: numChangingDevSlider.value == 0 ? qsTr("all") : numChangingDevSlider.value
                            Layout.minimumWidth: 20
                        }
                    }

                    Text{
                        text:qsTr("Smoothness")
                        color:"#fff";
                    }
                    Slider{
                        id: smoothnessSlider
                        minimumValue: 0
                        maximumValue: 0.5
                        Layout.fillWidth: true;
                        onValueChanged: {
                            if(pressed){
                                var mesage = new Object();
                                mesage.smoothnessChanged = value;
                                wsc.send = JSON.stringify(mesage);
                            }
                        }
                    }
                }

        }

        MultiplyerWrapper{
            id: shpereMultiply
           // anchors.top: colLayout.bottom
            multiplyerName: qsTr("Sphere Multiplyer")
            multiplyerDelegate: "multiplyers/SphereMultiplyer.qml"
        }

        MultiplyerWrapper{
            id: loudnessMultiply
           // anchors.top: shpereMultiply.bottom
            multiplyerName: qsTr("Loudness Multiplyer")
            multiplyerDelegate: "multiplyers/LoundnessMultiplyer.qml"
        }
    }


    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
            if(msg.config !== undefined){
                if(msg.config.foregroundTrigger !== undefined)
                    foregroundBtn.requestId = msg.config.foregroundTrigger;
                if(msg.config.backgroundTrigger !== undefined)
                    backroundBtn.requestId = msg.config.backgroundTrigger;
                if(msg.config.smoothnessChanged !== undefined)
                    smoothnessSlider.value  = msg.config.smoothnessChanged;
                if(msg.config.colorButton!==undefined)
                    colorPickerButton.requestId = msg.config.colorButton;
                if(msg.config.hasOwnProperty("selectDevManager"))
                    vDevManager.requestId = msg.config.selectDevManager;
                if(msg.config.hasOwnProperty("sphereMulti"))
                    shpereMultiply.requestId = msg.config.sphereMulti;
                if(msg.config.hasOwnProperty("loudnessMulti"))
                    loudnessMultiply.requestId = msg.config.loudnessMulti;

            }
            if(msg.smoothnessChanged !== undefined)
                smoothnessSlider.value  = msg.smoothnessChanged;
        }
    }

}
