import QtQuick 2.0
import WebSocketConnector 1.1

Item {

    property int channel : -1;
    property int universe : -1;

    function work(){
        if(channel < 0 || universe < 0)
            return;
        var msg = {"channel" : channel, "universe" : universe};
        ws.send = JSON.stringify(msg);
    }

    onChannelChanged: work();
    onUniverseChanged: work();

    WebSocketConnector{
        id: ws
        onMessage: {
        }
        requestType: "chennelTester"
    }
}
