import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import WebSocketConnector 1.1
import FileOutput 1.0
import FileInput 1.0

Item{
    id: prevContainer
    property string workdir
    property string effectName
    property int effectId

    property string currentEffectCode;

    property bool loaded: false
    property var component;
    property alias myWs: ws

    property string filename
    property string type;

    signal contentLoaded();

    function save(){
        var msg = {};
        msg[type + "Code"] = currentEffectCode;
        ws.send = JSON.stringify(msg);
    }

    function reload(){
        load(currentEffectCode);
    }

    function load(effectCode){
        if(component !== null && component !== undefined){
            component.destroy();
        }

        component = Qt.createQmlObject(effectCode,prevContainer);
        loaded = true;

        contentLoaded();
    }


    WebSocketConnector{
        id:ws
        onMessage: {
            console.log(JSON.stringify(msg))
            if(msg.hasOwnProperty(type + "Code")){
                currentEffectCode = msg[type + "Code"];
                if(workdir !== "")
                    fileOut.content = msg[type + "Code"];
                else
                    load(currentEffectCode);
            }
        }
        registrationParams: JSON.stringify({"as":type});
        requestId: effectId;
    }

    FileOutput{
        id: fileOut
        path: filename
        onWritten:{
            fileIn.path = filename
        }
    }

    FileInput{
        id: fileIn
        watch: true
        onContentRead: {
            currentEffectCode = content;
            load(content);
        }
    }
    Component.onDestruction: {
        fileOut.deleteFile();
    }
}
