WorkerScript.onMessage = function(params) {

    params.model.clear();

    if(params.hasEmptyOption){
        params.model.append({"text" : qsTr("- Please select -"), "effectId" : -1});
    }

    for (var z = 0; z < params.data.length; z++){
        var effect = params.data[z];
        params.model.append({"text" : effect.effectName, "effectId" : effect.effectId});
    }

    params.model.sync();

}
