import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0
import WebSocketConnector 1.1

ComboBox{
    id: effectCombo
    property bool hasEmptyOption: false;
    signal finished();

    model: ListModel{
        id: effectModel
        ListElement{
            text: "Effekte werden geladen"
            effectId: -1
        }
    }


    WorkerScript{
        id: worker
        source: "BeamerEffectHandlerImporter.js"
    }

    WebSocketConnector{
        id: wsc
        requestType: "beamereffecthandler"
        onMessage: {
            console.log(JSON.stringify(msg));

            if(msg.hasOwnProperty("effects")){
                worker.sendMessage({"model":effectModel,"data":msg.effects,"hasEmptyOption":hasEmptyOption})
                finished();

            }
        }
    }

    function getSelectedRequestId(){
        return effectModel.get(effectCombo.currentIndex).effectId;
    }
    function getSelectedName(){
        return effectCombo.currentText;
    }

    function setSelectedName(name){
        console.log("num:" + effectModel.count);
        for(var z = 0; z < effectModel.count; z++){
            console.log(effectModel.get(z).text);
            if(effectModel.get(z).text === name){
                effectCombo.currentIndex = z;
                return;
            }
        }
    }

    function setSelectedId(id){
        console.log("num:" + effectModel.count);
        for(var z = 0; z < effectModel.count; z++){
            console.log(effectModel.get(z).text);
            if(effectModel.get(z).effectId === id){
                effectCombo.currentIndex = z;
                return;
            }
        }
    }
}
