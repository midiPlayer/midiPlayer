import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import Qt.labs.settings 1.0
import WebSocketConnector 1.1

Item {
    GridLayout{
        anchors.fill: parent
        columns: 2

        Text{
            text:qsTr("Workdirectory")
            color: "#fff"
        }
        Row{
            TextField{
                id: workdir
                enabled: false
            }
            Button{
                text:qsTr("select")
                onClicked: {
                    workdirDialog.open()
                }
            }
        }


        Text{
            text:qsTr("New Effect")
            color: "#fff"
        }
        Row{
            TextField{
                id: newName
                placeholderText: qsTr("name")
            }
            Button{
                text:qsTr("Add Effect")
                enabled: newName.text !== ""
                onClicked: {
                    var msg = {"addEffect":newName.text}
                    wsc.send = JSON.stringify(msg);
                }
            }
        }

        Text{
            text:qsTr("Edit Effect")
            color: "#fff"
        }
        Row{
            BeamerEffectCombo {
                id: effectCombo
            }
            Button{
                text:qsTr("Edit")
                onClicked: {
                    openEffect(effectCombo.getSelectedRequestId(),
                               effectCombo.getSelectedName());
                }
            }
            Button{
                text:qsTr("Delete")
                onClicked: {
                    msgdialog.visible = true;
                }

                MessageDialog{
                    id: msgdialog
                    title: qsTr("Realy delete")
                    text: qsTr("Do you realy want to delete %L1?").arg(effectCombo.getSelectedName());
                    icon: StandardIcon.Question
                    standardButtons: StandardButton.Yes | StandardButton.Abort
                    onYes: {
                        wsc.send = JSON.stringify({"delEffect":effectCombo.getSelectedRequestId()});

                    }
                    visible: false;
               }
            }
        }



    }

    FileDialog{
        id: workdirDialog
        title: qsTr("Please select a filename")
        selectFolder: true
        folder: workdir.text

        onAccepted:{
            workdir.text = workdirDialog.folder;
            settings.lastFolder = workdir.text;
        }
    }
    Settings {
        id: settings
        property alias lastFolder: workdir.text;
    }


    WebSocketConnector{
        id: wsc
        requestType: "beamereffecthandler"
        onMessage: {
            console.log(JSON.stringify(msg));

            if(msg.hasOwnProperty("newEffect"))
                openEffect(msg.newEffect,newName.text);
        }
    }

    function openEffect(id, name){
        stackView.pushNamedView("Edit",Qt.resolvedUrl("EffectEditor.qml"),{
                           "effectId":id,
                           "effectName":name,
                           "workdir":workdir.text.replace("file://","")});
    }
}
