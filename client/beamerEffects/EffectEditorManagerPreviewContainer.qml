import QtQuick 2.0

EffectEditorPreviewContainer {

    filename:workdir + "/" +  effectName + "_manager.qml";
    type: "manager"
    signal send(var msg);
    function recieve(msg){
        if(component !== null)
            component.recieve(msg);
    }
}
