import QtQuick 2.0

EffectEditorPreviewContainer {

    filename: workdir + "/" +  effectName + "_effect.qml";
    type: "beamer"

    function trigger(){
        if(component !== null)
            component.trigger();
    }

    function recieve(msg){
        if(component !== null)
            component.recieve(msg);
    }
}
