import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2


Item {
    id:editor
    property int effectId
    property string workdir
    property string effectName
    property var lastSend;

    ColumnLayout{
        anchors.fill: parent;
        Item{
            Layout.fillWidth: true;
            Layout.preferredHeight: 50;
            Layout.alignment: Qt.AlignTop
            RowLayout{
                anchors.fill: parent;
                Button{
                    text: qsTr("Trigger")
                    onClicked: {
                        prev.trigger();
                    }
                }

                Button{
                    text: qsTr("Save")
                    onClicked: {
                        prev.save();
                        manager.save();
                        mainMenu.save();
                    }
                }
                Button{
                    text: qsTr("Reload")
                    onClicked: {
                        prev.reload();
                        manager.reload();
                    }
                }
            }
        }

        Item{
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            SplitView{
                orientation: Qt.Vertical
                anchors.fill: parent
                Item{
                    height:parent.height/2
                    EffectEditorManagerPreviewContainer {
                        id: manager
                        anchors.fill: parent
                        effectId:editor.effectId
                        effectName: editor.effectName
                        workdir: editor.workdir

                        onSend: {
                            prev.recieve(msg);
                            lastSend = msg;
                        }
                        onContentLoaded: {
                            if(lastSend !== null)
                                recieve(lastSend);
                        }
                    }
                }
                Rectangle{
                    color:"black";
                    Layout.fillHeight: true

                    EffectEditorEffectPreviewContainer {
                        id: prev
                        anchors.fill: parent
                        effectId:editor.effectId
                        effectName: editor.effectName
                        workdir: editor.workdir

                        onContentLoaded: {
                            if(lastSend !== null)
                                recieve(lastSend);
                        }
                    }
                }
            }
        }
    }
}
