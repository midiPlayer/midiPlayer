import QtQuick 2.4
import QtQuick.Layouts 1.1
import WebSocketConnector 1.1

Item {
    id: mainMenu
    RowLayout{
        anchors.fill: parent
    GridLayout {
        columns: 3
        Layout.alignment:Qt.AlignCenter
        Layout.minimumWidth: 300
        Layout.preferredWidth: 800
        Layout.preferredHeight: 500

        MainMenuItem{
            id: scene
            imageSrc: "icons/settings.png"
            name: qsTr("Current Scene")
            link:"DiaScene.qml"
            property int requestId;
            params: {"requestId":requestId}
            Layout.preferredWidth: parent.width / 6;
        }

        MainMenuItem{
            imageSrc: "icons/trigger.png"
            name: qsTr("Configure Trigger")
            link:"TriggerConfig.qml"
            Layout.preferredWidth: parent.width / 6;
        }

        MainMenuItem{
            imageSrc: "icons/settings.png"
            name: qsTr("Devices")
            //link:"BeamerShutterControl.qml"
            link:"DynamicVirtualDeviceProvider.qml"
            Layout.preferredWidth: parent.width / 6;
        }

        MainMenuItem{
            imageSrc: "icons/settings.png"
            name: qsTr("Beamer effects")
            //link:"BeamerShutterControl.qml"
            link:"beamerEffects/BeamerEffectHandler.qml"
            Layout.preferredWidth: parent.width / 6;
        }

        MainMenuItem{
            imageSrc: "icons/settings.png"
            name: qsTr("use as Beamer")
            //link:"BeamerShutterControl.qml"
            link:"beamer/DevIdView.qml"
            Layout.preferredWidth: parent.width / 6;
        }
    }
   }

    Component.onCompleted: {
        if(stackView.overideStartView !== "")
            stackView.pushNamedView("Restarted",Qt.resolvedUrl(stackView.overideStartView),{});
    }

    WebSocketConnector{
        id: ws
        requestType: "mainwindow"
        onMessage: {
            if(msg.hasOwnProperty("rootScene")){
                scene.requestId = msg.rootScene;
            }
        }
    }

    function save(){
        ws.send = JSON.stringify({"save":true});
    }
}

