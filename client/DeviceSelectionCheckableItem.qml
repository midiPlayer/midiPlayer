import QtQuick 2.0

DeviceSelectionItem {
    property bool dissabled : false;
    property bool checked : accepted;

    state:{
        if(dissabled)
            return "dissabled";
        if(checked)
            return "checked";
        return "";
    }
}
