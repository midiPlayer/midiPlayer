#ifndef RGBWCOLOR_H
#define RGBWCOLOR_H

#include <QObject>
#include <QColor>
#include <QJsonObject>

class RGBWColor : public QObject
{
Q_OBJECT

public:
    RGBWColor();
    RGBWColor(RGBWColor *copy);
    QString getRGBPrev();
    Q_PROPERTY(double r READ getR  WRITE setR NOTIFY colorChanged)
    Q_PROPERTY(double g READ getG  WRITE setG NOTIFY colorChanged)
    Q_PROPERTY(double b READ getB  WRITE setB NOTIFY colorChanged)
    Q_PROPERTY(double w READ getW  WRITE setW NOTIFY colorChanged)
    Q_PROPERTY(double a READ getA  WRITE setA NOTIFY colorChanged)

    Q_PROPERTY(QString string READ getString  WRITE setString)
    Q_PROPERTY(QString legacyString READ getLegacyString  WRITE setLegacyString NOTIFY colorChanged)
    Q_PROPERTY(QString argbString READ getArgbString WRITE setArgbString NOTIFY colorChanged)

    Q_PROPERTY(double passivR READ getR  WRITE setRPassiv)
    Q_PROPERTY(double passivG READ getG  WRITE setGPassiv)
    Q_PROPERTY(double passivB READ getB  WRITE setBPassiv)
    Q_PROPERTY(double passivW READ getW  WRITE setWPassiv)

    Q_PROPERTY(bool hasR READ getHasR  WRITE setHasR)
    Q_PROPERTY(bool hasG READ getHasG  WRITE setHasG)
    Q_PROPERTY(bool hasB READ getHasB  WRITE setHasB)
    Q_PROPERTY(bool hasW READ getHasW  WRITE setHasW)

    Q_PROPERTY(QJsonObject serialized READ getSerialized WRITE setSerialized NOTIFY colorChanged)
    Q_PROPERTY(QString preview READ getRGBPrev NOTIFY colorChanged)
    Q_PROPERTY(QString deviceWhiteColor READ getDeviceWhiteColor WRITE setDeviceWhiteColor)
    Q_PROPERTY(double brightness READ getBrightness WRITE setBrightness NOTIFY colorChanged)
    Q_PROPERTY(RGBWColor* copy READ getCopy)

    double getR();
    double getG();
    double getB();
    double getW();
    double getA() const;

    bool getHasR();
    bool getHasG();
    bool getHasB();
    bool getHasW();

    double getBrightness();
    QString getDeviceWhiteColor();
    RGBWColor* getCopy();
    void setBrightness(double d);


public slots:
    void setR(double r);
    void setG(double g);
    void setB(double b);
    void setW(double w);
    void setA(double value);

    void setHasR(bool has);
    void setHasG(bool has);
    void setHasB(bool has);
    void setHasW(bool has);

    void setRPassiv(double r);
    void setGPassiv(double g);
    void setBPassiv(double b);
    void setWPassiv(double w);

    void setSerialized(QJsonObject json);
    QJsonObject getSerialized();

    void setString(QString colorString);
    void setLegacyString(QString legString);
    void setArgbString(QString argb);
    QString getArgbString();
    QString getString();
    QString getLegacyString();


    void setDeviceWhiteColor(QString name);

    void setHsv(float h, float s, float v);

signals:
    void colorActiveChanged();
    void colorChanged();
private:
double r,g,b,w, a;
bool hasR, hasG, hasB, hasW;
QColor maximizeColor(QColor c);
QString formateNumber(double num);

QColor deviceWhiteColor;

};

#endif // RGBWCOLOR_H
