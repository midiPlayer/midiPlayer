
WorkerScript.onMessage = function(message) {
    var model = message.model;
    if(message.hasOwnProperty("clear") && message.clear === true){
        model.clear();
    }
    if(message.hasOwnProperty("add")){
        if(message.hasOwnProperty("displayName")){
            model.append({"devId":message.add,"displayName":message.displayName, "accepted" : message.accepted});
        }
        else{
            model.append({"devId":message.add,"displayName":message.add,"accepted" : message.accepted});
        }
    }
    if(message.hasOwnProperty("rm")){
        for(var i = 0; i < model.count; i++){
            var d = model.get(i);
            if(d.devId === message.rm){
                model.remove(i);
                break;
            }
        }
    }
    model.sync();
}
