import QtQuick 2.7
import WebSocketConnector 1.1
Item {
    visible: false;
    property WebSocketConnector parentWsc;
    property string remoteId;

    Component.onCompleted: {
        parentWsc.onMessage.connect(function(msg) {
            if(msg.hasOwnProperty(remoteId)){
                wsc.requestId = msg[remoteId];
            }
        });

        valueChanged.connect(function(){
            if(timer.running){
                messageScheduld = true;
                return;
            }
            var msg = {"v"  :value};
            wsc.send = JSON.stringify(msg);
            timer.start();
        });
    }
    property bool messageScheduld: false;
    Timer{
        id:timer
        interval: 100
        onTriggered: {
            if(!messageScheduld)
                return;
            var msg = {"v"  :value};
            wsc.send = JSON.stringify(msg);
            messageScheduld = false;
        }
    }

    WebSocketConnector{
        id: wsc
        onMessage: {
            if(msg.hasOwnProperty("v"))
                value = msg.v;
        }
    }

}
