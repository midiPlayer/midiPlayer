WorkerScript.onMessage = function(params) {

    params.model.clear();

    for (var z = 0; z < params.data.length; z++){
        var dev = params.data[z];
        params.model.append({"text" : dev.devId, "requestId" : dev.providerId});
    }

    params.model.sync();

}
