import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

import QtQuick.Controls.Material 2.0

ApplicationWindow {
    id: applicationWindow

    Material.theme: Material.Dark
    Material.foreground:"#fff"
    Material.background:"#576d73"
    Material.accent: "#369cb6"

    title: qsTr("Beamer")
    width: 800
    height: 600
    visible: true

    color:"#002b38"

    StackView {
        anchors.fill:parent
        id: stackView
        Layout.fillWidth: true;
        Layout.fillHeight: true;
        // Implements back key navigation
        focus: true

        initialItem: Qt.resolvedUrl("MyConnectView.qml")
    }

}
