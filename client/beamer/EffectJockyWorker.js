WorkerScript.onMessage = function(params) {

    var conf = params.state;



    //add new scenes
    for(var i = 0; i < conf.length; i++){
        var owner = conf[i].owner;
        var effectId = conf[i].effect;
        var config = conf[i].conf;
        var opacity = conf[i].opacity;




        var oldIndex = -1;
        for(var j = i; j < params.model.count; j++){
            if(params.model.get(j).owner === owner && params.model.get(j).effect === effectId){
                oldIndex = j;
                break;
            }
        }
        if(oldIndex !== -1){//found
            params.model.move(oldIndex,i,1);
        }
        else{//notFound --> insert
            params.model.insert(i,{"owner":owner,"effect":effectId,"config":0, "effectOpacity":opacity,
                                "creationMessage":JSON.stringify(params.msg)})
        }
    }

    if(params.model.count - i > 0)
        params.model.remove(i,params.model.count - i);

    params.model.sync();

}
