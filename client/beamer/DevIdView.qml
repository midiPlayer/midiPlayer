import QtQuick 2.0
import WebSocketConnector 1.1
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

Item {

    id: devIdView
    property string lastDevId;


    ColumnLayout{
        anchors.centerIn: parent
        ComboBox{
            id: devIdCombo
            Layout.preferredWidth: 200
            model:ListModel{
                ListElement{
                    text: "laden ..."
                    requestId: -1
                }
                onRowsInserted: {
                    devIdCombo.selectedRequestId = get(devIdCombo.currentIndex).requestId;
                }
            }
            onCurrentIndexChanged: {
                selectedRequestId = model.get(currentIndex).requestId;
            }

            property int selectedRequestId
        }
        Button{
            text: qsTr("Verbinden")
            enabled: devIdCombo.selectedRequestId !== -1;
            onClicked: {
                devIdView.lastDevId = devIdCombo.currentText;
                stackView.pushNamedView("Beamer",Qt.resolvedUrl("EffectJocky.qml"),{"requestId":devIdCombo.selectedRequestId});
            }
        }
    }

    WorkerScript{
        id:worker
        source: "DevIdViewWorker.js";
    }

    WebSocketConnector{
    requestType: "beamerDispatcher"
        onMessage: {
            console.log(JSON.stringify(msg))
            if(msg.hasOwnProperty("avBeamer")){
                worker.sendMessage({"data":msg.avBeamer,"model":devIdCombo.model})
                for (var z = 0; z < msg.avBeamer.length; z++){
                    if(msg.avBeamer[z].devId === devIdView.lastDevId){
                        stackView.pushNamedView("Beamer",Qt.resolvedUrl("EffectJocky.qml"),{"requestId":msg.avBeamer[z].providerId});
                    }
                }
            }
        }
    }

    Settings{
        id: settings
        property alias lastDevId: devIdView.lastDevId;
    }
}
