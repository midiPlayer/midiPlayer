import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import WebSocketConnector 1.1
import "./"

Rectangle {
    color:"black"
    property alias requestId: ws.requestId

    Keys.onEscapePressed: {
        stackView.popNamed();
        stackView.overideStartView = ""
    }

    Component.onCompleted: {
        topBar.hide();
        stackView.overideStartView = "beamer/DevIdView.qml"
        applicationWindow.fullscreen();
    }

    MouseArea{
        anchors.fill: parent;

        onPressed: {
            stackView.popNamed();
            stackView.overideStartView = ""
        }
    }

    ColumnLayout{
        anchors.fill: parent;
        Repeater{
            id: effectRep
            model: ListModel{
                ListElement{
                    owner:-1
                    config:0
                    effect:-1
                    effectOpacity:0
                    creationMessage:"";
                }
                Component.onCompleted: {
                    remove()
                }
            }
            /*
            delegate: Button{
                text:owner
                onClicked: {
                    text = text*1 + 1;
                }
            }*/
            delegate: EffectViewer {
                id: prev
                effectId: effect
                anchors.fill: parent
                onContentLoaded: {
                    if(config !== null)
                        recieve(config);
                    myReciever(JSON.parse(creationMessage));
                }
                opacity: effectOpacity

                function myReciever(msg){
                    console.log("recieved:" + JSON.stringify(msg));
                    if(msg.hasOwnProperty("state")){
                        for(var z = 0; z < msg.state.length; z++){
                            if(msg.state[z].effect === effect && msg.state[z].owner === owner){ //for me
                               recieve(msg.state[z].conf);
                               if(msg.state[z].triggered)
                                   trigger();
                                break;
                            }
                        }
                    }

                    if(msg.hasOwnProperty("colors")){
                        for(var z = 0; z < msg.colors.length; z++){
                            if(msg.colors[z].owner === owner){ //for me
                               color(msg.colors[z].colorName,msg.colors[z].color);
                            }
                        }
                    }
                }

                Component.onCompleted: {
                    ws.onMessage.connect(myReciever);
                }
                Component.onDestruction: {
                    ws.onMessage.disconnect(myReciever);
                }
            }
        }
    }

    WorkerScript{
        id: worker
        source: "EffectJockyWorker.js"
    }

    WebSocketConnector{
        id: ws
        onMessage: {
            console.log(JSON.stringify(msg))
            if(msg.hasOwnProperty("state")){
                worker.sendMessage({"state": msg.state, "msg":msg, "model":effectRep.model})
            }
        }
    }

    Component.onDestruction: {
        topBar.show();
        applicationWindow.fullscreenOff();
    }
}
