import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import WebSocketConnector 1.1

Item{
    id: prevContainer
    property string workdir
    property string effectName
    property int effectId

    property string currentEffectCode;

    property var component;

    property string type: "beamer";

    signal contentLoaded();

    function trigger(){
        if(component !== null)
            component.trigger();
    }

    function color(name, value){
        if(component !== null)
            component.color(name,value);
    }

    function recieve(msg){
        if(component !== null)
            component.recieve(msg);
    }

    function load(effectCode){
        if(component !== null && component !== undefined){
            component.destroy();
        }

        component = Qt.createQmlObject(effectCode,prevContainer);

        contentLoaded();
    }


    WebSocketConnector{
        id:ws
        onMessage: {
            console.log(JSON.stringify(msg))
            if(msg.hasOwnProperty(type + "Code")){
                currentEffectCode = msg[type + "Code"];
                load(currentEffectCode);
            }
        }
        registrationParams: JSON.stringify({"as":type});
        requestId: effectId;
    }
}
