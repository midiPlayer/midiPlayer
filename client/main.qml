import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import QtQuick.Controls.Material 2.0

ApplicationWindow {
    id: applicationWindow

    Material.theme: Material.Dark
    Material.foreground:"#fff"
    Material.background:"#576d73"
    Material.accent: "#369cb6"

    title: qsTr("LightClient")
    width: 1000
    height: 800
    visible: true
    property var backPressHandler: function() {return false};

    color:"#002b38"

    function fullscreen(){
        applicationWindow.visibility = Window.FullScreen;
    }

    function fullscreenOff(){
        applicationWindow.visibility = Window.Windowed;
    }


    IOManager {
        id: ioManager
    }

    signal keyPress(var event);

ColumnLayout{
    anchors.fill: parent;
    spacing: 0
    Keys.onPressed: {
        //event.accepted = true;

        if(event.key === Qt.Key_S && event.modifiers === Qt.ControlModifier){
            console.log("STRG+S");
            ioManager.save();
        }
        if(event.key === Qt.Key_O && event.modifiers === Qt.ControlModifier){
            ioManager.open();
        }

        if(event.key === Qt.Key_F11){
            if(applicationWindow.visibility === Window.FullScreen)
                fullscreenOff();
            else
                fullscreen();
        }

        keyPress(event);

    }


    Item{
        id: topBar
        Layout.fillWidth:true;
        Layout.preferredHeight: 40;

        Behavior on opacity{
            NumberAnimation{
                duration: 200
            }
        }

        function hide(){
            Layout.preferredHeight = 0;
        }
        function show(){
            Layout.preferredHeight = 40;
        }

        RowLayout{
            anchors.fill: parent
            Item{
                id:backBtn
                Layout.fillHeight: true
                Layout.preferredWidth: height

                Image {
                    source: "icons/back.png"
                   fillMode: Image.PreserveAspectFit
                    height: parent.height * 0.6
                    anchors.centerIn: parent
                }
                Image {
                    id: touched
                    source: "icons/backTouched.png"
                   fillMode: Image.PreserveAspectFit
                    height: parent.height * 0.6
                    anchors.centerIn: parent
                    visible: false
                }
                MouseArea{
                    anchors.fill: parent;
                    onPressed: {
                        touched.visible = true;

                    }
                    onReleased: {
                        touched.visible = false;
                        if(!backPressHandler())
                            stackView.popNamed()

                    }
                    onCanceled: {
                        touched.visible = false;
                    }
                    onExited: {
                        touched.visible = false;
                    }
                }
            }


            Item{
                id: navigationRow
                Layout.fillWidth: true
                Layout.fillHeight: true
                property var  labels : [];

                function refresh(){
                    rep.model = labels;
                }

                RowLayout{
                    anchors.fill: parent;
                    Repeater{
                        id: rep
                        model:navigationRow.labels
                        delegate: Item{
                            Layout.preferredHeight: parent.height - 10
                           Layout.preferredWidth: arrowBack.width + textRect.width + frontArrow.width
                            Image{
                                id: arrowBack
                                source: "images/arrowBack.svg"
                                height: parent.height
                                width: 6;
                                opacity: 51/256
                            }
                            Rectangle{
                                id: textRect
                                anchors.left: arrowBack.right
                                height: parent.height;
                                width: myText.width + 20;
                                Text{
                                    anchors.centerIn: parent
                                    id: myText
                                    text: modelData
                                    color: "#369cb6"
                                }
                                color:"#33000000"
                            }
                            Image{
                                id:frontArrow
                                source: "images/arrowFront.svg"
                                height: parent.height
                                width: 6;
                                anchors.left: textRect.right
                                opacity: 51/256
                            }


                            MouseArea{
                                anchors.fill: parent;
                                onClicked: {
                                    console.log()
                                    while(navigationRow.labels.length > 0 &&
                                           navigationRow.labels[navigationRow.labels.length - 1] !== modelData){
                                        navigationRow.labels.pop();
                                        stackView.pop();
                                    }
                                    navigationRow.refresh();
                                }
                            }
                        }
                    }
                    Item{
                        Layout.fillWidth: true;
                    }
                }
            }
        }
    }

    StackView {
        id: stackView
        Layout.fillWidth: true;
        Layout.fillHeight: true;
        // Implements back key navigation
        focus: true
        Keys.onReleased: {if ((event.key === Qt.Key_Back && stackView.depth > 1)
                            || (event.key === Qt.Key_Left && event.modifiers & Qt.AltModifier)) {
                             if(!backPressHandler())
                                 stackView.popNamed()
                             event.accepted = true;
                         }
        }

        property string overideStartView : "";



        initialItem: Qt.resolvedUrl("ConnectView.qml")
        //initialItem: Qt.resolvedUrl("ColorPicker/ColorPicker.qml")
        onDepthChanged: {
            topBar.opacity = (depth > 1)*1;
        }

        function pushNamedView(name, url, params){
            navigationRow.labels.push(name);
            navigationRow.refresh();
            push(url,params);
            currentItem.forceActiveFocus();
        }

        function popNamed(){
            navigationRow.labels.pop();
            navigationRow.refresh();
            stackView.pop();
            currentItem.forceActiveFocus();
        }

        function popAll(){
            navigationRow.labels = [];
            stackView.pop(null);
        }
    }
    }

    Settings {
            id: settings
            property alias overideNext: stackView.overideStartView;
    }

}
