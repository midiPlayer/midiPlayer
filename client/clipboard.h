#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <QObject>
#include <QClipboard>

class Clipboard : public QObject
{
Q_OBJECT

public:
    Q_PROPERTY(QString clip READ getClipboard WRITE setClipboard)

    Clipboard();
    QString getClipboard();
    void setClipboard(QString clip);

private:
    QClipboard* clipboard;
};

#endif // CLIPBOARD_H
