import QtQuick 2.4
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
Column {

    property alias model: columnRepeater.model

    property int selectedId: -1
    property string selectedName: ""

    property bool enabled : false;
    opacity: enabled ? 1 : 0.3

    Repeater {
        id: columnRepeater
        delegate: accordion
    }

    ButtonGroup{
        id: selectionGroup
    }

    Component {
        id: accordion
        Column {
            width: parent.width

            Item {
                id: infoRow

                width: parent.width
                height: childrenRect.height
                property bool expanded: false

                MouseArea {
                    anchors.fill: parent
                    onClicked: infoRow.expanded = !infoRow.expanded
                    enabled: subs.count > 0 ? true : false
                }

                RowLayout{
                    width: parent.width
                    spacing: 5

                    Image {
                        id: carot

                        sourceSize.width: 16
                        sourceSize.height: 16
                        source: 'images/triangle.svg'
                        visible: subs.count > 0 ? true : false
                        transform: Rotation {
                            origin.x: 5
                            origin.y: 10
                            angle: infoRow.expanded ? 90 : 0
                            Behavior on angle { NumberAnimation { duration: 150 } }
                        }
                    }

                    Text {

                        font.pointSize: 12
                        visible: parent.visible

                        color: 'white'
                        text: name
                        Layout.fillWidth: true
                    }

                    RadioButton{
                    id: radio
                    visible: infoRow.visible
                    ButtonGroup.group: selectionGroup
                    onCheckedChanged: {
                        if(checked){
                            selectedId = sceneId
                            selectedName = name
                        }
                    }
                }

                }
            }

            ListView {
                id: subentryColumn
                x: 20
                width: parent.width - x
                height: childrenRect.height * opacity
                visible: opacity > 0
                opacity: infoRow.expanded ? 1 : 0
                delegate: accordion
                model: subs
                interactive: false
                Behavior on opacity { NumberAnimation { duration: 200 } }
            }
        }
    }
}
