import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import WebSocketConnector 1.1
import "RemoteTypes/"

Item {
    ColumnLayout{
        width:300
        height: 300
        anchors.centerIn: parent
        spacing: 10
        Item{
            Layout.fillWidth: true
            Layout.preferredHeight: 30
            GridLayout{
                columns: 2
                anchors.fill: parent
                Text{
                    color:"#fff"
                    text: qsTr("MinLevel")
                }
                Slider{
                    id: minLevelSlider
                    Layout.fillWidth: true
                    minimumValue: -100
                    maximumValue: 0
                    onValueChanged: {
                        var msg = {"minLevel":value};
                        ws.send = JSON.stringify(msg)
                    }
                }

                Text{
                    color:"#fff"
                    text: qsTr("Onset Level")
                }

                Slider{
                    id:onsetLevelSlider
                    Layout.fillWidth: true
                    minimumValue: 0
                    maximumValue: 1
                    RemoteType{
                        parentWsc:ws
                        remoteId:"onsetLevel"
                        property alias value : onsetLevelSlider.value
                    }
                }
            }
        }
        Item{
            Layout.preferredHeight: 30
            Layout.fillWidth: true
            TriggerSourceBtn{

            }
        }
    }
    WebSocketConnector{
        id:ws
        requestType: "JackProcessor"
        onMessage: {
            console.log(JSON.stringify(msg))
            if(msg.minLevel !== undefined)
                minLevelSlider.value = msg.minLevel;
        }
    }
}

