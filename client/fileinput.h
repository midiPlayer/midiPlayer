#ifndef FileInput_H
#define FileInput_H
#include <QObject>
#include <QFileSystemWatcher>


class FileInput: public QObject
{
Q_OBJECT

public:
    Q_PROPERTY(QString path READ getPath WRITE setPath)
    Q_PROPERTY(bool watch READ getWatch WRITE setWatch)
    FileInput();
    void setPath(QString pathP);
    QString getPath();

    bool getWatch() const;
    void setWatch(bool value);

signals:
    void contentRead(QString content);

public slots:
    void read();

private:
    QString path;
    bool watch;
    QFileSystemWatcher watcher;
};

#endif // FileInput_H
