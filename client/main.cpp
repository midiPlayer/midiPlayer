#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QQuickStyle>
#include "../webSocketConnector/websocketconnector.h"
#include "rgbwcolor.h"
#include "rgbhsvcolor.h"
#include "fileoutput.h"
#include "fileinput.h"
#include "programmer.h"
#include "setcursor.h"
#include "clipboard.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<WebSocketConnector>("WebSocketConnector", 1,1, "WebSocketConnector");
    qmlRegisterType<RGBWColor>("RGBWColor", 1,1, "RGBWColor");
    qmlRegisterType<RgbHsvColor>("RgbHsvColor", 1,0, "RgbHsvColor");
    qmlRegisterType<FileOutput>("FileOutput", 1,0, "FileOutput");
    qmlRegisterType<FileInput>("FileInput", 1,0, "FileInput");
    qmlRegisterType<Programmer>("Programmer", 1,0, "Programmer");
    qmlRegisterType<SetCursor>("SetCursor", 1,0, "SetCursor");
    qmlRegisterType<Clipboard>("Clipboard", 1,0, "Clipboard");

    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    app.setOrganizationName("fdg");
    app.setOrganizationDomain("fdg-ab.de");
    app.setApplicationName("light control");

    return app.exec();
}
