#ifndef PROGRAMMER_H
#define PROGRAMMER_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QTimer>

class Programmer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString ports READ scanPorts)

public:
    Programmer();
    QString scanPorts();
signals:
    void openFailed();
    void programmSucceded();
    void programmTimeout();

public slots:
    void programm(QString port, QString data);
    void timeout();
    void read();
private:
    QSerialPort serial;
    QTimer timer;
};

#endif // PROGRAMMER_H
