#include "rgbwcolor.h"
#include <QColor>
#include <QDebug>
#include <QVector4D>
#include <QList>
#include <QJsonDocument>

#define RED 'r'
#define GREEN 'g'
#define BLUE 'b'
#define WHITE 'w'

RGBWColor::RGBWColor() : r(0), g(0), b (0), w(0), a(1.0), deviceWhiteColor(0,0,0),hasR(false),hasG(false),hasB(false),hasW(false)
{

}

RGBWColor::RGBWColor(RGBWColor *copy) : r(copy->getR()),g(copy->getG()),b(copy->getB()), w(copy->getW()),
    deviceWhiteColor(copy->deviceWhiteColor),hasR(copy->hasR),hasG(copy->hasG),hasB(copy->hasB),hasW(copy->hasW)
{

}

QString RGBWColor::getRGBPrev()
{
    //TODO: gewichtung anpassen
 double pr = r * 0.75 + (w *0.25 * deviceWhiteColor.redF());
 double pg = g * 0.75 + (w *0.25 * deviceWhiteColor.greenF());
 double pb = b * 0.75 + (w *0.25 * deviceWhiteColor.blueF());

 QColor c((int)(pr*255),(int)(pg*255),(int)(pb*255));
 return maximizeColor(c).name();
}

QColor RGBWColor::maximizeColor(QColor c)
{
    c.toHsv();
    int  h = c.hue();
    int s = c.saturation();
    c.setHsv(h,s,255);
    c.toRgb();
    return c;
}

QString RGBWColor::formateNumber(double num)
{
    QString ret = QString::number((int)(num*255),16);
    while(ret.length() <= 1)
        ret = "0" + ret;
    return ret;
}

double RGBWColor::getR()
{
    return r;
}

double RGBWColor::getG()
{
    return g;
}

double RGBWColor::getB()
{
    return b;
}

double RGBWColor::getW()
{
    return w;
}

bool RGBWColor::getHasR()
{
    return hasR;
}

bool RGBWColor::getHasG()
{
    return hasG;
}

bool RGBWColor::getHasB()
{
    return hasB;
}

bool RGBWColor::getHasW()
{
    return hasW;
}

void RGBWColor::setHsv(float h, float s, float v)
{
    QColor c;
    c.setHsvF(h,s,v);
    r = c.redF();
    g = c.greenF();
    b = c.blueF();
    hasR = true;
    hasG = true;
    hasB = true;
    hasW = false;
}

double RGBWColor::getA() const
{
    return a;
}

void RGBWColor::setA(double value)
{
    a = value;
}

double RGBWColor::getBrightness()
{
    if(!hasR && !hasG && !hasB && !hasW)
        return 0;
    return (r+g+b+w)/(hasR + hasG + hasB + hasW);
}

QString RGBWColor::getDeviceWhiteColor()
{
    return deviceWhiteColor.name();
}

RGBWColor *RGBWColor::getCopy()
{
    return new RGBWColor(this);
}

bool doublePValComp(const double* f1, const double* f2)
{
    return *f1 > *f2;
}

void RGBWColor::setBrightness(double d)
{
    QVector4D colorVect(r,g,b,0);

    double sum = r + g + b + w;
    colorVect /= sum ;

    colorVect *= d * 4.0;
    r = colorVect.x();
    g = colorVect.y();
    b = colorVect.z();
    w = colorVect.w();

    QList<double*> colorVals;
    colorVals.append(&r);
    colorVals.append(&g);
    colorVals.append(&b);
    //colorVals.append(&w);
    qSort(colorVals.begin(), colorVals.end(), doublePValComp);

    for(int i = 0; i < colorVals.size();i++){
        double *colorVal = colorVals.at(i);
        if(*colorVal > 1){
            double uebertrag = *colorVal - 1;
            uebertrag /= colorVals.size() - i - 1;//uebertrag pro nachfolger
            for(int j = i+1; j < colorVals.size();j++){
                double *nachfolger = colorVals.at(j);
                *nachfolger += uebertrag;
            }

            *colorVal = 1;
        }
        else
            break;
    }

    emit colorChanged();
}

void RGBWColor::setR(double r)
{
    setRPassiv(r);
    emit colorActiveChanged();
    emit colorChanged();
}

void RGBWColor::setG(double g)
{
    setGPassiv(g);
    emit colorActiveChanged();
    emit colorChanged();
}

void RGBWColor::setB(double b)
{
    setBPassiv(b);
    emit colorActiveChanged();
    emit colorChanged();
}

void RGBWColor::setW(double w)
{
    setWPassiv(w);
    emit colorActiveChanged();
    emit colorChanged();
}

void RGBWColor::setHasR(bool has)
{
    hasR = has;
}

void RGBWColor::setHasG(bool has)
{
    hasG = has;
}

void RGBWColor::setHasB(bool has)
{
    hasB = has;
}

void RGBWColor::setHasW(bool has)
{
    hasW = has;
}

void RGBWColor::setRPassiv(double r)
{
    this->r = r;
    emit colorChanged();
}

void RGBWColor::setGPassiv(double g)
{
    this->g = g;
    emit colorChanged();
}

void RGBWColor::setBPassiv(double b)
{
    this->b = b;
    emit colorChanged();
}

void RGBWColor::setWPassiv(double w)
{
    this->w = w;
    emit colorChanged();
}

void RGBWColor::setSerialized(QJsonObject json)
{
    //QJsonDocument d = QJsonDocument::fromJson(json.toUtf8());
    //QJsonObject o = d.object();
    QJsonObject o = json;
    this->hasR = true;
    this->hasG = true;
    this->hasB = true;
    this->hasW = true;

   r = o.value("r").toDouble();
   g = o.value("g").toDouble();
   b = o.value("b").toDouble();
   w = o.value("w").toDouble();
}

QJsonObject RGBWColor::getSerialized()
{
    QJsonObject ret;
    ret.insert("r",r);
    ret.insert("g",g);
    ret.insert("b",b);
    ret.insert("w",w);
    return ret;
    /*QJsonDocument d;
    d.setObject(ret);
    return QString(d.toJson());*/
}

void RGBWColor::setString(QString colorString)
{
    /*
     * ACHTUNG es wird nicht das standard format verwedet sonder ein eigenes mit Weiß am ende
     * valid formats:
     * rrggbb
     * rrggbbww
     * */
    hasR = true;
    hasG = true;
    hasB = true;
    hasW = true;
    if(colorString.length() >= 7){ //rrggbb
        bool ok;
        QStringRef redStr(&colorString,1,2);
        r = redStr.toInt(&ok,16)/255.0;
        QStringRef greenStr(&colorString,3,2);
        g = greenStr.toInt(&ok,16)/255.0;
        QStringRef blueStr(&colorString,5,2);
        b = blueStr.toInt(&ok,16)/255.0;
    }
    if(colorString.length() >= 9){//rrggbbww
        QStringRef whiteStr(&colorString,7,2);
        bool ok;
        w = whiteStr.toInt(&ok,16)/255.0;
    }
    else
        w = 0;
    emit colorChanged();
}

void RGBWColor::setLegacyString(QString legString)
{

    setString(legString + "00");
    hasW = false;
}

void RGBWColor::setArgbString(QString argb)
{
    bool ok;
    QStringRef aStr(&argb,1,2);
    a = aStr.toInt(&ok,16)/255.0;
    setLegacyString("#" + argb.right(6));
}

QString RGBWColor::getArgbString()
{
    return "#" + formateNumber(a) + formateNumber(r) + formateNumber(g) +
            formateNumber(b);
}

QString RGBWColor::getString()
{
    QString ret = "#" + formateNumber(r) + formateNumber(g) +
            formateNumber(b) + formateNumber(w);
    return ret;
}

QString RGBWColor::getLegacyString()
{
    QString ret = "#" + formateNumber(r) + formateNumber(g) +
            formateNumber(b);
    return ret;
}

void RGBWColor::setDeviceWhiteColor(QString name)
{
    deviceWhiteColor.setNamedColor(name);
    deviceWhiteColor = maximizeColor(deviceWhiteColor);
}

