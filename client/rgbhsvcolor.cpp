#include "rgbhsvcolor.h"

RgbHsvColor::RgbHsvColor() : QObject()
{

}

double RgbHsvColor::getHue()
{
    return color.hueF();
}

double RgbHsvColor::getSat()
{
    return  color.saturationF();
}

double RgbHsvColor::getVal()
{
    return color.valueF();
}

double RgbHsvColor::getR()
{
    return color.redF();
}

double RgbHsvColor::getG()
{
    return color.greenF();
}

double RgbHsvColor::getB()
{
    return color.blueF();
}

void RgbHsvColor::setR(double d)
{
    color.setRedF(d);
    emit colorChanged();
}

void RgbHsvColor::setG(double d)
{
    color.setGreenF(d);
    emit colorChanged();
}

void RgbHsvColor::setB(double d)
{
    color.setBlueF(d);
    emit colorChanged();
}

void RgbHsvColor::setHsv(double h, double s, double v)
{
    color.setHsvF(h,s,v);
    emit colorChanged();
}

QString RgbHsvColor::getRgbString()
{
    return color.name();
}

void RgbHsvColor::setRgbString(QString str)
{
    color.setNamedColor(str);
    emit colorChanged();
}

QString RgbHsvColor::hsvToRgb(double h, double s, double v)
{
    QColor tempC;
    tempC.setHsvF(h,s,v);
    return tempC.name();
}
