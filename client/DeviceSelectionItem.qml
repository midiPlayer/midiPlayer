import QtQuick 2.0


Item {
    id:item

    signal clicked();
    signal longClick();
    signal shortClick();
    property string myText: displayName

    width: 80; height: 80
    states: [
        State {
            name: "checked"
            PropertyChanges {
                target: coloredRect
                color:"#ffffff"

            }
        },
        State {
            name: "dissabled"
            PropertyChanges {
                target: coloredRect
                color:"#666666"

            }
        }
    ]

    transitions: Transition {
            ColorAnimation {  duration: 100; }
    }

    Rectangle{
        id:coloredRect
        color:"#22000000"
        radius: 2
        width: parent.width - 4;
        height: parent.height - 4;
        anchors.centerIn: parent;
        Text {
            text: myText;
            anchors.centerIn: parent
            color: "#369cb6";
        }
    }
    MouseArea{
        anchors.fill: parent;
        onClicked: {
            item.clicked();
        }

        Timer{
            id: longPessTimer;
            property bool longclick: false;
            interval: 300;
            repeat: false;
            function go(){
                longclick = false;
                restart();
            }
            onTriggered: {
                longclick = true;
                item.longClick();
            }

        }

        onPressed: {
            longPessTimer.go();
        }
        onReleased: {
            if(!longPessTimer.longclick)
                item.shortClick();
        }
    }
}
