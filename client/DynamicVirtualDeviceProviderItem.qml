import QtQuick 2.0

Item {
    width: 80; height: 80
    id:delegate
    signal longPressed();
    signal shortPressed();

    Rectangle{
        id:coloredRect
        color:"#22000000"
        radius: 2
        width: parent.width - 4;
        height: parent.height - 4;
        anchors.centerIn: parent;
        Text {
            text: devId;
            anchors.centerIn: parent
            color: "#369cb6";
        }


        PropertyAnimation{
            id:delAnim
            target: coloredRect
            property: "color"
            running: false
            from: "#44000000";
            to: "#ddff0000";
            duration: 500
        }
    }
    MouseArea{
        anchors.fill: parent;
        onPressed: {
            delAnim.start();
        }
        onReleased: {
            if(delAnim.running)
                shortPressed();
            else longPressed();

            delAnim.stop();
            coloredRect.color = "#22000000";
        }

    }
}
