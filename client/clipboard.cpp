#include "clipboard.h"
#include <QGuiApplication>
#include <QDebug>

Clipboard::Clipboard()
{
    clipboard = QGuiApplication::clipboard();
}

QString Clipboard::getClipboard()
{
    return clipboard->text();
}

void Clipboard::setClipboard(QString clip)
{
    clipboard->setText(clip);
}
