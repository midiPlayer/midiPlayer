import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import WebSocketConnector 1.1

Item{
    property alias requestId: ws.requestId
    property alias requestType: ws.requestType

    property string type: ""

    function chooseCorrectListModel(requestType){
        if(type === "diaScene"){
            return sceneTypeModelDia
        }
        else{
            return sceneTypeModelDisco
        }
    }

    ListModel{
        id: sceneTypeModelDia
        ListElement{
            sceneName: qsTr("Disco")
            type:"discoScene"
        }
        ListElement{
            sceneName: qsTr("Keyframe")
            type:"keyFrameScene"
        }
        ListElement{
            sceneName: qsTr("Dia")
            type:"diaScene"
        }
        ListElement{
            sceneName: qsTr("DJ")
            type:"djScene"
        }
    }

    ListModel{
        id: sceneTypeModelDisco
        ListElement{
            sceneName: qsTr("Disco")
            type:"discoScene"
        }
        ListElement{
            sceneName:qsTr("Beat")
            type: "beatScene1"
        }
        ListElement{
            sceneName:qsTr("Flash")
            type:"flashScene"
        }
        ListElement{
            sceneName:qsTr("Colorwave")
            type:"colorWaveScene"
        }
        ListElement{
            sceneName: qsTr("Music")
            type:"musicScene"
        }
        ListElement{
            sceneName: qsTr("Keyframe")
            type:"keyFrameScene"
        }
        ListElement{
            sceneName: qsTr("Screencolor")
            type:"screenColor"
        }
        ListElement{
            sceneName: qsTr("Dia")
            type:"diaScene"
        }
        ListElement{
            sceneName: qsTr("Beamer")
            type:"beamerScene"
        }
        ListElement{
            sceneName: qsTr("Mouse")
            type:"mouseScene"
        }
        ListElement{
            sceneName: qsTr("Line")
            type:"lineScene"
        }
    }

    ButtonGroup {
        buttons: [newCb,copyCb]
    }

    Column{
        anchors.fill: parent
        Row{
            height: parent.height*2/3
            width: parent.width
            Item{   //new one
                width: parent.width / 2
                height: parent.height
                ColumnLayout{
                    anchors.fill: parent
                    RowLayout{
                        Layout.alignment: Qt.AlignTop
                        width: parent.width
                        RadioButton{
                            id: newCb
                            checked: true
                        }

                        Text{
                            text: qsTr("New")
                            color:"#369cb6"
                            font.pointSize: 21
                            Layout.fillWidth: true;
                        }
                    }

                    GridView{
                        id:grid
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        signal switchAllOff();
                        property string selectedType: ""
                        property string selectedName: ""
                        clip:true

                        onSelectedNameChanged:{
                            nameEdit.generateNewName(selectedName);
                        }

                        delegate: DeviceSelectionItem{
                            myText: sceneName
                            property string mState: ""
                            onClicked: {
                                grid.switchAllOff();
                                mState = "checked"
                                grid.selectedType = type
                                grid.selectedName = sceneName
                            }
                            state:newCb.checked ? mState : "dissabled"

                            Component.onCompleted: {
                                grid.switchAllOff.connect(function(){
                                    mState = "";
                                });
                            }
                        }
                        model: chooseCorrectListModel(requestType)
                        cellWidth: 85
                        cellHeight: 85

                        focus: true
                    }

                }
                MouseArea{
                    anchors.fill: parent
                    onClicked: newCb.checked = true;
                    visible: !newCb.checked
                }
            }
            Rectangle{//vert. line
                height: parent.height - 10
                width:1
                color:"#000"
            }

            Item{//copy
                width: parent.width / 2
                height: parent.height
                ColumnLayout{
                    anchors.fill: parent
                    RowLayout{
                        Layout.alignment: Qt.AlignTop
                        width: parent.width
                        RadioButton{
                            id: copyCb
                        }

                        Text{
                            text: qsTr("Copy")
                            color:"#369cb6"
                            font.pointSize: 21
                            Layout.fillWidth: true;
                        }
                    }
                    Item{
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        AddTree{
                            id: copySelect
                            anchors.fill: parent
                            enabled: copyCb.checked
                            onSelectedNameChanged: {
                                nameEdit.generateCopyName(selectedName)
                            }
                        }
                    }

                }

                MouseArea{
                    anchors.fill: parent
                    onClicked: copyCb.checked = true;
                    visible: !copyCb.checked
                }

            }
        }
        Rectangle{ //horiz. line
            height: 1
            width:parent.width
            color:"#000"
        }
        Item{//name & ok
            width: parent.width
            height: parent.height * 1/3
            RowLayout{
                anchors.fill: parent
                TextField{
                    id: nameEdit
                    Layout.preferredWidth: 250
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    function generateNewName(type){
                        text = type + " Scene";
                    }

                    function generateCopyName(copy){
                        text = qsTr("copy of") + " " + copy;
                    }

                    onFocusChanged: {
                        if(focus === true){
                            selectAll();
                        }
                    }
                }
                Button{
                    text:qsTr("Add")
                    enabled: {
                        return ((grid.selectedType !== "" && newCb.checked)
                                || (copyCb.checked && copySelect.selectedId !== -1)) && nameEdit.text!== "";
                    }

                    onClicked: {
                        if(newCb.checked){
                            var msg = {"addScene":{"name":nameEdit.text,"type":grid.selectedType}};
                            ws.send = JSON.stringify(msg);
                            stackView.popNamed();
                        }
                        else{
                            var msg = {"addScene":{"name":nameEdit.text,"type":"copy" + copySelect.selectedId}};
                            ws.send = JSON.stringify(msg);
                            stackView.popNamed();
                        }
                    }

                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                }
            }
        }

    }

    WebSocketConnector{
        id: ws
    }
}

