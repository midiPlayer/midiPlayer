import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.2
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import WebSocketConnector 1.1
import Clipboard 1.0

Item {

    property alias requestId : ws.requestId
    width: parent.width
    height: parent.height
    id: discoScene
    property PushLockBtn currentSolo: null

    focus: true;
    Keys.onReleased: {
        if(event.key === Qt.Key_Plus){
            openAdd();
        }
    }
    Keys.onPressed: {
        if(event.key === Qt.Key_V && event.modifiers === Qt.ControlModifier){
            console.log(clipboard.clip);
            var data = JSON.parse(clipboard.clip);
            if(data.hasOwnProperty("type") && data.type === "discodelegate"){
                console.log("strg + V")
                ws.send = JSON.stringify({"addScene" : {"type":"copy" + data.scene, "name":""}});

            }
        }
    }

    Clipboard{
        id: clipboard
    }


    function openAdd(){
        stackView.pushNamedView("Add Scene",Qt.resolvedUrl("AddScene.qml"),{"requestId":requestId});
    }

    ColumnLayout{
        anchors.fill: parent;
    Item{
        Layout.fillWidth: true;
        Layout.fillHeight: true;

        ListView {
            anchors.fill: parent;
            id: discolistView

        property int dragItemIndex: -1

        model: ListModel {
            id: listModel

            function rowMovedManualy(){
                var msg = new Object();
                msg.orderChanged = [];
                for(var i = 0;i < count; i++){
                    msg.orderChanged.push(listModel.get(i).modelData.subSceneId);
                }
                ws.send = JSON.stringify(msg);
            }

        }

        delegate: DiscoSceneDelegate{
            parentWsc: ws
        }
        }
    }
    RowLayout{
        Layout.preferredHeight: 50;
        Layout.fillWidth: true;
        Button{
            text: "+"
            onClicked: {
                discoScene.openAdd();
            }
        }
    }

    }
    ListModel{
        id: fusionTypeModel
        ListElement{
            name:"max"
            text: qsTr("maximum")
        }
        ListElement{
            name:"min"
            text: qsTr("minimum")
        }
        ListElement{
            name:"override"
            text: qsTr("override")
        }
        ListElement{
            name:"multiply"
            text: qsTr("Multiply")
        }
        ListElement{
            name:"sub"
            text: qsTr("Subtract")
        }
        ListElement{
            name:"add"
            text: qsTr("Add")
        }
    }


    WorkerScript {
            id: importer
            source: "DiscoSceneImport.js"
        }


    WebSocketConnector{
        id: ws
        onMessage: {

            importer.sendMessage({"msg":msg,"listModel":listModel,"fusionTypeModel":fusionTypeModel});
        }
    }



}
