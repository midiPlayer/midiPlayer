import QtQuick 2.7
import QtQuick.Layouts 1.1
import WebSocketConnector 1.1
import QtQuick.Controls 1.2

    DragRect {
        id:listDelegate
        width:parent.width;
        height: contentRect.height;
        dragging: false
        content:
            Item{
            width:listDelegate.width
            height: contentRect.height;

        MouseArea{
            id: mouseArea
            anchors.fill: parent;
            onPressed: {
                if (mouse.source === Qt.MouseEventNotSynthesized){
                    listDelegate.dragging = true;
                    drag.target = listDelegate.target;
                }
                else{
                    //start timer
                    touchDragStartTimer.xOnPress = mouse.x;
                    touchDragStartTimer.yOnPress = mouse.y;
                    touchDragStartTimer.restart();
                }
            }
            Timer{
                id: touchDragStartTimer
                property int xOnPress;
                property int yOnPress;
                onTriggered: {
                    if(Math.abs(mouseArea.mouseX - xOnPress) < 10 &&
                            Math.abs(mouseArea.mouseY - yOnPress) < 10){
                        listDelegate.dragging = true;
                        mouseArea.drag.target = listDelegate.target;
                    }
                }
                repeat: false
                running: false
                interval: 300
            }

            onReleased: {
                drag.target = null;
                touchDragStartTimer.stop();
                listDelegate.dragging = false;
            }

            onClicked: {
                diaWSC.requestId = requestId;
                diaLayout.visible=true;
                diaLayout.id = id;
                diaLayout.index = index;
            }
            onDoubleClicked: {
                diaList.currentIndex = index;
                var msg = {"currentScene":id};
                wsc.send = JSON.stringify(msg);
                diaList.currentIndex = index;
            }

            //drag.target: listDelegate.target
            drag.axis: Drag.YAxis
        }

        Rectangle{
            id: contentRect;
            color:"#00000000"//transparent
            anchors.fill: parent
            height:text.height + 20

            states: [
                State {
                    when: dragging
                    PropertyChanges {
                        target: contentRect
                        color:"#33000000"
                    }
                }]

        RowLayout{
            anchors.fill: parent;
                Text{
                    anchors.centerIn: parent
                    width: parent.parent.width;
                    Layout.maximumWidth: parent.width;
                    id:text
                    text:name;
                    color:"#369cb6"
                    font.pointSize: 15;
                    wrapMode: Text.WrapAnywhere;
                }
            }
        }
    }
}

