import QtQuick 2.0
import QtQuick.Controls 1.4

TextField {
    property double value;
    onTextChanged: {
        if(text === "")
            text = "0";
    }

    onEditingFinished: {
        var tmp = text;
        tmp = tmp.replace(",",".");
        tmp = parseFloat(tmp);
        if(isNaN(tmp))
            tmp = 0;
        value = tmp;
    }

    onValueChanged: {
        text = Math.round(value* 100) / 100.0;
    }
}
