import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import WebSocketConnector 1.1

import "scenes/keyframe/"


Item {
    //property alias requestId: ws.requestId
    property WebSocketConnector ws;
    Component.onCompleted: {
        //console.log("rqid:" + requestId);
        ws.onMessage.connect(function(msg){
            console.log(JSON.stringify(msg));
            if(msg.hasOwnProperty("setTrigger")){
                timerRb.checked = msg.setTrigger.timer;
                keyframeTriggerRb.checked = msg.setTrigger.planed;
            }
            if(msg.hasOwnProperty("timerInterval")){
                intervalSlider.setValue = msg.timerInterval;
            }
            if(msg.hasOwnProperty("timerRand")){
                randSlider.value = msg.timerRand*100;
            }
            if(msg.hasOwnProperty("planed")){
                repeat.enabledTriggers = msg.planed;
                repeat.triggersModified();
            }
        });
        ws.send = JSON.stringify({"getState":true});
    }

    ColumnLayout{
        spacing: 20
        width: parent.width - 40;
        height: parent.height - 40;
        anchors.centerIn: parent;

        ColumnLayout{
        RowLayout{
            CheckBox{
                id: timerRb

                onCheckStateChanged: {
                    ws.send = JSON.stringify({"setTrigger":{"timer":checked}});
                }
            }

            Text{
                font.pixelSize: 20
                text:qsTr("Configure Timer Trigger")
                color: "#369cb6"

                MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        timerRb.checked = !timerRb.checked;
                    }
                }
            }
        }
        RowLayout{
            Text{
                color: "#369cb6"
                text: qsTr("Interval:")
                Layout.preferredWidth: 100
            }
            Layout.fillWidth: true;
            ExponentialSlider{
                id: intervalSlider
                Layout.fillWidth: true;
                maxValue: 50000
                onReadValueActiveChanged: {
                    var msg = {"timerInterval":readValue};
                    ws.send = JSON.stringify(msg);
                }
            }
            Text{
                text: Math.round(intervalSlider.readValue / 10)/100 + "s";
                color: "#369cb6";
            }

            opacity: {return timerRb.checked ? 1.0 : 0.4}
        }

        RowLayout{
            Layout.fillWidth: true;
            Text{
                color: "#369cb6"
                text: qsTr("Randomness:")
                Layout.preferredWidth: 100
            }
            Slider{
                id: randSlider
                Layout.fillWidth: true;
                from: 0
                to: 100
                onValueChanged: {
                    var msg = {"timerRand":value/100};
                    ws.send = JSON.stringify(msg);
                }
            }
            Text{
                text: Math.round(randSlider.value) + "%";
                color: "#369cb6";
            }
            opacity: {return timerRb.checked ? 1.0 : 0.4}
        }
        }

        ColumnLayout{
        RowLayout{
            CheckBox{
                id: keyframeTriggerRb
                onCheckStateChanged: {
                    ws.send = JSON.stringify({"setTrigger":{"planed":checked}});
                }
            }

            Text{
                font.pixelSize: 20
                text:qsTr("Keyframe Triggers")
                color: "#369cb6"

                MouseArea{
                    anchors.fill: parent
                    onClicked:{
                        keyframeTriggerRb.checked = !keyframeTriggerRb.checked;
                    }
                }
            }
        }
        RowLayout{
            Text{
                color: "#369cb6"
                text: qsTr("Triggers:")
                Layout.preferredWidth: 100
            }

            Repeater{
                id: repeat
                property var enabledTriggers: [1,2]
                signal triggersModified();

                function sendTriggers(){
                    ws.send = JSON.stringify({"planed" : enabledTriggers})
                }

                model: PlannedTriggerColorModel{

                }

                delegate: Rectangle{
                        id: deleg

                        width:30
                        height: 30
                        color:myColor

                        Layout.margins: {
                            right: 7
                        }

                        function updateRadius(){
                            radius =  getRadius();
                        }

                        function getRadius(){
                            return repeat.enabledTriggers.indexOf(index) != -1  ? 2 : (width / 2)
                        }

                        Component.onCompleted: {
                            repeat.triggersModified.connect(function(){
                             updateRadius();
                            });
                        }

                        radius: getRadius();
                        Behavior on radius {
                             NumberAnimation { duration: 200 }
                        }
                        MouseArea{
                            anchors.fill: parent
                            enabled: keyframeTriggerRb.checked
                            onClicked: {
                                var myIdex = repeat.enabledTriggers.indexOf(index);
                                if(myIdex === -1){//add
                                    repeat.enabledTriggers.push(index);
                                    console.log(JSON.stringify(repeat.enabledTriggers));
                                    parent.updateRadius();
                                    repeat.sendTriggers();
                                }
                                else{//rm
                                    repeat.enabledTriggers.splice(myIdex,1);
                                    parent.updateRadius();
                                    repeat.sendTriggers();
                                }
                            }
                        }

                        //opacity: repeat.enabledTriggers.indexOf(index) != -1  ? 1 : 0.3
                }
            }

            Layout.fillWidth: true;

            opacity: {return keyframeTriggerRb.checked ? 1.0 : 0.4}
        }
        }

    }
}
