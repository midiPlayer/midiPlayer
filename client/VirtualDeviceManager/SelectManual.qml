import QtQuick 2.0
import WebSocketConnector 1.1

import "../"

SelectGroup{

    property alias requestId : ws.requestId
    property bool disabled : false
    wsc:ws

    WebSocketConnector{
        id: ws
        onMessage: {
            console.log(JSON.stringify(msg));
        }
    }
}
