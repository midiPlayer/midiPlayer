import QtQuick 2.0
import WebSocketConnector 1.1

SelectSubGroup {
    wsc:subWs
    property alias requestId : subWs.requestId

    WebSocketConnector{
        id: subWs
        onMessage: {
            console.log(JSON.stringify(msg));
        }
    }
}
