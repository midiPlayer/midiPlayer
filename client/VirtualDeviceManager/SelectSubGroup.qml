import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import WebSocketConnector 1.1
import "../"

DeviceSelectionGroup{
    id: group

    property string type: "";
    property WebSocketConnector wsc;

    property bool canToggle: false

    item: DeviceSelectionCheckableItem{
        onShortClick: {
            if(canToggle){
                wsc.send = JSON.stringify({ "toggle" : devId});
                wsc.onMessage.connect(selektorCallback);
            }
            else{
                onLongClick();
            }
        }
        onLongClick: {
            wsc.send = JSON.stringify({ "getGroupSelektor" : devId});
            wsc.onMessage.connect(selektorCallback);
        }

        function selektorCallback(msg){
            var detailedSelector = "";
            if(msg.groupSelektor.type === "stripeSelector")
                detailedSelector = "SelectStripe.qml";
            else if(msg.groupSelektor.type === "SubGroupSelector")
                detailedSelector = "SelectSubGroupStandalone.qml";
            else if(msg.groupSelektor.type === "manualSelector")
                detailedSelector = "SelectManual.qml";


            if(detailedSelector !== ""){
                stackView.pushNamedView(devId,Qt.resolvedUrl(detailedSelector),
                               {"requestId":msg.groupSelektor.id});
            }
            else{
                console.log("can't find selector for:" + msg.groupSelektor.type);
            }

            wsc.onMessage.disconnect(selektorCallback);
        }

        Component.onCompleted: {
            wsc.onMessage.connect(function(msg){
                if(msg.hasOwnProperty("addAccepted") && msg.addAccepted === devId){
                    checked = true;
                }
                if(msg.hasOwnProperty("rmAccepted") && msg.rmAccepted === devId){
                    checked = false;
                }
            });
        }
    }


    Component.onCompleted: {
        wsc.onMessage.connect(function(msg){
        if(msg.hasOwnProperty("devices")){
            group.clearDevices();
            for(var i = 0; i < msg.devices.length; i++){
                if(type === "" | msg.devices[i].type === type){
                    group.addDeviceWithDisplayName(msg.devices[i].devId, msg.devices[i].name,msg.devices[i].selected);
                }
            }
        }
      });
    }
}
