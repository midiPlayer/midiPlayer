import QtQuick 2.0

Column {
    width: parent.width;
    spacing: 5
    property alias text : t.text
    property color color : "#369cb6"
    Text {
        id: t
        color: parent.color
        font.pointSize: 18
    }
    Rectangle{
        height: 1
        width: parent.width
        color: parent.color
    }
}
