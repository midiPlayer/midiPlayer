import QtQuick 2.0
import QtQuick.Controls 1.4

    Button{
        property int requestId;
        text: qsTr("Devices")
        onClicked: {
            stackView.pushNamedView("Devices",Qt.resolvedUrl("Select.qml"),{"requestId":requestId});
        }
    }
