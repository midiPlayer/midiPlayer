import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import WebSocketConnector 1.1
import "../"

DeviceSelectionGroup{
    id: group

    property string type: "";
    property WebSocketConnector wsc;

    item: DeviceSelectionCheckableItem{

        onClicked: {
            if(dissabled)
                return;
            checked  = !checked;
        }

        onCheckedChanged: {
            var msg = {"rmAccepted":devId};
            if(checked)
                msg = {"addAccepted":devId};
            wsc.send = JSON.stringify(msg);
        }

        Component.onCompleted: {

            wsc.onMessage.connect(function(msg){
                if(msg.hasOwnProperty("addAccepted") && msg.addAccepted === devId){
                    checked = true;
                }
                if(msg.hasOwnProperty("rmAccepted") && msg.rmAccepted === devId){
                    checked = false;
                }
            });
        }
    }


    Component.onCompleted: {
        wsc.onMessage.connect(function(msg){
        if(msg.hasOwnProperty("devices")){
            group.clearDevices();
            for(var i = 0; i < msg.devices.length; i++){
                if(type === "" || msg.devices[i].type === type){
                    group.addDeviceWithDisplayName(msg.devices[i].devId,msg.devices[i].name,msg.devices[i].selected);
                }
            }
        }
      });
    }
}
