import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import WebSocketConnector 1.1
import "../"
import "../RemoteTypes"

ColumnLayout{
    property alias requestId : wsc.requestId

    property alias isDeviceDissabled : doNotUse.checked

    SelectStripeHeadline{
        text: qsTr("How to use the Stripe?")
        Layout.fillWidth: true
    }

    ButtonGroup {
        buttons: deviceModeRow.children
    }
    Column{
        id: deviceModeRow
        spacing: 0
        MyRadio{
            id: doNotUse
            text: qsTr("Do not use this Stripe")
        }
        MyRadio{
            id: asOneDevice
            text: qsTr("Use like a single Device")
        }
        MyRadio{
            id: asSingleDevices
            text: qsTr("Use each LED like a single Device")
            checked:true;
        }

    }

    RemoteType{
        remoteId: "disabled"
        parentWsc: wsc
        property alias value : doNotUse.checked
    }

    RemoteType{
        remoteId: "singleDevice"
        parentWsc: wsc
        property alias value : asOneDevice.checked
    }

    WebSocketConnector{
        id: wsc
    }

}
