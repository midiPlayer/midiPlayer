import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import WebSocketConnector 1.1
import "../"
import "../RemoteTypes"

ColumnLayout{
    id:col
    property alias requestId : wsc.requestId
    property bool dissabled : false;
    
    SelectStripeHeadline{
        text: qsTr("Which devices of the Stripe?")
        Layout.fillWidth: true
    }
    
    CheckBox {
        id: useAll
        text: qsTr("Use All devices ")
        onCheckedChanged: {
            if(checked){
                fromTo.checked = false;
                useEach.checked = false;
                selectManual.checked = false;
            }
        }
        checked: true
        enabled: !dissabled
    }
    
    CheckBox {
        id: fromTo
        text: qsTr("Specify a range of Devices")
        onCheckedChanged: {
            if(checked){
                useAll.checked = false;
                selectManual.checked = false;
            }
            else if(!useEach.checked)
                useAll.checked = true;
        }
        enabled: !dissabled
    }
    
    GridLayout{
        id: fromToGrid
        columns: 2
        columnSpacing: 10
        x: 40
        SelectStripeText{
            text: qsTr("from")
            enabled: fromTo.checked && !dissabled
        }
        NumberField{
            id: from
            min : "1"
            enabled: fromTo.checked  && !dissabled
        }
        SelectStripeText{
            text: qsTr("to")
            enabled: fromTo.checked  && !dissabled
        }
        NumberField{
            id:to
            min : "1"
            enabled: fromTo.checked  && !dissabled
            
        }
    }
    
    CheckBox {
        id: useEach
        text: qsTr("Use each n-th LED")
        onCheckedChanged: {
            if(checked){
                useAll.checked = false;
                selectManual.checked = false;
            }
            else if(!fromTo.checked){
                useAll.checked = true;
            }
        }
        enabled: !dissabled
    }
    
    GridLayout{
        x:40
        columnSpacing: 10
        columns: 3
        
        SelectStripeText{
            text: qsTr("each")
            enabled: useEach.checked  && !dissabled
            
        }
        NumberField{
            id:each
            min : "1"
            enabled: useEach.checked  && !dissabled
        }
        
        SelectStripeText {
            text: qsTr("LED")
            enabled: useEach.checked  && !dissabled
        }
        
        SelectStripeText{
            text: qsTr("offset")
            enabled: useEach.checked  && !dissabled
            
        }
        NumberField{
            id:eachOffset
            min : "0"
            max: (each.value - 1) + ""
            enabled: useEach.checked  && !dissabled
        }
        
        
    }
    
    CheckBox {
        id: selectManual
        text: qsTr("Select Manual")
        onCheckedChanged: {
            if(checked){
                useAll.checked = false;
                fromTo.checked = false;
                useEach.checked = false;
            }
            else{
                useAll.checked = true;
            }
        }
        enabled: !dissabled
    }
    SelectManual{
        id: manualSelektor
        disabled: col.dissabled || !selectManual.checked
    }

    RemoteType{
        remoteId: "from"
        parentWsc: wsc
        property alias value : from.value
    }
    RemoteType{
        remoteId: "to"
        parentWsc: wsc
        property alias value : to.value
    }
    RemoteType{
        remoteId: "each"
        parentWsc: wsc
        property alias value : each.value
    }
    RemoteType{
        remoteId: "eachOffset"
        parentWsc: wsc
        property alias value : eachOffset.value
    }
    RemoteType{
        remoteId: "useEach"
        parentWsc: wsc
        property alias value : useEach.checked
    }
    RemoteType{
        remoteId: "fromTo"
        parentWsc: wsc
        property alias value : fromTo.checked
    }
    RemoteType{
        remoteId: "selektManual"
        parentWsc: wsc
        property alias value : selectManual.checked
    }

    WebSocketConnector{
        id: wsc
        onMessage: {
            if(msg.hasOwnProperty("manualSelektor"))
                manualSelektor.requestId = msg.manualSelektor;
        }
    }
}
