import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import WebSocketConnector 1.1
import "../"
import "../RemoteTypes"

Item{
    property alias requestId : stripeWs.requestId

    Flickable{
        anchors.fill: parent
        contentHeight: col.height
        ScrollBar.vertical: ScrollBar { }

        ColumnLayout{
            id:col
            width: parent.width - 40
            anchors.horizontalCenter: parent.horizontalCenter
            SelectUngroup {
                id: ungroup
            }

            SelectStripeLed {
                id: selectLed
                dissabled: ungroup.isDeviceDissabled
            }
        }
    }

    WebSocketConnector{
        id: stripeWs
        onMessage: {
            if(msg.hasOwnProperty("ledSelektor"))
                selectLed.requestId = msg.ledSelektor;
            if(msg.hasOwnProperty("ungroupSelektor"))
                ungroup.requestId = msg.ungroupSelektor;
        }
    }
}
