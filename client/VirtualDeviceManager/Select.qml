import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import WebSocketConnector 1.1

Item{
    property alias requestId : ws.requestId

    width:parent.width
    height: parent.height

    ScrollView{
        id: scroller
        width:parent.width
        height: parent.height

        ColumnLayout{
            width: scroller.viewport.width      // ensure correct width
            height: children.height

                SelectGroup{
                    id:rgbGroup
                    typeName: qsTr("RGB-Devices")
                    type: "rgb"
                    wsc:manualWsc
                }
                SelectGroup{
                    id:rgbwGroup
                    typeName: qsTr("RGBW-Devices")
                    type: "rgbw"
                    wsc:manualWsc
                }
                SelectGroup{
                    id:singleGroup
                    typeName: qsTr("Single-Devices")
                    type: "white"
                    wsc:manualWsc
                }
                SelectGroup{
                    id:beamerGroup
                    typeName: qsTr("Beamer")
                    type: "beamer"
                    wsc:manualWsc
                }
                SelectSubGroup{
                    id:beamercolor
                    typeName: qsTr("Beamer-Farben")
                    type: "group-BeamEffectGrp"
                    wsc:subGroupWsc
                }
                SelectSubGroup{
                    id:stipeGroupe
                    typeName: qsTr("LED-Stipes")
                    type: "group-stripe"
                    canToggle: true;
                    wsc:subGroupWsc
                }
                SelectSubGroup{
                    id:user
                    typeName: qsTr("Userdefined-Groups")
                    canToggle: true;
                    type: "group-userDefined"
                    wsc:subGroupWsc
                }
        }

    }

    function sendData(){
        var model = beamerGroup.model;
        for(var i = 0;i< model.count;i++){
            if(model.get(i).accepted)
                console.log(model.get(i).devId);
        }
    }

    WebSocketConnector{
        id: ws
        onMessage: {
            if(msg.hasOwnProperty("manual"))
                manualWsc.requestId = msg.manual;
            if(msg.hasOwnProperty("subGroup"))
                subGroupWsc.requestId = msg.subGroup;
        }
    }

    WebSocketConnector{
        id:manualWsc
        onMessage: {
            console.log(JSON.stringify(msg));
        }
    }
    WebSocketConnector{
        id:subGroupWsc
        onMessage: {
            console.log(JSON.stringify(msg));
        }
    }

    function handleStackPush(){
        if(stackView.depth === myStackPos){
            ws.send = JSON.stringify({"requestUpdate":true});
        }
    }

    property int myStackPos : -1;
    Component.onCompleted: {
        stackView.onCurrentItemChanged.connect(handleStackPush);
        myStackPos = stackView.depth;
    }
}
