import QtQuick 2.0

Text {
    property bool enabled : true;
    color: enabled ? "#fff" : "#bbb";
}
