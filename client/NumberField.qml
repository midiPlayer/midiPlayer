import QtQuick 2.7
import QtQuick.Controls 2.0

TextField {
    id: tf
    property string min : "";
    property string max : "";
    property int value;
    property bool acceptEmpty: false
    property int emptyValue: 0;
    property bool isEmpty: true;

    horizontalAlignment: TextInput.AlignRight

    MouseArea{
        anchors.fill: parent;
        onWheel: {
            if(wheel.angleDelta.y > 0)
                tf.text = (tf.text*1) + 1;
            else
                tf.text -= 1;
        }
        onPressed: {
            mouse.accepted = false;
        }

        onReleased: {
            mouse.accepted = false;
        }

        onClicked: {
            mouse.accepted = false;
        }
    }
    onTextChanged: {
        if(text === "" && acceptEmpty){
            value = emptyValue;
            isEmpty = true;
            return;
        }

        var i = parseInt(text,10);
        if(isNaN(i))
            i = 0;

        if(min !== ""  && !isNaN(min) && i < min){
            i = min
        }

        if(max !== "" && !isNaN(max) && i > max){
            i = max;
        }
        value = i;
        text = i;
        isEmpty = false;
    }

    onValueChanged: {
        text = value;
    }
}
