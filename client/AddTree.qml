import QtQuick 2.5
import QtQuick.Controls 2.0
import WebSocketConnector 1.1

Flickable{
    anchors.fill: parent
    ScrollBar.vertical: ScrollBar { }
    property alias enabled: accordion.enabled
    property alias selectedId : accordion.selectedId
    property alias selectedName : accordion.selectedName
    contentHeight: accordion.height
    clip:true

    Accordion {
        id: accordion

        width: parent.width


        model:ListModel{
            ListElement{
                name: 'Cash'
                sceneId: 1
                subs: []
            }
        }

        WorkerScript{
            id: worker
            source: "AddTreeWorker.js"
        }

        WebSocketConnector{
            requestType: "mainwindow"
            onMessage: {
                if(msg.hasOwnProperty("sceneTree")){
                    console.log(JSON.stringify(msg.sceneTree));
                    worker.sendMessage({"model":accordion.model,"data":msg.sceneTree});
                }
            }
            Component.onCompleted: {
                send = JSON.stringify({"getSceneTree":true});
            }
        }
      }



}
