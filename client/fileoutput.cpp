#include "fileoutput.h"

#include <QFile>
#include <QTextStream>

FileOutput::FileOutput():path(),content(),contentSet(false)
{

}

void FileOutput::setPath(QString pathP)
{
    path = pathP;
    run();
}

void FileOutput::setContent(QString contentP)
{
    content = contentP;
    contentSet = true;
    run();
}

QString FileOutput::getPath()
{
    return path;
}

QString FileOutput::getContent()
{
    return content;
}

void FileOutput::deleteFile()
{
    QFile f(path);
    f.remove();
    path = "";
}

void FileOutput::run()
{
    if(path.length() != 0 && contentSet){
        QFile f(path);
        if (!f.open(QIODevice::ReadWrite |
                    QIODevice::Truncate))
               return;
           QTextStream out(&f);
           out.setCodec("UTF-8");
           out << content;
           out.flush();
           f.flush();
           f.close();
           emit written();
    }

}
