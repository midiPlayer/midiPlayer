import QtQuick 2.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Item {
    width: 300
    height: cols.height

    function getData(){
        var ret = [];
        for(var i = 0; i < posModel.count; i++){
            var pos = posModel.get(i);
            if(pos.empty)
                continue;
            ret.push({"index":pos.ledIndex,
                       "x":pos.ledX,
                       "y":pos.ledY,
                       "z":pos.ledZ});
        }
        return ret;
    }

    function setData(data){
        modelWorker.sendMessage({"model":posModel, "imp":data});
    }

    ColumnLayout{
        id: cols;
    Row{
        spacing: 10
        Text{
            text:"LED-Index"
            color:"#ffffff"
            width: 150
        }
        Text{
            text:"X"
            color:"#ffffff"
            width: 50
        }
        Text{
            text:"Y"
            color:"#ffffff"
            width: 50
        }
        Text{
            text:"Z"
            color:"#ffffff"
            width: 50
        }
    }
    Repeater{
        model: ListModel{
            id: posModel
        }

        onModelChanged: {
            console.log("mdoel changed");
        }

        Component.onCompleted: {
            modelWorker.sendMessage({"model":posModel});
        }

        Row{
            spacing: 10

            NumberField{
                width: 150
                text: empty ? ""  : ledIndex
                acceptEmpty: true
                onValueChanged: {
                    save();
                }
                onIsEmptyChanged: {
                    save();
                }

                function save(){
                    modelWorker.sendMessage({"model":posModel,"set":{"prop":"ledIndex","val":value,"ind":index}});
                    modelWorker.sendMessage({"model":posModel,"set":{"prop":"empty","val":isEmpty,"ind":index}});
                }
                onEditingFinished: {
                    //Remove filed if empty
                    if(empty){
                        modelWorker.sendMessage({"model":posModel,"rm":index});
                    }
                }
            }
            FloatField{
                width: 50
                value: ledX
                onValueChanged: {
                    modelWorker.sendMessage({"model":posModel,"set":{"prop":"ledX","val":value,"ind":index}});
                }
            }
            FloatField{
                width: 50
                value: ledY
                onValueChanged: {
                    modelWorker.sendMessage({"model":posModel,"set":{"prop":"ledY","val":value,"ind":index}});
                }
            }
            FloatField{
                width: 50
                value: ledZ
                onValueChanged: {
                    modelWorker.sendMessage({"model":posModel,"set":{"prop":"ledZ","val":value,"ind":index}});
                }
            }
        }
        }
    }

    WorkerScript {
            id: modelWorker
            source: "DynamicVirtualDeviceProviderAddPositionsTableWorker.js"
   }

}
