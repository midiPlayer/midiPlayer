import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import WebSocketConnector 1.1

ColumnLayout{
    property alias typeName : devIdText.text;

    property Component item : DeviceSelectionItem{

    }
    property alias additionalHeadlineUiLoader : uiPlaceholder;

    Layout.fillWidth: true;

    Layout.leftMargin: 20
    Layout.rightMargin: 20


RowLayout{
    spacing: 20
    Text{
        id:devIdText
        color:"#369cb6"
        font.pointSize: 16
        text:typeName
        visible: text != ""
    }

    Item{//spacer
        Layout.fillWidth: true;
    }

    Loader { id: uiPlaceholder }

}

    Flow {
        Layout.fillWidth: true;
        Repeater {
            width:parent.width;
            id: grid
            model: ListModel{
            }
            delegate: item
        }
    }

    function clearDevices(){
        worker.sendMessage({"model":grid.model,"clear":true});
    }

    function addGenericDevice(devId){
        worker.sendMessage({"model":grid.model,"add":devId,"accepted":false});
    }

    function addDevice(devId,accepted){
        worker.sendMessage({"model":grid.model,"add":devId,"accepted":accepted});
    }

    function addDeviceWithDisplayName(devId,displayName,accepted){
        worker.sendMessage({"model":grid.model,"displayName":displayName, "add":devId,"accepted":accepted});
    }

    function rmDevice(devId){
        worker.sendMessage({"model":grid.model,"rm":devId});
    }

    WorkerScript {
            id: worker
            source: "DeviceSelectionGroupWorker.js"
   }
}
