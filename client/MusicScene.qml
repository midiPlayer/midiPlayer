import QtQuick 2.0

import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import WebSocketConnector 1.1

Item{
    width: parent.width;
    height:colLayout.height +20
    id:main
    property alias requestId : wsc.requestId

    ColumnLayout{
        id: colLayout
        width: parent.width - 2*20
        height: children.height
        x:20

            RowLayout{
                spacing: 20
                width: parent.width
                Text{
                    height: parent.height
                    verticalAlignment: Text.AlignVCenter
                    text:qsTr("Color")
                    color:"#fff";
                }
                MusicPlayer{
                    id:player
                }
            }

    }


    WebSocketConnector{
        id: wsc
        onMessage: {
            if(msg.hasOwnProperty("player")){
                player.requestId = msg.player;
            }
        }
    }

}

