#ifndef RGBHSVCOLOR_H
#define RGBHSVCOLOR_H
#include <QColor>
#include <QObject>

class RgbHsvColor : public QObject
{
Q_OBJECT

    Q_PROPERTY(QString rgb READ getRgbString WRITE setRgbString NOTIFY colorChanged)
    Q_PROPERTY(double hue READ getHue NOTIFY colorChanged)
    Q_PROPERTY(double sat READ getSat NOTIFY colorChanged)
    Q_PROPERTY(double val READ getVal NOTIFY colorChanged)

    Q_PROPERTY(double r READ getR WRITE setR NOTIFY colorChanged)
    Q_PROPERTY(double g READ getG WRITE setG NOTIFY colorChanged)
    Q_PROPERTY(double b READ getB WRITE setB NOTIFY colorChanged)

public:
    RgbHsvColor();

public slots:
    double getHue();
    double getSat();
    double getVal();

    double getR();
    double getG();
    double getB();

    void setR(double d);
    void setG(double d);
    void setB(double d);

    void setHsv(double h, double s, double v);

    QString getRgbString();
    void setRgbString(QString str);

    QString hsvToRgb(double h, double s, double v);

signals:
    void colorChanged();
private:
    QColor color;
};

#endif // RGBHSVCOLOR_H
