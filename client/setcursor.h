#ifndef SETCURSOR_H
#define SETCURSOR_H

#include "QObject"

class SetCursor: public QObject
{
Q_OBJECT

public:
    SetCursor();
public slots:
    void setPos(int x, int y);
};

#endif // SETCURSOR_H
