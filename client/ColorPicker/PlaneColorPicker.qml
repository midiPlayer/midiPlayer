import QtQuick 2.7
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import RGBWColor 1.1
import RgbHsvColor 1.0
ColorPickerMethod{

    RowLayout{
        anchors.fill: parent
        Item{
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            ColorPlane{
                anchors.centerIn: parent
                width: parent.width - 80
                height: parent.height - 80

                onTouchAt: {
                    colorSelected(color);
                    console.log(color);
                }
                onTouchMoved: {
                    colorMoved(color);
                }
                brightness: valueSlider.value
            }
        }
        Slider{
            id: valueSlider
            orientation: Qt.Vertical
            Layout.minimumWidth: 60
            Layout.preferredHeight: parent.height - 80;
            value: last.brightness
            //value:0.5
            minimumValue: 0
            maximumValue: 1
        }
    }
    RGBWColor{
        id:last
        hasR: true
        hasG: true
        hasB: true
        hasW: true
        legacyString: lastColor
    }
}
