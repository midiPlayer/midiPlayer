import QtQuick 2.0
import QtQuick.Layouts 1.3

Item {
    signal colorSelected(string color);
    signal colorMoved(var color);
    property string lastColor

    Layout.fillHeight: true;
    Layout.fillWidth: true;
}
