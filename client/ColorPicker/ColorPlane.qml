import QtQuick 2.0
import QtGraphicalEffects 1.0
import RgbHsvColor 1.0
import RGBWColor 1.1
Item {

    property double brightness;
    onBrightnessChanged: {
        filter.requestPaint();
    }

    signal touchAt(var color);
    signal touchMoved(var color);


    RgbHsvColor{
        id:rgbHsv
    }

    function colorStringAt(asdf, b){
        rgbHsv.setHsv(0.5,b,1);
        return rgbHsv.rgb;
    }

    LinearGradient {
        anchors.fill: parent
        start: Qt.point(0, 0)
        end: Qt.point(width, 0)
        gradient: Gradient {
           GradientStop { position: 1.0; color: {return rgbHsv.hsvToRgb(0.0,1,1);} } // 1/6
           GradientStop { position: 0.85; color: {return rgbHsv.hsvToRgb(0.85,1,1);}}// 1/3 = 2*1/6
           GradientStop { position: 0.76; color: {return rgbHsv.hsvToRgb(0.76,1,1);} } //1/2 = 3* 1/6
           GradientStop { position: 0.5;  color: {return rgbHsv.hsvToRgb(0.5,1,1);} }//...
           GradientStop { position: 0.33; color: {return rgbHsv.hsvToRgb(0.33,1,1);} }
           GradientStop { position: 0.16; color: {return rgbHsv.hsvToRgb(0.16,1,1);} }
           GradientStop { position: 0.0;  color: {return rgbHsv.hsvToRgb(0.0,1,1);} }
        }
    }

    LinearGradient {
        anchors.fill: parent
        start: Qt.point(0, 0)
        end: Qt.point(0, height)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#FFFFFFFF" }
            GradientStop { position: 1.0; color: "#00000000" }
        }
    }

    MouseArea{
        anchors.fill: parent
        onPressed:{
            //touchAt(1- (mouseX / width),mouseY / height);
            rgbw.setHsv(mouseX / width,mouseY/height,1);
            rgbw.brightness = brightness;
            touchAt(rgbw.legacyString);
        }
        onMouseXChanged: {
            moved();
        }
        onMouseYChanged: {
            moved();
        }

        function moved(){
            rgbw.setHsv(mouseX / width,mouseY/height,1);
            rgbw.brightness = brightness;
            touchMoved(rgbw.legacyString);
        }
    }

    Component.onCompleted: {
        console.log("c @:" + colorStringAt(0.5,1));
    }

    RgbHsvColor{
        id: col
    }
    Canvas{
        //visible:false
        id: filter
        anchors.fill: parent;
        onPaint: {
            var ctx = getContext("2d");
            /*for(var a = 0; a < 1; a += 0.01){
                for(var b = 0; b < 1; b += 0.01){
                    var c = (2.0 - a) -b;
                    if(c > 0 && c < 1){
                        col.r = a;
                        col.g = b;
                        col.b = c;
                        ctx.fillRect(col.hue * width,col.sat*height,1,1);
                    }
                }
            }*/
            context.clearRect (0, 0, width, height);

            if(brightness > 0){
                ctx.beginPath();
                ctx.moveTo(0,height);
                var teiler = 6 * 20; //sollte ein vielfaches von 6 sein
                for(var zaehler  = 0; zaehler <= teiler; zaehler++){
                    var x = zaehler/teiler;
                    rgbw.setHsv(x,1,1);
                   // console.log("selected color:" + rgbw.legacyString);
                    rgbw.brightness = brightness;
                   // console.log("new color:" + rgbw.legacyString);
                    col.rgb = rgbw.legacyString;
                    //ctx.fillRect(x * width - 1,col.sat*height - 1,2,2);
                    ctx.lineTo(x * width,col.sat*height);
                }
                ctx.lineTo(width,height);
                //ctx.stroke();
                ctx.fill();
                ctx.closePath();
            }
        }
    }
    /*
      adjust color
      */

    RGBWColor{
        id: rgbw
    }

    function adjC(color){
        rgbw.legacyString = color;
        rgbw.brightness = brightness;
        console.log(rgbw.legacyString);
        return rgbw.legacyString;
    }
}
