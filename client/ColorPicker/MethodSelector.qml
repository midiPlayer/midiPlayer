import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "../"

Item{
    id: methodeSelector
    width: 80
    height: parent.height
    //color:"#10000000"
    property string selectedMethod;
    
    ColumnLayout{
        anchors.fill: parent;
        Item{
            Layout.fillHeight: true
        }

        Repeater{
            model: [{"Name":qsTr("Plane"),"Handler":"PlaneColorPicker.qml"},
            {"Name":qsTr("RGB"),"Handler":"RGBColorPicker.qml"},
            {"Name":qsTr("Angle"),"Handler":"AngleColorPicker.qml"}]
            ColorPickerTypeDelegate {
                name: modelData.Name
                handler: modelData.Handler
                onClicked:{
                    methodeSelector.selectedMethod = handler;
                }
                active: methodeSelector.selectedMethod == handler;
            }
        }

        Item{
            Layout.fillHeight: true
        }
    }
}
