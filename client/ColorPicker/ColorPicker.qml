import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2
import "../"
import "../RemoteTypes"

Item{
    id: colPickerItem
    property ColorPickerButton button

    WorkerScript {
            id: addColor
            source: "ColorPickerAddColor.js"
        }

    function save(){
        var colorString=""
        for(var i = 0; i < selectedColors.model.count; i++){
            if(i!=0) colorString+=","
            colorString+=selectedColors.model.get(i).color
        }
        button.myColor=colorString
    }

    MessageDialog{
        id: msgdialog
        title: qsTr("Message")
        text: qsTr("This Scene needs at least %L1 colors! Please choose some more!").arg(button.minColorNum)
        onAccepted: {
            visible = false;
        }
        Component.onCompleted: visible = false
   }

    Component.onCompleted: {
        applicationWindow.backPressHandler=function() {

            if(button.minColorNum > selectedColors.model.count){
                msgdialog.visible = true
                return true;
            }

            applicationWindow.backPressHandler=function() {
                return false;
            }

            save()

            return false;
        }
        if(button.myColor!=""){
            var colorsOfButton=button.myColor.split(",");
            for(var i = 0; i < colorsOfButton.length; i++){
            addColor.sendMessage({"action":"add","color":colorsOfButton[i],"model":selectedColors.model})
            }
        }

    }


    Row{
        anchors.fill: parent

        MethodSelector {
            id: methodeSelector
            selectedMethod: button.selectionMethod;
            onSelectedMethodChanged: {
                button.selectionMethod = selectedMethod;
            }

        }

        ColumnLayout{
            id: colorPicker
            width: parent.width - 80
            height: parent.height
            spacing: 10


            function colorSelected(color) {
                if(button.onlyOne){             //es darf (für diese Szene) nur eine Farbe ausgewählt werden
                    button.myColor = color;
                    stackView.pop()
                    applicationWindow.backPressHandler=function() {
                        return false;
                    }
                }
                else{
                    if(!selectedColors.contains(color)) {  //folgendes passiert, wenn hinzuzufügende Farbe noch nicht enthalten ist
                        addColor.sendMessage({"action":"add","color":color,"model":selectedColors.model})
                    }
                }
            }

            property var lastColor : null;
            function colorChanged(color){
                if(prevBtn.isChecked()){
                    console.log("prev:" + JSON.stringify(color));
                    button.preview(color);
                }
                lastColor = color;
            }

            SelectedColors {
                id: selectedColors
            }

            PushLockBtn{
                id: prevBtn
                text: "P"
                visible: button.hasPreview;
                onStateOff: {
                    button.previewOff();
                }
                onStateOn:{
                    if(colorPicker.lastColor !== null){
                        button.preview(colorPicker.lastColor);
                    }
                }
            }

            Loader{
                id: methodLoader
                source: methodeSelector.selectedMethod
                Layout.fillWidth: true;
                Layout.fillHeight: true;
                onLoaded: {
                    item.lastColor = selectedColors.getLastColor();
                    item.onColorSelected.connect(function(color){
                        colorPicker.colorSelected(color);
                    });
                    item.onColorMoved.connect(function(color){
                        colorPicker.colorChanged(color);
                    });

                }
                Component.onCompleted: {
                    selectedColors.onColorsChanged.connect(function(){
                        item.lastColor = selectedColors.getLastColor();
                    });
                }
            }
        }
    }
    Component.onDestruction: {
        button.previewOff();
    }
}
