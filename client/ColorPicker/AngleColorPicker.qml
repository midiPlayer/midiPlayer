import QtQuick 2.0
import RGBWColor 1.1
import QtQuick.Controls 1.4

ColorPickerMethod {
    onLastColorChanged: {
        rgbwColor.legacyString = lastColor;
        rotator.rotation = rgbwColor.getGrayscale() * 360;
    }

    RGBWColor{
        id:rgbwColor
        hasR: true
        hasG: true
        hasB: true
        hasW: true
        function setGrayscale(gray){
            r = gray;
            g = gray;
            b = gray;
            w = gray;
        }
        function getGrayscale(){
            return (r+g+b) / 3.0;
        }
    }

    Button{
        text: qsTr("Done")
        onClicked: {
            colorSelected(rgbwColor.legacyString);
        }
    }

    Item{
        id:rotContainer
        width: 200
        height: 200
        anchors.centerIn: parent;
        Image {
            id: rotator
            source: "images/mhRotator.svg"
            anchors.fill: parent
            onRotationChanged: {
                rgbwColor.setGrayscale(rotation / 360)
                colorMoved(rgbwColor.serialized);
            }
        }
        MouseArea{
            anchors.fill: parent;

            onPressed: {
                rotator.opacity = 0.7;
                startAngle = getAngle(mouseX,mouseY)
                startRotation = rotator.rotation;
            }
            onReleased: {
                rotator.opacity = 1;
                startAngle = -1;
            }

            onMouseXChanged: {
                mouseMoved();
            }
            onMouseYChanged: {
                mouseMoved();
            }


            function mouseMoved(){
                var delta = getAngle(mouseX,mouseY) - startAngle;

                var newRot = (startRotation + delta/(2*Math.PI)*360)%360;
                if(newRot < 0)
                    newRot += 360;
                rotator.rotation = newRot;
            }

            property double startAngle : -1;
            property double startRotation;

            function dstToCenter(x,y){
                var dx = x - rotContainer.width / 2;
                var dy = y - rotContainer.height / 2;
                return Math.sqrt(dx*dx+dy*dy);
            }

            function getAngle(x,y){
                var dx = (x- rotContainer.width / 2) / dstToCenter(x,y);
                var dy = (y- rotContainer.height / 2) / dstToCenter(x,y);
                var angle = Math.asin(dy);

                if(dx < 0)
                    angle = Math.PI - angle;

                angle += Math.PI / 2;

                return angle;
            }

        }
    }
}
