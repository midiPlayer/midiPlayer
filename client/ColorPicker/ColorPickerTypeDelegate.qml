import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

Rectangle{
    id: delegate
    Layout.preferredWidth: parent.width;
    Layout.preferredHeight: 80;
    property alias name: text.text
    property bool active: false;
    property string handler;
    signal clicked();
    
    color: active ? "#22ffffff" : "#22000000";
    radius: 2;
    Text{
        id: text
        anchors.centerIn: parent;
        color: "#fff"
    }

    MouseArea{
        anchors.fill: parent;
        onClicked: {
            delegate.clicked();
        }
    }
}
