import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

Item{
    Layout.preferredHeight: 100
    Layout.fillWidth: true
    
    property alias model: selectedColorLVModel

    signal colorsChanged();

    function getLastColor(){
        return selectedColorLVModel.count > 0 ? selectedColorLVModel.get(selectedColorLVModel.count - 1).color : "#ffffff";
    }


    function contains(color){
        var found = false;
        for(var i = 0; i < selectedColorLVModel.count; i++){
           if(color === selectedColorLVModel.get(i).color){
               found=true
               break
           }
        }
        return found;
    }
    
    ListView{
        id: selectedColorLV
        
        model: ListModel{
            id: selectedColorLVModel

            onRowsInserted: {
                colorsChanged();
            }

            /* ListElement{
                        color: "red"
                    }
                    */
            onDataChanged: {
                if(liveMode.checked)
                    save();
            }
        }
        
        
        width: parent.width*0.975
        height: parent.height
        anchors.centerIn: parent
        orientation: Qt.Horizontal
        layoutDirection: Qt.LeftToRight
        
        delegate:
            
            Item{
            
            property int myIndex: index
            //Layout.maximumWidth: 300
            //Layout.minimumWidth: 100
            //height : parent.height;
            width: 100
            height: parent.height
            ColorButton{
                anchors.centerIn: parent
                width: parent.width*0.9
                height: parent.height*0.9
                bcolor: color
                onSelected: {
                    addColor.sendMessage({"action":"delete", "model":selectedColorLVModel, "myIndex":index})
                }
            }
        }
        
    }
}
