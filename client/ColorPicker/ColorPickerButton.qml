import QtQuick 2.0
import WebSocketConnector 1.1
import QtQuick.Layouts 1.1
import "../RemoteTypes"
Item{
    id: cpb
    property string myColor: "#FFFFFF"
    property bool onlyOne: false
    property int minColorNum: 4
    property alias requestId : colorPickerWSC.requestId
    property string selectionMethod

    width: colorPickerButton.width
    height: colorPickerButton.height

    property bool initFinished : false;

    property bool hasPreview: false;

    function previewOff(){
        if(!hasPreview)
            return;
        colorPickerWSC.send = JSON.stringify({"previewOff":true});
    }

    function preview(color){
        console.log("prev1:" + color);
        if(!hasPreview)
            return;

        colorPickerWSC.send = JSON.stringify({"preview":color});
    }

    onMyColorChanged: {
       if(myColor.search(",")!=-1){ //if not -1, more than one color is needed
          colorPickerButton.color="#AAAAAA"
       }
       else if(myColor=="") colorPickerButton.color="#999999"
       else{

          colorPickerButton.color=myColor
          }

       if(initFinished)
            colorPickerWSC.send=JSON.stringify({"colorChanged":myColor})
       }

    RemoteType{
        id: lastSelectionMethod
        property alias value: cpb.selectionMethod;
        parentWsc: colorPickerWSC;
        remoteId: "selectionMethod"
    }

    WebSocketConnector{
        id: colorPickerWSC
        onMessage:{
            if(msg.colorChanged!==undefined){
                initFinished = false;
                myColor=msg.colorChanged;
                initFinished = true;
            }
            if(msg.hasOwnProperty("hasPreview")){
                hasPreview = msg.hasPreview;
            }
        }
    }
        Rectangle{
            id:colorPickerButton
            radius: height/5
            height: 30
            width: 80
            onColorChanged: {
                var regex=/(#((([A-F]|[a-f]).....)|(..([A-F]|[a-f])...)|(....([A-F]|[a-f]).)))/i //regexp überprüft, ob Farbe hell oder dunkel ist
                if((color+"").search(regex)==-1){
                    //farbe ist dunkel
                    text.color="#FFFFFF"
                }
            }

            Text{
               id: text
               text: qsTr("Color")
               color:"#000"
               font.pixelSize: parent.height*0.5
               verticalAlignment: Text.AlignVCenter
               horizontalAlignment: Text.AlignHCenter
               width:parent.width
               height:parent.height
            }

            Image{
                id: touched
                source: "images/schatten4colorPickerButton.png"
                height: parent.height
                width: parent.width
                visible: false
                anchors.fill: parent

            }

            MouseArea{
                anchors.fill: parent
                onPressed:{

                    touched.visible = true;
                }
                onReleased: {
                    stackView.push(Qt.resolvedUrl("ColorPicker.qml"),{"button":cpb});
                    touched.visible = false;
                }
                onCanceled: {
                    touched.visible = false;
                }
                onExited: {
                    touched.visible = false;
                }

            }
        }
    }


