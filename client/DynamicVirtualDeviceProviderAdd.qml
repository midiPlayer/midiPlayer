import QtQuick 2.4
import QtQuick.Controls 1.4
import WebSocketConnector 1.1

import QtQuick.Dialogs 1.2

Item {
    /**
      input:
      name
      channel (auto test)
      ???verhalten eei doppelter kanalbelegung???
      position
      */

    property string type;
    property WebSocketConnector ws;
    property string devId : "";

    property bool hasPosition : type !== "group-stripe" && type !== "group-userDefined"

    Grid {
        anchors.centerIn: parent;
        id: grid1
        anchors.fill: parent
        columns: 2
        spacing: 20;

        Text {
            text: qsTr("Name")
            color:"#fff"
        }

        TextField {
            id: nameEdit
            enabled: devId === "";
        }

        Text {
            text: qsTr("Lichtfarbe")
            color:"#fff"
            visible: type == "white"
        }

        TextField {
            id: whileColorEdit
            visible: type == "white"
        }


        Text {
            text: qsTr("Modulo-Mode")
            color:"#fff"
            visible: type == "white" || type == "rgb" || type == "rgbw"
        }

        MyCheckBox{
            id: moduloMode
            text: qsTr("enabled")
            visible: type == "white" || type == "rgb" || type == "rgbw"
        }

        Text {
            text: qsTr("Anzahl der LEDs auf dem Stripe")
            color:"#fff"
            visible: type == "group-stripe"
        }

        NumberField {
            id: numLEDs
            min: "1"
            text:"1"
            visible: type == "group-stripe"
        }

        Text {
            text: qsTr("Typ der LEDs")
            color:"#fff"
            visible: type == "group-stripe"
        }

        ComboBox {
            id: devTypes
            visible: type == "group-stripe"
            model: ListModel {
              id: devTypesModel
              ListElement { text: "RGB"; type: "rgb" }
              ListElement { text: "RGBW"; type: "rgbw" }
            }
            function getSelectedType(){
                return devTypesModel.get(currentIndex).type;
            }

            function setSelectedType(selectedType){
                for(var i = 0; i < devTypesModel.count;i++){
                    if(devTypesModel.get(i).type == selectedType){
                        currentIndex = i;
                        break;
                    }
                }
            }
        }

        Text{
            text:qsTr("DMX Universe")
            color:"#fff"
            visible : type !== "group-userDefined" && type != "beamer"
        }

        NumberField {
            id: dmxUniverse
            text : "0"
            max:"1000"
            min:"0"
            visible : type !== "group-userDefined" && type != "beamer"
        }

        Text {
            text: qsTr("DMX-Channel")
            color:"#fff"
            visible : type !== "group-userDefined" && type != "beamer"
        }

        NumberField {
            id: dmxChannel
            text : "0"
            max:"512"
            min:"0"
            visible : type !== "group-userDefined" && type != "beamer"
        }

        Text {
            text: qsTr("Find free channel")
            color:"#fff"
        }

        Button{
            text: qsTr("find")
            onClicked: {
                ws.onMessage.connect(findOnMsg);
                var action = (devId == "" ? "add" : "edit");
                var msg = {"findFreeChannel":
                    {
                        "channel":dmxChannel.value,
                        "universe":dmxUniverse.value,
                        "numDev": numLEDs.value
                    }
                };
                ws.send = JSON.stringify(msg);
            }

            function findOnMsg(msg){
                if(msg.hasOwnProperty("freeChannel")){
                    console.log("got it!");
                    dmxChannel.value = msg.freeChannel.channel;
                    dmxUniverse.value = msg.freeChannel.universe;

                    ws.onMessage.disconnect(findOnMsg);
                }
            }
        }

        Text {
            text: qsTr("Positions")
            color:"#fff"
            visible: type == "group-stripe"
        }

        DynamicVirtualDeviceProviderAddPositionsTable{
            id: positions
            visible: type == "group-stripe"
        }

        Text {
            text: qsTr("Position X")
            color:"#fff"
            visible: hasPosition
        }

        TextField {
            id: posX
            visible: hasPosition
        }

        Text {
            text: qsTr("Position Y")
            color:"#fff"
            visible: hasPosition
        }

        TextField {
            id: posY
            visible: hasPosition
        }

        Text {
            text: qsTr("Position Z")
            color:"#fff"
            visible: hasPosition
        }

        TextField {
            id: posZ
            visible: hasPosition
        }

        Text {
            text: qsTr("program stripe")
            color:"#fff"
            visible: type == "group-stripe" || type == "rgb"
        }

        Button{
            text: qsTr("program stripe")
            onClicked: {
                if(type == "group-stripe"){
                    stackView.pushNamedView("Programm Device",Qt.resolvedUrl("ProgramDevice.qml"),{"program":{
                                           "mode":"multi-stripe",
                                           "numLed" : numLEDs.value,
                                           "channel" : dmxChannel.value,
                                           "universe" : dmxUniverse.value,
                                           "ledType" : devTypes.getSelectedType()}});
                }
                else if(type == "rgb"){
                    stackView.pushNamedView("ProgrammDevice",Qt.resolvedUrl("ProgramDevice.qml"),{"program":{
                                           "mode":"single-stripe",
                                           "numLed" : 1,
                                           "channel" : dmxChannel.value,
                                           "universe" : dmxUniverse.value,
                                           "ledType" : "rgb"}});
                }
            }
            visible: type == "group-stripe" || type == "rgb"
        }

        Button{
            text:{
                if(devId === "") return qsTr("Add"); return qsTr("Save");
            }
            onClicked: {
                console.log(positions.getData());

                ws.onMessage.connect(onMsg);
                var action = (devId == "" ? "add" : "edit");
                var msg = {};
                msg[action] = {
                        "type":type,
                        "devId":nameEdit.text,
                        "channel":dmxChannel.value,
                        "universe":dmxUniverse.value,
                        "whiteColor":whileColorEdit.text,
                        "numDev":numLEDs.value,
                        "devsType":devTypes.getSelectedType(),
                        "positions":positions.getData(),
                        "x":parseInt(posX.text),
                        "y":parseInt(posY.text),
                        "z":parseInt(posZ.text),
                        "moduloMode":moduloMode.checked
                    };
                ws.send = JSON.stringify(msg);
            }
        }

    }

    function onMsg(m){
        ws.onMessage.disconnect(onMsg);
        if(m.hasOwnProperty("add") && m.add.devId === nameEdit.text)
            stackView.popNamed();
        if(m.hasOwnProperty("edit") && m.edit.devId === nameEdit.text)
            stackView.popNamed();
        if(m.hasOwnProperty("err") && m.err === "devIdInUse"){
            errDialog.text = "The device name is allready in use!";
            errDialog.visible = true;
        }
        if(m.hasOwnProperty("err") && m.err === "universeOverflow"){
            errDialog.text = "The last channel is out of the universe!";
            errDialog.visible = true;
        }
        if(m.hasOwnProperty("err") && m.err === "channelCollision"){
            errDialog.text = "The channel range is allready in use by device \"" + m.errArg + "\"";
            errDialog.visible = true;
        }
     }

    ChannelTester{
        id:tester;
        channel: dmxChannel.value;
        universe: dmxUniverse.value;
    }

    MessageDialog {
        id: errDialog
        title: qsTr("Error: device cant't be created")
        text: ""
        onAccepted: {
            visible = false;
        }
        modality: "ApplicationModal";
    }

    function loadForm(msg){
        if(msg.hasOwnProperty("get")){
            nameEdit.text = msg.get.devId;
            if(msg.get.hasOwnProperty("universe"))
                dmxUniverse.text = msg.get.universe;
            if(msg.get.hasOwnProperty("channel"))
                dmxChannel.text = msg.get.channel;
            if(msg.get.hasOwnProperty("x"))
                posX.text = msg.get.x;
            if(msg.get.hasOwnProperty("y"))
                posY.text = msg.get.y;
            if(msg.get.hasOwnProperty("z"))
                posZ.text = msg.get.z;
            if(msg.get.hasOwnProperty("whiteColor"))
                whileColorEdit.text = msg.get.whiteColor;
            if(msg.get.hasOwnProperty("numDev"))
                numLEDs.text = msg.get.numDev;
            if(msg.get.hasOwnProperty("devsType"))
                devTypes.setSelectedType(msg.get.devsType);
            if(msg.get.hasOwnProperty("positions"))
                positions.setData(msg.get.positions);
            if(msg.get.hasOwnProperty("moduloMode"))
                moduloMode.checked = msg.get.moduloMode;

            ws.onMessage.disconnect(loadForm);
        }
    }

    Component.onCompleted: {
        ws.onMessage.connect(loadForm);

        if(devId !== ""){//edit --> request data
            var msg = {"get": {"devId":devId}};
            ws.send = JSON.stringify(msg);
        }
    }
}
