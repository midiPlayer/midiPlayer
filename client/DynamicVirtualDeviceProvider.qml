import QtQuick 2.0
import QtQuick.Layouts 1.2
import QtQuick.Controls 1.4
import WebSocketConnector 1.1

Item{
    property alias requestId : ws.requestId

    width:parent.width
    height: parent.height

    ScrollView{
        id: scroller
        width:parent.width
        height: parent.height

        ColumnLayout{
            width: scroller.viewport.width      // ensure correct width
            height: children.height

                DynamicVirtualDeviceProviderGroup{
                    id:rgbGroup
                    typeName: qsTr("RGB-Devices")
                    type: "rgb"
                    wsc:ws
                }
                DynamicVirtualDeviceProviderGroup{
                    id:rgbwGroup
                    typeName: qsTr("RGBW-Devices")
                    type: "rgbw"
                    wsc:ws
                }
                DynamicVirtualDeviceProviderGroup{
                    id:singleGroup
                    typeName: qsTr("Single-Devices")
                    type: "white"
                    wsc:ws
                }
                DynamicVirtualDeviceProviderGroup{
                    id:beamerGroup
                    typeName: qsTr("Beamer")
                    type: "beamer"
                    wsc:ws
                }
                DynamicVirtualDeviceProviderGroup{
                    id:stripeGroup
                    typeName: qsTr("LED-Stripes")
                    type: "group-stripe"
                    wsc:ws
                }
                DynamicVirtualDeviceProviderGroup{
                    id:userGroup
                    typeName: qsTr("Userdefined Groups")
                    type: "group-userDefined"
                    wsc:ws
                }
        }

    }

    WebSocketConnector{
        id: ws
        requestType: "dydevMgr"
        onMessage: {
            console.log(JSON.stringify(msg));
        }
    }



}
