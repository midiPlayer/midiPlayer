WorkerScript.onMessage = function(message) {
    var model = message.model;

    if(message.hasOwnProperty("imp")){
        model.clear();
        var imp = message.imp;
        for(var i = 0; i < imp.length; i++){
            var impRow = imp[i];
            model.append({"ledIndex" : impRow.index, "ledX":impRow.x,"ledY":impRow.y,"ledZ":impRow.z,"empty" :false});
        }
    }

    if(message.hasOwnProperty("set")){
        model.setProperty(message.set.ind,message.set.prop,message.set.val);
    }

    if(message.hasOwnProperty("rm")){
        model.remove(message.rm);
    }

    if(!hasFreeColumn(model))
        model.append({"ledIndex" : 0, "ledX":0,"ledY":0,"ledZ":0,"empty" :true});

    model.sync();
}

function hasFreeColumn(model){
    for(var i = 0; i < model.count; i++){
        if(model.get(i).empty)
            return true;
    }

    return false;
}
