

import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.0
import Programmer 1.0
import Qt.labs.settings 1.0
import QtQuick.Dialogs 1.2

Item{
    property var program : ({})
    Grid {
        anchors.centerIn: parent;
        id: grid1
        anchors.fill: parent
        columns: 2
        spacing: 20;

        Text{
            text: qsTr("select port")
            color: "#fff"
        }

        Row{
            spacing: 10
            ComboBox{
                id: portSelect;
                model: {return JSON.parse(programmer.ports)}

                function getSelected(){
                    return model[currentIndex];
                }
            }

            Button{
                text: qsTr("rescan")
                onClicked: {
                    portSelect.model = JSON.parse(programmer.ports);
                }
            }
        }

        Text{
            text: qsTr("WLAN SSID")
            color: "#fff"
        }

        TextField{
            id: ssid
            color: "#fff";
        }

        Text{
            text: qsTr("WLAN password")
            color: "#fff"
        }

        TextField{
            id: pw
            echoMode: TextInput.Password;
            color: "#fff";
        }

        Text{
            text: qsTr("start programming")
            color: "#fff"
        }

        Button{
            id: progBtn
            states: [
                State {
                    name: "default"
                    PropertyChanges {
                       target: progBtn
                       text: qsTr("program");
                       enabled :true
                    }
                },
                State {
                    name: "programing"
                    PropertyChanges {
                       target: progBtn
                       text: qsTr("programing");
                       enabled :false
                    }
                }
            ]

            state : "default";
            onClicked: {
                var prog = program;
                prog.ssid = ssid.text;
                prog.pw = pw.text;
                progBtn.state = "programing";
                programmer.programm(portSelect.getSelected(),JSON.stringify(prog));
            }
        }
    }

    MessageDialog {
        id: errDialog
        title: qsTr("Error: Programming failed")
        text: ""
        onAccepted: {
            visible = false;
        }
        modality: "ApplicationModal";
    }

    Programmer{
        id: programmer

        onOpenFailed: {
            console.log("open failed");
            errDialog.text = qsTr("Failed to open Serial Port - maybe another program is using the interface?");
            errDialog.visible = true;
            progBtn.state = "default";
        }

        onProgrammSucceded: {
            console.log("success");
            stackView.popNamed();
        }

        onProgrammTimeout: {
            console.log("timeout");
            errDialog.text = qsTr("Programing timed out");
            errDialog.visible = true;
            progBtn.state = "default";
        }
    }

    Settings{
        property alias wlanSsid: ssid.text
        property alias wlanPw: pw.text
    }

}
