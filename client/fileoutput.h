#ifndef FileOutput_H
#define FileOutput_H
#include <QObject>


class FileOutput: public QObject
{
Q_OBJECT

public:
    Q_PROPERTY(QString path READ getPath WRITE setPath)
    Q_PROPERTY(QString content READ getContent WRITE setContent)
    FileOutput();
    void setPath(QString pathP);
    void setContent(QString contentP);
    QString getPath();
    QString getContent();

signals:
    void written();

public slots:
    void deleteFile();

private:
    void run();
    QString path;
    QString content;
    bool contentSet;


};

#endif // FileOutput_H
