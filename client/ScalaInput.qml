import QtQuick 2.0
import QtQuick.Layouts 1.3
import WebSocketConnector 1.1
import QtQuick.Controls 1.3
import "RemoteTypes"


Item {
    property alias requestId : wsc.requestId
    width: parent.width;
    height:colLayout.height

    ColumnLayout{
        id: colLayout
        width: parent.width
        height: children.height

        GridLayout{
            width: parent.width
            Layout.fillWidth: true;
            columns: 2
            rowSpacing: 10


            Text{
               color: "#fff"
               text: qsTr("Amplification")
            }
            Slider{
                id: ampSlider
                Layout.fillWidth: true;
                minimumValue: 1
                maximumValue: 100
                stepSize: 1
                RemoteType{
                    parentWsc:wsc
                    remoteId:"amp"
                    property alias value : ampSlider.value
                }
            }

            Text{
               color: "#fff"
               text: qsTr("Smoothness In")
            }
            Slider{
                id: smoothSliderIn
                Layout.fillWidth: true;
                minimumValue: 0
                maximumValue: 100
                stepSize: 1
                RemoteType{
                    parentWsc:wsc
                    remoteId:"smoothIn"
                    property alias value : smoothSliderIn.value
                }
            }

            Text{
               color: "#fff"
               text: qsTr("Smoothness Out")
            }
            Slider{
                id: smoothSliderOut
                Layout.fillWidth: true;
                minimumValue: 0
                maximumValue: 100
                stepSize: 1
                RemoteType{
                    parentWsc:wsc
                    remoteId:"smoothOut"
                    property alias value : smoothSliderOut.value
                }
            }
        }
    }


    WebSocketConnector{
        id: wsc
        onMessage: {
            console.log(JSON.stringify(msg));
        }
    }
}
