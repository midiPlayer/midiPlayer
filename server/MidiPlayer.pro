TEMPLATE = subdirs

SUBDIRS += app
CONFIG(debug, debug|release) {
    SUBDIRS += tests
}
