#ifndef OLADEVICEPROVIDER_H
#define OLADEVICEPROVIDER_H
#include "outputdevice.h"
#include <ola/DmxBuffer.h>
#include <ola/io/SelectServer.h>
#include <ola/Logging.h>
#include <ola/OlaClientWrapper.h>
#include <ola/Callback.h>
#include "filtervirtualdevicemanager.h"
#include "ungroupvirtualdevicemanager.h"
#include <QSet>

class OlaDeviceProvider : public OutputDevice
{

public:
    OlaDeviceProvider(VirtualDeviceManager *manager);
    ~OlaDeviceProvider();
    void publish(QMap<QString, QSharedPointer<DeviceState> > outDevices,QMap<QString, QSharedPointer<DeviceState> > changes);
    void startChannelPreview(int universe, int channel);
    void stopChannelPreview();
private:
    FilterVirtualDeviceManager filterDevManager;
    UngroupVirtualDeviceManager ungrouped;
    bool active;
    ola::client::OlaClientWrapper wrapper;
    bool inChannelPrevMode;
    QSet<int> usedUniverses;
    void blackoutAllUniverses();
};

#endif // OLADEVICEPROVIDER_H
