#include "diascene.h"
#include "websocketserver.h"
#include <QJsonArray>
#define KEY_DIAS "dias"
DiaScene::DiaScene(WebSocketServer *ws,
                   AudioProcessor *jackP, SceneBuilder *builderP, MonitorIO* monitorIoP, PlanedTriggerHandler *triggerHandlerP, QString name, QJsonObject serialized):
                Scene(name,serialized),WebSocketServerProvider(ws),
                SceneProvider(),
                current(-1),fadingTo(-1),fadeTimer(),fusion("fusion"),wss(ws),builder(builderP),
                jack(jackP), monitorIo(monitorIoP), sceneIdCountr(0), triggerHandler(triggerHandlerP)
{

    connect(jack,SIGNAL(musicNotification()),this,SLOT(music()));
    if(!serialized.isEmpty())
        loadSerialized(serialized);

    builder.setSceneProvider(this);
}


QMap<QString, QSharedPointer<DeviceState> > DiaScene::getDeviceState()
{
    fusion.reset();

    if(current != -1)
        fusion.import(scenes.value(current).data()->scene.data());
    if(fadingTo != -1){
        QSharedPointer<Dia> fadingToDia  =scenes.value(fadingTo);
        float percentage = fadeTimer.elapsed()/(fadingToDia.data()->fadeInDuration*1000);
        if(percentage> 1){
            percentage = 1;
            if(current != -1)
                scenes.value(current).data()->stop();
            current = fadingTo;
            fadingTo = -1;
        }
        fusion.fusion(fadingToDia.data()->scene,ChannelDeviceState::AV,percentage);
    }
    return fusion.getDeviceState();
}

void DiaScene::clientRegistered(QJsonObject, int id)
{
    sendMsg(getState(true),id,true);
}


void DiaScene::clientMessage(QJsonObject msg, int id)
{
    if(msg.contains("resetScene")){
        if(current != -1){
            scenes.value(current).data()->stop();
            scenes.value(current).data()->start();
        }
        if(fadingTo != -1){
            scenes.value(fadingTo).data()->stop();
            scenes.value(fadingTo).data()->start();
        }
        sendMsgButNotTo(msg,id,true);
    }
    if(msg.contains("nextScene")){
        next();
        sendMsg(getState(false),true);
    }
    if(msg.contains("currentScene")){
        int newId = msg.value("currentScene").toInt(-1);
        if(newId != -1 && order.contains(newId) && newId != current){
            fadeTimer.restart();
            if(fadingTo != -1)
                scenes.value(fadingTo).data()->stop();
            scenes.value(newId).data()->start();
            fadingTo = newId;
        }
        sendMsgButNotTo(getState(false),id,true);
    }
    if(msg.contains("prevScene")){
        prev();
        sendMsg(getState(false),true);
    }

    if(msg.contains("addScene")){
        QJsonObject addCmd = msg.value("addScene").toObject();
        QSharedPointer<Scene> newScene = builder.buildNew(addCmd.value("type").toString(),addCmd.value("name").toString());
        if(newScene.isNull()){
           qDebug("duscoScene: ERROR: unknown scene type");
           return;
        }
        addScene(newScene,addCmd.value("name").toString(),"",1.0);
    }
    if(msg.contains("deleteScene")){
        int id = msg.value("deleteScene").toInt(-1);
        if(current != -1 && current == id){
            scenes.value(id).data()->stop();
            current = fadingTo == -1 ? -1 : fadingTo;
            fadingTo = -1;//fading is not necessary any more
        }
        if(fadingTo != -1 && fadingTo == id){
            scenes.value(id).data()->stop();
            fadingTo = -1;
        }
        if(scenes.contains(id)){
            scenes.remove(id);
            order.removeAll(id);
        }
        emit scenesChanged();
        sendMsg(getState(true),true);
    }
    if(msg.contains("cloneScene")){
        int sceneId = msg.value("cloneScene").toInt(-1);
        addScene(QSharedPointer<Dia>(new Dia(scenes.value(sceneId).data()->serialize(&builder)
                                             ,&builder,wss,jack,monitorIo,triggerHandler)));
    }
    if(msg.contains("musicNotification")){
        setNextOnMusic(msg.value("musicNotification").toBool(false));
        sendMsgButNotTo(msg,id,true);
    }

    if(msg.contains("orderChanged")){
        QJsonArray readOrder = msg.value("orderChanged").toArray();
        if(readOrder.size() != order.size()){
            qDebug() << "ERROR: incomplete order!";
            return;
        }
        QList<int> newOrder;
        foreach (QJsonValue val, readOrder) {
            int i = val.toInt(-1);
            if(i == -1 || i >= order.size()){
                qDebug() << "ERROR: invalid item!";
                return;
            }
            newOrder.append(i);
        }

        order = newOrder;
        sendMsgButNotTo(msg,id,true);
    }
}

QString DiaScene::getRequestType()
{

    return "diaScene";
}

void DiaScene::stop()
{
    if(current != -1)
        scenes.value(current).data()->stop();
    if(fadingTo != -1)
        scenes.value(fadingTo).data()->stop();
}

void DiaScene::start()
{
    if(current != -1)
        scenes.value(current).data()->start();
    if(fadingTo != -1)
        scenes.value(fadingTo).data()->start();
    if(current == -1 && fadingTo == -1 && scenes.count() > 0){
        current = order.at(0);
        scenes.value(current).data()->start();
    }
}

QJsonObject DiaScene::serialize()
{
    QJsonObject serialized;

    QJsonArray dias;
    foreach(int index,order){
        QSharedPointer<Dia> dia = scenes.value(index);
        dias.append(dia.data()->serialize(&builder));
    }
    serialized.insert(KEY_DIAS,dias);
    return serializeScene(serialized);
}

QString DiaScene::getSceneTypeString()
{
    return getSceneTypeStringStaticaly();
}

QString DiaScene::getSceneTypeStringStaticaly()
{
    return "diaScene";
}

void DiaScene::addScene(QSharedPointer<Scene> scene,QString name,QString desc,float fadeInDuration)
{
    addScene(QSharedPointer<Dia>(new Dia(scene,name,desc,fadeInDuration,wss,jack,monitorIo, triggerHandler)));
    emit scenesChanged();
}

void DiaScene::addScene(QSharedPointer<Dia> dia)
{

    scenes.insert(sceneIdCountr,dia);
    order.append(sceneIdCountr);
    sceneIdCountr++;
    sendMsg(getState(true),true);
}

void DiaScene::loadSerialized(QJsonObject serialized)
{
    if(serialized.contains(KEY_DIAS))
    foreach (QJsonValue diaVal, serialized.value(KEY_DIAS).toArray()) {
        QJsonObject diaJson = diaVal.toObject();
        QSharedPointer<Dia> scene = QSharedPointer<Dia>(
                    new Dia(diaJson,&builder,wss,jack,monitorIo,triggerHandler));
        if(!(scene.isNull() || scene.data()->scene.isNull()))
            addScene(scene);
    }
    emit scenesInitialized();
}

void DiaScene::music()
{
    if(nextOnMusic){
        next();
        sendMsg(getState(false),true);
        nextOnMusic = false;
        QJsonObject msg;
        msg.insert("musicNotification",nextOnMusic);
        sendMsg(msg,true);
    }
}

QJsonObject DiaScene::getState(bool addScenes)
{
    QJsonObject msg;
    if(addScenes){
        QJsonArray dias;
        for (int i = 0; i < order.length();i++) {
            QSharedPointer<Dia> dia = scenes.value(order.at(i));
            QJsonObject diaObj;
            diaObj.insert("id",order.at(i));
            diaObj.insert("requestId",dia.data()->providerId);
            diaObj.insert("name",dia.data()->name);
            dias.append(diaObj);
        }
        msg.insert("scenes",dias);
    }
    if(current != -1)
        msg.insert("currentScene",(fadingTo == -1 ? current : fadingTo));
    return msg;
}

void DiaScene::next()
{
    int currentPos = order.indexOf(current);
    if(order.length() != 0 && currentPos < order.length() -1){
        fadeTimer.restart();
        if(fadingTo != -1)
            scenes.value(order.at(fadingTo)).data()->stop();
        scenes.value(order.at(currentPos+1)).data()->start();
        fadingTo = order.at(currentPos + 1);
    }
}

void DiaScene::prev()
{
    int currentPos = order.indexOf(current);
    if(order.length() != 0 && currentPos > 0){
        fadeTimer.restart();

        if(fadingTo != -1)
            scenes.value(order.at(fadingTo)).data()->stop();
        scenes.value(order.at(currentPos-1)).data()->start();
        fadingTo = order.at(currentPos - 1);
    }
}

void DiaScene::setNextOnMusic(bool enable)
{
    qDebug() << "mssic norif " << enable;
    nextOnMusic = enable;
    if(enable)
        jack->requestMusicNotification();
}

QList<QSharedPointer<Scene> > DiaScene::getScenes()
{
    return getSubScenes();
}

QList<QSharedPointer<Scene> > DiaScene::getSubScenes()
{
    QList<QSharedPointer<Scene> > ret;
    foreach(QSharedPointer<Dia> dia, scenes.values()){
        ret.append(dia.data()->scene);
    }

    return ret;
}
