#include "scene.h"
#include <math.h>
#include <stdio.h>
#include <time.h>
#include <QDebug>
#include <typeinfo>
#define KEY_NAME "sceneName"
#define KEY_DESC "sceneDesc"
#define KEY_SCENE_TYPE "sceneType"
#define KEY_SCENE_ID "id"


Scene::Scene(QString nameP, QJsonObject serialized) :
    QObject(),
    name(nameP)
{
    if(serialized.length() != 0){
        name = serialized.value(KEY_NAME).toString();
    }

    if(serialized.contains(KEY_SCENE_ID)){
        int i = serialized.value(KEY_SCENE_ID).toInt(-1);
        if(i == -1)
            id = getFreeId();
        else if(usedIds()->contains(i)){
            qDebug() << "WARINING: Scene id was allready in use! - Generated new one";
            id = getFreeId();
        }
        else{
            useId(i);
            id = i;
        }
    }
    else{
        id = getFreeId();
    }
}

Scene::Scene(Scene *clone):
    QObject(),
    name(clone->id),
    id(getFreeId())
{

}


QString Scene::getName()
{
    return name;
}

QList<QSharedPointer<Scene> > Scene::getSubScenes()
{
    return QList<QSharedPointer<Scene> >();
}

int Scene::getId()
{
    return id;
}

QSet<int> *Scene::usedIds()
{
    static QSet<int> used;
    return &used;
}

void Scene::setName(const QString &value)
{
    name = value;
}



int Scene::getFreeId()
{
    int i = 0;
    while(usedIds()->contains(i))
        i++;
    useId(i);
    return i;
}

void Scene::useId(int id)
{
    Scene::usedIds()->insert(id);
}


QJsonObject Scene::serializeScene(QJsonObject inherited)
{
    inherited.insert(KEY_NAME,name);
    inherited.insert(KEY_SCENE_ID,id);
    return  inherited;
}
