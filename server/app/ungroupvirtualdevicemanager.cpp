#include "ungroupvirtualdevicemanager.h"
#include "devices/groupdevice.h"

UngroupVirtualDeviceManager::UngroupVirtualDeviceManager(VirtualDeviceManager *parent) :
    parentVdMgr(parent)
{
    connect(parent,SIGNAL(virtualDevicesChanged()),this,SLOT(updateDevices()));
    updateDevices();
}

QHash<QString, QSharedPointer<Device> > UngroupVirtualDeviceManager::getDevices()
{
    return ungrouped;
}

void UngroupVirtualDeviceManager::updateDevices()
{
    ungrouped = parentVdMgr->getDevices();

    foreach (QSharedPointer<GroupDevice> gDev, parentVdMgr->getGroupDevices()) {
        ungroupGroup(gDev);
    }

    emit virtualDevicesChanged();
}

void UngroupVirtualDeviceManager::ungroupGroup(QSharedPointer<GroupDevice> group)
{
    QHash<QString, QSharedPointer<Device> > gDevs = group.data()->getChildren();
    foreach(QString gDevKey, gDevs.keys())
        ungrouped.insert( gDevKey,gDevs.value(gDevKey));
    foreach(QSharedPointer<GroupDevice> subGroup, group.data()->getChildGroups()){
        ungroupGroup(subGroup);
    }
}
