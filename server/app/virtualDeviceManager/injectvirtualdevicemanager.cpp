#include "injectvirtualdevicemanager.h"
#include <QDebug>
#include "../devices/groupdevice.h"

InjectVirtualDeviceManager::InjectVirtualDeviceManager(VirtualDeviceManager *previousMgr):
    blockRecursion(false)
{
    addInjektor(previousMgr);
    devicesChangedAtomic();
}

void InjectVirtualDeviceManager::addInjektor(VirtualDeviceManager *injektor)
{
    connect(injektor,SIGNAL(virtualDevicesChanged()),this,SLOT(devicesChanged()));
    injektors.append(injektor);
    devicesChangedAtomic();
}

void InjectVirtualDeviceManager::rmInjektor(VirtualDeviceManager *injektor)
{
    disconnect(injektor,SIGNAL(virtualDevicesChanged()),this,SLOT(devicesChanged()));
    injektors.removeAll(injektor);
    devicesChangedAtomic();
}

QHash<QString, QSharedPointer<Device> > InjectVirtualDeviceManager::getDevices()
{
    QHash<QString, QSharedPointer<Device> > ret;
    foreach(VirtualDeviceManager *injektor, injektors){
        foreach (QSharedPointer<Device> dev, injektor->getDevices().values()) {
            ret.insert(dev.data()->getDeviceId(),dev);
        }
    }
    return ret;
}

QHash<QString, QSharedPointer<GroupDevice> > InjectVirtualDeviceManager::getGroupDevices()
{
    QHash<QString, QSharedPointer<GroupDevice> > ret;
    foreach(VirtualDeviceManager *injektor, injektors){
        foreach (QSharedPointer<GroupDevice> dev, injektor->getGroupDevices().values()) {
            ret.insert(dev.data()->getGroupId(),dev);
        }
    }
    return ret;
}

void InjectVirtualDeviceManager::devicesChanged()
{
    devicesChangedAtomic();
}

void InjectVirtualDeviceManager::devicesChangedAtomic()
{
    if(blockRecursion)
        return;
    blockRecursion = true;

    emit virtualDevicesChanged();
    blockRecursion = false;
}
