#ifndef INJECTVIRTUALDEVICEMANAGER_H
#define INJECTVIRTUALDEVICEMANAGER_H

#include "virtualdevicemanager.h"


class InjectVirtualDeviceManager : public VirtualDeviceManager
{
Q_OBJECT

public:
    InjectVirtualDeviceManager(VirtualDeviceManager* previousMgr);

    void addInjektor(VirtualDeviceManager* injektor);
    void rmInjektor(VirtualDeviceManager* injektor);

private:

    QList<VirtualDeviceManager*> injektors;
    bool blockRecursion;

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();

public slots:
    void devicesChanged();
    void devicesChangedAtomic();
};

#endif // INJECTVIRTUALDEVICEMANAGER_H
