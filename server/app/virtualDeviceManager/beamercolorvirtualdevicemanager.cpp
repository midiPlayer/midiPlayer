#include "beamercolorvirtualdevicemanager.h"
#include "../devices/beamereffectgroupdevice.h"
#include "../scenes/beamerscene.h"


BeamerColorVirtualDeviceManager::BeamerColorVirtualDeviceManager(VirtualDeviceManager *previousDevMgrP,
        BeamerScene* sceneP):
    previousDevMgr(previousDevMgrP),
    beamerFilter(previousDevMgr, {Device::Beamer}),
    ungrouped(&beamerFilter),
    scene(sceneP),
    myGroups(),
    blockRekursion(false)
{
    connect(&ungrouped,SIGNAL(virtualDevicesChanged()),this,SLOT(reloadDevices()));
    reloadDevices();
}

QHash<QString, QSharedPointer<Device> > BeamerColorVirtualDeviceManager::getDevices()
{

    return QHash<QString, QSharedPointer<Device> >();
}

QHash<QString, QSharedPointer<GroupDevice> > BeamerColorVirtualDeviceManager::getGroupDevices()
{
    return myGroups;
}

void BeamerColorVirtualDeviceManager::reloadDevices()
{
    if(blockRekursion)
        return;

    myGroups.clear();

    QString devId = QString::number(scene->getId());
    QSharedPointer<BeamerEffectGroupDevice> newGroup(
                new BeamerEffectGroupDevice(devId,
                                            scene,
                                            ungrouped.getDevices()));
    connect(newGroup.data(),SIGNAL(colorsChanged()),this,SLOT(notifyAboutChange()));
    myGroups.insert(devId,newGroup);
    notifyAboutChange();
}

void BeamerColorVirtualDeviceManager::notifyAboutChange()
{
    if(blockRekursion)
        return;
    blockRekursion = true;
    emit virtualDevicesChanged();
    blockRekursion = false;
}
