#ifndef VIRTUALDEVICEMANAGER_H
#define VIRTUALDEVICEMANAGER_H
#include <QObject>
#include <QMap>
#include "../devices/device.h"
class GroupDevice;

class VirtualDeviceManager : public QObject
{
Q_OBJECT

public:
    VirtualDeviceManager();
    virtual QHash<QString,QSharedPointer<Device> > getDevices()  = 0;
    virtual QHash<QString,QSharedPointer<GroupDevice> > getGroupDevices();
signals:
    void virtualDevicesChanged();
private:
};

#endif // VIRTUALDEVICEMANAGER_H
