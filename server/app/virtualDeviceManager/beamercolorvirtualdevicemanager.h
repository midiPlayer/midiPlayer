#ifndef BEAMERCOLORVIRTUALDEVICEMANAGER_H
#define BEAMERCOLORVIRTUALDEVICEMANAGER_H

#include "virtualdevicemanager.h"
#include "../ungroupvirtualdevicemanager.h"
#include "../filtervirtualdevicemanager.h"
#include "../beamer/beamereffecthandler.h"

class BeamerScene;

class BeamerColorVirtualDeviceManager : public VirtualDeviceManager
{
Q_OBJECT

public:
    BeamerColorVirtualDeviceManager(VirtualDeviceManager* previousDevMgrP, BeamerScene *sceneP);

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();

public slots:
    void reloadDevices();
    void notifyAboutChange();

private:
    VirtualDeviceManager* previousDevMgr;
    FilterVirtualDeviceManager beamerFilter;
    UngroupVirtualDeviceManager ungrouped;
    BeamerScene* scene;
    QHash<QString, QSharedPointer<GroupDevice> > myGroups;
    bool blockRekursion;

};

#endif // BEAMERCOLORVIRTUALDEVICEMANAGER_H
