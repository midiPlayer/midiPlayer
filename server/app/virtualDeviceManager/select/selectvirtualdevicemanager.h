#ifndef SELECTVIRTUALDEVICEMANAGER_H
#define SELECTVIRTUALDEVICEMANAGER_H
#include "../virtualdevicemanager.h"
#include "../../serializable.h"
#include <QObject>
#include "../../websocketserverprovider.h"
#include "selector.h"
#include "manualselector.h"
#include "subgroupselector.h"
#include "doaftervirtualdeviceschangedmanager.h"

class SelectVirtualDeviceManager : public VirtualDeviceManager, public Serializable, public WebSocketServerProvider
{
Q_OBJECT

public:
    SelectVirtualDeviceManager(VirtualDeviceManager *input,WebSocketServer *ws,QJsonObject serialized);
    QHash<QString,QSharedPointer<Device> > getDevices();
    ~SelectVirtualDeviceManager();

    int id;

    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject, int){}
    QString getRequestType();

    QJsonObject serialize();
public slots:
    void norifyAboutChanges();

private:
    DoAfterVirtualDevicesChangedManager doAfter;
    ManualSelector manualSelector;
    SubGroupSelector subGroupSelector;

public:
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();
};

#endif // SELECTVIRTUALDEVICEMANAGER_H
