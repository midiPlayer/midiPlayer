#include "doaftervirtualdeviceschangedmanager.h"

DoAfterVirtualDevicesChangedManager::DoAfterVirtualDevicesChangedManager(VirtualDeviceManager *parentP):
    parent(parentP),
    emitting(false)
{
    connect(parent,SIGNAL(virtualDevicesChanged()),this,SLOT(notifyAboutChange()));
}

void DoAfterVirtualDevicesChangedManager::notifyAboutChange()
{
    emitting = true;
    emit virtualDevicesChanged();
    emitting = false;
    emit afterVirtualDevicesChanged();
}

bool DoAfterVirtualDevicesChangedManager::isEmitting() const
{
    return emitting;
}

QHash<QString, QSharedPointer<Device> > DoAfterVirtualDevicesChangedManager::getDevices()
{
    return parent->getDevices();
}

QHash<QString, QSharedPointer<GroupDevice> > DoAfterVirtualDevicesChangedManager::getGroupDevices()
{
    return parent->getGroupDevices();
}
