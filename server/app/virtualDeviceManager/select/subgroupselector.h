#ifndef SUBGROUPSELECTOR_H
#define SUBGROUPSELECTOR_H

#include "selector.h"
#include "groupselecotrwrapper.h"

#include <stdexcept>

class SubGroupSelector : public Selector, public WebSocketServerProvider
{
Q_OBJECT

public:
    SubGroupSelector(VirtualDeviceManager* devManagerP, WebSocketServer* ws, QJsonObject serialized);

public slots:
    void updateSelectors();
    void notifyAboutChanges();
    void sendSelektors(int clinetId = -1);

private:
    VirtualDeviceManager* devManager;
    WebSocketServer* wss;
    QMap<QString,QSharedPointer<GroupSelecotrWrapper> > groupSelectors;
    QJsonObject lastSerializedSelectors;
    bool updating;

    QSharedPointer<GroupSelecotrWrapper> createSelectorFor(QSharedPointer<GroupDevice> groupDev);

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int clientId);

    // Serializable interface
public:
    QJsonObject serialize();

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();

    // Selector interface
public:
    bool anySelected();

    // WebSocketServerProviderIdProvider interface
public:
    int getProviderId() const;

    // Selector interface
public:
    void toggle();
};

#endif // SUBGROUPSELECTOR_H
