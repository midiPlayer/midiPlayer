#include "selectvirtualdevicemanager.h"
#include <QJsonArray>
#include "../../websocketserver.h"




SelectVirtualDeviceManager::SelectVirtualDeviceManager(VirtualDeviceManager *input, WebSocketServer *ws, QJsonObject serialized) :
    VirtualDeviceManager(),
    Serializable(),
    WebSocketServerProvider(ws),
    doAfter(input),
    manualSelector(&doAfter,ws,serialized.value("manualSelector").toObject()),
    subGroupSelector(&doAfter,ws,serialized.value("subGroupSelector").toObject())
{
    connect(&manualSelector,SIGNAL(virtualDevicesChanged()),this,SLOT(norifyAboutChanges()));
    connect(&subGroupSelector,SIGNAL(virtualDevicesChanged()),this,SLOT(norifyAboutChanges()));
    connect(&doAfter,SIGNAL(afterVirtualDevicesChanged()),this,SLOT(norifyAboutChanges()));

    static int ic = 0;
    id = ic++;
}

SelectVirtualDeviceManager::~SelectVirtualDeviceManager()
{

}

QHash<QString, QSharedPointer<Device> > SelectVirtualDeviceManager::getDevices()
{
    QHash<QString, QSharedPointer<Device> > ret = manualSelector.getDevices();
    foreach(QSharedPointer<Device> dev, subGroupSelector.getDevices()){
        ret.insert(dev.data()->getDeviceId(),dev);
    }
    return ret;
}

QHash<QString, QSharedPointer<GroupDevice> > SelectVirtualDeviceManager::getGroupDevices()
{
    return subGroupSelector.getGroupDevices();
}


void SelectVirtualDeviceManager::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject wellcomMsg;
    wellcomMsg.insert("manual",manualSelector.getProviderId());
    wellcomMsg.insert("subGroup",subGroupSelector.getProviderId());
    sendMsg(wellcomMsg,clientId,true);
}


QString SelectVirtualDeviceManager::getRequestType()
{
    return "selectVirtualDeviceManager";
}


QJsonObject SelectVirtualDeviceManager::serialize()
{
    QJsonObject ret;
    QJsonObject manualSerialized = manualSelector.serialize();
    if(!manualSerialized.isEmpty())
        ret.insert("manualSelector",manualSerialized);
    QJsonObject subGroupSerialized = subGroupSelector.serialize();
    if(!subGroupSerialized.isEmpty())
        ret.insert("subGroupSelector",subGroupSerialized);
    return ret;
}

void SelectVirtualDeviceManager::norifyAboutChanges()
{
    if(doAfter.isEmitting())
        return;
    emit virtualDevicesChanged();
}
