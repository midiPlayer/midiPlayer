#include "groupdevice2vdevmgradapter.h"
#include "../../devices/groupdevice.h"

GroupDevice2VDevMgrAdapter::GroupDevice2VDevMgrAdapter(QSharedPointer<GroupDevice> groupDeviceP):
    groupDevice(groupDeviceP)
{
    emit virtualDevicesChanged();
}

QHash<QString, QSharedPointer<Device> > GroupDevice2VDevMgrAdapter::getDevices()
{
    return groupDevice.data()->getChildren();
}

QHash<QString, QSharedPointer<GroupDevice> > GroupDevice2VDevMgrAdapter::getGroupDevices()
{
    return groupDevice.data()->getChildGroups();
}

void GroupDevice2VDevMgrAdapter::setGroupDevice(const QSharedPointer<GroupDevice> &value)
{
    groupDevice = value;
    emit virtualDevicesChanged();
}
