#ifndef STRIPEGROUPSELECTOR_H
#define STRIPEGROUPSELECTOR_H
#include "selector.h"
#include "../../devices/stripegroupdevice.h"
#include "stripeledgroupselector.h"
#include "regroupselektor.h"

#define SELEKTOR_TYPE_STRIPE "stripe"

class StripeGroupSelector : public Selector, public WebSocketServerProvider
{
Q_OBJECT

public:
    StripeGroupSelector(WebSocketServer *ws, VirtualDeviceManager *devManager, QJsonObject serialized = QJsonObject());

private:
    StripeLedGroupSelector ledSelektor;
    RegroupSelektor ungroupSelektor;

    // Serializable interface
public:
    QJsonObject serialize();

    // WebSocketServerProvider interface
public:
    QString getRequestType();
    void clientRegistered(QJsonObject, int clientIdCounter);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject, int){}

public slots:
    void notifyAboutChange();

    // Selector interface
public:
    bool anySelected();

    // WebSocketServerProviderIdProvider interface
public:
    int getProviderId() const;

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();

    // Selector interface
public:
    void toggle();
};

#endif // STRIPEGROUPSELECTOR_H
