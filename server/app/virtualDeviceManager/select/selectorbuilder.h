#ifndef SELECTORBUILDER_H
#define SELECTORBUILDER_H

#include <QSharedPointer>
#include "selector.h"

class VirtualDeviceManager;

class SelectorBuilder
{
public:
    SelectorBuilder();
    virtual ~SelectorBuilder(){}
    virtual QSharedPointer<Selector> buildSelector(VirtualDeviceManager* parentDevMgr, WebSocketServer* ws, QJsonObject serialized) = 0;
};

#endif // SELECTORBUILDER_H
