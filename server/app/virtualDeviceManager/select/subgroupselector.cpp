#include "subgroupselector.h"
#include "../../devices/groupdevice.h"

#include <QJsonArray>

SubGroupSelector::SubGroupSelector(VirtualDeviceManager *devManagerP, WebSocketServer *ws, QJsonObject serialized):
    Selector(),
    WebSocketServerProvider(ws),
    devManager(devManagerP),
    wss(ws),
    lastSerializedSelectors(serialized.value("selectors").toObject()),
    updating(false)
{
    connect(devManager,SIGNAL(virtualDevicesChanged()),this,SLOT(updateSelectors()));
    updateSelectors();
}

void SubGroupSelector::updateSelectors()
{
    updating = true;
    QHash<QString, QSharedPointer<GroupDevice> > groups = devManager->getGroupDevices();
    bool changed = false;

    //create / update selektors
    foreach (QSharedPointer<GroupDevice> groupDev, groups.values()) {
         if(groupSelectors.contains(groupDev.data()->getGroupId())){
            groupSelectors.value(groupDev.data()->getGroupId()).data()->updateGroup(groupDev);
         }else{ //create new selector
            QSharedPointer<GroupSelecotrWrapper> selektor = createSelectorFor(groupDev);
            connect(selektor.data(),SIGNAL(virtualDevicesChanged()),this,SLOT(sendSelektors()));
            groupSelectors.insert(groupDev.data()->getGroupId(),selektor);
            changed = true;
         }
    }

    //delete unused selektors
    foreach(QString groupId, groupSelectors.keys()){
        if(!groups.contains(groupId)){
            disconnect(groupSelectors.value(groupId).data(),SIGNAL(virtualDevicesChanged()),this,SLOT(sendSelektors()));
            groupSelectors.remove(groupId);
            changed = true;
        }
    }

    updating = false;

    notifyAboutChanges();
    if(changed)
        sendSelektors();
}

void SubGroupSelector::notifyAboutChanges()
{
    emit virtualDevicesChanged();
}

void SubGroupSelector::sendSelektors(int clinetId)
{
    if(updating)
        return;

    if(!isSomeoneListening())
      return;


    QJsonArray devs;
    foreach(QSharedPointer<GroupDevice> group, devManager->getGroupDevices()){
        if(group.data()->isEmpty())
            continue;
        if(!groupSelectors.contains(group.data()->getGroupId())){
            qDebug() << "no selector for " << group.data()->getGroupId();
            continue;
        }
        QJsonObject devJson;
        devJson.insert("devId",group.data()->getGroupId());
        devJson.insert("name",group.data()->getDisplayName());
        devJson.insert("type","group-" + group.data()->getGroupType());
        devJson.insert("selected",groupSelectors.value(group.data()->getGroupId()).data()->anySelected());
        devs.append(devJson);
    }

    QJsonObject ret;
    ret.insert("devices",devs);
    if(clinetId != -1)
        sendMsg(ret,clinetId,true);
    else
        sendMsg(ret,true);
}

QSharedPointer<GroupSelecotrWrapper> SubGroupSelector::createSelectorFor(QSharedPointer<GroupDevice> groupDev)
{
    QJsonObject serializedSelector;
    if(lastSerializedSelectors.contains(groupDev.data()->getGroupId()))
        serializedSelector = lastSerializedSelectors.value(groupDev.data()->getGroupId()).toObject();

    QSharedPointer<GroupSelecotrWrapper> ret(new GroupSelecotrWrapper(wss,groupDev,serializedSelector));
    connect(ret.data(),SIGNAL(virtualDevicesChanged()),this,SLOT(notifyAboutChanges()));
    return ret;
}

QString SubGroupSelector::getRequestType()
{
    return "SubGroupSelector";
}

void SubGroupSelector::clientRegistered(QJsonObject, int clientId)
{
    sendSelektors(clientId);
}

void SubGroupSelector::clientMessage(QJsonObject msg, int clientId)
{
    if(msg.contains("getGroupSelektor")){
        QString groupId = msg.value("getGroupSelektor").toString();
        if(groupSelectors.contains(groupId)){
            QJsonObject ret;

            QJsonObject groupSelectorJson;
            groupSelectorJson.insert("id",groupSelectors.value(groupId).data()->getProviderId());
            groupSelectorJson.insert("type",groupSelectors.value(groupId).data()->getRequestType());
            ret.insert("groupSelektor",groupSelectorJson);
            sendMsg(ret, clientId,true);
        }
    }
    if(msg.contains("toggle")){
        QString groupId = msg.value("toggle").toString();
        if(groupSelectors.contains(groupId)){
            groupSelectors.value(groupId).data()->toggle();
        }
    }
}

QJsonObject SubGroupSelector::serialize()
{
    QJsonObject selectorsJson;
    foreach (QString groupId, groupSelectors.keys()) {
        QJsonObject serializedSelector = groupSelectors.value(groupId).data()->serialize();
        if(!serializedSelector.isEmpty())
            selectorsJson.insert(groupId,serializedSelector);
    }

    QJsonObject serialized;
    if(!selectorsJson.isEmpty())
        serialized.insert("selectors",selectorsJson);
    return serialized;
}

QHash<QString, QSharedPointer<Device> > SubGroupSelector::getDevices()
{
    QHash<QString, QSharedPointer<Device> > ret;
    foreach(QSharedPointer<GroupSelecotrWrapper> groupSelector, groupSelectors.values()){
        foreach(QSharedPointer<Device> dev, groupSelector.data()->getDevices()){
            ret.insert(dev.data()->getDeviceId(),dev);
        }
    }
    return ret;
}

QHash<QString, QSharedPointer<GroupDevice> > SubGroupSelector::getGroupDevices()
{
    QHash<QString, QSharedPointer<GroupDevice> > ret;
    foreach(QSharedPointer<GroupSelecotrWrapper> groupSelector, groupSelectors.values()){
        foreach(QSharedPointer<GroupDevice> groupDev, groupSelector.data()->getGroupDevices()){
            ret.insert(groupDev.data()->getGroupId(),groupDev);
        }
    }
    return ret;
}

bool SubGroupSelector::anySelected()
{
    foreach (QSharedPointer<GroupSelecotrWrapper> selector, groupSelectors) {
        if(selector.data()->anySelected())
            return true;
    }
    return false;
}

int SubGroupSelector::getProviderId() const
{
    return providerId;
}

void SubGroupSelector::toggle()
{
    //we can't toggle
}
