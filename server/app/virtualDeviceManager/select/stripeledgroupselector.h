#ifndef STRIPELEDGROUPSELECTOR_H
#define STRIPELEDGROUPSELECTOR_H
#include "selector.h"
#include "../../RemoteTypes/remotenumber.h"
#include "../../RemoteTypes/remotebool.h"
#include "manualselector.h"

class StripeLedGroupSelector : public Selector, public WebSocketServerProvider
{
Q_OBJECT

public:
    StripeLedGroupSelector(WebSocketServer *ws, VirtualDeviceManager *devMgrP, QJsonObject serialized = QJsonObject());

public slots:
    void filter();
    void notifyAboutChanged();

private:
    VirtualDeviceManager* devMgr;
    ManualSelector manualSelektor;
    RemoteNumber from;
    RemoteNumber to;
    RemoteNumber each;
    RemoteNumber eachOffset;

    RemoteBool fromTo;
    RemoteBool useEach;
    RemoteBool selektManual;

    QHash<QString, QSharedPointer<Device> > filteredDevices;
    QHash<QString, QSharedPointer<GroupDevice> > filteredGroups;


    // Serializable interface
public:
    QJsonObject serialize();

    // WebSocketServerProvider interface
public:
    QString getRequestType();
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject, int) {}

    // WebSocketServerProviderIdProvider interface
public:
    int getProviderId() const;

    // Selector interface
public:
    bool anySelected();

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();
};

#endif // STRIPELEDGROUPSELECTOR_H
