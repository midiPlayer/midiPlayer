#include "groupselecotrwrapper.h"
#include "../../devices/groupdevice.h"

GroupSelecotrWrapper::GroupSelecotrWrapper(WebSocketServer* ws,
                                           QSharedPointer<GroupDevice> groupDevice,
                                           QJsonObject serialized):
    Selector(),
    g2vdevMgrAdapter(groupDevice),
    selector(groupDevice.data()->getSelectorBuilder().data()->buildSelector(&g2vdevMgrAdapter,ws,serialized.value("selector").toObject()))
{
    connect(selector.data(),SIGNAL(virtualDevicesChanged()),this,SLOT(notifyAboutchanges()));
}

void GroupSelecotrWrapper::toggle()
{
    selector.data()->toggle();
}

void GroupSelecotrWrapper::notifyAboutchanges()
{
    emit virtualDevicesChanged();
}

void GroupSelecotrWrapper::updateGroup(QSharedPointer<GroupDevice> newGroup)
{
    g2vdevMgrAdapter.setGroupDevice(newGroup);
}


bool GroupSelecotrWrapper::anySelected()
{
    return selector.data()->anySelected();
}

QHash<QString, QSharedPointer<Device> > GroupSelecotrWrapper::getDevices()
{
    return selector.data()->getDevices();
}

QHash<QString, QSharedPointer<GroupDevice> > GroupSelecotrWrapper::getGroupDevices()
{
    return selector.data()->getGroupDevices();
}

int GroupSelecotrWrapper::getProviderId() const
{
    return selector.data()->getProviderId();
}

QJsonObject GroupSelecotrWrapper::serialize()
{
    QJsonObject serialized;
    QJsonObject serializedSelektor = selector.data()->serialize();
    if(!serializedSelektor.isEmpty())
        serialized.insert("selector",serializedSelektor);
    return serialized;
}

QString GroupSelecotrWrapper::getRequestType()
{
    return selector.data()->getRequestType();
}


