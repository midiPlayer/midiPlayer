#include "regroupselektor.h"
#include "../../devices/regroupgroupdevice.h"
//regroups devices so they are handled as one

#define DISSABLED_DEFAULT true
#define SINGLE_DEVICE_DEFUALT false

RegroupSelektor::RegroupSelektor(WebSocketServer *ws, VirtualDeviceManager* parentDevMgr, QJsonObject serialized):
    Selector(),
    WebSocketServerProvider(ws),
    devMgr(parentDevMgr),
    disabled(ws,this, serialized.value("disabled").toBool(DISSABLED_DEFAULT)),
    singleDevice(ws,this,serialized.value("singleDevice").toBool(SINGLE_DEVICE_DEFUALT))
{
    connect(devMgr,SIGNAL(virtualDevicesChanged()),this,SLOT(valueChanged()));
    connect(&disabled,SIGNAL(valueChanged()),this,SLOT(valueChanged()));
    connect(&singleDevice,SIGNAL(valueChanged()),this,SLOT(valueChanged()));
}

void RegroupSelektor::valueChanged()
{
    emit virtualDevicesChanged();
}

QJsonObject RegroupSelektor::serialize()
{
    QJsonObject ret = Selector::serialize();
    if(disabled.getBoolValue() != DISSABLED_DEFAULT)
        ret.insert("disabled",disabled.getBoolValue());
    if(singleDevice.getBoolValue() != SINGLE_DEVICE_DEFUALT)
        ret.insert("singleDevice",singleDevice.getBoolValue());
    return ret;
}

bool RegroupSelektor::isDissabled()
{
    return disabled.getBoolValue();
}

void RegroupSelektor::dissable()
{
    disabled.setBoolValue(true);
    emit valueChanged();
}

void RegroupSelektor::enableUngrouped()
{
    disabled.setBoolValue(false);
    singleDevice.setBoolValue(false);
    emit valueChanged();
}

QString RegroupSelektor::getRequestType()
{
    return "ungroupSelektor";
}

void RegroupSelektor::clientRegistered(QJsonObject, int clientIdCounter)
{
    QJsonObject hello;
    hello.insert("disabled",disabled.providerId);
    hello.insert("singleDevice",singleDevice.providerId);
    sendMsg(hello,clientIdCounter,true);
}

QHash<QString, QSharedPointer<Device> > RegroupSelektor::getDevices()
{
    if(disabled.getBoolValue())
        return QHash<QString, QSharedPointer<Device> >();

    if(!singleDevice.getBoolValue()){
        return devMgr->getDevices(); //not as a single Device --> don't group --> poassthrough
    }

    return QHash<QString, QSharedPointer<Device> >(); //devices will be grouped; return empty
}

QHash<QString, QSharedPointer<GroupDevice> > RegroupSelektor::getGroupDevices()
{
    if(disabled.getBoolValue())
        return QHash<QString, QSharedPointer<GroupDevice> >();

    if(!singleDevice.getBoolValue()){//not as a single device --> don't group
        return devMgr->getGroupDevices();
    }

    //regroup
    QSharedPointer<RegroupGroupDevice> newGroup(new RegroupGroupDevice(devMgr->getDevices(),devMgr->getGroupDevices()));
    QHash<QString, QSharedPointer<GroupDevice> > ret;
    ret.insert(newGroup.data()->getGroupId(),newGroup);
    return ret;
}

bool RegroupSelektor::anySelected()
{
    return !disabled.getBoolValue();
}

int RegroupSelektor::getProviderId() const
{
    return providerId;
}
