#ifndef MANUALSELECTOR_H
#define MANUALSELECTOR_H

#include "../../websocketserverprovider.h"
#include "selector.h"

class ManualSelector : public Selector, public WebSocketServerProvider
{
Q_OBJECT

public:
    ManualSelector(VirtualDeviceManager *parentManagerP, WebSocketServer *ws, QJsonObject serialized);

private:
    QStringList filter;
    VirtualDeviceManager *parentManager;
    QHash<QString, QSharedPointer<Device> > filtered;
    QJsonObject generateDevicesMaessage();

public slots:
    void filterDevices();

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int clientId);
    QStringList getCleanedFilter();

    // Serializable interface
public:
    QJsonObject serialize();

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();

    // Selector interface
public:
    bool anySelected();

    // WebSocketServerProviderIdProvider interface
public:
    int getProviderId() const;

    // Selector interface
public:
    void toggle();
};

#endif // MANUALSELECTOR_H
