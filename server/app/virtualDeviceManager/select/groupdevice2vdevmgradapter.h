#ifndef GROUPDEVICE2VDEVMGRADAPTER_H
#define GROUPDEVICE2VDEVMGRADAPTER_H

/*
 * This class adapts a group device to virtualdevice manager
 * */

#include "../virtualdevicemanager.h"

class GroupDevice2VDevMgrAdapter : public VirtualDeviceManager
{
public:
    GroupDevice2VDevMgrAdapter(QSharedPointer <GroupDevice> groupDeviceP);

private:
    QSharedPointer <GroupDevice> groupDevice;

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();
    void setGroupDevice(const QSharedPointer<GroupDevice> &value);
};

#endif // GROUPDEVICE2VDEVMGRADAPTER_H
