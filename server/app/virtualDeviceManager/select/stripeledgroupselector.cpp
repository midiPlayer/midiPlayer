#include "stripeledgroupselector.h"
#include "../../filteredgroupdevice.h"


/*
 * selects which leds of the stripe to use
 * useses a the manual selector
 * */

#define KEY_MANUAL_SELEKTOR "manualSelect"
#define FROM_DEFUALT 0
#define TO_DEFAULT 0
#define EACH_DEFAULT 2
#define EACH_OFFSET_DEFAULT 0
#define FROM_TO_DEFAULT false
#define USE_EACH_DEFAULT false
#define SELECT_MANUAL_DEFAULT false

StripeLedGroupSelector::StripeLedGroupSelector(WebSocketServer *ws, VirtualDeviceManager* devMgrP, QJsonObject serialized):
    Selector(),
    WebSocketServerProvider(ws),
    devMgr(devMgrP),
    manualSelektor(devMgr, ws,serialized.value(KEY_MANUAL_SELEKTOR).toObject()),
    from(ws,this, serialized.value("from").toInt(FROM_DEFUALT)),
    to(ws,this, serialized.value("to").toInt(TO_DEFAULT)),
    each(ws,this,serialized.value("each").toInt(EACH_DEFAULT)),
    eachOffset(ws,this,serialized.value("eachOffset").toInt(EACH_OFFSET_DEFAULT)),
    fromTo(ws,this,serialized.value("fromTo").toBool(FROM_TO_DEFAULT)),
    useEach(ws,this,serialized.value("useEach").toBool(USE_EACH_DEFAULT)),
    selektManual(ws,this,serialized.value("selektManual").toBool(SELECT_MANUAL_DEFAULT))
{
    connect(&manualSelektor,SIGNAL(virtualDevicesChanged()),this,SLOT(notifyAboutChanged()));

    connect(&selektManual,SIGNAL(valueChanged()),this,SLOT(notifyAboutChanged()));
    connect(&from,SIGNAL(valueChanged()),this,SLOT(filter()));
    connect(&to,SIGNAL(valueChanged()),this,SLOT(filter()));
    connect(&each,SIGNAL(valueChanged()),this,SLOT(filter()));
    connect(&eachOffset,SIGNAL(valueChanged()),this,SLOT(filter()));
    connect(&fromTo,SIGNAL(valueChanged()),this,SLOT(filter()));
    connect(&useEach,SIGNAL(valueChanged()),this,SLOT(filter()));

    connect(devMgr,SIGNAL(virtualDevicesChanged()),this,SLOT(filter()));

    filter();
}

void StripeLedGroupSelector::filter()
{
    filteredDevices.clear();
    filteredGroups.clear();

    int i = -1;
    foreach(QSharedPointer<Device> dev, devMgr->getDevices().values()){
        i++;
        if(fromTo.getBoolValue() &&
                !((from.getNumber() < i && i < to.getNumber())
                  || (to.getNumber() < i && i < from.getNumber())))
            continue;
        if(useEach.getBoolValue()
                && i % each.getNumber() != eachOffset.getNumber())
            continue;
        filteredDevices.insert(dev.data()->getDeviceId(),dev);
    }

    foreach(QSharedPointer<GroupDevice> group, devMgr->getGroupDevices().values()){
        i++;
        if(fromTo.getBoolValue() &&
                !((from.getNumber() < i && i < to.getNumber())
                  || (to.getNumber() < i && i < from.getNumber())))
            continue;
        if(useEach.getBoolValue()
                && i % each.getNumber() != eachOffset.getNumber())
            continue;
        filteredGroups.insert(group.data()->getGroupId(),group);
    }
    notifyAboutChanged();
}

void StripeLedGroupSelector::notifyAboutChanged()
{
    emit virtualDevicesChanged();
}

QJsonObject StripeLedGroupSelector::serialize()
{
    QJsonObject ret = Selector::serialize();
    QJsonObject manSerialized = manualSelektor.serialize();
    if(!manSerialized.isEmpty() && selektManual.getBoolValue())
        ret.insert(KEY_MANUAL_SELEKTOR,manSerialized);

    if(from.getNumber() != FROM_DEFUALT)
        ret.insert("from",from.getNumber());
    if(to.getNumber() != TO_DEFAULT)
        ret.insert("to",to.getNumber());
    if(each.getNumber() != EACH_DEFAULT)
        ret.insert("each",each.getNumber());
    if(eachOffset.getNumber() != EACH_OFFSET_DEFAULT)
        ret.insert("eachOffset",eachOffset.getNumber());
    if(fromTo.getBoolValue() != FROM_TO_DEFAULT)
        ret.insert("fromTo",fromTo.getBoolValue());
    if(useEach.getBoolValue() != USE_EACH_DEFAULT)
        ret.insert("useEach",useEach.getBoolValue());
    if(selektManual.getBoolValue() != SELECT_MANUAL_DEFAULT)
        ret.insert("selektManual",selektManual.getBoolValue());
    return ret;
}

QString StripeLedGroupSelector::getRequestType()
{
    return "LedSelektor";
}

void StripeLedGroupSelector::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject hello;
    hello.insert("manualSelektor",manualSelektor.providerId);
    hello.insert("from",from.providerId);
    hello.insert("to",to.providerId);
    hello.insert("each",each.providerId);
    hello.insert("eachOffset",eachOffset.providerId);
    hello.insert("fromTo",fromTo.providerId);
    hello.insert("useEach",useEach.providerId);
    hello.insert("selektManual",selektManual.providerId);
    sendMsg(hello,clientId,true);
}


bool StripeLedGroupSelector::anySelected()
{
    return true;
}

QHash<QString, QSharedPointer<Device> > StripeLedGroupSelector::getDevices()
{
    if(selektManual.getBoolValue())
        return manualSelektor.getDevices();
    return filteredDevices;
}

QHash<QString, QSharedPointer<GroupDevice> > StripeLedGroupSelector::getGroupDevices()
{
    if(selektManual.getBoolValue())
        return manualSelektor.getGroupDevices();
    return filteredGroups;
}

int StripeLedGroupSelector::getProviderId() const
{
    return providerId;
}
