/*
 * Provides as simple filter for ungrouped devices;
 * Groups can not be selected / are ignored
 * */


#include "manualselector.h"

#include <QJsonArray>

#define KEY_FILTER "filter"

ManualSelector::ManualSelector(VirtualDeviceManager *parentManagerP, WebSocketServer *ws, QJsonObject serialized):
    Selector(),
    WebSocketServerProvider(ws),
    filter(),
    parentManager(parentManagerP),
    filtered()
{
    connect(parentManager,SIGNAL(virtualDevicesChanged()),this,SLOT(filterDevices()));

    foreach(QJsonValue dev, serialized.value(KEY_FILTER).toArray()){
        filter.append(dev.toString());
    }
    filterDevices();
}

QJsonObject ManualSelector::generateDevicesMaessage()
{
    QJsonObject ret;
    QJsonArray devs;
    foreach(QSharedPointer<Device> dev, parentManager->getDevices().values()){
        QJsonObject devJson;
        devJson.insert("devId",dev.data()->getDeviceId());
        devJson.insert("name",dev.data()->getDisplayName());
        devJson.insert("type",dev.data()->getTypeString());
        devJson.insert("selected",filter.contains(dev.data()->getDeviceId()));
        devs.append(devJson);
    }
    ret.insert("devices",devs);

    return ret;
}

void ManualSelector::filterDevices()
{
    filtered.clear();
    foreach(QSharedPointer<Device> dev, parentManager->getDevices()){
        if(filter.contains(dev.data()->getDeviceId()))
            filtered.insert(dev.data()->getDeviceId(),dev);
    }
    emit virtualDevicesChanged();
}

QString ManualSelector::getRequestType()
{
    return "manualSelector";
}

void ManualSelector::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject ret = generateDevicesMaessage();
    sendMsg(ret,clientId,true);
}

void ManualSelector::clientMessage(QJsonObject msg, int clientId)
{
    if(msg.contains("requestUpdate")){
        QJsonObject ret = generateDevicesMaessage();
        sendMsg(ret,true);
        return;
    }
    if(msg.contains("addAccepted")){
        QString devId = msg.value("addAccepted").toString();
        if(parentManager->getDevices().contains(devId) && !filter.contains(devId))
            filter.append(devId);
        sendMsgButNotTo(msg,clientId,true);
    }
    if(msg.contains("rmAccepted")){
        QString devId = msg.value("rmAccepted").toString();
        filter.removeAll(devId);
    }
    sendMsgButNotTo(msg,clientId,true);
    filterDevices();
}

/*
 * cleans up the filter from deleted devices.
 * */
QStringList ManualSelector::getCleanedFilter()
{
    QList<QString> existing = parentManager->getDevices().keys();
    QStringList cleaned;
    foreach(QString devId, filter){
        if(existing.contains(devId))
            cleaned.append(devId);
    }
    return cleaned;
}

QJsonObject ManualSelector::serialize()
{
    QJsonObject ret = Selector::serialize();
    QJsonArray cleandFilter = QJsonArray::fromStringList(getCleanedFilter());
    if(!cleandFilter.empty())
        ret.insert(KEY_FILTER, cleandFilter);
    return ret;
}

QHash<QString, QSharedPointer<Device> > ManualSelector::getDevices()
{
    return filtered;
}

bool ManualSelector::anySelected()
{
    return getCleanedFilter().length() != 0;
}

int ManualSelector::getProviderId() const
{
    return providerId;
}

void ManualSelector::toggle()
{
    //we can't toggle
}
