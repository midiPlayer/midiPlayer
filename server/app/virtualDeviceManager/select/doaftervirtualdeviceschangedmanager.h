#ifndef DOAFTERVIRTUALDEVICESCHANGEDMANAGER_H
#define DOAFTERVIRTUALDEVICESCHANGEDMANAGER_H

#include "../virtualdevicemanager.h"

class DoAfterVirtualDevicesChangedManager : public VirtualDeviceManager
{
Q_OBJECT

public:
    DoAfterVirtualDevicesChangedManager(VirtualDeviceManager* parentP);

signals:
    void afterVirtualDevicesChanged();
public slots:
    void notifyAboutChange();

private:
    VirtualDeviceManager* parent;
    bool emitting;

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();
    bool isEmitting() const;
};

#endif // DOAFTERVIRTUALDEVICESCHANGEDMANAGER_H
