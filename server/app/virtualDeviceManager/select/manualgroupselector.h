#ifndef MANUALGROUPSELECTOR_H
#define MANUALGROUPSELECTOR_H
#include "selector.h"

#define SELEKTOR_TYPE_MANUAL "manual"

class ManualGroupSelector: public Selector
{
public:
    ManualGroupSelector(WebSocketServer* ws, QSharedPointer<GroupDevice> group, QJsonObject serialized = QJsonObject());

private:
    QSharedPointer<GroupDevice> myGroup;
    QList<QString> filter;

    // Serializable interface
public:
    QJsonObject serialize();

    // GroupSelector interface
    QJsonObject generateDevicesMaessage();

public:
    QMap<QString, QSharedPointer<GroupDevice> > getFilteredGroup();
    QMap<QString, QSharedPointer<Device> > getFilteredDevices();
    void updateGroup(QSharedPointer<GroupDevice> newGroup);
    QString getType();

    // GroupSelector interface
public:
    bool anySelected();
};

#endif // MANUALGROUPSELECTOR_H
