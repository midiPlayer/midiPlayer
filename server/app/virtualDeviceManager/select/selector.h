#ifndef SELECTOR_H
#define SELECTOR_H
#include "../../websocketserverprovideridprovider.h"
#include "../../virtualDeviceManager/virtualdevicemanager.h"
class GroupDevice;

class Selector : public VirtualDeviceManager, public Serializable, public WebSocketServerProviderIdProvider
{
Q_OBJECT

public:
    Selector();
    virtual ~Selector(){}
    virtual bool anySelected() = 0;
    virtual void toggle() {}

    // Serializable interface
public:
    QJsonObject serialize();
};

#endif // SELECTOR_H
