#ifndef GROUPSELECOTRWRAPPER_H
#define GROUPSELECOTRWRAPPER_H

#include "selector.h"
#include "selectorbuilder.h"
#include "groupdevice2vdevmgradapter.h"

class GroupSelecotrWrapper : public Selector
{
Q_OBJECT

public:
    GroupSelecotrWrapper(WebSocketServer* ws,
                         QSharedPointer<GroupDevice> groupDevice, QJsonObject serialized);

    void toggle();
public slots:
    void notifyAboutchanges();

private:
    GroupDevice2VDevMgrAdapter g2vdevMgrAdapter;
    QSharedPointer<Selector> selector;


    // Selector interface
public:
    void updateGroup(QSharedPointer<GroupDevice> newGroup);
    bool anySelected();

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();

    // WebSocketServerProviderIdProvider interface
public:
    int getProviderId() const;

    // Serializable interface
public:
    QJsonObject serialize();

    // WebSocketServerProviderIdProvider interface
public:
    QString getRequestType();
};

#endif // GROUPSELECOTRWRAPPER_H
