#ifndef REGROUPSELEKTOR_H
#define REGROUPSELEKTOR_H

#include "selector.h"
#include "../../RemoteTypes/remotenumber.h"
#include "../../RemoteTypes/remotebool.h"

#define SELEKTOR_TYPE_UNGROUP "ungroup"

class RegroupSelektor : public Selector, public WebSocketServerProvider
{
Q_OBJECT

public:
    RegroupSelektor(WebSocketServer *ws, VirtualDeviceManager *parentDevMgr, QJsonObject serialized = QJsonObject());

public slots:
    void valueChanged();

private:
    VirtualDeviceManager* devMgr;
    RemoteBool disabled;
    RemoteBool singleDevice;

    // Serializable interface
public:
    QJsonObject serialize();
    bool isDissabled();
    void dissable();
    void enableUngrouped();

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientIdCounter);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject, int) {}

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();

    // Selector interface
public:
    bool anySelected();

    // WebSocketServerProviderIdProvider interface
public:
    int getProviderId() const;
};

#endif // REGROUPSELEKTOR_H
