#include "stripegroupselector.h"
#include "../../filteredgroupdevice.h"

/**
  * Combines LED-Group-Selector and Ungroup Selector in order to filter Arrays of devices like LED stripes
  * */

#define KEY_LED_SELEKTOR "ledSelektor"
#define KEY_UNGROUP_SELEKOTR "ungroupSelektor"

StripeGroupSelector::StripeGroupSelector(WebSocketServer *ws, VirtualDeviceManager* devManager, QJsonObject serialized):
    Selector(),
    WebSocketServerProvider(ws),
    ledSelektor(ws,devManager,serialized.value(KEY_LED_SELEKTOR).toObject()),
    ungroupSelektor(ws,&ledSelektor,serialized.value(KEY_UNGROUP_SELEKOTR).toObject())
{
    connect(&ungroupSelektor,SIGNAL(virtualDevicesChanged()),this,SLOT(notifyAboutChange()));
    notifyAboutChange();
}

QJsonObject StripeGroupSelector::serialize()
{
    QJsonObject ret = Selector::serialize();
    QJsonObject ledSerialized = ledSelektor.serialize();
    if(!ledSerialized.isEmpty())
        ret.insert(KEY_LED_SELEKTOR,ledSerialized);
    QJsonObject unggroupSerialized = ungroupSelektor.serialize();
    if(!unggroupSerialized.isEmpty())
        ret.insert(KEY_UNGROUP_SELEKOTR,unggroupSerialized);
    return ret;
}

QString StripeGroupSelector::getRequestType()
{
    return "stripeSelector";
}


void StripeGroupSelector::notifyAboutChange()
{
    emit virtualDevicesChanged();
}

void StripeGroupSelector::clientRegistered(QJsonObject, int clientIdCounter)
{
    QJsonObject hello;
    hello.insert(KEY_LED_SELEKTOR,ledSelektor.providerId);
    hello.insert(KEY_UNGROUP_SELEKOTR,ungroupSelektor.providerId);
    sendMsg(hello,clientIdCounter,true);
}

int StripeGroupSelector::getProviderId() const
{
    return providerId;
}

QHash<QString, QSharedPointer<Device> > StripeGroupSelector::getDevices()
{
    return ungroupSelektor.getDevices();
}

QHash<QString, QSharedPointer<GroupDevice> > StripeGroupSelector::getGroupDevices()
{
    return ungroupSelektor.getGroupDevices();
}

void StripeGroupSelector::toggle()
{
    if(ungroupSelektor.isDissabled())
        ungroupSelektor.enableUngrouped();
    else
        ungroupSelektor.dissable();
}

bool StripeGroupSelector::anySelected()
{
    return ledSelektor.anySelected() && ungroupSelektor.anySelected();
}
