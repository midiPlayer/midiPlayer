#ifndef OSCCONNECTOR_H
#define OSCCONNECTOR_H

#include "oscconnectordeck.h"
#include <QMap>

namespace lo {
    class ServerThread;
}

class OscConnector
{
public:
    OscConnector();
    ~OscConnector();
    OscConnectorDeck* getDeck(int nr);
    int getNumDecks();

private:
    lo::ServerThread* loServer;
    QMap<int,OscConnectorDeck*> decks;
};

#endif // OSCCONNECTOR_H
