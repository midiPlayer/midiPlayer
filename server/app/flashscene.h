#ifndef FLASHSCENE_H
#define FLASHSCENE_H
#include "scene.h"
#include "websocketserverprovider.h"
#include "audioprocessor.h"
#include "trigger.h"
#include <QTime>
#include "colorbutton.h"
#include "devices/devicestate.h"
#include "filtervirtualdevicemanager.h"
#include "virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "ungroupvirtualdevicemanager.h"
#include <QMap>
#include "randomvirtualdeviceprovider.h"
#include "RemoteTypes/remotenumber.h"
#include "scenes/planedtriggerhandler.h"
#include "RemoteTypes/remotefloat.h"
#include "RemoteTypes/remotebool.h"
#include "scenes/previewinjektor.h"

class FlashScene : public Scene, public WebSocketServerProvider
{
    Q_OBJECT

public:
    FlashScene(WebSocketServer* ws, AudioProcessor *jackP, VirtualDeviceManager *manager, PlanedTriggerHandler *triggerH, QString name, QJsonObject serialized);
    QList<Device>getLights();
    QList<Device> getUsedLights();

    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();

    void start();
    void stop();
    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject,int) {}
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
    QJsonObject serialize();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();
    void getPercentage(float percentage, int elapsed);

    float getPercentage(int elapsed);

public slots:
    void triggered();
    void reloadDevices();
    void reloadColor();
private:
    Trigger trigger;
    FilterVirtualDeviceManager filterVDevManager;
    SelectVirtualDeviceManager selectVDevMgr;
    UngroupVirtualDeviceManager UngroupVDevMgr;
    RandomDeviceChooser randomDevs;
    bool flashEnabled;
    QMap<QString, QSharedPointer<DeviceState> >flashState;
    QTime time;
    float smoothness;//0...0.5
    RemoteFloat fadeIn;
    RemoteFloat fadeOut;
    RemoteBool interrupt;
    RemoteBool relativeDuration;
    RemoteFloat flashDuration;
    int calculatedDuration;//in ms
    int beatSpeed;//in ms
    float timePer;
    ColorButton colorButton;

    int numChangingDevices;
    int timeDiff;
    bool sameColor;

    QMap<QString, int> timeDiffs;//in ms
    PreviewInjektor prevInj;
};

#endif // FLASHSCENE_H
