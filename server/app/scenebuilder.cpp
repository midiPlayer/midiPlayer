#include "scenebuilder.h"
#include "flashscene.h"
#include "discoscene.h"
#include "scenes/beatscene1.h"
#include "colorwavescene.h"
#include "scenes/keyframe/keyframescene.h"
#include "musicscene.h"
#include "screencolorscene.h"
#include "diascene.h"
#include "scenes/dj/djscene.h"
#include "scenes/beamerscene.h"
#include "scenes/mousescene.h"
#include "scenes/linescene.h"

SceneBuilder::SceneBuilder(WebSocketServer *wssP,
                           VirtualDeviceManager *manager,
                           AudioProcessor *jackP,
                           MonitorIO *monitorIoP,
                           OscConnector *osccP,
                           QSharedPointer<DiaScene> *baseSceneP,
                           BeamerEffectHandler* beamerEffectH):
    wss(wssP),
    vDevManager(manager),
    jack(jackP),
    monitorIo(monitorIoP),
    oscc(osccP),
    sceneProvider(NULL),
    osccDeck(NULL),
    baseScene(baseSceneP),
    triggerHandler(NULL),
    beamerEffectHandler(beamerEffectH),
    injekter(NULL)
{

}

SceneBuilder::SceneBuilder(SceneBuilder *parent):
    wss(parent->wss),
    vDevManager(parent->vDevManager),
    jack(parent->jack),
    monitorIo(parent->monitorIo),
    oscc(parent->oscc),
    sceneProvider(parent->sceneProvider),
    osccDeck(parent->osccDeck),
    baseScene(parent->baseScene),
    triggerHandler(parent->triggerHandler),
    beamerEffectHandler(parent->beamerEffectHandler),
    injekter(parent->injekter)
{

}

QSharedPointer<Scene> SceneBuilder::buildNew(QString sceneType, QString name)
{
    return buildInt(sceneType,name,QJsonObject());
}

QSharedPointer<Scene> SceneBuilder::buildInt(QString sceneType, QString name, QJsonObject serialized)
{   
    if(sceneType.indexOf("copy") == 0){//copy existing
        bool ok;
        int sceneId = sceneType.replace("copy","").toInt(&ok);
        if(!ok)
            throw("invalid scene id");
        QSharedPointer<Scene> copyFrom = findScene(*baseScene,sceneId);
        if(copyFrom.isNull()){
            throw("scene not found");
        }

        QSharedPointer<Scene> copy = copyScene(copyFrom);
        if(name != "")
            copy.data()->setName(name);
        return copy;

    }
    else if(sceneType == FlashScene::getSceneTypeStringStaticaly()){
        return QSharedPointer<Scene>(new FlashScene(wss,jack,vDevManager,triggerHandler,name,serialized));
    }
    else if(sceneType == DiscoScene::getSceneTypeStringStaticaly()){
        return QSharedPointer<Scene>(new DiscoScene(wss,this,vDevManager,name,serialized));
    }
    else if(sceneType == BeatScene1::getSceneTypeStringStaticaly()){
        return QSharedPointer<Scene>(new BeatScene1(vDevManager,jack,wss,name,triggerHandler,serialized));
    }
    else if(sceneType == ColorWaveScene::getSceneTypeStringStaticaly()){
        return QSharedPointer<Scene>(new ColorWaveScene(vDevManager,wss,jack,triggerHandler,name,serialized));
    }
    else if(sceneType == KeyFrameScene::getSceneTypeStringStaticaly()){
        return QSharedPointer<Scene>(new KeyFrameScene(vDevManager,name,wss,jack,osccDeck,this,triggerHandler, serialized));
    }
    else if(sceneType == MusicScene::getSceneTypeStringStaticaly()){
        return QSharedPointer<Scene>(new MusicScene(name,wss,serialized));
    }
    else if(sceneType == ScreenColorScene::getSceneTypeStringStaticaly()){
        return QSharedPointer<Scene>(new ScreenColorScene(vDevManager,wss,name,serialized));
    }
    else if(sceneType == DiaScene::getSceneTypeStringStaticaly()){
        return QSharedPointer<DiaScene>(new DiaScene(wss,jack,this,monitorIo,triggerHandler,name,serialized));
    }
    else if(sceneType == BeamerScene::getSceneTypeStringStaticly()){
        if(injekter == NULL)
            throw("can't build BeamerScene here - no Injektor was passed");
        return QSharedPointer<BeamerScene>(new BeamerScene(name,vDevManager,beamerEffectHandler,wss,jack,triggerHandler,injekter,serialized));
    }
    else if(sceneType == DjScene::getSceneTypeStringStaticaly()){
        if(sceneProvider == NULL)
            throw("can't build DjScene here - no SceneProvider was passed");
        return QSharedPointer<DjScene>(new DjScene(name,oscc,wss,sceneProvider,vDevManager,this,serialized));
    }
    else if(sceneType == MouseScene::getSceneTypeStringStaticly()){
        return QSharedPointer<MouseScene>(new MouseScene(name,vDevManager,wss,serialized));
    }
    else if(sceneType == LineScene::getSceneTypeStringStyticly()){
        return QSharedPointer<LineScene>(new LineScene(name,wss,jack,triggerHandler,vDevManager,serialized));
    }

    return QSharedPointer<Scene>();
}

void SceneBuilder::setInjekter(InjectVirtualDeviceManager *value)
{
    injekter = value;
}

void SceneBuilder::setVDevManager(VirtualDeviceManager *value)
{
    vDevManager = value;
}

PlanedTriggerHandler *SceneBuilder::getTriggerHandler() const
{
    return triggerHandler;
}

void SceneBuilder::setTriggerHandler(PlanedTriggerHandler *value)
{
    triggerHandler = value;
}

QSharedPointer<Scene> SceneBuilder::findScene(QSharedPointer<Scene> base, int searchedId)
{
    if(base.data()->getId() == searchedId){
        return base;
    }

    foreach(QSharedPointer<Scene> subScene, base.data()->getSubScenes()){
        QSharedPointer<Scene> ret = findScene(subScene,searchedId);
        if(!ret.isNull())
            return ret;
    }
    return QSharedPointer<Scene>();
}

void SceneBuilder::setOsccDeck(OscConnectorDeck *value)
{
    osccDeck = value;
}

QSharedPointer<Scene> SceneBuilder::copyScene(QSharedPointer<Scene> old)
{
    return rebuild(serializeScene(old));
}

void SceneBuilder::setSceneProvider(SceneProvider *value)
{
    sceneProvider = value;
}

QSharedPointer<Scene> SceneBuilder::rebuild(QJsonObject serialized)
{
    return buildInt(serialized.value("sceneType").toString(),"",serialized.value("sceneParams").toObject());
}

QJsonObject SceneBuilder::serializeScene(QSharedPointer<Scene> scene)
{
    return serializeScene(scene.data());
}

QJsonObject SceneBuilder::serializeScene(Scene *scene)
{
    return serializeScene(scene,scene->serialize());
}

QJsonObject SceneBuilder::serializeScene(Scene *scene, QJsonObject serialized)
{
    QJsonObject ret;
    ret.insert("sceneType",scene->getSceneTypeString());
    ret.insert("sceneParams",serialized);
    return ret;
}

