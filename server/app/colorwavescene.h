#ifndef COLORWAVESCENE_H
#define COLORWAVESCENE_H
#include "scene.h"
#include "websocketserverprovider.h"
#include "trigger.h"
#include <QTime>
#include "colorbutton.h"
#include "filtervirtualdevicemanager.h"
#include "virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "ungroupvirtualdevicemanager.h"
#include "scenes/planedtriggerhandler.h"

class ColorWaveScene : public Scene, public WebSocketServerProvider
{
Q_OBJECT

public:
    ColorWaveScene(VirtualDeviceManager *manager,WebSocketServer *ws, AudioProcessor *jackP,PlanedTriggerHandler *triggerH,  QString name, QJsonObject serialized = QJsonObject());
    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();
    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
    void stop();
    void start();
    QJsonObject serialize();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();
public slots:
    void triggered();
    void reinitDevices();
    void reinitColors();

private:
    FilterVirtualDeviceManager filterDeviceManager;
    SelectVirtualDeviceManager selectDevManager;
    UngroupVirtualDeviceManager ungrouped;
    Trigger trigger;
    QMap<QString, QSharedPointer<DeviceState> > onState;
    QTime beatStopwatch;
    bool isRunning;
    float activeRadius;// in meters
    float speed;// in m/s
    ColorButton colorButton;

    int centerDevPos;
    QVector3D center;
    float getPercentageForDistance(float distance);
};

#endif // COLORWAVESCENE_H
