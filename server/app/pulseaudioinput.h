#ifndef PULSEAUDIOINPUT_H
#define PULSEAUDIOINPUT_H

#include "audioinput.h"
#include <pulse/simple.h>
#include <pulse/error.h>
#include <QThread>

#define BUFSIZE 512
#define SAMPLERATE 44100

class AudioProcessor;

class PulseAudioInput : public QThread, public AudioInput
{
Q_OBJECT

public:
    PulseAudioInput(AudioProcessor* parent);
private:
    AudioProcessor* audioP;
    pa_simple *s;
    int error;
    float buf[BUFSIZE];

    // QThread interface
protected:
    void run();


    // AudioInput interface
public:
    void enable();
};

#endif // PULSEAUDIOINPUT_H
