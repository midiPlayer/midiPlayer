#include "stopwatch.h"
#include "websocketserver.h"

Stopwatch::Stopwatch(WebSocketServer *ws) :
    WebSocketServerProvider(ws),
    time(getTimestamp()),
    running(false),
    snaped(0),
    timer(),
    speed(1.0)
{

    timer.setInterval(1000);
    connect(&timer,SIGNAL(timeout()),this,SLOT(sendTimeSync()));
}

void Stopwatch::start(bool notify)
{
    time = getTimestamp();
    running = true;

    if(notify){
        QJsonObject ret;
        ret.insert("start",true);
        sendMsg(ret,true);
    }

    timer.start();
}

void Stopwatch::stop(bool notify)
{
    snaped = getMSecs();
    running = false;

    if(notify){
        QJsonObject ret;
        ret.insert("stop",true);
        sendMsg(ret,true);
    }

    timer.stop();
}

void Stopwatch::resume(bool notify)
{
    setTo(snaped,notify);
    running = true;

    if(notify){
        QJsonObject ret;
        ret.insert("resume",true);
        sendMsg(ret,true);
    }
    timer.start();
}

int Stopwatch::getMSecs()
{
    if(running)
        return int(float(getTimestamp() - time) * speed);
    return snaped;
}

void Stopwatch::setTo(long ms, bool notify)
{
    snaped = ms;
    time = getTimestamp() -long(float(ms) / speed);
    if(notify)
        sendTimeSync();
}

void Stopwatch::clientRegistered(QJsonObject, int id)
{
    QJsonObject ret;
    ret.insert("set",getMSecs());
    if(running)
        ret.insert("start",true);
    else
        ret.insert("stop",true);
    sendMsg(ret,id,true);
}

void Stopwatch::clientMessage(QJsonObject msg, int id)
{
    if(msg.contains("start")){
        start(false);
        emit started();
        sendMsgButNotTo(msg,id,true);
    }
    if(msg.contains("stop")){
        stop(false);
        emit stoped();
        sendMsgButNotTo(msg,id,true);
    }
    if(msg.contains("resume")){
        resume(false);
        emit resumed();
        sendMsgButNotTo(msg,id,true);
    }
    if(msg.contains("set")){
        setTo((long)msg.value("set").toDouble(),false);
        emit timeSet();
        sendMsgButNotTo(msg,id,true);
    }
}

QString Stopwatch::getRequestType()
{
    return "stopwatch";
}

bool Stopwatch::getRunning() const
{
    return running;
}

long Stopwatch::getTimestamp()
{
    return QDateTime::currentMSecsSinceEpoch();
}

void Stopwatch::sendTimeSync()
{
    QJsonObject ret;
    ret.insert("set",getMSecs());
    ret.insert("speed",speed);
    sendMsg(ret,true);
}

void Stopwatch::setSpeed(float newSpeed)
{
    if(running){
        stop();
        speed = newSpeed;
        resume(true);
    }
    else{
        speed = newSpeed;
    }
}
