#include "remotebeat.h"
#include "websocketserver.h"

RemoteBeat::RemoteBeat(WebSocketServer *ws, AudioProcessor *jp, QObject *parent) :
    QObject(parent),
    WebSocketServerProvider(ws)
{
    connect(jp,SIGNAL(beatNotification()),this,SLOT(onBeat()));
    connect(jp,SIGNAL(onsetNotification()),this,SLOT(onOnset()));
    
}

QString RemoteBeat::getRequestType()
{
    return "remoteBeat";
}

void RemoteBeat::onOnset()
{
    QJsonObject msg;
    msg.insert("type","onset");
    sendMsg(msg);
}

void RemoteBeat::onBeat()
{
    QJsonObject msg;
    msg.insert("type","beat");
    sendMsg(msg);
}

