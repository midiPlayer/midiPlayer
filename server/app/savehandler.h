#ifndef SAVEHANDLER_H
#define SAVEHANDLER_H


class SaveHandler
{
public:
    SaveHandler();
    ~SaveHandler() {}
    virtual void save() = 0;
};

#endif // SAVEHANDLER_H
