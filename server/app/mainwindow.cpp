#include "mainwindow.h"
#include "scenes/keyframe/keyframe.h"
#include <QDebug>
#include <QSetIterator>
#include <QListIterator>
#include "time.h"
#include "math.h"
#include "fusionscene.h"
#include "flashscene.h"
#include "devices/whitedevice.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QCoreApplication>

#include "devices/channeldevicestate.h"

#define SETTINGS_KEY "settings"
#define SETTING_KEY_MAINSCENE_JSON "mainscene"
#define SETTING_KEY_JACKP "jackp"
#define SETTING_KEY_DEVS "devs"

/*
 *Hier werden alle Szenen sowie die Klasse JackProcessor zur Kommunikation über jack instanziiert.
*/


MainWindow::MainWindow(WebSocketServer* ws) :
    WebSocketServerProvider(ws),
    wss(ws),
    oscc(),
    status(),
    offsetRequested(true),
    settings(),
    jackProcessor(wss,getSettings().value(SETTING_KEY_JACKP).toObject(),this),
    outDevices(),
    timer(this),
    getChangesRunning(false),
    virtualDevManager(wss,getSettings().value(SETTING_KEY_DEVS).toObject()),
    ungroupedDevs(&virtualDevManager),
    monitorIO(wss),
    beamerEffectHandler(wss),
    sceneBuilder(wss,&virtualDevManager,&jackProcessor,&monitorIO,&oscc,&mainScene,&beamerEffectHandler),
    olaDeviceProvider(&virtualDevManager),
    remoteBeat(wss,&jackProcessor),
    mainScene(),
    filieIoProv(wss,&mainScene,this),
    channelTester(wss,&olaDeviceProvider),
    beamerDispatcher(wss,&virtualDevManager),
    beamerOutDevice()
{
    //availableDevices = Device::loadDevicesFromXml("~/devices.xml");
    //needed for settings
    QCoreApplication::setOrganizationName("fdg");
    QCoreApplication::setOrganizationDomain("fdg-ab.de");
    QCoreApplication::setApplicationName("light master");

    outDevices.append(&olaDeviceProvider);
    outDevices.append(&beamerOutDevice);

    connect(wss,SIGNAL(clientClosed()),this,SLOT(save()));


    loadScenes(getSettings().value(SETTING_KEY_MAINSCENE_JSON).toObject());

    connect(&timer,SIGNAL(timeout()),this,SLOT(trigger()));
    timer.setInterval(40);

    jackProcessor.init(this);

    //save();
//    exit(1);

}


//This method is called by jackprocesssor
//if offset is true all lamps will be transmitet not only th changes
void MainWindow::getChanges()
{
    QMap<QString, QSharedPointer<DeviceState> > newState;
    getChangesRunning = true;
    QMap<QString, QSharedPointer<DeviceState> > changes;

    newState = mainScene.data()->getDeviceState();


    QHash<QString, QSharedPointer<Device> > avDev = ungroupedDevs.getDevices();
    foreach (QString deviceId, avDev.keys()) {
        if(!newState.contains(deviceId)){//setzte alle unbenutzen Lampen auf 0
            newState.insert(deviceId,avDev.value(deviceId).data()->createEmptyState());
        }
    }


    /*
    //single device saver:
    //replaces 0 by 0.01 to keep the single device always warm
    foreach (QString devId, newState.keys()) {
        QSharedPointer<DeviceState> newSateDev = newState.value(devId);
        if(newSateDev.isNull())
            continue;
        QSharedPointer<ChannelDeviceState> state = newSateDev.dynamicCast<ChannelDeviceState>();
        if(state.isNull())
            continue;
        if(state.data()->device->getType() == Device::White &&
                state.data()->getChannelValue(state.data()->getFirstChannel()) == 0.0 ){
            state.data()->setChannel(state.data()->getFirstChannel(),0.01);
        }
    }*/


    //test for changes
    changes.clear();
    foreach (QString devId, newState.keys()) {
        newState.value(devId).data()->publish();
        if(offsetRequested || !status.contains(devId) ||
                !status.value(devId).data()->equal(newState.value(devId).data())){
            if(status.contains(devId))
                status.remove(devId);
            status.insert(devId,newState.value(devId).data()->clone());
            changes.insert(devId,newState.value(devId));
            newState.value(devId).data()->publishChanges();
        }
    }

    offsetRequested = false;


    foreach (OutputDevice *out,outDevices) {
        out->publish(newState,changes);
    }

    getChangesRunning = false;

}

void MainWindow::loadScenes(QJsonObject data)
{

    wss->disconnectAllClients();

    timer.stop();
    mainScene = QSharedPointer<DiaScene>(new DiaScene(wss,&jackProcessor,&sceneBuilder,&monitorIO, NULL, "main"));
    mainScene.data()->loadSerialized(data);
    mainScene.data()->start();

    timer.start();

}


void MainWindow::trigger()
{
    if(getChangesRunning){
        qDebug() << "still running!";
        return;
    }
    getChanges();
}


QJsonObject MainWindow::getSettings()
{
    QString settingStr = settings.value(SETTINGS_KEY).toString();
    QJsonDocument d = QJsonDocument::fromJson(settingStr.toUtf8());
    return d.object();
}

QString MainWindow::getRequestType()
{
    return "mainwindow";
}

void MainWindow::clientRegistered(QJsonObject, int id)
{
    QJsonObject ret;
    ret.insert("rootScene",mainScene.data()->providerId);
    sendMsg(ret,id,false);
}

void MainWindow::clientMessage(QJsonObject msg, int clientId)
{
    if(msg.contains("getSceneTree")){
        QJsonObject ret;
        ret.insert("sceneTree",getSceneJson(mainScene));
        sendMsg(ret,clientId,false);
    }
    if(msg.contains("save")){
        save();
    }
}

QJsonObject MainWindow::getSceneJson(QSharedPointer<Scene> scene)
{
    QJsonObject sceneJson;
    sceneJson.insert("name",scene.data()->getName());
    sceneJson.insert("sceneId",scene.data()->getId());
    QJsonArray subsJ;
    foreach (QSharedPointer<Scene> subScene, scene.data()->getSubScenes()) {
        subsJ.append(getSceneJson(subScene));
    }
    sceneJson.insert("subs",subsJ);
    return sceneJson;
}

void MainWindow::save()
{
    QJsonObject settingsJson;
    settingsJson.insert(SETTING_KEY_MAINSCENE_JSON,mainScene.data()->serialize());
    settingsJson.insert(SETTING_KEY_JACKP,jackProcessor.serialize());
    settingsJson.insert(SETTING_KEY_DEVS,virtualDevManager.serialize());

    QJsonDocument d;
    d.setObject(settingsJson);
    qDebug() << "SAVED!!!!SAVED!!!!SAVED!!!!SAVED!!!!SAVED!!!!SAVED!!!!SAVED!!!!SAVED!!!!";
    //qDebug() << d.toJson();
    settings.setValue(SETTINGS_KEY,d.toJson(QJsonDocument::Compact));
    //settings.setValue(SETTINGS_KEY,d.toJson());
}

MainWindow::~MainWindow()
{
    save();
}
