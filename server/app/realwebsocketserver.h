#ifndef REALWEBSOCKETSERVER_H
#define REALWEBSOCKETSERVER_H

#include "websocketserver.h"
#include <unordered_map>
#include <unordered_set>
class RealWebSocketserver : public WebSocketServer
{
Q_OBJECT

public:
    RealWebSocketserver();

public slots:
    virtual void onNewConnection();
    void onTextMessage(QString message);
    void onConnectionClosed();

private:
    std::unordered_map<int, WebSocketServerProvider*> providerById;
    std::unordered_set<WebSocketServerProvider*> registeredProvieder;
    int providerIdCounter;
    QWebSocketServer *m_pWebSocketServer;
    QList<QWebSocket *> m_clients;
    void registerId(int id, QJsonObject params, QWebSocket *client);
public:
    void registerProvider(WebSocketServerProvider *me);
    void unregisterProvider(WebSocketServerProvider *me);
    void sendData(QJsonObject data, QWebSocket *reciever, WebSocketServerProvider *providerById, bool onlyToProviderId);
    void disconnectAllClients();
};

#endif // REALWEBSOCKETSERVER_H
