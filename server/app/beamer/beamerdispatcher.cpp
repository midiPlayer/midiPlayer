#include "beamerdispatcher.h"
#include "../devices/beamerdevice.h"

#include <QJsonArray>

BeamerDispatcher::BeamerDispatcher(WebSocketServer* ws, VirtualDeviceManager* vDevMgr):
    WebSocketServerProvider(ws),
    filterDevMgr(vDevMgr,{Device::Beamer}),
    ungroupDevMgr(&filterDevMgr)
{
    connect(&ungroupDevMgr,SIGNAL(virtualDevicesChanged()),this,SLOT(sendBeamerMsg()));
}

QString BeamerDispatcher::getRequestType()
{
    return "beamerDispatcher";
}

QJsonObject BeamerDispatcher::getBeamerMsg()
{
    QJsonArray beamersJson;
    foreach(QSharedPointer<Device> dev, ungroupDevMgr.getDevices().values()){
        QSharedPointer<BeamerDevice> beamer(dev.dynamicCast<BeamerDevice>());
        QJsonObject beamerJson;
        beamerJson.insert("devId",beamer.data()->getDeviceId());
        beamerJson.insert("providerId",beamer.data()->providerId);
        beamersJson.append(beamerJson);
    }


    QJsonObject resp;
    resp.insert("avBeamer",beamersJson);

    return resp;
}

void BeamerDispatcher::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject resp = getBeamerMsg();
    sendMsg(resp,clientId,false);
}

void BeamerDispatcher::sendBeamerMsg()
{
    QJsonObject resp = getBeamerMsg();
    sendMsg(resp,false);
}
