#ifndef BEAMERDISPATCHER_H
#define BEAMERDISPATCHER_H

#include "../websocketserverprovider.h"
#include "../virtualDeviceManager/virtualdevicemanager.h"
#include "../filtervirtualdevicemanager.h"
#include "../ungroupvirtualdevicemanager.h"
#include <QObject>

class BeamerDispatcher : public QObject, public WebSocketServerProvider
{
Q_OBJECT

public:
    BeamerDispatcher(WebSocketServer *ws,
                     VirtualDeviceManager* vDevMgr);

    // WebSocketServerProvider interface
public:
    QString getRequestType();
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject, int){}

public slots:
    void sendBeamerMsg();

private:
    FilterVirtualDeviceManager filterDevMgr;
    UngroupVirtualDeviceManager ungroupDevMgr;
    QJsonObject getBeamerMsg();
};

#endif // BEAMERDISPATCHER_H
