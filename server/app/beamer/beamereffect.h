#ifndef BEAMEREFFECT_H
#define BEAMEREFFECT_H

#include <QFile>
#include <QFileSystemWatcher>
#include <QString>
#include "../websocketserverprovider.h"
#include "../serializable.h"
#include "../savehandler.h"

class BeamerEffect : public QObject, public WebSocketServerProvider, public Serializable
{
Q_OBJECT

public:
    BeamerEffect(QString name, QString pathP, WebSocketServer *ws);

    QString getName() const;
    void setName(const QString &value);

    QString getEffectCode() const;
    void setEffectCode(const QString &value);

    QString getManagerCode() const;
    void setManagerCode(const QString &value);

    class CreationFailedExecption : public std::exception{

    };

public slots:
    void readCode();

signals:
    void colorsChanged();

private:
    QString name;
    QString path;
    QString effectCode;
    QString managerCode;

    bool isValid;

    QSet<int> registeredBeamers;
    QSet<int> registeredManagers;
    QStringList colors;
    QFileSystemWatcher watcher;

    void updateAllBeamer();
    void updateAllManager();
    void updateManager(int clientId);
    void updateBeamer(int clientId);
    void parseColors();

    void writeEffectCode();
    void writeManagerCode();
public:
    QString getRequestType();
    void clientRegistered(QJsonObject msg, int clientId);
    void clientUnregistered(QJsonObject msg, int clientId);
    void clientMessage(QJsonObject msg, int);

    // Serializable interface
    QJsonObject serialize();
    QStringList getColors() const;
    QString getPath() const;
    bool getIsValid() const;
};

#endif // BEAMEREFFECT_H
