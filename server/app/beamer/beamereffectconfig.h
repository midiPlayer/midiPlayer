#ifndef BEAMEREFFECTCONFIG_H
#define BEAMEREFFECTCONFIG_H


#include "beamereffect.h"
#include "beamereffecthandler.h"

class BeamerEffectConfig : public Serializable
{
public:
    BeamerEffectConfig(QSharedPointer<BeamerEffect> effectP, QJsonObject configP,int owner);
    BeamerEffectConfig(BeamerEffectConfig* copy);
    BeamerEffectConfig(QJsonObject serialized,BeamerEffectHandler *handler, int owner);
    virtual ~BeamerEffectConfig(){}

    QSharedPointer<BeamerEffect> getEffect() const;
    void setEffect(const QSharedPointer<BeamerEffect> &value);

    QJsonObject getConfig() const;
    void setConfig(const QJsonObject &value);

    float getOpacity() const;
    void setOpacity(float value);
    bool operator ==(BeamerEffectConfig const& b);

private:
    QSharedPointer<BeamerEffect> effect;
    QJsonObject config;
    float opacity;
    int ownerRequestId;
    bool isTriggered;


    // Serializable interface
public:
    QJsonObject serialize();
    int getSceneRequestId() const;
    bool getIsTiggered() const;
    void trigger();
};

#endif // BEAMEREFFECTCONFIG_H
