#include "beameroutdevice.h"
#include "beamermessage.h"
#include <QList>
#include <QSharedPointer>
#include "../devices/beamerdevicestate.h"
#include "../devices/beamercolordevicestate.h"

BeamerOutDevice::BeamerOutDevice()
{

}

void BeamerOutDevice::publish(QMap<QString, QSharedPointer<DeviceState> >all, QMap<QString, QSharedPointer<DeviceState> >)
{
    QMap<QString, QSharedPointer<BeamerMessage> > messages;
    foreach(QSharedPointer<DeviceState> state, all.values()){
        QSharedPointer<BeamerDeviceState> beamerState = state.dynamicCast<BeamerDeviceState>();
        if(!beamerState.isNull()){
            QString devId = beamerState.data()->getDevice()->getDeviceId();
            if(messages.contains(devId)){
                messages.value(devId).data()->setState(beamerState);
            }
            else{
                messages.insert(devId,QSharedPointer<BeamerMessage>(new BeamerMessage(beamerState)));
            }
        }
        else{
            QSharedPointer<BeamerColorDeviceState> colorState = state.dynamicCast<BeamerColorDeviceState>();
            if(!colorState.isNull()){
                QString devId = colorState.data()->getDevice()->getBeamer().data()->getDeviceId();
                if(messages.contains(devId)){
                    messages.value(devId).data()->addColorState(colorState);
                }
                else{
                    messages.insert(devId,QSharedPointer<BeamerMessage>(new BeamerMessage(colorState)));
                }
            }
        }

    }

    foreach (QSharedPointer<BeamerMessage> message, messages) {
        message.data()->send();
    }
}
