#include "../beamer/beamereffecthandler.h"
#include <QDir>
#include <QDirIterator>
#include <QJsonArray>

BeamerEffectHandler::BeamerEffectHandler(WebSocketServer* ws) :
    WebSocketServerProvider(ws),
    effectsPath(QDir::homePath() + "/workspace/qt/midiPlayer-git/beamerEffects/"),
    wss(ws),
    watcher()
{
    /*foreach(QJsonValue effectJson,serialized.value("effects").toArray()){
        QSharedPointer<BeamerEffect> effect(new BeamerEffect(ws,saver,effectJson.toObject()));
        effects.insert(effect.data()->providerId,effect);
    }*/

    watcher.addPath(effectsPath);
    connect(&watcher,SIGNAL(directoryChanged(QString)),this,SLOT(loadEffects()));

    loadEffects();
}

QSharedPointer<BeamerEffect> BeamerEffectHandler::getBeamerEffectByName(QString name)
{
    foreach(QSharedPointer<BeamerEffect> effect, effects){
        if(effect.data()->getName() == name)
            return effect;
    }
    return QSharedPointer<BeamerEffect>();
}

void BeamerEffectHandler::loadEffects()
{
    QDirIterator d(effectsPath);
    while (d.hasNext()) {
        QString path = d.next();
        if(d.fileInfo().isDir()){
            if(d.fileInfo().isHidden())
                continue;
            qDebug() << path;

            bool exists = false;
            foreach (QSharedPointer<BeamerEffect> existing, effects) {
                if(existing.data()->getPath() == path){
                    exists = true;
                    break;
                }
            }
            if(exists)
                continue;

            QSharedPointer<BeamerEffect> effect(new BeamerEffect(d.fileName(),path,wss));
            effects.insert(effect.data()->providerId,effect);
        }
    }
}

int BeamerEffectHandler::addEmptyEffect(QString name)
{
    throw("unimplemented");
    /*QSharedPointer<BeamerEffect> effect(new BeamerEffect(wss,saver));
    int provId = effect.data()->providerId;
    effect.data()->setName(name);
    effects.insert(provId,effect);
    emit effectsChanged();
    QJsonObject resp = getEffectsJson();
    sendMsg(resp,false);
    return provId;*/
}

QString BeamerEffectHandler::getEffectsPath() const
{
    return effectsPath;
}

void BeamerEffectHandler::setEffectsPath(const QString &value)
{
    watcher.removePath(effectsPath);
    effectsPath = value;
    watcher.addPath(effectsPath);
}

QMap<int, QSharedPointer<BeamerEffect> > BeamerEffectHandler::getEffects() const
{
    return effects;
}

QString BeamerEffectHandler::getRequestType()
{
    return "beamereffecthandler";
}

QJsonObject BeamerEffectHandler::getEffectsJson()
{
    QJsonArray effectsJson;
    foreach(QSharedPointer<BeamerEffect> effect, effects.values()){
        QJsonObject effectJson;
        if(!effect.data()->getIsValid()) //hide invalid
            continue;
        effectJson.insert("effectId",effect.data()->providerId);
        effectJson.insert("effectName",effect.data()->getName());
        effectsJson.append(effectJson);
    }
    QJsonObject resp;
    resp.insert("effects",effectsJson);

    return resp;
}

void BeamerEffectHandler::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject resp = getEffectsJson();
    sendMsg(resp,clientId,false);
}

void BeamerEffectHandler::clientMessage(QJsonObject msg, int clientId)
{
    if(msg.contains("addEffect")){
        QJsonObject resp;
        resp.insert("newEffect",addEmptyEffect(msg.value("addEffect").toString()));
        sendMsg(resp,clientId,false);
    }
    if(msg.contains("delEffect")){
        effects.remove(msg.value("delEffect").toInt(-1));
        emit effectsChanged();
        QJsonObject resp = getEffectsJson();
        sendMsg(resp,false);
    }
}
