#include "beamereffectconfig.h"

BeamerEffectConfig::BeamerEffectConfig(QSharedPointer<BeamerEffect> effectP, QJsonObject configP, int owner):
    effect(),
    config(configP),
    opacity(1.0),
    ownerRequestId(owner),
    isTriggered(false)
{

}

BeamerEffectConfig::BeamerEffectConfig(BeamerEffectConfig *copy):
    effect(copy->getEffect()),
    config(copy->getConfig()),
    opacity(copy->getOpacity()),
    ownerRequestId(copy->ownerRequestId),
    isTriggered(copy->isTriggered)
{

}

BeamerEffectConfig::BeamerEffectConfig(QJsonObject serialized, BeamerEffectHandler *handler, int owner):
    effect(handler->getBeamerEffectByName(serialized.value("effect").toString())),
    config(serialized.value("config").toObject()),
    opacity(serialized.value("opacity").toDouble(1.0)),
    ownerRequestId(owner),
    isTriggered(false)
{

}

QSharedPointer<BeamerEffect> BeamerEffectConfig::getEffect() const
{
    return effect;
}

void BeamerEffectConfig::setEffect(const QSharedPointer<BeamerEffect> &value)
{
    effect = value;
    emit
}

QJsonObject BeamerEffectConfig::getConfig() const
{
    return config;
}

void BeamerEffectConfig::setConfig(const QJsonObject &value)
{
    config = value;
}

float BeamerEffectConfig::getOpacity() const
{
    return opacity;
}

void BeamerEffectConfig::setOpacity(float value)
{
    opacity = value;
}

bool BeamerEffectConfig::operator ==(const BeamerEffectConfig &b)
{
    if(effect != b.effect)
        return false;
    if(config != b.config)
        return false;
    if(opacity != b.opacity)
        return false;
    if(isTriggered != b.isTriggered)
        return false;
    return true;
}

bool BeamerEffectConfig::getIsTiggered() const
{
    return isTriggered;
}

void BeamerEffectConfig::trigger()
{
    isTriggered = true;
}

int BeamerEffectConfig::getSceneRequestId() const
{
    return ownerRequestId;
}

QJsonObject BeamerEffectConfig::serialize()
{
    QJsonObject ser;
    if(!effect.isNull())
        ser.insert("effect",effect.data()->getName());
    ser.insert("config",config);
    ser.insert("opacity",opacity);
    return ser;
}
