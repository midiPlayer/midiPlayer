#ifndef BEAMEREFFECTHANDLER_H
#define BEAMEREFFECTHANDLER_H

#include <QFileSystemWatcher>
#include <QMap>
#include "beamereffect.h"
#include "../websocketserverprovider.h"
#include "../savehandler.h"

class BeamerEffectHandler : public QObject, public WebSocketServerProvider
{
Q_OBJECT

public:
    BeamerEffectHandler(WebSocketServer* ws);

    QSharedPointer<BeamerEffect> getBeamerEffectByName(QString name);

private:
    int addEmptyEffect(QString name);

    QString effectsPath;
    WebSocketServer* wss;
    QMap<int,QSharedPointer<BeamerEffect> > effects;
    QFileSystemWatcher watcher;

    QJsonObject getEffectsJson();


    // WebSocketServerProvider interface
public:
    QString getRequestType();
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int clientId);
    QMap<int, QSharedPointer<BeamerEffect> > getEffects() const;

    QString getEffectsPath() const;
    void setEffectsPath(const QString &value);

public slots:
    void loadEffects();

signals:
    void effectsChanged();
};

#endif // BEAMEREFFECTHANDLER_H
