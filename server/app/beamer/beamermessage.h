#ifndef BEAMERMESSAGE_H
#define BEAMERMESSAGE_H

class BeamerDeviceState;
class BeamerColorDeviceState;
class BeamerDevice;
#include <QMap>
#include <QSharedPointer>

class BeamerMessage
{
public:
    BeamerMessage();
    BeamerMessage(BeamerMessage* other);
    BeamerMessage(QSharedPointer<BeamerDeviceState> devState);
    BeamerMessage(QSharedPointer<BeamerColorDeviceState> colorState);
    void send();

    void setState(const QSharedPointer<BeamerDeviceState> &value);
    void addColorState(QSharedPointer<BeamerColorDeviceState> colorState);
    QJsonObject getMessage();
    BeamerMessage getDiffMsg(BeamerMessage* other);
    bool isEmpty();

private:
    QSharedPointer<BeamerDeviceState> state;
    QMap<QString,QSharedPointer<BeamerColorDeviceState> > colorStates;
    BeamerDevice *getBeamer();
};

#endif // BEAMERMESSAGE_H
