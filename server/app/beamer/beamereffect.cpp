#include "beamereffect.h"

BeamerEffect::BeamerEffect(QString name, QString pathP, WebSocketServer* ws):
    WebSocketServerProvider(ws),
    Serializable(),
    name(name),
    path(pathP),
    isValid(false),
    watcher(){

    readCode();
    watcher.addPath(pathP);
    connect(&watcher,SIGNAL(directoryChanged(QString)),this,SLOT(readCode()));
}

QString BeamerEffect::getName() const
{
    return name;
}

void BeamerEffect::setName(const QString &value)
{
    name = value;
}

QString BeamerEffect::getEffectCode() const
{
    return effectCode;
}

void BeamerEffect::setEffectCode(const QString &value)
{
    effectCode = value;
    parseColors();
    updateAllBeamer();
    writeEffectCode();
}

QString BeamerEffect::getManagerCode() const
{
    return managerCode;
}

void BeamerEffect::setManagerCode(const QString &value)
{
    managerCode = value;
    writeManagerCode();
}

void BeamerEffect::readCode()
{
    isValid = false;
    QFile effectFile(path + "/effect.qml");
    if (!effectFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << path << " effect is invalid";
        return;
    }

     QTextStream in(&effectFile);
     QString newEffectCode = in.readAll();
     effectFile.close();

     QFile managerFile(path + "/manager.qml");
     if (!managerFile.open(QIODevice::ReadOnly | QIODevice::Text))
         return;

      QTextStream in2(&managerFile);
      QString newManagerCode = in2.readAll();
      managerFile.close();
      isValid = true;

      if(newEffectCode != effectCode || newManagerCode != managerCode){
          effectCode = newEffectCode;
          managerCode = newManagerCode;
          parseColors();
          updateAllBeamer();
      }
}

void BeamerEffect::writeEffectCode()
{
    qDebug() << "write effect Code!";
    QFile effecFile(path + "/effect.qml");
    if (effecFile.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)) {
        QTextStream effectStream(&effecFile);
        effectStream << effectCode;
        effectStream.flush();
        effecFile.close();
    }
    else{
        qDebug() << "coul'd not open effect file for write";
    }
}

void BeamerEffect::writeManagerCode()
{
    qDebug() << "write manager Code!";
    QFile managerFile(path + "/manager.qml");
    if (managerFile.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)) {
        QTextStream managerStream(&managerFile);
        managerStream << managerCode;
        managerStream.flush();
        managerFile.close();
    }
    else{
        qDebug() << "coul'd not open manager file for write";
    }
}

bool BeamerEffect::getIsValid() const
{
    return isValid;
}

QString BeamerEffect::getPath() const
{
    return path;
}

QString BeamerEffect::getRequestType()
{
    return "beamereffect";
}

void BeamerEffect::updateAllManager()
{
    foreach (int clientId, registeredManagers) {
        updateManager(clientId);
    }
}

void BeamerEffect::updateManager(int clientId)
{
    QJsonObject resp;
    resp.insert("managerCode",managerCode);
    sendMsg(resp,clientId,true);
}

void BeamerEffect::updateAllBeamer()
{
    foreach (int clientId, registeredBeamers) {
        updateBeamer(clientId);
    }
}

void BeamerEffect::updateBeamer(int clientId)
{
    QJsonObject resp;
    resp.insert("beamerCode",effectCode);
    sendMsg(resp,clientId,true);
}

void BeamerEffect::parseColors()
{
    colors.clear();
    QString firstLine = effectCode.split("\n").first();
    if(!firstLine.contains("//")){
        emit colorsChanged();
        return;
    }
    firstLine = firstLine.replace("//","");//remove comment signs
    QStringList vars = firstLine.split(",",QString::SkipEmptyParts);
    foreach(QString var, vars){
        colors.append(var.replace(" ",""));
    }
    qDebug() << colors;
    emit colorsChanged();
}


QStringList BeamerEffect::getColors() const
{
    return colors;
}

void BeamerEffect::clientRegistered(QJsonObject msg, int clientId)
{
    if(!msg.contains("as"))
        return;
    if(msg.value("as") == "beamer"){
        registeredBeamers.insert(clientId);

        updateBeamer(clientId);
    }
    else if(msg.value("as") == "manager"){
        registeredManagers.insert(clientId);
        updateManager(clientId);
    }
}

void BeamerEffect::clientUnregistered(QJsonObject msg, int clientId)
{
    registeredBeamers.remove(clientId);
    registeredManagers.remove(clientId);
}

void BeamerEffect::clientMessage(QJsonObject msg, int)
{
    if(msg.contains("beamerCode")){
        setEffectCode(msg.value("beamerCode").toString());
    }
    if(msg.contains("managerCode"))
        setManagerCode(msg.value("managerCode").toString());
}

QJsonObject BeamerEffect::serialize()
{
    QJsonObject ret;
    ret.insert("name",name);
    ret.insert("effectCode",effectCode);
    ret.insert("managerCode",managerCode);
    return ret;
}
