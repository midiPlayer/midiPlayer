#ifndef BEAMEROUTDEVICE_H
#define BEAMEROUTDEVICE_H

#include "../outputdevice.h"
class BeamerOutDevice: public OutputDevice
{
public:
    BeamerOutDevice();

    // OutputDevice interface
public:
    void publish(QMap<QString, QSharedPointer<DeviceState> > all, QMap<QString, QSharedPointer<DeviceState> >);
};

#endif // BEAMEROUTDEVICE_H
