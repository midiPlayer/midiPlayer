#include "beamermessage.h"
#include "../devices/beamerdevicestate.h"
#include "../devices/beamercolordevicestate.h"
#include <QJsonArray>

BeamerMessage::BeamerMessage()
{

}

BeamerMessage::BeamerMessage(BeamerMessage *other):
    state(other->state),
    colorStates(other->colorStates)
{

}

BeamerMessage::BeamerMessage(QSharedPointer<BeamerDeviceState> devState):
    state(devState)
{

}

BeamerMessage::BeamerMessage(QSharedPointer<BeamerColorDeviceState> colorState)
{
    colorStates.insert(colorState.data()->getDevice()->getDeviceId(),colorState);

}

void BeamerMessage::send()
{
 getBeamer()->publish(this);
}

void BeamerMessage::setState(const QSharedPointer<BeamerDeviceState> &value)
{
    if(getBeamer() != value.data()->getDevice())
        throw("wrong device");
    state = value;
}

void BeamerMessage::addColorState(QSharedPointer<BeamerColorDeviceState> colorState)
{
    if(getBeamer() != colorState.data()->getDevice()->getBeamer())
        throw("wrong device");

    QString devId = colorState.data()->getDevice()->getDeviceId();
    if(colorStates.contains(devId))
        return;
    colorStates.insert(devId,colorState);
}

QJsonObject BeamerMessage::getMessage()
{
    QJsonObject msg;
    if(!state.isNull()){
        QJsonArray effectsJson;
        foreach(BeamerEffectConfig conf, state.data()->getEffects()){
            QJsonObject effectJson;
            if(conf.getEffect().isNull())
                continue;
            effectJson.insert("effect",conf.getEffect().data()->providerId);
            effectJson.insert("opacity",conf.getOpacity());
            effectJson.insert("conf",conf.getConfig());
            effectJson.insert("owner",conf.getSceneRequestId());
            effectJson.insert("triggered",conf.getIsTiggered());
            effectsJson.append(effectJson);
        }
        msg.insert("state",effectsJson);
    }

    QJsonArray colors;
    foreach (QSharedPointer<BeamerColorDeviceState> colorState, colorStates.values()) {
        QJsonObject colorStateJson;
        colorStateJson.insert("owner",colorState.data()->getDevice()->getScene()->providerId);
        colorStateJson.insert("colorName",colorState.data()->getDevice()->getColor());
        colorStateJson.insert("color",colorState.data()->getColor().getRGB().name(QColor::HexArgb));
        colors.append(colorStateJson);
    }
    msg.insert("colors",colors);
    return msg;
}

BeamerMessage BeamerMessage::getDiffMsg(BeamerMessage *other)
{
    BeamerMessage ret;
    bool stateChanged = false;
    if(!other->state.isNull() && !other->state.data()->equal(state.data())){
            ret.state = state;
            stateChanged = true;
    }
    if(stateChanged)
        ret.colorStates = colorStates;//add All
    else{   //add changed
        foreach(QString color,colorStates.keys()){
            if(!other->colorStates.contains(color) ||
                !other->colorStates.value(color).data()->equal(colorStates.value(color).data())){
                ret.colorStates.insert(color,colorStates.value(color));
            }
        }
    }

    for(QMap<QString,QSharedPointer<BeamerColorDeviceState> >::iterator it = other->colorStates.begin();
        it != other->colorStates.end();
        it++){
        if(!colorStates.contains(it.key())){//color state was deleted (scene was deactivated)
            BeamerColorDevice* colorDev = it.value().data()->getDevice();
            if(colorDev == NULL)
                continue;
            QSharedPointer<BeamerColorDeviceState> empty = colorDev->createEmptyState()
                                               .dynamicCast<BeamerColorDeviceState>();
            empty.data()->setOpacity(0);
            ret.colorStates.insert(it.key(),empty);
        }
    }


    return ret;
}

bool BeamerMessage::isEmpty()
{
    return state.isNull() && colorStates.isEmpty();
}

BeamerDevice* BeamerMessage::getBeamer()
{
    if(!state.isNull())
        return state.data()->getDevice();
  return colorStates.values().first().data()->getDevice()->getBeamer().data();
}
