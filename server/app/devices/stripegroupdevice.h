#ifndef STRIPEGROUPDEVICE_H
#define STRIPEGROUPDEVICE_H

#include "../devices/groupdevice.h"
#define GROUP_TYPE_STRIPE "stripe"
#include "../devices/device.h"

class StripeGroupDevice : public GroupDevice
{
public:
    StripeGroupDevice(QString devId, int numDev, int firstC, int univ, Device::DeviceType devT, QMap<int, QVector3D> devPos);
    StripeGroupDevice(QJsonObject serialized);

    // Serializable interface
public:
    QJsonObject serialize();

    // Device interface
public:
    bool isSerializable();

    // GroupDevice interface
public:
    QHash<QString, QSharedPointer<Device> > getChildren();
    QHash<QString, QSharedPointer<GroupDevice> > getChildGroups();
    QSharedPointer<SelectorBuilder> getSelectorBuilder();

private:
    int numDevices;
    int firstChannel;
    int universe;
    Device::DeviceType devTypes;
    QMap<int,QVector3D> positions;

    QVector3D interpolatePosition(int index);

    QHash<QString, QSharedPointer<Device> > children;
    void generateChildren();

public:
    QString getGroupType();
    int getUniverse() const;
    void setUniverse(int value);
    int getFirstChannel() const;
    void setFirstChannel(int value);
    int getNumDevices() const;
    void setNumDevices(int value);
    QMap<int, QVector3D> getPositions() const;
    Device::DeviceType getDevTypes() const;
    void setDevTypes(const Device::DeviceType &value);
    QJsonArray serializePositions();
    static QMap<int, QVector3D> deSerializedPositions(QJsonArray positionsJson);
    void setPositions(const QMap<int, QVector3D> &value);
    int getNumChannel();

};

#endif // STRIPEGROUPDEVICE_H
