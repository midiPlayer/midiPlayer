#ifndef CHANNELDEVICE_H
#define CHANNELDEVICE_H
#include "../devices/device.h"
#include "../devices/channeldevicestate.h"
class ChannelDevice : public Device
{
public:
    ChannelDevice(int firstChannelP, int numChannelsP, int universeP, QString devIdP,
                  DeviceType typeP, bool moduloModeP, QVector3D pos = QVector3D(0,0,0));
    ChannelDevice(QJsonObject serialized, DeviceType type);

    QJsonObject serialize();
    bool isSerializable();

    int getNumChannels();
    int getFirstChannel();
    QSharedPointer<DeviceState> createEmptyState();
    QSharedPointer<ChannelDeviceState> createEmptyChannelState();
    static bool istChannelDeviceType(Device::DeviceType type);

    int getUniverse() const;
    void setUniverse(int value);

    void setNumChannel(int value);

    void setFirstChannel(int value);

    bool getModuloMode() const;
    void setModuloMode(bool value);

private:
    int firstChannel;
    int numChannel;
    int universe;
    bool moduloMode;
};

#endif // CHANNELDEVICE_H
