#include "../devices/channeldevice.h"

#define KEY_FIRST_CHANNEL_ID "device_first_channel"
#define KEY_NUM_CHANNEL "device_num_channel"
#define KEY_NUM_UNIVERSE "device_universe"


ChannelDevice::ChannelDevice(int firstChannelP,
                             int numChannelsP,
                             int universeP,
                             QString devIdP,
                             DeviceType typeP,
                             bool moduloModeP,
                             QVector3D pos) :
    Device(devIdP,typeP,pos),
    firstChannel(firstChannelP),
    numChannel(numChannelsP),
    universe(universeP),
    moduloMode(moduloModeP)
{

}

ChannelDevice::ChannelDevice(QJsonObject serialized,DeviceType type) : Device(serialized,type),
    firstChannel(serialized.value(KEY_FIRST_CHANNEL_ID).toInt()),
    numChannel(serialized.value(KEY_NUM_CHANNEL).toInt()),
    universe(serialized.value(KEY_NUM_UNIVERSE).toInt()),
    moduloMode(serialized.value("moduloMode").toBool(false))
{

}

QJsonObject ChannelDevice::serialize()
{
 QJsonObject ret = Device::serialize();
 ret.insert(KEY_FIRST_CHANNEL_ID,getFirstChannel());
 ret.insert(KEY_NUM_CHANNEL,getNumChannels());
 ret.insert(KEY_NUM_UNIVERSE,getUniverse());
 ret.insert("moduloMode",moduloMode);
 return ret;
}

bool ChannelDevice::isSerializable()
{
    return true;
}

int ChannelDevice::getNumChannels()
{
    return numChannel;
}

int ChannelDevice::getFirstChannel()
{
    return firstChannel;
}

QSharedPointer<DeviceState> ChannelDevice::createEmptyState()
{
    return createEmptyChannelState();
}

QSharedPointer<ChannelDeviceState> ChannelDevice::createEmptyChannelState()
{
    QSharedPointer<ChannelDeviceState> ret(new ChannelDeviceState(this));
    return ret;
}

bool ChannelDevice::istChannelDeviceType(Device::DeviceType type)
{
    return (type == Device::Beamer || type == Device::RGB || type == Device::RGBW || type == Device::White);
}

int ChannelDevice::getUniverse() const
{
    return universe;
}

void ChannelDevice::setUniverse(int value)
{
    if(universe < 0)
        throw "universe must be > 0";
    universe = value;
}

void ChannelDevice::setNumChannel(int value)
{
    numChannel = value;
}

void ChannelDevice::setFirstChannel(int value)
{
    firstChannel = value;
}

bool ChannelDevice::getModuloMode() const
{
    return moduloMode;
}

void ChannelDevice::setModuloMode(bool value)
{
    moduloMode = value;
}
