#include "regroupgroupdevice.h"

RegroupGroupDevice::RegroupGroupDevice(QHash<QString, QSharedPointer<Device> > devicesP,
                                       QHash<QString, QSharedPointer<GroupDevice> > groupsP)
    : GroupDevice(""),
      devices(devicesP),
      groups(groupsP)
{
    generateId();
}

QSharedPointer<SelectorBuilder> RegroupGroupDevice::getSelectorBuilder()
{
    throw("should not be called");
    return QSharedPointer<SelectorBuilder>();
}

QHash<QString, QSharedPointer<Device> > RegroupGroupDevice::getChildren()
{
    return devices;
}

QHash<QString, QSharedPointer<GroupDevice> > RegroupGroupDevice::getChildGroups()
{
    return groups;
}

QString RegroupGroupDevice::getGroupType()
{
    return "regroup";
}

QString RegroupGroupDevice::getGroupId()
{
    return generatedId;
}

void RegroupGroupDevice::setDevices(const QHash<QString, QSharedPointer<Device> > &value)
{
    devices = value;
    generateId();
}

void RegroupGroupDevice::setGroups(const QHash<QString, QSharedPointer<GroupDevice> > &value)
{
    groups = value;
    generateId();
}

void RegroupGroupDevice::generateId()
{

    QList<QString> keys = devices.keys();
    keys += groups.keys();
    qSort(keys.begin(),keys.end());

    generatedId = "";
    foreach(QString singleId, keys){
        generatedId += singleId;
    }
    if(generatedId.isEmpty())
        generatedId = "emptyGroup";
}

