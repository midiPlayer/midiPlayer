#include "beamereffectgroupdevice.h"
#include "beamercolorgroupdevice.h"
#include "../beamer/beamereffect.h"
#include "../virtualDeviceManager/select/subgroupselector.h"

BeamerEffectGroupDevice::BeamerEffectGroupDevice(QString devIdP,
        BeamerScene *sceneP,
        QHash<QString, QSharedPointer<Device> > beamersP):
        GroupDevice(devIdP),
        devId(devIdP),
        scene(sceneP),
        beamers(beamersP)
{
    if(!scene->getEffect().isNull())
        connect(sceneP->getEffect().data(),SIGNAL(colorsChanged()),this,SLOT(reloadDevices()));
    reloadDevices();
}

void BeamerEffectGroupDevice::reloadDevices()
{
    colorGroups.clear();

    if(!scene->getEffect().isNull()){
        foreach(QString color, scene->getEffect().data()->getColors()){
            QString nDevId = devId + "-" + color;
            colorGroups.insert(nDevId,
                QSharedPointer<BeamerColorGroupDevice>(new BeamerColorGroupDevice(
                                                           nDevId,
                                                           scene,
                                                           beamers,
                                                           color)));
        }
    }
    emit colorsChanged();
}

QHash<QString, QSharedPointer<Device> > BeamerEffectGroupDevice::getChildren()
{
    return QHash<QString, QSharedPointer<Device> >();//has no children only child groups
}

QHash<QString, QSharedPointer<GroupDevice> > BeamerEffectGroupDevice::getChildGroups()
{
    return colorGroups;
}

QString BeamerEffectGroupDevice::getGroupType()
{
    return GROUP_TYPE_BEAMER_EFFECT;
}

QSharedPointer<SelectorBuilder> BeamerEffectGroupDevice::getSelectorBuilder()
{
    class mySceneBuilder: public SelectorBuilder{
    public:
        mySceneBuilder(){}

        QSharedPointer<Selector> buildSelector(VirtualDeviceManager* parentDevMgr,
                                               WebSocketServer* ws,
                                               QJsonObject serialized){
            return QSharedPointer<SubGroupSelector>(new SubGroupSelector(parentDevMgr,ws, serialized));
        }
    };
    return QSharedPointer<mySceneBuilder>(new mySceneBuilder());
}

QString BeamerEffectGroupDevice::getDisplayName()
{
    return scene->getName();
}
