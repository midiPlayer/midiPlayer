#include "beamercolordevicestate.h"

BeamerColorDeviceState::BeamerColorDeviceState(BeamerColorDevice* deviceP):
    ColorfulDeviceState(),
    color(),
    device(deviceP)
{
    device->addDestListener(this);
}

BeamerColorDeviceState::BeamerColorDeviceState(BeamerColorDeviceState *copy) :
    ColorfulDeviceState(),
    color(copy->color),
    device(copy->device)
{
    device->addDestListener(this);
}

BeamerColorDeviceState::~BeamerColorDeviceState()
{
    if(device != NULL)
        device->rmDestListener(this);
}

QSharedPointer<DeviceState> BeamerColorDeviceState::fusionWith(QSharedPointer<DeviceState> upper,
                                                               DeviceState::FusionType type, float fadeOpacity)
{
    QSharedPointer<BeamerColorDeviceState> ret = QSharedPointer<BeamerColorDeviceState>(new BeamerColorDeviceState(this));
    QSharedPointer<BeamerColorDeviceState> upperBDS = upper.dynamicCast<BeamerColorDeviceState>();
    if(upperBDS.isNull())
        throw("BeamerDeviceState: fusion type mismatch!");
    if(device != upperBDS.data()->device)
        throw("BeamerDeviceState:  device mismatch!");


    ret.data()->color = color.fusionWith(upperBDS.data()->color,fadeOpacity,type);
    return ret;
}

QSharedPointer<DeviceState> BeamerColorDeviceState::fusionAlone(DeviceState::FusionType type, float fadeOpacity)
{
    QSharedPointer<BeamerColorDeviceState> ret = QSharedPointer<BeamerColorDeviceState>(new BeamerColorDeviceState(this));
    if(type == DeviceState::AV){
        ret.data()->multiply(1-fadeOpacity);
    }
    return ret;
}

bool BeamerColorDeviceState::equal(DeviceState *other)
{
    BeamerColorDeviceState *otherBcds  = dynamic_cast<BeamerColorDeviceState*>(other);

    if(otherBcds == NULL)
        return false;

    if(color != otherBcds->color || getOpacity() != otherBcds->getOpacity())
        return false;

    return true;
}

QSharedPointer<DeviceState> BeamerColorDeviceState::clone()
{
    return QSharedPointer<BeamerColorDeviceState>(new BeamerColorDeviceState(this));
}

RGBWColor BeamerColorDeviceState::getColor() const
{
    return color;
}

void BeamerColorDeviceState::setColor(const RGBWColor &value)
{
    color = value;
}

BeamerColorDevice *BeamerColorDeviceState::getDevice() const
{
    return device;
}

float BeamerColorDeviceState::getOpacity() const
{
    return color.getA();
}

void BeamerColorDeviceState::setOpacity(float value)
{
    color.setA(value);
}

void BeamerColorDeviceState::multiply(float percentage)
{
    color.multiplyAlpha(percentage);
}

void BeamerColorDeviceState::beamerColorDeviceDestroyed()
{
    device = NULL;
}

void BeamerColorDeviceState::setRGB(QColor newColor)
{
    setColor(RGBWColor(newColor));
}

void BeamerColorDeviceState::setRGBW(RGBWColor newColor)
{
    setColor(newColor);
}

QSharedPointer<ColorfulDeviceState> BeamerColorDeviceState::copyToColorfulSharedPointer()
{
    return QSharedPointer<BeamerColorDeviceState>(new BeamerColorDeviceState(this));
}

