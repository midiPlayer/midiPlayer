#include "usergroupdevice.h"
#include "../virtualDeviceManager/select/stripegroupselector.h"

#define KEY_SELEKTION "selektion"
UserGroupDevice::UserGroupDevice(VirtualDeviceManager*, WebSocketServer *, QJsonObject serialized) :
    GroupDevice(serialized)
  //selektion(avDev,ws,serialized.value(KEY_SELEKTION).toObject())
{

}

UserGroupDevice::UserGroupDevice(QString name, VirtualDeviceManager *, WebSocketServer *) :
    GroupDevice(name)
  /*selektion(avDev,ws)*/
{

}

UserGroupDevice::UserGroupDevice(QString name):
    GroupDevice(name)
{

}

UserGroupDevice::~UserGroupDevice()
{

}

QJsonObject UserGroupDevice::serialize()
{
    QJsonObject ret = GroupDevice::serialize();
    //ret.insert(KEY_SELEKTION,selektion.serialize());
    return ret;
}

QHash<QString, QSharedPointer<Device> > UserGroupDevice::getChildren()
{
    //return selektion.getDevices();
    return childern;
}

QHash<QString, QSharedPointer<GroupDevice> > UserGroupDevice::getChildGroups()
{
    //return selektion.getGroupDevices();
    return childGroups;
}

QString UserGroupDevice::getGroupType()
{
    return GROUP_TYPE_USER;
}

QSharedPointer<SelectorBuilder> UserGroupDevice::getSelectorBuilder()
{
    class mySceneBuilder: public SelectorBuilder{
    public:
        mySceneBuilder(){}

        QSharedPointer<Selector> buildSelector(VirtualDeviceManager* parentDevMgr,
                                               WebSocketServer* ws,
                                               QJsonObject serialized){
            return QSharedPointer<StripeGroupSelector>(new StripeGroupSelector(ws, parentDevMgr, serialized));
        }
    };
    return QSharedPointer<mySceneBuilder>(new mySceneBuilder());
}
