#ifndef GROUPDEVICE_H
#define GROUPDEVICE_H

#include "../devices/device.h"
#include "../websocketserver.h"
#include "../virtualDeviceManager/select/selectorbuilder.h"
class VirtualDeviceManager;

class GroupDevice : public Serializable
{
public:
    GroupDevice(QString devId);
    GroupDevice(GroupDevice *copy);
    GroupDevice(QJsonObject serialized);

    virtual QSharedPointer<SelectorBuilder> getSelectorBuilder() = 0;

    virtual ~GroupDevice() {}
public:
    virtual QHash<QString, QSharedPointer<Device> > getChildren() = 0;
    QHash<QString, QSharedPointer<Device> > getAllChildren();
    virtual QHash<QString, QSharedPointer<GroupDevice> > getChildGroups() = 0;
    bool isEmpty();
    virtual QString getGroupType() = 0;
    static QSharedPointer<GroupDevice> buildGroup(QJsonObject serialized, VirtualDeviceManager *avDevs, WebSocketServer *ws);
    virtual QString getGroupId();
    virtual QString getDisplayName();

private:
    QString myId;

    // Serializable interface
public:
    QJsonObject serialize();
    class UnknownGroupTypeException: public std::exception{
    public:
      virtual const char* what() const throw()
      {
        return "unimplemented Group Type";
      }
    };
};

#endif // GROUPDEVICE_H
