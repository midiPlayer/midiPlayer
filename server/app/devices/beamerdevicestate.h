#ifndef BEAMERDEVICESTATE_H
#define BEAMERDEVICESTATE_H

#include "devicestate.h"
#include "beamerdevice.h"
#include "../beamer/beamereffectconfig.h"

class BeamerDevice;

class BeamerDeviceState : public DeviceState
{
public:
    BeamerDeviceState(BeamerDevice* deviceP);
    BeamerDeviceState(BeamerDevice* deviceP, BeamerEffectConfig effect);
    BeamerDeviceState(BeamerDeviceState* copy);

    // DeviceState interface
public:
    QSharedPointer<DeviceState> fusionWith(QSharedPointer<DeviceState> upperDeviceState, FusionType type, float);
    QSharedPointer<DeviceState> fusionAlone(FusionType type, float opacity);
    bool equal(DeviceState *other);
    QSharedPointer<DeviceState> clone();

    QList<BeamerEffectConfig> getEffects() const;
    void addEffect(BeamerEffectConfig effect);

    BeamerDevice *getDevice() const;

private:
    QList<BeamerEffectConfig> effects;
    BeamerDevice* device;
};

#endif // BEAMERDEVICESTATE_H
