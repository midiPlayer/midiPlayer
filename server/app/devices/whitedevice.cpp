        #include "../devices/whitedevice.h"
    #include <QJsonObject>
    #define KEY_COLOR_OF_DEVICE "device_color"

    WhiteDevice::WhiteDevice(int firstChannel, int numChannels, int universeP, QString devIdP, bool moduloModeP, QColor colorOfDeviceP, QVector3D pos):
        ChannelDevice(firstChannel, numChannels, universeP, devIdP, Device::White, moduloModeP, pos), colorOfDevice(colorOfDeviceP)
    {
    }

    WhiteDevice::WhiteDevice(QJsonObject serialized) :
        ChannelDevice(serialized,White),
        colorOfDevice(serialized.value(KEY_COLOR_OF_DEVICE).toString("#ffffff"))
    {

    }

    QJsonObject WhiteDevice::serialize()
    {
        QJsonObject ret = ChannelDevice::serialize();
        ret.insert(KEY_COLOR_OF_DEVICE,colorOfDevice.name());
        return ret;
    }

    QColor WhiteDevice::getColorOfDevice()
    {
        return colorOfDevice;
    }

    void WhiteDevice::setColorOfDevice(const QColor &value)
    {
        colorOfDevice = value;
    }
