#ifndef BEAMERDEVICE_H
#define BEAMERDEVICE_H

#include "device.h"
#include "../websocketserverprovider.h"
#include "../beamer/beamermessage.h"

class BeamerDeviceState;

class BeamerDevice : public Device, public WebSocketServerProvider
{
public:
    BeamerDevice(QString devId, WebSocketServer*ws, QVector3D pos);
    BeamerDevice(QJsonObject serialized, WebSocketServer*ws);

    QJsonObject serialize();
    bool isSerializable();
    QSharedPointer<DeviceState> createEmptyState();

    // WebSocketServerProvider interface
public:
    QString getRequestType();
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject, int){}

    void publish(BeamerMessage* msg);
private:
    BeamerMessage lastMsg;
};

#endif // BEAMERDEVICE_H
