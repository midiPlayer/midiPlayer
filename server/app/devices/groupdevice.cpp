#include "../devices/groupdevice.h"
#include "../filteredgroupdevice.h"
#include "../devices/stripegroupdevice.h"
#include "../devices/usergroupdevice.h"

#define KEY_GROUP_TYPE "gType"
#define KEY_GROUT_Id "gid"

GroupDevice::GroupDevice(QString devId) : myId(devId)
{

}

GroupDevice::GroupDevice(GroupDevice *copy) : myId(copy->myId)
{
    qDebug() << "copy";
}

GroupDevice::GroupDevice(QJsonObject serialized) : myId(serialized.value(KEY_GROUT_Id).toString())
{

}

QHash<QString, QSharedPointer<Device> > GroupDevice::getAllChildren()
{
    QHash<QString, QSharedPointer<Device> > ret = getChildren();
    foreach(QSharedPointer<GroupDevice> subGroup,getChildGroups()){
        foreach(QSharedPointer<Device> dev, subGroup.data()->getAllChildren().values())
            ret.insert(dev.data()->getDeviceId(),dev);
    }
    return ret;
}

bool GroupDevice::isEmpty()
{
    return getAllChildren().isEmpty();
}

QSharedPointer<GroupDevice> GroupDevice::buildGroup(QJsonObject serialized,VirtualDeviceManager* avDevs, WebSocketServer *ws)
{
    QString gType = serialized.value(KEY_GROUP_TYPE).toString();
    if(gType == GROUP_TYPE_STRIPE){
        return QSharedPointer<GroupDevice>(new StripeGroupDevice(serialized));
    }
    else if(gType == GROUP_TYPE_USER){
        return QSharedPointer<GroupDevice>(new UserGroupDevice(avDevs,ws,serialized));
    }
    else{
        throw(UnknownGroupTypeException());
    }
}

QString GroupDevice::getGroupId()
{
    return myId;
}

QString GroupDevice::getDisplayName()
{
    return getGroupId();
}


QJsonObject GroupDevice::serialize()
{
    QJsonObject serialized;
    serialized.insert(KEY_GROUP_TYPE,getGroupType());
    serialized.insert(KEY_GROUT_Id,myId);
    return serialized;
}
