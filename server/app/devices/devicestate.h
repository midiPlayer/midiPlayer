#ifndef DEVICESTATE_H
#define DEVICESTATE_H
#include <QSharedPointer>

class Device;

class DeviceState
{
public:
    DeviceState();
    virtual ~DeviceState(){}
    enum FusionType { AV,MAX,MIN,OVERRIDE,Multiply,Subtract,Add};
    virtual QSharedPointer<DeviceState> fusionWith(QSharedPointer<DeviceState> upper, FusionType type, float opacity) = 0;
    virtual QSharedPointer<DeviceState> fusionAlone(FusionType type, float opacity) = 0;
    virtual bool equal(DeviceState *other) = 0;
    virtual void publish() {}
    virtual void publishChanges() {}
    virtual QSharedPointer<DeviceState> clone() = 0;
};

#endif // DEVICESTATE_H
