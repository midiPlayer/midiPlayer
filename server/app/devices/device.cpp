#include "../devices/device.h"
#include <QDebug>
#include "../devices/devicestate.h"
#include "../devices/channeldevice.h"
#include "../devices/whitedevice.h"
#include "../devices/groupdevice.h"
#include "../devices/beamerdevice.h"
#include <QJsonArray>

#define KEY_DEVICE_TYPE "device_type"
#define KEY_DEV_ID "device_id"
#define KEY_DEV_POS_X "device_posx"
#define KEY_DEV_POS_Y "device_posy"
#define KEY_DEV_POS_Z "device_posz"


Device::Device(QString devIdP, Device::DeviceType typeP, QVector3D pos):
    devId(devIdP),type(typeP),position(pos)
{
}

Device::Device(QJsonObject serialized, DeviceType devt) : devId(serialized.value(KEY_DEV_ID).toString()),
    type(devt),
    position(serialized.value(KEY_DEV_POS_X).toDouble(),
             serialized.value(KEY_DEV_POS_Y).toDouble(),
             serialized.value(KEY_DEV_POS_Z).toDouble())
{
}

Device::Device(Device *copy) : devId(copy->devId), type(copy->type), position(copy->position)
{

}



bool Device::deviceEqual(Device *other)
{
    return other->getDeviceId() == getDeviceId();
}



QString Device::getDeviceId() const
{
    return devId;
}

Device::DeviceType Device::getType()
{
    return type;
}

QString Device::getTypeString()
{
    return getTypeString(getType());
}

QVector3D Device::getPosition() const
{
    return position;
}

void Device::setPosition(const QVector3D &value)
{
    position = value;
}


QJsonObject Device::serialize()
{
    QJsonObject ret;

    ret.insert(KEY_DEVICE_TYPE,getTypeString());
    ret.insert(KEY_DEV_ID,getDeviceId());
    ret.insert(KEY_DEV_POS_X,position.x());
    ret.insert(KEY_DEV_POS_Y,position.y());
    ret.insert(KEY_DEV_POS_Z,position.z());
    return ret;
}

QSharedPointer<Device> Device::buildDevice(QJsonObject serialized, WebSocketServer* ws)
{
    DeviceType devT = getTypeForString(serialized.value(KEY_DEVICE_TYPE).toString());
    switch (devT) {
    case RGB:
        return QSharedPointer<Device>(new ChannelDevice(serialized,RGB));
    case RGBW:
        return QSharedPointer<Device>(new ChannelDevice(serialized,RGBW));
    case White:
        return QSharedPointer<Device>(new WhiteDevice(serialized));
    case Beamer:
        return QSharedPointer<BeamerDevice>(new BeamerDevice(serialized, ws));
    default:
        throw("device type can't be deserialized yet!");
    }
}



Device::DeviceType Device::getTypeForString(QString str)
{
    if(str == "white")
        return White;
    else if(str == "rgb")
        return RGB;
    else if(str == "rgbw")
        return RGBW;
    else if(str == "beamer")
        return Beamer;
    else if(str == "beamerColor")
        return BeamerColor;
    else if(str == "musicPlayer")
        return MusicPlayer;
    else
        return Unknown;

}

QString Device::getTypeString(Device::DeviceType type)
{
    switch (type) {
    case White:
        return "white";
    case RGB:
        return "rgb";
    case RGBW:
        return "rgbw";
    case Beamer:
        return "beamer";
    case BeamerColor:
        return "beamerColor";
    case MusicPlayer:
        return "musicPlayer";
    default:
        return "unknown";
    }
}

QString Device::getDisplayName()
{
    return getDeviceId();
}


bool operator==(const Device &a,const Device &b)
{
    return a.getDeviceId() == b.getDeviceId();
}

