#include "beamercolordevice.h"
#include "beamercolordevicestate.h"

BeamerColorDevice::BeamerColorDevice(QString devId, BeamerScene *scene,
        QSharedPointer<BeamerDevice> beamerP,
        QString colorP):
    Device(devId,Device::BeamerColor,beamerP.data()->getPosition()),
    scene(scene),
    beamer(beamerP),
    color(colorP)
{
    static int i = 0;
    debugId = i++;
}

BeamerColorDevice::~BeamerColorDevice()
{
    foreach (BeamerColorDevice::DestructionListener* l, destListener) {
        l->beamerColorDeviceDestroyed();
    }
}

bool BeamerColorDevice::isSerializable()
{
    return false;
}

QSharedPointer<DeviceState> BeamerColorDevice::createEmptyState()
{
    return QSharedPointer<BeamerColorDeviceState>(new BeamerColorDeviceState(this));
}

QSharedPointer<BeamerDevice> BeamerColorDevice::getBeamer() const
{
    return beamer;
}

QString BeamerColorDevice::getColor() const
{
    return color;
}

BeamerScene *BeamerColorDevice::getScene() const
{
    return scene;
}

void BeamerColorDevice::addDestListener(BeamerColorDevice::DestructionListener *l)
{
    destListener.append(l);
}

void BeamerColorDevice::rmDestListener(BeamerColorDevice::DestructionListener *l)
{
    destListener.removeAll(l);
}

QString BeamerColorDevice::getDisplayName()
{
    return beamer.data()->getDisplayName();
}
