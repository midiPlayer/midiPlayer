#include "beamerdevicestate.h"


BeamerDeviceState::BeamerDeviceState(BeamerDevice *deviceP):
    DeviceState(),
    effects(),
    device(deviceP)
{
}

BeamerDeviceState::BeamerDeviceState(BeamerDevice *deviceP, BeamerEffectConfig effect):
DeviceState(),
effects(),
device(deviceP)
{
effects.append(effect);
}

BeamerDeviceState::BeamerDeviceState(BeamerDeviceState *copy):
    DeviceState(),
    effects(copy->getEffects()),
    device(copy->device)
{

}

QSharedPointer<DeviceState> BeamerDeviceState::fusionWith(QSharedPointer<DeviceState> upperDeviceState, DeviceState::FusionType type, float)
{
    QSharedPointer<BeamerDeviceState> upper = upperDeviceState.dynamicCast<BeamerDeviceState>();
    QSharedPointer<BeamerDeviceState> ret = QSharedPointer<BeamerDeviceState>(new BeamerDeviceState(this));
    if(upper.isNull() || upper.data()->getDevice()->getDeviceId() != getDevice()->getDeviceId())
        throw "not compatible";//we can only fusion equal devices
    switch(type){
     case MAX:
        ret.data()->effects = upper.data()->effects + effects;
        break;
    default :
        qDebug() << "this Fusiontype is currently not implemented";
        break;
    }
    return ret.dynamicCast<DeviceState>();
}

QSharedPointer<DeviceState> BeamerDeviceState::fusionAlone(FusionType type, float opacity)
{
    QSharedPointer<BeamerDeviceState> ret = QSharedPointer<BeamerDeviceState>(new BeamerDeviceState(this));
    if(type == AV){
        ret.data()->effects.clear();
        foreach(BeamerEffectConfig effect, effects){
            effect.setOpacity(effect.getOpacity() * opacity);
            ret.data()->effects.append(effect);
        }
    }
    return ret;
}

bool BeamerDeviceState::equal(DeviceState *other)
{
    BeamerDeviceState *otherBds = dynamic_cast<BeamerDeviceState*>(other);
    if(otherBds == NULL)
        return false;
    if(device != otherBds->device)
        return false;
    if(effects != otherBds->effects)
        return false;
    return true;
}

QSharedPointer<DeviceState> BeamerDeviceState::clone()
{
    return QSharedPointer<BeamerDeviceState>(new BeamerDeviceState(this));
}

QList<BeamerEffectConfig> BeamerDeviceState::getEffects() const
{
    return effects;
}

void BeamerDeviceState::addEffect(BeamerEffectConfig effect)
{
    effects.append(effect);
}

BeamerDevice *BeamerDeviceState::getDevice() const
{
    return device;
}

