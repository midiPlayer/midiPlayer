#ifndef BEAMEREFFECTGROUPDEVICE_H
#define BEAMEREFFECTGROUPDEVICE_H

#include "../devices/groupdevice.h"
#include "../scenes/beamerscene.h"
#include "../virtualDeviceManager/select/selectorbuilder.h"

#define GROUP_TYPE_BEAMER_EFFECT "BeamEffectGrp"

class BeamerEffectGroupDevice : public QObject, public GroupDevice
{
Q_OBJECT

public:
    BeamerEffectGroupDevice(QString devIdP, BeamerScene* sceneP,
                            QHash<QString, QSharedPointer<Device> > beamersP);

public slots:
    void reloadDevices();
signals:
    void colorsChanged();

    // GroupDevice interface
public:
    QHash<QString, QSharedPointer<Device> > getChildren();
    QHash<QString, QSharedPointer<GroupDevice> > getChildGroups();
    QString getGroupType();

private:
    QHash<QString, QSharedPointer<GroupDevice> > colorGroups;
    QString devId;
    BeamerScene* scene;
    QHash<QString,QSharedPointer<Device> > beamers;

    // GroupDevice interface
public:
    QSharedPointer<SelectorBuilder> getSelectorBuilder();

    // GroupDevice interface
public:
    QString getDisplayName();

};

#endif // BEAMEREFFECTGROUPDEVICE_H
