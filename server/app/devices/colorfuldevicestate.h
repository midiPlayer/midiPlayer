#ifndef COLORFULDEVICESTATE_H
#define COLORFULDEVICESTATE_H

#include "../rgbwcolor.h"
#include <QColor>
#include "devicestate.h"

class ColorfulDeviceState : public DeviceState
{
public:
    ColorfulDeviceState();
    virtual ~ColorfulDeviceState() {}
    virtual void setRGB(QColor color) = 0;
    virtual void setRGBW(RGBWColor color) = 0;
    void setGrayscale(float gray);
    virtual QSharedPointer<ColorfulDeviceState> copyToColorfulSharedPointer() = 0;
    virtual float getOpacity() const = 0;
    virtual void setOpacity(float value) = 0;
    virtual void multiply(float percentage) = 0;

private:
    float opacity;
};

#endif // COLORFULDEVICESTATE_H
