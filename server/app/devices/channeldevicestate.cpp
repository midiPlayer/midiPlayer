#include "../devices/channeldevicestate.h"
#include <QDebug>
#include <QJsonArray>

#include "../devices/channeldevice.h"
#include "../devices/whitedevice.h"
#include <QSharedPointer>
#include "../devices/devicenotfoundexception.h"
#include "math.h"


#define KEY_DEVICESTATE_DEV_ID "devicestate_dev_id"
#define KEY_DEVICESTATE_CHANNELS "devicestate_channels"
#define KEY_DEVICESTATE_DMX_ID "devicestate_dmx_id"
#define KEY_DEVICESTATE_VALUE "devicestate_value"
#define KEY_DEVICESTATE_OPACITY "devicestate_opacity"


ChannelDeviceState::ChannelDeviceState(ChannelDevice *deviceP) :
    ColorfulDeviceState(),
    Serializable(),
    device(deviceP),
    dmxChannels(),
    opacity(1.0)
{
    for(int i = device->getFirstChannel(); i < device->getFirstChannel() + device->getNumChannels();i++){
        dmxChannels.insert(i,0.0);
    }
}

ChannelDeviceState::ChannelDeviceState(const ChannelDeviceState &state) :
    ColorfulDeviceState(),
    device(state.device),
    dmxChannels(state.dmxChannels),
    opacity(state.opacity)
{ }

ChannelDeviceState::ChannelDeviceState(ChannelDeviceState *state) :
    ColorfulDeviceState(),
    device(state->device),
    dmxChannels(state->dmxChannels),
    opacity(state->opacity)
{ }


ChannelDeviceState::ChannelDeviceState(QJsonObject serialized,VirtualDeviceManager *manager) :
    ColorfulDeviceState(),
    dmxChannels(),
    opacity(serialized.value("opacity").toDouble())
{
    QHash<QString,QSharedPointer<Device> > available = manager->getDevices();
    QString devId = serialized.value(KEY_DEVICESTATE_DEV_ID).toString();
    if(!available.contains(devId))
        throw(DeviceNotFoundException());

    device = (available.value(devId).dynamicCast<ChannelDevice>().data());

    foreach (QJsonValue value, serialized.value(KEY_DEVICESTATE_CHANNELS).toArray()) {
        dmxChannels.insert(value.toObject().value(KEY_DEVICESTATE_DMX_ID).toInt(),value.toObject().value(KEY_DEVICESTATE_VALUE).toDouble());
    }
}

QSharedPointer<DeviceState> ChannelDeviceState::fusionWith(QSharedPointer<DeviceState> upperDeviceState, FusionType type, float fadeOpacity)
{
    QSharedPointer<ChannelDeviceState> upper = upperDeviceState.dynamicCast<ChannelDeviceState>();
    QSharedPointer<ChannelDeviceState> ret = QSharedPointer<ChannelDeviceState>(new ChannelDeviceState(this));
    ret.data()->setOpacity(1.0);
    if(upper.isNull() || upper.data()->getDevice()->getDeviceId() != getDevice()->getDeviceId()
            || this->getNumChannels() != upper.data()->getNumChannels()
            || this->getChannels() != upper.data()->getChannels()
            || device->getUniverse() != upper.data()->getDevice()->getUniverse())
        throw "not compatible";//we can only fusion equal devices
    switch(type){
     case MAX:
        foreach (int channel, getChannels()) {
            float upV = upper.data()->getChannelValue(channel) * upper.data()->getOpacity();
            float loV = getChannelValue(channel) * getOpacity();
            ret.data()->setChannel(channel, upV > loV ? upV : loV);
        }
        break;
     case MIN:
        foreach (int channel, getChannels()) {
            float upV = upper.data()->getChannelValue(channel) * upper.data()->getOpacity();
            float loV = getChannelValue(channel) * getOpacity();
            ret.data()->setChannel(channel, upV < loV ? upV : loV);
        }
        break;
    case OVERRIDE:
        fadeOpacity = upper.data()->getOpacity();//override fadeOpacity and go on with AV
     case AV:
        foreach (int channel, getChannels()) {
            ret.data()->setChannel(channel,upper.data()->getChannelValue(channel)*fadeOpacity+getChannelValue(channel)*(1.0f-fadeOpacity));
        }
        ret.data()->setOpacity(upper.data()->getOpacity() * fadeOpacity + getOpacity() * (1.0 - fadeOpacity));
        break;
    case Multiply:
        foreach (int channel, getChannels()) {
            ret.data()->setChannel(channel,upper.data()->getChannelValue(channel)*getChannelValue(channel)*getOpacity()*upper.data()->getOpacity());
        }
        break;
    case Subtract:
        ret.data()->setOpacity(getOpacity());
        foreach (int channel, getChannels()) {
            float v = getChannelValue(channel);
            v -= upper.data()->getChannelValue(channel) * upper.data()->getOpacity();
            if(v < 0){
                if(device->getModuloMode())
                    v += 1.0f;
                else
                    v = 0;
            }
            ret.data()->setChannel(channel,v);
        }
        break;
    case Add:
        ret.data()->setOpacity(getOpacity());
        foreach (int channel, getChannels()) {
            float v = getChannelValue(channel);
            v += upper.data()->getChannelValue(channel) * upper.data()->getOpacity();
            if(v > 1){
                if(device->getModuloMode())
                    v -= 1.0f;
                else
                    v = 1;
            }
            ret.data()->setChannel(channel,v);
        }
        break;
    default :
        qDebug() << "this Fusiontype is currently not implemented";
        break;
    }
    return ret.dynamicCast<DeviceState>();
}

QSharedPointer<DeviceState> ChannelDeviceState::fusionAlone(DeviceState::FusionType type, float fadeOpacity)
{
    QSharedPointer<ChannelDeviceState> ret = copyToSharedPointer();
    if(type == DeviceState::AV){
        ret.data()->multiply(1-fadeOpacity);
    }
    return ret;
}

void ChannelDeviceState::setChannel(int channel, float value)
{
    dmxChannels.remove(channel);
    dmxChannels.insert(channel,value);
}

/**
 * @brief ChannelDeviceState::getChannelValue
 * @param channel
 * @param noOpacity if set, opacity will be handled as black.
 * @return
 */
float ChannelDeviceState::getChannelValue(int channel, bool noOpacity)
{
    if(noOpacity)
        return dmxChannels.value(channel)*getOpacity();
    return dmxChannels.value(channel);
}

QList<int> ChannelDeviceState::getChannels() const
{
    return dmxChannels.keys();
}

int ChannelDeviceState::getNumChannels()
{
    return dmxChannels.size();
}

int ChannelDeviceState::getFirstChannel()
{
    return dmxChannels.keys().first();
}

QJsonObject ChannelDeviceState::serialize()
{
    QJsonObject ret;
    ret.insert(KEY_DEVICESTATE_DEV_ID,getDevice()->getDeviceId());
    QJsonArray channelsJosn;
    foreach (int key, dmxChannels.keys()) {
        QJsonObject channelJson;
        channelJson.insert(KEY_DEVICESTATE_DMX_ID,key);
        channelJson.insert(KEY_DEVICESTATE_VALUE,dmxChannels.value(key));
        channelsJosn.append(channelJson);
    }
    ret.insert(KEY_DEVICESTATE_CHANNELS,channelsJosn);
    ret.insert(KEY_DEVICESTATE_OPACITY,getOpacity());
    return ret;
}

QJsonArray ChannelDeviceState::getClientJson()
{
    QJsonArray channelsJosn;
    foreach (int key, dmxChannels.keys()) {
        channelsJosn.append(dmxChannels.value(key));
    }
    return channelsJosn;
}

void ChannelDeviceState::setClientJson(QJsonArray json)
{
    if(json.size() == dmxChannels.size()){
        for(int i = 0; i < dmxChannels.size() ; i++){
            setChannel(i+getFirstChannel(),json.at(i).toDouble());
        }
    }
    else{
        qDebug() << "ERROR (Devicestate): wrong channel count;";
    }
}

ChannelDeviceState ChannelDeviceState::operator *(float percentage)
{
    ChannelDeviceState ret = this->copy();
    ret.multiply(percentage);
    return ret;
}

void ChannelDeviceState::setRGB(QColor color)
{
    int first = getFirstChannel();
    if(device->getType() == Device::RGB || device->getType() == Device::RGBW || device->getType() == Device::Beamer){
        setChannel(first+0,color.redF());
        setChannel(first+1,color.greenF());
        setChannel(first+2,color.blueF());
        /*if(device->getType() == Device::RGBW)
            setChannel(first +3,color.toHsv().valueF() * color.toHsv().saturationF());*/
    }
    if(device->getType() == Device::White)
        setChannel(first,(color.redF() + color.greenF() + color.blueF()) / 3.0);
}

void ChannelDeviceState::setRGBW(RGBWColor color)
{
    int first = getFirstChannel();
    if(device->getType() == Device::RGB || device->getType() == Device::RGBW || device->getType() == Device::Beamer){
        setChannel(first+0,color.getR());
        setChannel(first+1,color.getG());
        setChannel(first+2,color.getB());
        if(device->getType() == Device::RGBW)
            setChannel(first +3,color.getW());
    }
    if(device->getType() == Device::White){
         WhiteDevice* wDev = dynamic_cast<WhiteDevice*>(device);
         if(wDev != 0){
             QColor devCol = wDev->getColorOfDevice();
             float per = 0;
             per += 1.0/3.0 * devCol.redF() * color.getR();
             per += 1.0/3.0 * devCol.greenF() * color.getG();
             per += 1.0/3.0 * devCol.blueF() * color.getB();
             if(color.getW() > per)
                 per = color.getW();
             setChannel(first,per);
         }

    }
}

ChannelDeviceState ChannelDeviceState::copy()
{
    return ChannelDeviceState(this);
}

QSharedPointer<ChannelDeviceState> ChannelDeviceState::copyToSharedPointer()
{
    return QSharedPointer<ChannelDeviceState>(new ChannelDeviceState(this));
}

bool ChannelDeviceState::equal(DeviceState *other)
{
    ChannelDeviceState *otherCDS = dynamic_cast<ChannelDeviceState*>(other);
    if(other == NULL)
        return false;
    return (dmxChannels == otherCDS->dmxChannels && getOpacity() == otherCDS->getOpacity());
}

void ChannelDeviceState::tryImport(ChannelDeviceState other)
{
    int numChannels = dmxChannels.size();
    int firstChannel = getFirstChannel();
    dmxChannels.clear();
    int count = 0;
    foreach (float v, other.dmxChannels.values()) {
        if(count >= numChannels)
            break;
        dmxChannels.insert(firstChannel+count,v);
        count ++;
    }
}

ChannelDevice *ChannelDeviceState::getDevice()
{
    return device;
}

float ChannelDeviceState::getOpacity() const
{
    return opacity;
}

void ChannelDeviceState::setOpacity(float value)
{
    opacity = value;
}

QSharedPointer<DeviceState> ChannelDeviceState::clone()
{
    return QSharedPointer<DeviceState>(new ChannelDeviceState(this));
}

QSharedPointer<ColorfulDeviceState> ChannelDeviceState::copyToColorfulSharedPointer()
{
    return copyToSharedPointer();
}

void ChannelDeviceState::multiply(float percentage)
{
    opacity *= percentage;
    if(opacity > 1.0)
        opacity = 1.0;
    else if(opacity < 0)
        opacity = 0;
}


