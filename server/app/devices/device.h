#ifndef DEVICE_H
#define DEVICE_H

#include <QList>
#include <QFile>
#include <QMap>
#include <QVector3D>
#include "devicestate.h"
#include "../serializable.h"
#include "../websocketserver.h"

class ChannelDeviceState;

class Device : public Serializable
{
public:
    enum DeviceType{White,RGB,RGBW,Beamer,Unknown,MusicPlayer,BeamerColor};

    Device(QString devIdP,DeviceType typeP,QVector3D pos = QVector3D(0,0,0));
    Device(QJsonObject serialized, DeviceType devt);
    Device(Device *copy);
    virtual ~Device() {}

    //static QHash<QString, QSharedPointer<Device> > deserializeDevices(QJsonArray serialized);
    //static QJsonArray serializeDevices(QList<Device> devices);
    bool deviceEqual(Device *other);
    static QMap<int,float> toLagacy(QList<Device> devices);
    QString getDeviceId() const;
    DeviceType getType();
    QString getTypeString();
    QVector3D getPosition() const;
    void setPosition(const QVector3D &value);

    QJsonObject serialize();
    virtual bool isSerializable() = 0;

    static QSharedPointer<Device> buildDevice(QJsonObject serialized, WebSocketServer *ws);

    virtual QSharedPointer<DeviceState> createEmptyState() = 0;

    static DeviceType getTypeForString(QString str);
    static QString getTypeString(DeviceType type);
    virtual QString getDisplayName();

private:
    QString devId;
    DeviceType type;
    QVector3D position;

};
//QDebug operator<<(QDebug dbg, const Device &type);
bool operator==( const Device &a, const Device &b);
#endif // DEVICE_H
