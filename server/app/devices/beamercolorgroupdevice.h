#ifndef BEAMERCOLORGROUPDEVICE_H
#define BEAMERCOLORGROUPDEVICE_H

#include "groupdevice.h"
#include "../beamer/beamereffect.h"
#include "../scenes/beamerscene.h"

#define GROUP_TPYE_BEAMER_COLOR "BeamColGrp"

class BeamerColorGroupDevice : public GroupDevice
{
public:
    BeamerColorGroupDevice(
            QString devId,
            BeamerScene* scene,
            QHash<QString, QSharedPointer<Device> > beamers,
            QString colorP);

    // GroupDevice interface
public:
    QHash<QString, QSharedPointer<Device> > getChildren();
    QHash<QString, QSharedPointer<GroupDevice> > getChildGroups();
    QString getGroupType();
    QSharedPointer<SelectorBuilder> getSelectorBuilder();
    QString color;

private:
    QHash<QString, QSharedPointer<Device> > children;


    // GroupDevice interface
public:
    QString getDisplayName();

};

#endif // BEAMERCOLORGROUPDEVICE_H
