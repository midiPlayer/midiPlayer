#ifndef BEAMERCOLORDEVICE_H
#define BEAMERCOLORDEVICE_H

#include "../devices/device.h"
#include "../devices/beamerdevice.h"
#include <QSharedPointer>
#include "../scenes/beamerscene.h"
class BeamerDevice;

class BeamerColorDevice : public Device
{
public:
    BeamerColorDevice(QString devId,
                      BeamerScene* scene,
                      QSharedPointer<BeamerDevice> beamerP,
                      QString colorP);

    ~BeamerColorDevice();


    // Device interface
public:
    bool isSerializable();
    QSharedPointer<DeviceState> createEmptyState();

    QSharedPointer<BeamerDevice> getBeamer() const;

    QString getColor() const;

    BeamerScene *getScene() const;

    class DestructionListener{
    public:
        virtual void beamerColorDeviceDestroyed() = 0;
    };

    void addDestListener(BeamerColorDevice::DestructionListener *l);
    void rmDestListener(BeamerColorDevice::DestructionListener *l);

private:
    BeamerScene* scene;
    QSharedPointer<BeamerDevice> beamer;
    QString color;
    int debugId;

    QList<BeamerColorDevice::DestructionListener*> destListener;

    // Device interface
public:
    QString getDisplayName();
};

#endif // BEAMERCOLORDEVICE_H
