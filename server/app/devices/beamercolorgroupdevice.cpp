#include "beamercolorgroupdevice.h"
#include "../devices/beamerdevice.h"
#include "../devices/beamercolordevice.h"

BeamerColorGroupDevice::BeamerColorGroupDevice(QString devId,
        BeamerScene *scene,
        QHash<QString, QSharedPointer<Device> > beamers,
        QString colorP):
    GroupDevice(devId),
    color(colorP)
{
    foreach(QSharedPointer<Device> dev, beamers){
        QSharedPointer<BeamerDevice> beamer = dev.dynamicCast<BeamerDevice>();
        if(beamer.isNull())
            continue;

        QString nDevId = devId + beamer.data()->getDeviceId();
        children.insert(nDevId,QSharedPointer<BeamerColorDevice>(
                    new BeamerColorDevice(nDevId,
                                          scene,beamer,colorP)));
    }
}

QHash<QString, QSharedPointer<Device> > BeamerColorGroupDevice::getChildren()
{
    return children;
}

QHash<QString, QSharedPointer<GroupDevice> > BeamerColorGroupDevice::getChildGroups()
{
    return QHash<QString, QSharedPointer<GroupDevice> >();
}

QString BeamerColorGroupDevice::getGroupType()
{
    return GROUP_TPYE_BEAMER_COLOR;
}

QSharedPointer<SelectorBuilder> BeamerColorGroupDevice::getSelectorBuilder()
{
    class mySceneBuilder: public SelectorBuilder{
    public:
        mySceneBuilder(){}

        QSharedPointer<Selector> buildSelector(VirtualDeviceManager* parentDevMgr,
                                               WebSocketServer* ws,
                                               QJsonObject serialized){
            return QSharedPointer<ManualSelector>(new ManualSelector(parentDevMgr,ws, serialized));
        }
    };
    return QSharedPointer<mySceneBuilder>(new mySceneBuilder());
}

QString BeamerColorGroupDevice::getDisplayName()
{
    return color;
}
