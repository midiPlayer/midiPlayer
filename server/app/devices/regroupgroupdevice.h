#ifndef REGROUPGROUPDEVICE_H
#define REGROUPGROUPDEVICE_H

#include "groupdevice.h"

class RegroupGroupDevice : public GroupDevice
{
public:
    RegroupGroupDevice(QHash<QString, QSharedPointer<Device> > devicesP, QHash<QString, QSharedPointer<GroupDevice> > groupsP);

    // GroupDevice interface
public:
    QSharedPointer<SelectorBuilder> getSelectorBuilder();
    QHash<QString, QSharedPointer<Device> > getChildren();
    QHash<QString, QSharedPointer<GroupDevice> > getChildGroups();
    QString getGroupType();
    QString getGroupId();

    void setDevices(const QHash<QString, QSharedPointer<Device> > &value);
    void setGroups(const QHash<QString, QSharedPointer<GroupDevice> > &value);

private:
    QString generatedId;
    QHash<QString, QSharedPointer<Device> > devices;
    QHash<QString, QSharedPointer<GroupDevice> > groups;
    void generateId();

    // GroupDevice interface
public:
    bool isEmpty();
};

#endif // REGROUPGROUPDEVICE_H
