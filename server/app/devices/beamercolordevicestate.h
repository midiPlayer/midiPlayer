#ifndef BEAMERCOLORDEVICESTATE_H
#define BEAMERCOLORDEVICESTATE_H

#include "../devices/devicestate.h"
#include "../rgbwcolor.h"
#include "../beamer/beamereffect.h"
#include "../devices/beamercolordevice.h"
#include "colorfuldevicestate.h"

class BeamerColorDeviceState : public ColorfulDeviceState, public BeamerColorDevice::DestructionListener
{
public:
    BeamerColorDeviceState(BeamerColorDevice *deviceP);
    BeamerColorDeviceState(BeamerColorDeviceState* copy);
    ~BeamerColorDeviceState();

    // DeviceState interface
public:
    QSharedPointer<DeviceState> fusionWith(QSharedPointer<DeviceState> upper, FusionType type, float fadeOpacity);
    QSharedPointer<DeviceState> fusionAlone(FusionType type, float fadeOpacity);
    bool equal(DeviceState *other);
    QSharedPointer<DeviceState> clone();

    RGBWColor getColor() const;
    void setColor(const RGBWColor &value);

private:
    RGBWColor color;
    BeamerColorDevice* device;



    // ColorfulDeviceState interface
public:
    void setRGB(QColor newColor);
    void setRGBW(RGBWColor newColor);


    // ColorfulDeviceState interface
public:
    QSharedPointer<ColorfulDeviceState> copyToColorfulSharedPointer();
    BeamerColorDevice *getDevice() const;

    // ColorfulDeviceState interface
public:
    float getOpacity() const;
    void setOpacity(float value);
    void multiply(float percentage);

    // DestructionListener interface
public:
    void beamerColorDeviceDestroyed();
};

#endif // BEAMERCOLORDEVICESTATE_H
