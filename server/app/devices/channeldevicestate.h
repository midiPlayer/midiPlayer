#ifndef CHANNELDEVICESTATE_H
#define CHANNELDEVICESTATE_H
#include <QMap>
#include "../serializable.h"
#include "../devices/devicestate.h"
#include "../virtualDeviceManager/virtualdevicemanager.h"
#include <exception>
#include <QColor>
#include "../rgbwcolor.h"
#include "colorfuldevicestate.h"

class ChannelDevice;

class ChannelDeviceState : public ColorfulDeviceState, public Serializable
{

public:

    ChannelDeviceState(ChannelDevice *deviceP);
    ChannelDeviceState(const ChannelDeviceState &state);
    ChannelDeviceState(ChannelDeviceState *state);
    ChannelDeviceState(QJsonObject serialized,VirtualDeviceManager *manager);
    ChannelDevice *device;
    QMap<int,float> dmxChannels;
    QSharedPointer<DeviceState> fusionWith(QSharedPointer<DeviceState> upperDeviceState, FusionType type, float fadeOpacity);
    QSharedPointer<DeviceState> fusionAlone(FusionType type, float fadeOpacity);

    void setChannel(int channel,float value);
    float getChannelValue(int channel, bool noOpacity = false);
    QList<int> getChannels() const;
    int getNumChannels();
    int getFirstChannel();
    QJsonObject serialize();
    QJsonArray getClientJson();
    void setClientJson(QJsonArray json);
    ChannelDeviceState operator *(float percentage);

    void setRGB(QColor color);
    void setRGBW(RGBWColor color);

    ChannelDeviceState copy();
    QSharedPointer<ChannelDeviceState> copyToSharedPointer();

    bool equal(DeviceState *other);

    void tryImport(ChannelDeviceState other);

    ChannelDevice* getDevice();

private:
    float opacity;

    // DeviceState interface
public:
    QSharedPointer<DeviceState> clone();

    // ColorfulDeviceState interface
public:
    QSharedPointer<ColorfulDeviceState> copyToColorfulSharedPointer();

    // ColorfulDeviceState interface
public:

    void multiply(float percentage);
    float getOpacity() const;
    void setOpacity(float value);
};

#endif // DEVICESTATE_H
