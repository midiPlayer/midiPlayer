#ifndef USERGROUPDEVICE_H
#define USERGROUPDEVICE_H

#include "../devices/groupdevice.h"
#include "../virtualDeviceManager/select/selectvirtualdevicemanager.h"

#define GROUP_TYPE_USER "userDefined"

class UserGroupDevice : public GroupDevice
{
public:
    UserGroupDevice(VirtualDeviceManager*, WebSocketServer*, QJsonObject serialized);
    UserGroupDevice(QString name, VirtualDeviceManager*, WebSocketServer* ws);
    UserGroupDevice(QString name);
    ~UserGroupDevice();
    // Serializable interface

public:
    QJsonObject serialize();
    QHash<QString, QSharedPointer<Device> > childern;
    QHash<QString, QSharedPointer<GroupDevice> > childGroups;

    // GroupDevice interface
public:
    QHash<QString, QSharedPointer<Device> > getChildren();
    QHash<QString, QSharedPointer<GroupDevice> > getChildGroups();
    QString getGroupType();
    QSharedPointer<SelectorBuilder> getSelectorBuilder();
};

#endif // USERGROUPDEVICE_H
