#ifndef WHITEDEVICE_H
#define WHITEDEVICE_H
#include "../devices/channeldevice.h"
#include <QColor>
class WhiteDevice : public ChannelDevice
{
public:
    WhiteDevice(int firstChannel, int numChannels, int universeP, QString devIdP,
                bool moduloModeP,
                QColor colorOfDeviceP = QColor(255,255,255),
                QVector3D pos = QVector3D(0,0,0));
    WhiteDevice(QJsonObject serialized);
    QJsonObject serialize();
    QColor getColorOfDevice();
    void setColorOfDevice(const QColor &value);

private:
    QColor colorOfDevice;
};

#endif // WHITEDEVICE_H
