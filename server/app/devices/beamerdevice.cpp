#include "beamerdevice.h"
#include "beamerdevicestate.h"
#include "../beamer/beamermessage.h"

#include <QJsonArray>

BeamerDevice::BeamerDevice(QString devId, WebSocketServer *ws, QVector3D pos) :
    Device(devId,Device::Beamer,pos),
    WebSocketServerProvider(ws)
{

}

BeamerDevice::BeamerDevice(QJsonObject serialized, WebSocketServer *ws):
    Device(serialized,Device::Beamer),
    WebSocketServerProvider(ws)
{

}

QJsonObject BeamerDevice::serialize()
{
    QJsonObject ret = Device::serialize();

    return ret;
}

bool BeamerDevice::isSerializable()
{
    return true;
}

QSharedPointer<DeviceState> BeamerDevice::createEmptyState()
{
    return QSharedPointer<BeamerDeviceState>(new BeamerDeviceState(this));
}

QString BeamerDevice::getRequestType()
{
    return "beamerDevice";
}

void BeamerDevice::clientRegistered(QJsonObject, int clientId)
{
    if(!lastMsg.isEmpty())
        sendMsg(lastMsg.getMessage(),clientId,true);
}

void BeamerDevice::publish(BeamerMessage* msg)
{
    BeamerMessage diffMsg = msg->getDiffMsg(&lastMsg);
    if(!diffMsg.isEmpty())
        sendMsg(diffMsg.getMessage(),true);
    lastMsg = BeamerMessage(msg);
}
