#include "../devices/stripegroupdevice.h"
#include <QJsonArray>
#include "../devices/channeldevice.h"
#include "../virtualDeviceManager/select/stripegroupselector.h"

#define KEY_NUM "numDev"
#define KEY_FIRST_CHANNEL "firstC"
#define KEY_UNIVERSE "universe"
#define KEY_DEV_TYPES "devTypes"
#define KEY_DEV_POS "positions"
#define KEY_DEV_POS_INDEX "index"
#define KEY_DEV_POS_X "x"
#define KEY_DEV_POS_Y "y"
#define KEY_DEV_POS_Z "z"

StripeGroupDevice::StripeGroupDevice(QString devId, int numDev, int firstC, int univ, Device::DeviceType devT, QMap<int, QVector3D> devPos) :
    GroupDevice(devId),
    numDevices(numDev),
    firstChannel(firstC),
    universe(univ),
    devTypes(devT),
    positions(devPos)
{
    generateChildren();
}

QMap<int, QVector3D> StripeGroupDevice::deSerializedPositions(QJsonArray positionsJson)
{
    QMap<int, QVector3D> ret;
    foreach (QJsonValue v, positionsJson) {
        QJsonObject posJson = v.toObject();
        ret.insert(posJson.value(KEY_DEV_POS_INDEX).toInt(),
                         QVector3D(
                             posJson.value(KEY_DEV_POS_X).toDouble(),
                             posJson.value(KEY_DEV_POS_Y).toDouble(),
                             posJson.value(KEY_DEV_POS_Z).toDouble()));
    }
    return ret;
}

StripeGroupDevice::StripeGroupDevice(QJsonObject serialized) :
    GroupDevice(serialized),
    numDevices(serialized.value(KEY_NUM).toInt()),
    firstChannel(serialized.value(KEY_FIRST_CHANNEL).toInt()),
    universe(serialized.value(KEY_UNIVERSE).toInt()),
    devTypes(Device::getTypeForString(serialized.value(KEY_DEV_TYPES).toString())),
    positions()
{
    positions = deSerializedPositions(serialized.value(KEY_DEV_POS).toArray());
    generateChildren();
}

QJsonArray StripeGroupDevice::serializePositions()
{
    QJsonArray posJson;
    foreach(int index, positions.keys()){
        QJsonObject p;
        QVector3D vect = positions.value(index);
        p.insert(KEY_DEV_POS_INDEX,index);
        p.insert(KEY_DEV_POS_X,vect.x());
        p.insert(KEY_DEV_POS_Y,vect.y());
        p.insert(KEY_DEV_POS_Z,vect.z());
        posJson.append(p);
    }

    return posJson;
}

QJsonObject StripeGroupDevice::serialize()
{
    QJsonObject serialize = GroupDevice::serialize();
    serialize.insert(KEY_NUM,numDevices);
    serialize.insert(KEY_FIRST_CHANNEL,firstChannel);
    serialize.insert(KEY_UNIVERSE,universe);
    serialize.insert(KEY_DEV_TYPES,Device::getTypeString(devTypes));

    QJsonArray posJson = serializePositions();
    serialize.insert(KEY_DEV_POS,posJson);
    return serialize;
}

bool StripeGroupDevice::isSerializable()
{
    return true;
}

QHash<QString, QSharedPointer<Device> > StripeGroupDevice::getChildren()
{
    return children;
}

QHash<QString, QSharedPointer<GroupDevice> > StripeGroupDevice::getChildGroups()
{
    return QHash<QString, QSharedPointer<GroupDevice> >();// a stripe has no child groups
}

QSharedPointer<SelectorBuilder> StripeGroupDevice::getSelectorBuilder()
{
    class mySceneBuilder: public SelectorBuilder{
    public:
        mySceneBuilder(){}

        QSharedPointer<Selector> buildSelector(VirtualDeviceManager* parentDevMgr,
                                               WebSocketServer* ws,
                                               QJsonObject serialized){
            return QSharedPointer<StripeGroupSelector>(new StripeGroupSelector(ws, parentDevMgr, serialized));
        }
    };
    return QSharedPointer<mySceneBuilder>(new mySceneBuilder());
}

void StripeGroupDevice::setPositions(const QMap<int, QVector3D> &value)
{
    positions = value;
}

int StripeGroupDevice::getNumChannel()
{
    int numChannel = 0;
    if(getDevTypes() == Device::RGB)
        numChannel = getNumDevices() * 3;
    else if(getDevTypes() == Device::RGBW)
        numChannel = getNumDevices() * 4;
    else
        throw("unimlemented device tyüe for stripe");
    return numChannel;
}

Device::DeviceType StripeGroupDevice::getDevTypes() const
{
    return devTypes;
}

void StripeGroupDevice::setDevTypes(const Device::DeviceType &value)
{
    devTypes = value;
    generateChildren();
}

QMap<int, QVector3D> StripeGroupDevice::getPositions() const
{
    return positions;
}

int StripeGroupDevice::getNumDevices() const
{
    return numDevices;
}

void StripeGroupDevice::setNumDevices(int value)
{
    numDevices = value;
    generateChildren();
}

int StripeGroupDevice::getFirstChannel() const
{
    return firstChannel;
}

void StripeGroupDevice::setFirstChannel(int value)
{
    firstChannel = value;
    generateChildren();
}

void StripeGroupDevice::setUniverse(int value)
{
    universe = value;
    generateChildren();
}

int StripeGroupDevice::getUniverse() const
{
    return universe;
}

QVector3D StripeGroupDevice::interpolatePosition(int index)
{
    if(positions.size() == 0)
        return QVector3D(0,0,0);
    if(positions.size() == 1)
        return positions.value(0);

    int ai = -1;
    int bi = -1;

    foreach(int i, positions.keys()){
        ai = bi;
        bi = i;
        if(i > index && ai != -1)
            break;
    }
    QVector3D a = positions.value(ai);
    QVector3D b = positions.value(bi);
    return a + ((b-a) * (((float)index - (float)ai)/((float)bi-(float)ai)));
}

void StripeGroupDevice::generateChildren()
{
    children.clear();
    for(int i = 0; i < numDevices; i++){
        QVector3D pos = interpolatePosition(i);
        QString id = getGroupId() + "#" + QString::number(i);
        switch (devTypes) {
        case Device::RGB:
            children.insert(id, QSharedPointer<Device>(new ChannelDevice(firstChannel + i * 3,3,universe,id,devTypes,false,pos)));
            break;
        case Device::RGBW:
            children.insert(id, QSharedPointer<Device>(new ChannelDevice(firstChannel + i * 4,4,universe,id,devTypes,false,pos)));
            break;
        default:
            throw("device type not supported");
            break;
        }
    }
}

QString StripeGroupDevice::getGroupType()
{
 return "stripe";
}
