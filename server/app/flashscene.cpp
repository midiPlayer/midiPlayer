#include "flashscene.h"
#include "websocketserver.h"
#include <algorithm>
#include "devices/channeldevicestate.h"
#include "math.h"
#include "devices/groupdevice.h"


#define KEY_TRIGGER "trigger"
#define KEY_COLOR "color"
#define KEY_DEVS "devices"

#define KEY_NUM_CHANGING_DEVS "changingDevices"
#define KEY_TIME_DIFF "timeDiff"
#define KEY_SAME_COLOR "sameColor"

FlashScene::FlashScene(WebSocketServer* ws, AudioProcessor *jackP, VirtualDeviceManager *manager,
                       PlanedTriggerHandler *triggerH,
                       QString name, QJsonObject serialized) : Scene(name,serialized),
    WebSocketServerProvider(ws),
    trigger(ws,jackP,triggerH),
    filterVDevManager(manager,{Device::BeamerColor,Device::RGB,Device::RGBW,Device::White}),
    selectVDevMgr(&filterVDevManager,ws, serialized.value(KEY_DEVS).toObject()),
    UngroupVDevMgr(&selectVDevMgr),
    randomDevs(&selectVDevMgr),
    flashEnabled(false),
    flashState(),
    time(),
    fadeIn(ws,this,serialized.value("fadeIn").toDouble(0)),
    fadeOut(ws,this,serialized.value("fadeOut").toDouble(0)),
    interrupt(ws,this,serialized.value("interrupt").toBool(true)),
    relativeDuration(ws,this,serialized.value("relativeDuration").toBool(false)),
    flashDuration(ws,this,serialized.value("flashDuration").toDouble(0.2)),
    calculatedDuration(20),
    beatSpeed(INT_MAX),
    timePer(0.0f),
    colorButton(ws),
    numChangingDevices(0),
    timeDiff(0),
    sameColor(true),
    timeDiffs(),
    prevInj(&UngroupVDevMgr)
{

    colorButton.setPreviewInj(&prevInj);
    trigger.triggerConfig.insert(Trigger::BEAT);
    connect(&trigger,SIGNAL(trigger()),this,SLOT(triggered()));
    connect(&colorButton,SIGNAL(colorChanged()),this,SLOT(reloadColor()));
    if(serialized.length() > 0){
        if(serialized.contains(KEY_COLOR))
            colorButton.loadSerialized(serialized.value(KEY_COLOR).toObject());
        if(serialized.contains(KEY_TRIGGER))
            trigger.loadSerialized(serialized.value(KEY_TRIGGER).toObject());
        else
            trigger.triggerConfig.insert(Trigger::BEAT);


        numChangingDevices = serialized.value(KEY_NUM_CHANGING_DEVS).toInt(numChangingDevices);
        sameColor = serialized.value(KEY_SAME_COLOR).toBool(sameColor);
        timeDiff = serialized.value(KEY_TIME_DIFF).toDouble(timeDiff);
    }


    connect(&UngroupVDevMgr,SIGNAL(virtualDevicesChanged()),this,SLOT(reloadDevices()));

    reloadDevices();
}

void FlashScene::reloadDevices()
{
    QJsonObject config;
    config.insert("numDevs",selectVDevMgr.getDevices().count() + selectVDevMgr.getGroupDevices().count());
    sendMsg(config,true);

    reloadColor();
}

void FlashScene::reloadColor()
{
    if(colorButton.getColors().length() == 0)
        return;

    QColor color = colorButton.getRandomDiffrentColor();

    QHash<QString,QSharedPointer<Device> > avDevs = UngroupVDevMgr.getDevices();
    flashState.clear();
    timeDiffs.clear();
    QList<QString> choosenDevs = randomDevs.choose(numChangingDevices);

    foreach(QString devId, selectVDevMgr.getDevices().keys()){
        timeDiffs.insert(devId,(timeDiff == 0 ? 0 :random() % timeDiff));
    }
    foreach (QSharedPointer<GroupDevice> group, selectVDevMgr.getGroupDevices()) {
        int timeD = timeDiff == 0 ? 0 :random() % timeDiff;
        foreach(QString cid, group.data()->getAllChildren().keys())
            timeDiffs.insert(cid,timeD);
    }

    foreach(QSharedPointer<Device> dev, avDevs.values() ){
        QSharedPointer<DeviceState> state = dev.data()->createEmptyState();

        QSharedPointer<ColorfulDeviceState> cDevState = state.dynamicCast<ColorfulDeviceState>();
        if(choosenDevs.contains(dev.data()->getDeviceId())){
            cDevState.data()->setRGB(color);
            if(!sameColor)
                color = colorButton.getRandomDiffrentColor();
        }
        else{
            cDevState.data()->setOpacity(0);//not chosen --> transparent
        }
        flashState.insert(dev.data()->getDeviceId(),state);
    }
}


float FlashScene::getPercentage(int elapsed)
{
    float in = fadeIn.val();
    float out = fadeOut.val();
    float sum = in + out;
    if(sum > 1){
        in = in / sum;
        out = out/ sum;
    }

    float percentage = 0;
    if(elapsed < 0)
    {
        percentage = 0;
    }
    else if(elapsed < calculatedDuration*in){//fading in
        percentage = std::min(elapsed / (calculatedDuration*in),1.0f);
    }
    else if(elapsed < calculatedDuration - calculatedDuration*out){ //main flash
        percentage = 1;
    }
    else if(elapsed < calculatedDuration){ //fading out
        percentage = std::min(1-((elapsed - (calculatedDuration - calculatedDuration*out)) /(calculatedDuration*out)),1.0f);
    }
    return percentage;
}

QMap<QString, QSharedPointer<DeviceState> > FlashScene::getDeviceState()
{
    int elapsed = time.elapsed();

    //output
    QMap<QString, QSharedPointer<DeviceState> > ret;
    foreach (QString devId, flashState.keys()) {
        QSharedPointer<DeviceState> state = flashState.value(devId);
        QSharedPointer<ColorfulDeviceState> cDevState = state.dynamicCast<ColorfulDeviceState>();
        float percentage = 0;
        if(flashEnabled){
            percentage = getPercentage(elapsed - timeDiffs.value(devId));
        }
        QSharedPointer<ColorfulDeviceState> copy = cDevState.data()->copyToColorfulSharedPointer();
        copy.data()->multiply(percentage);
        ret.insert(devId,copy);
    }

    if(elapsed > calculatedDuration + timeDiff)
        flashEnabled = false;

    return prevInj.injektDeviceState(ret);
}



void FlashScene::start()
{
    trigger.start();
}

void FlashScene::stop()
{
    trigger.stop();
}

void FlashScene::clientRegistered(QJsonObject, int id)
{
    QJsonObject replay;
    replay.insert("trigger",trigger.providerId);
    replay.insert("fadeIn",fadeIn.providerId);
    replay.insert("fadeOut",fadeOut.providerId);
    replay.insert("interrupt",interrupt.providerId);
    replay.insert("duration",flashDuration.providerId);
    replay.insert("relativeDuration",relativeDuration.providerId);
    replay.insert("color",colorButton.providerId);
    replay.insert("devManager",selectVDevMgr.providerId);
    replay.insert(KEY_NUM_CHANGING_DEVS,numChangingDevices);
    replay.insert(KEY_SAME_COLOR,sameColor);
    replay.insert(KEY_TIME_DIFF,timeDiff);
    replay.insert("numDevs",selectVDevMgr.getDevices().count() + selectVDevMgr.getGroupDevices().count());
    sendMsg(replay,id,true);
}

void FlashScene::clientMessage(QJsonObject msg, int id)
{
    if(msg.contains("durationChanged"))
        calculatedDuration = msg.value("durationChanged").toDouble(100);
    if(msg.contains(KEY_NUM_CHANGING_DEVS)){
        numChangingDevices =  round(msg.value(KEY_NUM_CHANGING_DEVS).toDouble(numChangingDevices));
    }
    if(msg.contains(KEY_SAME_COLOR)){
        sameColor = msg.value(KEY_SAME_COLOR).toBool(sameColor);
    }
    if(msg.contains(KEY_TIME_DIFF)){
        timeDiff = round(msg.value(KEY_TIME_DIFF).toDouble(timeDiff));
    }
    sendMsgButNotTo(msg,id,true);
}

QString FlashScene::getRequestType()
{
    return "falshScene";
}

QString FlashScene::getSceneTypeString()
{
    return getSceneTypeStringStaticaly();
}

QString FlashScene::getSceneTypeStringStaticaly()
{
    return "flashScene";
}

QJsonObject FlashScene::serialize(){
    QJsonObject ret;
    ret.insert(KEY_TRIGGER,trigger.serialize());
    ret.insert("fadeIn",fadeIn.val());
    ret.insert("fadeOut",fadeOut.val());
    ret.insert("interrupt",interrupt.getBoolValue());
    ret.insert("relativeDuration",relativeDuration.getBoolValue());
    ret.insert("flashDuration",flashDuration.val());
    ret.insert(KEY_COLOR,colorButton.serialize());
    ret.insert(KEY_DEVS,selectVDevMgr.serialize());
    ret.insert(KEY_NUM_CHANGING_DEVS,numChangingDevices);
    ret.insert(KEY_SAME_COLOR,sameColor);
    ret.insert(KEY_TIME_DIFF,timeDiff);
    return serializeScene(ret);
}

void FlashScene::triggered()
{
    if(!flashEnabled || interrupt.getBoolValue()){

        if(relativeDuration.getBoolValue() && time.elapsed() != 0)
            calculatedDuration = time.elapsed()*flashDuration.val();
        else
            calculatedDuration = flashDuration.val() * 2000;

        reloadColor();
        flashEnabled = true;
        beatSpeed = time.elapsed();
        time.restart();
    }
}
