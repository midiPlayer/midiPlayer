#ifndef DIA_H
#define DIA_H

#include <QString>
#include <QSharedPointer>
#include "scene.h"
#include "scenebuilder.h"
#include "websocketserverprovider.h"
#include "diascenemonitorcontrol.h"
#include "scenes/planedtriggerhandler.h"

class Dia : public WebSocketServerProvider
{
public:
    Dia(QJsonObject serialized, SceneBuilder *builder, WebSocketServer *wss, AudioProcessor *jackP, MonitorIO *monitorP, PlanedTriggerHandler* triggerHandler);
    Dia(QSharedPointer<Scene> sceneP, QString nameP, QString descP, float fadeInDurationP, WebSocketServer *wss, AudioProcessor *jackP, MonitorIO *monitorP, PlanedTriggerHandler* triggerHandler);
    QSharedPointer<Scene> scene;
    QString name;
    QString desc;
    float fadeInDuration;
    QJsonObject serialize(SceneBuilder *builder);

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();

    void start();
    void stop();

private:

    DiaSceneMonitorControl monitorControl;
};

#endif // DIA_H
