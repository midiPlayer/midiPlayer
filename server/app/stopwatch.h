#ifndef STOPWATCH_H
#define STOPWATCH_H
#include <QTimer>
#include "websocketserverprovider.h"
#include <QObject>

class Stopwatch : public QObject, public WebSocketServerProvider
{
Q_OBJECT

public:
    Stopwatch(WebSocketServer *ws);
    void start(bool notify = true);
    void stop(bool notify = true);
    void resume(bool notify = true);
    int getMSecs();
    void setTo(long ms, bool notify = true);

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
    bool getRunning() const;

private:
    long time;
    bool running;
    int snaped;
    QTimer timer;
    float speed;

    long getTimestamp();
public slots:
    void sendTimeSync();
    void setSpeed(float newSpeed);
signals:
    void started();
    void stoped();
    void resumed();
    void timeSet();
};

#endif // STOPWATCH_H
