#ifndef SCENE_H
#define SCENE_H

#include <QObject>
#include <QList>
#include <qset.h>
#include "devices/device.h"
#include <QJsonObject>
#include "serializable.h"

class Scene : public QObject, Serializable
{
    Q_OBJECT

public:
    explicit Scene(QString nameP, QJsonObject serialized = QJsonObject());
    explicit Scene(Scene* clone);
    virtual QMap<QString,QSharedPointer<DeviceState> > getDeviceState() = 0;
    QString getName();
    virtual void stop() {}
    virtual void start() {}
    virtual bool exitRequested() { return false;}
    virtual QString getSceneTypeString() = 0;

    virtual QJsonObject serialize() = 0;

    virtual QList<QSharedPointer<Scene> > getSubScenes();

    int getId();

    static QSet<int> *usedIds();

    void setName(const QString &value);

signals:

public slots:

private:
    QString name;
    int id;
    static int getFreeId();
    static void useId(int id);

protected:
    QJsonObject serializeScene(QJsonObject inherited = QJsonObject());
};

#endif // SCENE_H
