#include "channeltester.h"
#include "websocketserver.h"

ChannelTester::ChannelTester(WebSocketServer *ws, OlaDeviceProvider *olaD) :
    WebSocketServerProvider(ws),
    ola(olaD)
{
    
}

void ChannelTester::clientUnregistered(QJsonObject, int id)
{
    activeClients.remove(id);

    if(activeClients.size() == 0){//last Client left
        ola->stopChannelPreview();
    }
}

void ChannelTester::clientMessage(QJsonObject msg, int id)
{
    if(msg.contains("channel") && msg.contains("universe")){
        ola->startChannelPreview(msg.value("universe").toInt(),msg.value("channel").toInt());

        if(!activeClients.contains(id))
            activeClients.insert(id);
    }
}

QString ChannelTester::getRequestType()
{
    return "chennelTester";
}
