#include "filtervirtualdevicemanager.h"
#include "devices/groupdevice.h"
#include "filteredgroupdevice.h"

FilterVirtualDeviceManager::FilterVirtualDeviceManager(VirtualDeviceManager *manager, std::vector<Device::DeviceType> accepted) : vDevManager(manager),acceptedTypes()
{
    connect(manager,SIGNAL(virtualDevicesChanged()),this,SLOT(onVirtualDevsChanged()));
    for(auto const& type : accepted){
        acceptedTypes.append(type);
    }
    onVirtualDevsChanged();
}

QHash<QString, QSharedPointer<Device> > FilterVirtualDeviceManager::getDevices()
{
    return myDevs;
}

void FilterVirtualDeviceManager::addAcceptedType(Device::DeviceType type)
{

    acceptedTypes.append(type);
    onVirtualDevsChanged();
}

void FilterVirtualDeviceManager::onVirtualDevsChanged()
{
    myDevs.clear();
    QHash<QString,QSharedPointer<Device> > availableDevs = vDevManager->getDevices();
    QHash<QString,QSharedPointer<Device> >::iterator it;
    for(it = availableDevs.begin(); it != availableDevs.end(); it++){
        if(acceptedTypes.contains(it.value().data()->getType())){
            myDevs.insert(it.key(),it.value());
        }
    }

    foreach (QSharedPointer<GroupDevice> group, vDevManager->getGroupDevices()) {
        QList<QString> filter;
        generateFilter(group,&filter);
        myGroups.insert(group.data()->getGroupId(),
                        QSharedPointer<FilteredGroupDevice>(new FilteredGroupDevice(group,filter)));
    }
    emit virtualDevicesChanged();
}

void FilterVirtualDeviceManager::generateFilter(QSharedPointer<GroupDevice> group, QList<QString> *filter)
{
    foreach (QSharedPointer<Device> gDev, group.data()->getChildren()) {
        if(acceptedTypes.contains(gDev.data()->getType())){
            filter->append(gDev.data()->getDeviceId());
        }
    }
    foreach(QSharedPointer<GroupDevice> subGroup, group.data()->getChildGroups()){
        generateFilter(subGroup, filter);
    }
}

QHash<QString, QSharedPointer<GroupDevice> > FilterVirtualDeviceManager::getGroupDevices()
{
    return myGroups;
}
