#ifndef OSCCONNECTORDECK_H
#define OSCCONNECTORDECK_H

#include <QObject>
#include <lo/lo.h>

class OscConnector;

class OscConnectorDeck : public QObject
{
    Q_OBJECT

public:
    OscConnectorDeck(int nrP);

    int getNr() const;
    QString getTitle() const;
    float getPosition() const;
    float getVolume() const;

    void message(QString path, QString params,lo_arg **argv, int argc);

    bool getPlay() const;

    float getPositionRel() const;

    float getDuration() const;

    float getSpeed() const;

signals:
    void playChanged(bool play);
    void volumeChanged(float volume);
    void positionChanged(float positionRel);
    void titleChnaged(QString title);
    void speedChanged(float speed);
    void somethingChanged();

private:
    int nr;
    QString title;
    float positionRel;
    float duration;
    float volume;
    bool play;
    float speed;
};

#endif // OSCCONNECTORDECK_H
