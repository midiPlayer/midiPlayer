#include "dynamicvirtualdevicemanager.h"
#include "websocketserver.h"
#include "devices/whitedevice.h"
#include "devices/channeldevice.h"
#include <QColor>
#include <QJsonArray>
#include "devices/stripegroupdevice.h"
#include "devices/usergroupdevice.h"
#include "devices/device.h"
#include "devices/beamerdevice.h"

#define KEY_DEVICES "devs"
#define KEY_GROUPS "groups"

DynamicVirtualDeviceManager::DynamicVirtualDeviceManager(WebSocketServer *ws, QJsonObject serialized) :
    VirtualDeviceManager(),
    WebSocketServerProvider(ws),
    wss(ws)
{
    
    if(serialized.contains(KEY_DEVICES)){
        QJsonArray devsJson = serialized.value(KEY_DEVICES).toArray();
        foreach(QJsonValue devsJson, devsJson){
            QSharedPointer<Device> dev = Device::buildDevice(devsJson.toObject(),wss);
            devices.insert(dev.data()->getDeviceId(),dev);
        }

        QJsonArray groupsJson = serialized.value(KEY_GROUPS).toArray();
        foreach(QJsonValue groupJson, groupsJson){
            try{
                QSharedPointer<GroupDevice> group = GroupDevice::buildGroup(groupJson.toObject(),this,ws);
                groups.insert(group.data()->getGroupId(),group);
            }
            catch(GroupDevice::UnknownGroupTypeException e){
                qDebug() << "dynamicvirtualdevicemanager: could not creat group: " << e.what();
            }
        }
    }
}

QHash<QString, QSharedPointer<Device> > DynamicVirtualDeviceManager::getDevices()
{
    return devices;
}

void DynamicVirtualDeviceManager::clientRegistered(QJsonObject, int)
{
    foreach (QSharedPointer<Device> dev,devices) {
        notifyNewDevice(dev);
    }
    foreach (QSharedPointer<GroupDevice> gDev ,groups) {
        notifyNewGroup(gDev);
    }
}

void DynamicVirtualDeviceManager::findFreeChannel(int* channel, int* universe, int numDev)
{
    while(true){
        while(*channel < 512) {
            if(!checkChannelCollision(*channel,numDev,*universe,-1))
                return;
            (*channel)++;
        }
        *channel = 0;
        (*universe)++;
    }
}

void DynamicVirtualDeviceManager::clientMessage(QJsonObject msg, int id)
{
    if(msg.contains("add")){
        QJsonObject addCmd = msg.value("add").toObject();
        QString type = addCmd.value("type").toString();

        int firstC = addCmd.value("channel").toInt();
        int universe = addCmd.value("universe").toInt();

        QString devId = addCmd.value("devId").toString();

        bool moduloMode = addCmd.value("moduloMode").toBool();

        if(checkIdColision(devId, id))
            return;

        QVector3D pos(addCmd.value("x").toDouble(),
                      addCmd.value("y").toDouble(),
                      addCmd.value("z").toDouble());

        if(type == Device::getTypeString(Device::White)){
            if(checkChannelCollision(firstC,1,universe,id))
                return;
            addDev(QSharedPointer<Device>(
                       new WhiteDevice(
                           firstC, 1,universe, devId,
                           moduloMode,
                           QColor(addCmd.value("whiteColor").toString("#ffffff")),
                           pos)));
        }else if(type == Device::getTypeString(Device::RGB)){
            if(checkChannelCollision(firstC,3,universe,id))
                return;
            addDev(QSharedPointer<Device>(new ChannelDevice(firstC,3,universe,devId,Device::RGB, moduloMode, pos)));
        }else if(type == Device::getTypeString(Device::RGBW)){
            if(checkChannelCollision(firstC,4,universe,id))
                return;
            addDev(QSharedPointer<Device>(new ChannelDevice(firstC,4,universe,devId,Device::RGBW, moduloMode, pos)));
        }
        else if(type == Device::getTypeString(Device::Beamer)){
                    addDev(QSharedPointer<BeamerDevice>(new BeamerDevice(devId,wss,pos)));
        }else if(type == (QString("group-") + GROUP_TYPE_STRIPE)){
            int numDev = addCmd.value("numDev").toInt();
            StripeGroupDevice* newGroup = new StripeGroupDevice(devId,
                                                                numDev,
                                                                firstC,universe,
                                                                Device::getTypeForString(addCmd.value("devsType").toString()),
                                                                StripeGroupDevice::deSerializedPositions(addCmd.value("positions").toArray())
                                                                );
            if(checkChannelCollision(firstC,newGroup->getNumChannel(),universe,id))
                return;//dismiss on error
            addGroup(QSharedPointer<StripeGroupDevice>(newGroup));
        }else if(type == (QString("group-") + GROUP_TYPE_USER)){
            addGroup(QSharedPointer<UserGroupDevice>(new UserGroupDevice(devId,this,wss)));
        } else{
            throw "unimplemented device:" + type;
       }
    }

    if(msg.contains("edit")){
        QJsonObject edit = msg.value("edit").toObject();
        QString devId = edit.value("devId").toString();

        if(devices.contains(devId)){ // default device
            QSharedPointer<ChannelDevice> dev = devices.value(devId).dynamicCast<ChannelDevice>();
            if(!dev.isNull()){
                int universe = edit.value("universe").toInt();
                int firstC = edit.value("channel").toInt();
                if(checkChannelCollision(firstC,dev.data()->getNumChannels(),universe,id,devId))
                    return;

                dev.data()->setPosition(QVector3D(
                                        edit.value("x").toDouble(),
                                        edit.value("y").toDouble(),
                                        edit.value("z").toDouble()));
                dev.data()->setFirstChannel(firstC);
                dev.data()->setUniverse(universe);
                QSharedPointer<WhiteDevice> wDev = dev.dynamicCast<WhiteDevice>();
                if(!wDev.isNull()){
                    wDev.data()->setColorOfDevice(QColor(edit.value("whiteColor").toString()));
                }

                dev.data()->setModuloMode(edit.value("moduloMode").toBool());
            }
        }
        else if(groups.contains(devId)){
            QSharedPointer<GroupDevice> group = groups.value(devId);

            QSharedPointer<StripeGroupDevice> stripe = group.dynamicCast<StripeGroupDevice>();
            if(!stripe.isNull()){
                int firstC = edit.value("channel").toInt();
                int universe = edit.value("universe").toInt();
                stripe.data()->setNumDevices(edit.value("numDev").toInt());
                if(checkChannelCollision(firstC,stripe->getNumChannel(),universe,id,devId))
                    return;//dismiss on error
                stripe.data()->setDevTypes(
                            Device::getTypeForString(edit.value("devsType").toString()));
                stripe.data()->setFirstChannel(firstC);
                stripe.data()->setUniverse(universe);
                stripe.data()->setPositions(StripeGroupDevice::deSerializedPositions(edit.value("positions").toArray()));
            }
        }
        else
            return;

        sendMsg(msg,false);
        emit virtualDevicesChanged();

    }
    if(msg.contains("rm")){    
        rmDevOrGroup(msg.value("rm").toString());
    }

    if(msg.contains("findFreeChannel")){
        QJsonObject find = msg.value("findFreeChannel").toObject();
        int startU = find.value("universe").toInt(0);
        int startC = find.value("channel").toInt(0);
        findFreeChannel(&startC, &startU,find.value("numDev").toInt(1));
        QJsonObject reply;
        QJsonObject freeChannel;
        freeChannel.insert("channel",startC);
        freeChannel.insert("universe",startU);
        reply.insert("freeChannel",freeChannel);
        sendMsg(reply,false);
    }

    if(msg.contains("get")){
        QJsonObject get = msg.value("get").toObject();
        QString devId = get.value("devId").toString();

        QJsonObject ret;
        ret.insert("devId",devId);

        if(devices.contains(devId)){
            QSharedPointer<Device> dev = devices.value(devId);
            ret.insert("x",dev.data()->getPosition().x());
            ret.insert("y",dev.data()->getPosition().y());
            ret.insert("z",dev.data()->getPosition().z());

            QSharedPointer<ChannelDevice> cdev = dev.dynamicCast<ChannelDevice>();
            if(!cdev.isNull()){

                ret.insert("universe",cdev.data()->getUniverse());
                ret.insert("channel",cdev.data()->getFirstChannel());
                ret.insert("moduloMode",cdev.data()->getModuloMode());

                QSharedPointer<WhiteDevice> wDev = cdev.dynamicCast<WhiteDevice>();
                if(!wDev.isNull()){
                    ret.insert("whiteColor",wDev.data()->getColorOfDevice().name());
                }
            }
        }else if(groups.contains(devId)){
            QSharedPointer<GroupDevice> group = groups.value(devId);
            QSharedPointer<StripeGroupDevice> stripe = group.dynamicCast<StripeGroupDevice>();
            if(!stripe.isNull()){
                ret.insert("universe",stripe.data()->getUniverse());
                ret.insert("channel",stripe.data()->getFirstChannel());
                ret.insert("numDev",stripe.data()->getNumDevices());
                ret.insert("devsType",Device::getTypeString(stripe.data()->getDevTypes()));
                ret.insert("positions", stripe.data()->serializePositions());
            }
        }
        else{
            return;
        }

        QJsonObject resp;
        resp.insert("get",ret);
        sendMsg(resp,id,false);
    }

}

QString DynamicVirtualDeviceManager::getRequestType()
{
    return "dydevMgr";
}

QJsonObject DynamicVirtualDeviceManager::serialize()
{   
    QJsonObject ret;

    QJsonArray devsJson;

    foreach (QSharedPointer<Device> dev, devices) {
        if(dev.data()->isSerializable())
            devsJson.append(dev.data()->serialize());
    }

    QJsonArray groupsJson;
    foreach (QSharedPointer<GroupDevice> group, groups.values()) {
            groupsJson.append(group.data()->serialize());
    }

    ret.insert(KEY_DEVICES,devsJson);
    ret.insert(KEY_GROUPS,groupsJson);

    return ret;
}

void DynamicVirtualDeviceManager::addDev(QSharedPointer<Device> dev)
{
    devices.insert(dev.data()->getDeviceId(),dev);
    notifyNewDevice(dev);
    emit virtualDevicesChanged();
}

void DynamicVirtualDeviceManager::addGroup(QSharedPointer<GroupDevice> dev)
{
    groups.insert(dev.data()->getGroupId(),dev);
    notifyNewGroup(dev);
    emit virtualDevicesChanged();
}

void DynamicVirtualDeviceManager::notifyNewDevice(QSharedPointer<Device> dev)
{
    QJsonObject msg;
    QJsonObject add;
    add.insert("devId",dev.data()->getDeviceId());
    add.insert("type",dev.data()->getTypeString());
    msg.insert("add",add);
    sendMsg(msg,false);
}

void DynamicVirtualDeviceManager::notifyNewGroup(QSharedPointer<GroupDevice> dev)
{
    QJsonObject addCmd;
    addCmd.insert("devId",dev.data()->getGroupId());
    addCmd.insert("type","group-" + dev.data()->getGroupType());
    QJsonObject msg;
    msg.insert("add",addCmd);
    sendMsg(msg,false);
}

void DynamicVirtualDeviceManager::rmDevOrGroup(QString devId)
{
    if(devices.contains(devId))
        devices.remove(devId);
    else if(groups.contains(devId))
        groups.remove(devId);
    else
        return;

    QJsonObject msg;
    msg.insert("rm",devId);
    sendMsg(msg,false);


    emit virtualDevicesChanged();
}

bool DynamicVirtualDeviceManager::checkChannelCollision(int channel, int numChannel, int universe, int sender,QString skipDeviceId)
{
    if(numChannel < 1)
        throw "numChannels must be > 0";
    int last = channel + numChannel - 1;

    if(last >= 512){
        QJsonObject msg;
        msg.insert("err","universeOverflow");
        sendMsg(msg,sender,false);
        return true;//collision found;
    }

    foreach (QSharedPointer<Device> dev, devices) {
        if(dev.data()->getDeviceId() == skipDeviceId)
            continue;
        QSharedPointer<ChannelDevice> cDev = dev.dynamicCast<ChannelDevice>();
        if(cDev.isNull())
            continue;
        if(cDev.data()->getUniverse() != universe)//not in same universe --> no collision possible
            continue;
        int devFirst = cDev.data()->getFirstChannel();
        int devLast = devFirst + cDev.data()->getNumChannels() - 1;

        if(checkChannelCollisionIntern(channel, last,devFirst, devLast, sender, dev.data()->getDeviceId()))
                return true;
    }

    foreach (QSharedPointer<GroupDevice> group, groups) {
        if(group.data()->getGroupId() == skipDeviceId)
            continue;
        if(group.data()->getGroupType() == GROUP_TYPE_USER){
            continue;//group devices can't cause channel colisions
        }
        QSharedPointer<StripeGroupDevice> stripe = group.dynamicCast<StripeGroupDevice>();
        if(!stripe.isNull()){
            if(stripe.data()->getUniverse() != universe)
                continue;
            if(checkChannelCollisionIntern(channel,
                                        last,
                                        stripe.data()->getFirstChannel(),
                                        stripe.data()->getFirstChannel() + stripe.data()->getNumChannel(),
                                        sender,
                                        stripe.data()->getGroupId()))
                    return true;
        }
        else{
            throw("unknown group type");
        }
    }
    return false; //no collision found
}

bool DynamicVirtualDeviceManager::checkChannelCollisionIntern(int channel, int last, int devFirst, int devLast, int sender, QString devId)
{
    if((devFirst <= channel && channel <= devLast)||
            (devFirst <= last && last <= devLast)||
            (channel <= devFirst && devFirst <= last)){
        if(sender != -1){
            QJsonObject msg;
            msg.insert("err","channelCollision");
            msg.insert("errArg",devId);
            sendMsg(msg,sender,false);
        }
        return true;//collision found;
    }
    return false;
}

bool DynamicVirtualDeviceManager::checkIdColision(QString devId, int sender)
{
    if(devices.contains(devId) ||groups.contains(devId) || devId == ""){//check DevId
        QJsonObject msg;
        msg.insert("err","devIdInUse");
        sendMsg(msg,sender,false);
        return true; // colision found
    }
    return false; //no collision found
}

QHash<QString, QSharedPointer<GroupDevice> > DynamicVirtualDeviceManager::getGroupDevices()
{
    QHash<QString, QSharedPointer<GroupDevice> > ret = groups;
    QSharedPointer<UserGroupDevice> allStripes(new UserGroupDevice("allStripes",this,wss));
    foreach (QSharedPointer<GroupDevice> group, groups) {
        if(group.data()->getGroupType() == GROUP_TYPE_STRIPE)
        allStripes.data()->childGroups.insert(group.data()->getGroupId(),group);
    }
    ret.insert("allStripes",allStripes);

    QSharedPointer<UserGroupDevice> allStripesU(new UserGroupDevice("allStripesU",this,wss));
    foreach (QSharedPointer<GroupDevice> group, groups) {
        if(group.data()->getGroupType() == GROUP_TYPE_STRIPE){
            QHash<QString, QSharedPointer<Device> > children = group.data()->getChildren();
            foreach(QString cid, children.keys()){
                allStripesU.data()->childern.insert(cid,children.value(cid));
            }
        }
    }
    ret.insert("allStripesU",allStripesU);
    return ret;
}
