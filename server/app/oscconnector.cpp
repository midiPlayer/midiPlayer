#include "oscconnector.h"
#include <QDebug>

#include <lo/lo.h>
#include <lo/lo_cpp.h>

OscConnector::OscConnector() :
    loServer(new lo::ServerThread(7777))
{

    for(int i = 0; i < 4; i++){
        OscConnectorDeck* newDeck = new OscConnectorDeck(i);
        decks.insert(i,newDeck);
    }
    QMap<int,OscConnectorDeck*>* decksP = &decks;
    loServer->add_method(NULL, NULL, [decksP](const char *path,const char *pat,lo_arg **argv, int argc) {
        if(QString(path).indexOf("/mixxx/deck/") == 0 && argc > 1 && decksP->contains(argv[0]->i)){//beginns with
            decksP->value(argv[0]->i)->message(QString(path),QString(pat),argv,argc);
        }
    });


    loServer->start();
    //OscConnectorDeck* deck1 = new OscConnectorDeck(1,loServer);
}

OscConnector::~OscConnector()
{
    delete loServer;
    foreach (OscConnectorDeck *deck, decks.values()) {
        delete deck;
    }
}

OscConnectorDeck *OscConnector::getDeck(int nr)
{
    return decks.value(nr);
}

int OscConnector::getNumDecks()
{
    return decks.count();
}

