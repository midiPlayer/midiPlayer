#include "oladeviceprovider.h"
#include <QDebug>
#include "devices/channeldevice.h"
#include <QSharedPointer>
OlaDeviceProvider::OlaDeviceProvider(VirtualDeviceManager *manager):
    filterDevManager(manager, {Device::RGB, Device::RGBW, Device::White}),
    ungrouped(&filterDevManager),
    active(false),
    wrapper(),
    inChannelPrevMode(false)
{
    ola::InitLogging(ola::OLA_LOG_WARN, ola::OLA_LOG_STDERR);
    if (wrapper.Setup())
        active = true;
    else{
        qDebug() << "Setup failed";
        return;
    }
}

OlaDeviceProvider::~OlaDeviceProvider()
{
    wrapper.Cleanup();
}

void OlaDeviceProvider::publish(QMap<QString, QSharedPointer<DeviceState> > outDevices, QMap<QString, QSharedPointer<DeviceState> > changes)
{
    if(inChannelPrevMode)
        return;

    if(changes.count() == 0)
        return;

    QMap<int, QSharedPointer<ola::DmxBuffer> > buffers;

    foreach (QString devId, outDevices.keys()) {
        if(!ungrouped.getDevices().contains(devId))
            continue;

        QSharedPointer<ChannelDeviceState> d = outDevices.value(devId).dynamicCast<ChannelDeviceState>();
        int universe = d.data()->getDevice()->getUniverse();

        if(!usedUniverses.contains(universe))
            usedUniverses.insert(universe);

        if(!buffers.contains(universe)){
            QSharedPointer<ola::DmxBuffer> b = QSharedPointer<ola::DmxBuffer>(new ola::DmxBuffer());
            b.data()->Blackout();
            buffers.insert(universe,b);
        }

        QSharedPointer<ola::DmxBuffer> buffer = buffers.value(universe);

        if(d.isNull())
           continue;

          foreach(int c,d.data()->getChannels()){
             float cValue = d.data()->getChannelValue(c,true);
            buffer.data()->SetChannel(c,cValue*255.0*d.data()->getOpacity());
        }
    }
    foreach(int universe, buffers.keys()){
        wrapper.GetClient()->SendDMX(universe, *buffers.value(universe).data(), ola::client::SendDMXArgs());
    }
}

void OlaDeviceProvider::startChannelPreview(int universe, int channel)
{
    inChannelPrevMode = true;
    blackoutAllUniverses();
    usedUniverses.insert(universe);

    ola::DmxBuffer buffer;
    buffer.Blackout();
    buffer.SetChannel(channel,255);
    wrapper.GetClient()->SendDMX(universe, buffer, ola::client::SendDMXArgs());
}

void OlaDeviceProvider::stopChannelPreview()
{
    blackoutAllUniverses();
    inChannelPrevMode = false;
}

void OlaDeviceProvider::blackoutAllUniverses()
{
    ola::DmxBuffer buffer;
    buffer.Blackout();
    foreach (int universe, usedUniverses) {
        wrapper.GetClient()->SendDMX(universe, buffer, ola::client::SendDMXArgs());
    }
    usedUniverses.clear();
}

