#ifndef DYNAMICVIRTUALDEVICEMANAGER_H
#define DYNAMICVIRTUALDEVICEMANAGER_H

#include "virtualDeviceManager/virtualdevicemanager.h"
#include "websocketserverprovider.h"

class DynamicVirtualDeviceManager : public VirtualDeviceManager, public WebSocketServerProvider, public Serializable
{
Q_OBJECT

public:
    DynamicVirtualDeviceManager(WebSocketServer *ws, QJsonObject serialized);
    QHash<QString,QSharedPointer<Device> > getDevices();

    void clientRegistered(QJsonObject, int);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
    QJsonObject serialize();
private:
    QHash<QString, QSharedPointer<Device> > devices;
    QHash<QString, QSharedPointer<GroupDevice> > groups;
    void addDev(QSharedPointer<Device> dev);
    void addGroup(QSharedPointer<GroupDevice> dev);
    void notifyNewDevice(QSharedPointer<Device> dev);
    void notifyNewGroup(QSharedPointer<GroupDevice> dev);
    void rmDevOrGroup(QString devId);
    bool checkChannelCollision(int channel, int numChannel, int universe, int sender, QString skipDeviceId = "");
    bool checkChannelCollisionIntern(int channel, int last, int devFirst, int devLast, int sender, QString devId);
    bool checkIdColision(QString devId, int sender);
    WebSocketServer* wss;

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();
    void findFreeChannel(int *channel, int *universe, int numDev);
};

#endif // DYNAMICVIRTUALDEVICEMANAGER_H
