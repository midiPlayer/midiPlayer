#ifndef DISCOSCENE_H
#define DISCOSCENE_H

#include "scene.h"
#include <QString>
#include <QList>
#include <QMap>
#include "devices/device.h"
#include "fusionscene.h"
#include "websocketserverprovider.h"
#include "discosubscene.h"
#include <QSharedPointer>
#include "scenebuilder.h"
#include "virtualDeviceManager/injectvirtualdevicemanager.h"

class DiscoScene : public Scene, public WebSocketServerProvider
{
public:
    DiscoScene(WebSocketServer* ws, SceneBuilder* builderP, VirtualDeviceManager *vDevMgr, QString name, QJsonObject serialized = QJsonObject());
    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
    void stop();
    void start();
    QJsonObject serialize();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();
    void addEffect(QSharedPointer<Scene> scene);

private:
    FusionScene fusion;
    bool solo;
    QSharedPointer<DiscoSubScene> soloScene;
    int sceneIdCounter;
    QList<int> order;
    SceneBuilder sceneBuilder;
    InjectVirtualDeviceManager injektor;
    bool isRunning;
    QMap<int,QSharedPointer<DiscoSubScene> > effects;

    void addSubScene(QSharedPointer<DiscoSubScene> subScene);
    QJsonObject getStatus(bool showEffects, bool showOrder,bool serializeScene);

    // Scene interface
public:
    QList<QSharedPointer<Scene> > getSubScenes();
};

#endif // DISCOSCENE_H
