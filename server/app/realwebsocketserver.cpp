#include "realwebsocketserver.h"
#define PORT 8888
#include <QJsonArray>

RealWebSocketserver::RealWebSocketserver():
    WebSocketServer(),
    providerById(),
    providerIdCounter(0),
    m_pWebSocketServer(new QWebSocketServer(QStringLiteral("Light Server"),QWebSocketServer::NonSecureMode, this)),
    m_clients()
{
    if (m_pWebSocketServer->listen(QHostAddress::Any, PORT)) {
        connect(m_pWebSocketServer, SIGNAL(newConnection()),this, SLOT(onNewConnection()));
        connect(m_pWebSocketServer, SIGNAL(closed()), this, SLOT(onConnectionClosed()));
    }
}

void RealWebSocketserver::onNewConnection()
{
    QWebSocket *pSocket = m_pWebSocketServer->nextPendingConnection();

    connect(pSocket, SIGNAL(textMessageReceived(QString)), this, SLOT(onTextMessage(QString)));
    //connect(pSocket, &QWebSocket::binaryMessageReceived, this, &EchoServer::processBinaryMessage);
    connect(pSocket, SIGNAL(disconnected()), this, SLOT(onConnectionClosed()));

    m_clients << pSocket;
}

void RealWebSocketserver::onTextMessage(QString message)
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());

    QJsonDocument jsonResponse = QJsonDocument::fromJson(message.toUtf8());
    QJsonObject data = jsonResponse.object();
    qDebug() << data;

    if(data.contains("register")){
        QJsonValue value = data.value("register");
        if(value.isArray()){
            foreach(QJsonValue val, value.toArray()){
                for (auto const& p : registeredProvieder) {
                    if(p->getRequestType() == val.toString("")){
                        p->registerClient(data.value("parameters").toObject(),pClient);
                        break;
                    }
                }
            }
        }
        else if(value.isString()){
            for (auto const& p : registeredProvieder) {
                if(p->getRequestType() == value.toString("")){
                    p->registerClient(data.value("parameters").toObject(),pClient);
                    break;
                }
            }
        }

    }
    else if(data.contains("registerId")){
        QJsonValue value = data.value("registerId");
        if(value.isArray()){
            foreach(QJsonValue val, value.toArray()){
                registerId(val.toInt(-2), data.value("parameters").toObject(),pClient);
            }
        }
        else{
            registerId(value.toInt(-2), data.value("parameters").toObject(), pClient);
        }
    }
    else if(data.contains("unregister")){
        QString requestType = data.value("unregister").toString();
        for (auto const& p : registeredProvieder) {
            if(p->getRequestType() == requestType){
                p->unregisterClient(pClient);
            }
        }
    }
    else if(data.contains("unregisterId")){
        int requestId = data.value("unregisterId").toInt(-2);

        if(providerById.find(requestId) == providerById.end()){
            qDebug() << "unregistration failed: unknown provider id";
        }
        else{
            providerById[requestId]->unregisterClient(pClient);
        }
    }
    else if(data.contains("provider")){
        QString rqt = data.value("provider").toString("");
        for (auto const& p : registeredProvieder) {
            /*if(!provider.contains(p->providerId))
                continue;//provider was deleted*/
            if(p->getRequestType() == rqt){
                p->onMessage(data.value("data").toObject(),pClient);
            }
        }
    }
    else if(data.contains("providerId")){
        int  pid = data.value("providerId").toInt(-2);
        if(providerById.find(pid) == providerById.end()){
            qDebug() << "message assignment failed: unknown provider id";
        }
        else{
            providerById[pid]->onMessage(data.value("data").toObject(),pClient);
        }
    }
}

void RealWebSocketserver::onConnectionClosed()
{
    QWebSocket *pClient = qobject_cast<QWebSocket *>(sender());
    for(const auto& p : providerById) {
        p.second->unregisterClient(pClient);
    }
    emit clientClosed();
}

void RealWebSocketserver::registerId(int id,QJsonObject params,QWebSocket *client)
{
    if(providerById.find(id) == providerById.end()){
        qDebug() << "registration failed: unknown provider id";
    }
    else{
        providerById[id]->registerClient(params,client);
    }
}

void RealWebSocketserver::registerProvider(WebSocketServerProvider *me)
{
    if(registeredProvieder.find(me) == registeredProvieder.end()){//contains
        registeredProvieder.emplace(me);
        providerById.emplace(providerIdCounter,me);
        me->providerId = providerIdCounter++;
    }
}

void RealWebSocketserver::unregisterProvider(WebSocketServerProvider *me)
{
    providerById.erase(me->providerId);
    registeredProvieder.erase(me);

}

void RealWebSocketserver::sendData(QJsonObject data, QWebSocket *reciever, WebSocketServerProvider *provider, bool onlyToProviderId)
{
    QJsonObject msg;
    if(onlyToProviderId)
       msg.insert("providerId",provider->providerId);
    else
        msg.insert("provider",provider->getRequestType());
    msg.insert("data",data);
    QJsonDocument d;
    d.setObject(msg);
    QString msgJson = d.toJson();
    reciever->sendTextMessage(msgJson);
}

void RealWebSocketserver::disconnectAllClients()
{
    foreach (QWebSocket *socket, m_clients) {
        socket->close();
    }
}
