#include "scalainput.h"

#define KEY_AMP "ampfiler"
#define KEY_SMOOTHNESS_IN "smoothness_in"
#define KEY_SMOOTHNESS_OUT "smoothness_out"

ScalaInput::ScalaInput(WebSocketServer* ws, AudioProcessor* audioP, QJsonObject serialized):
    WebSocketServerProvider(ws),
    audio(audioP),
    amplification(ws,this, serialized.value(KEY_AMP).toInt(10)),
    inSmoothness(ws,this, serialized.value(KEY_SMOOTHNESS_IN).toInt(50)),
    outSmoothness(ws,this,serialized.value(KEY_SMOOTHNESS_OUT).toInt(50)),
    loudnessCtr(0),
    loudnessSum(0),
    loudness(0)
{

}

void ScalaInput::start()
{
    connect(audio,SIGNAL(loudness(float)),this,SLOT(setLoudness(float)));
}

void ScalaInput::stop()
{
    disconnect(audio,SIGNAL(loudness(float)),this,SLOT(setLoudness(float)));
}

QJsonObject ScalaInput::serialize()
{
    QJsonObject ret;
    ret.insert(KEY_AMP,amplification.getNumber());
    ret.insert(KEY_SMOOTHNESS_IN,inSmoothness.getNumber());
    ret.insert(KEY_SMOOTHNESS_OUT,outSmoothness.getNumber());
    return ret;
}

QString ScalaInput::getRequestType()
{
    return "scalaInput";
}

void ScalaInput::setLoudness(float newLoudness)
{
    if(newLoudness < 0)
        newLoudness = 0 - newLoudness;
    loudnessSum += newLoudness;
    if(loudnessCtr++ >10){

        float calLoudness = loudnessSum / 10.0;
        if(calLoudness > loudness){//ansteigend
            float smooth = float(inSmoothness.getNumber()) / 100.0f;
            loudness = calLoudness * (1.0 - smooth) + loudness * smooth;
        }
        else{ //absteigend
            float smooth = float(outSmoothness.getNumber()) / 100.0f;
            loudness = calLoudness * (1.0 - smooth) + loudness * smooth;
        }

        loudnessCtr = 0;
        loudnessSum = 0;
    }
}

void ScalaInput::clientRegistered(QJsonObject, int clientId){
    QJsonObject ret;
    ret.insert("amp",amplification.providerId);
    ret.insert("smoothIn",inSmoothness.providerId);
    ret.insert("smoothOut",outSmoothness.providerId);
    sendMsg(ret,clientId,true);
}

float ScalaInput::getLoudness()
{
    return loudness * float(amplification.getNumber()) / 50.0;
}
