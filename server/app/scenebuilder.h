#ifndef SCENEBUILDER_H
#define SCENEBUILDER_H

#include <QSharedPointer>
#include "websocketserver.h"
#include "audioprocessor.h"
#include "scene.h"
#include "virtualDeviceManager/virtualdevicemanager.h"
#include "monitorio.h"
#include "oscconnector.h"
#include "scenes/sceneprovider.h"
#include "scenes/planedtriggerhandler.h"
#include "beamer/beamereffecthandler.h"
#include "virtualDeviceManager/injectvirtualdevicemanager.h"

class Scene;
class DiaScene;
class SceneBuilder
{
public:
    SceneBuilder(WebSocketServer *wssP, VirtualDeviceManager *manager, AudioProcessor *jackP, MonitorIO *monitorIoP,
                 OscConnector* osccP, QSharedPointer<DiaScene> *baseSceneP, BeamerEffectHandler *beamerEffectH);
    SceneBuilder(SceneBuilder *parent);
    QSharedPointer<Scene> rebuild(QJsonObject serialized);
    QSharedPointer<Scene> buildNew(QString sceneType, QString name = "");
    QJsonObject serializeScene(QSharedPointer<Scene> scene);
    QJsonObject serializeScene(Scene* scene);
    QJsonObject serializeScene(Scene *scene,QJsonObject serialized);
    void setSceneProvider(SceneProvider *value);

    void setOsccDeck(OscConnectorDeck *value);

    QSharedPointer<Scene> copyScene(QSharedPointer<Scene> old);

    PlanedTriggerHandler *getTriggerHandler() const;
    void setTriggerHandler(PlanedTriggerHandler *value);

    void setInjekter(InjectVirtualDeviceManager *value);

    void setVDevManager(VirtualDeviceManager *value);

private:
    WebSocketServer* wss;
    VirtualDeviceManager *vDevManager;
    AudioProcessor *jack;
    MonitorIO *monitorIo;
    OscConnector* oscc;
    QSharedPointer<Scene> buildInt(QString sceneType, QString name, QJsonObject serialized);
    SceneProvider* sceneProvider;
    OscConnectorDeck* osccDeck;
    QSharedPointer<DiaScene> *baseScene;
    PlanedTriggerHandler* triggerHandler;
    BeamerEffectHandler* beamerEffectHandler;
    InjectVirtualDeviceManager* injekter;
    QSharedPointer<Scene> findScene(QSharedPointer<Scene> base, int searchedId);
};

#endif // SCENEBUILDER_H
