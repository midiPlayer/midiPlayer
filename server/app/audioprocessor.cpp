#include <jack/jack.h>
#include <jack/midiport.h>
#include <stdio.h>
#include "audioprocessor.h"
#include <QDebug>
#include "mainwindow.h"
#include <QMutexLocker>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <getopt.h>
#include <unistd.h>
#include <math.h>               /* for isfinite */
#include <string.h>             /* for strcmp */
#include "devices/device.h"
#include "websocketserver.h"
#include "jackaudioninput.h"
#include "pulseaudioinput.h"

//#define DISSABLE_JACK
//#define DISSABLE_ANALYSE

#define KEY_MIN_LEVEL "minLevel"

AudioProcessor::AudioProcessor(WebSocketServer *ws, QJsonObject serialized, QObject *parent) :
    QObject(parent),
    WebSocketServerProvider(ws),
    //audioIn(new JackAudionInput(this)),
    audioIn(new PulseAudioInput(this)),
    musicNotificationRequested(false),
    beatRequested(true),
    buffer_size(512),
    hop_size(256),
    pos(0),
    minLevel(serialized.value(KEY_MIN_LEVEL).toDouble(-80.0)),
    onsetLevel(ws,this,serialized.value("onsetLevel").toDouble(0.5)){



    //aubio
    smpl = new_fvec(hop_size);
    tempo_out = new_fvec(2);


  o = new_aubio_onset ("default", buffer_size, hop_size, audioIn->getSamplerate());
  setOnsetLevel();
  onset = new_fvec (1);
  fftgrain = new_cvec (hop_size);
  fft = new_aubio_fft(hop_size);
  tempo = new_aubio_tempo("default", buffer_size, hop_size, audioIn->getSamplerate());

  audioIn->enable();
  connect(&onsetLevel,SIGNAL(valueChanged()),this,SLOT(setOnsetLevel()));
}

AudioProcessor::~AudioProcessor() {
  delete audioIn;
  del_fvec(smpl);
  del_fvec(onset);
  del_fvec(tempo_out);
  del_aubio_onset(o);
  del_aubio_tempo(tempo);

  //delete[] audio;

}

int AudioProcessor::init(MainWindow* m) {
    return 0;
}

void AudioProcessor::pushAudio(float *audio, int nframes)
{
    if(audio == NULL)
        return;

    double sum = 0;
    double count = 0;

    for (unsigned int j=0;j<(unsigned)nframes;j++) {
        float abs = audio[j];
        if(abs < 0)
            abs = 0 - abs;
        sum += abs;

        count++;
         fvec_set_sample(smpl, audio[j], pos);

         if (pos == (int)(hop_size) - 1) {
               float silence = aubio_silence_detection(smpl, minLevel);
               //calback:
               aubio_onset_do (o,smpl , onset); //onsets
               float res = fvec_get_sample(onset, 0);
               if(res > 0.0f && !silence){
                    //qDebug()  << res;
                    emit onsetNotification();
                    qDebug() << "lib found onset";
               }
               aubio_tempo_do (tempo, smpl, tempo_out); //beat tracking
               float beat = fvec_get_sample (tempo_out, 0);
               if(beat != 0.0f && !silence){
                   emit beatNotification();
                   qDebug() << "lib found beat";
               }


/*               aubio_fft_do (fft,smpl,fftgrain);
               //cvec_print(fftgrain);


               int ctr = 0;
               float sum = 0;
               QString line;
               for(int i = 0; i < fftgrain->length; i++){
                    float f = cvec_norm_get_sample(fftgrain,i);
                    if(f < 0)
                        f*=-1;
                    sum += f;
                   if(ctr++ >= 15){
                    line += QString::number(int(sum)) + "    ";
                    ctr = 0;
                    sum = 0;
                   }
               }
               qDebug() << line;*/



               //end Callback
               pos = -1;
         }
         pos++;
    }
    //QString hash = "#";
    //qDebug() << "loudness: " << hash.repeated(sum / count * 100);
    emit loudness(sum/count);


    /*
     * TODO: It could be better to use the silence detection of aubio here.
    */

           if (musicNotificationRequested && nframes > 0 && audio != 0) {
             //emit chunkReady();
               qDebug() << "music!";
               musicNotificationRequested = false;
               emit musicNotification();
           }
}

void AudioProcessor::clientRegistered(QJsonObject, int id)
{
    QJsonObject reply;
    reply.insert("minLevel",minLevel);
    reply.insert("onsetLevel",onsetLevel.providerId);
    sendMsg(reply,id);
}

void AudioProcessor::clientMessage(QJsonObject msg, int clientId)
{
    if(msg.contains("minLevel"))
        minLevel = msg.value("minLevel").toDouble(-80.0);
    sendMsgButNotTo(msg,clientId);
}

QString AudioProcessor::getRequestType()
{
    return "JackProcessor";
}

QJsonObject AudioProcessor::serialize()
{
    QJsonObject serialized;
    serialized.insert(KEY_MIN_LEVEL,minLevel);
    serialized.insert("onsetLevel",onsetLevel.val());
    return serialized;
}


void AudioProcessor::requestMusicNotification()
{
    musicNotificationRequested = true;
}

void AudioProcessor::setOnsetLevel()
{
    aubio_onset_set_threshold (o, onsetLevel.val());
}
