#include "screencolorscenescanner.h"
#include "websocketserver.h"
#include <QJsonArray>
ScreenColorSceneScanner::ScreenColorSceneScanner(WebSocketServer* ws): WebSocketServerProvider(ws)
{
    
}

void ScreenColorSceneScanner::clientMessage(QJsonObject msg, int)
{
    QList<QColor> colors;
    foreach(QJsonValue colorName, msg.value("colors").toArray()){
        colors.append(QColor(colorName.toString()));
    }
    emit recievedColors(colors);
}

QString ScreenColorSceneScanner::getRequestType()
{
    return "screenScanner";
}
