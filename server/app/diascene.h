#ifndef DIASCENE_H
#define DIASCENE_H

#include "scene.h"
#include "websocketserverprovider.h"
#include "fusionscene.h"
#include <QTime>
#include "audioprocessor.h"
#include "dia.h"
#include "monitorio.h"
#include "scenes/sceneprovider.h"
#include "scenes/planedtriggerhandler.h"

class DiaScene : public Scene, public WebSocketServerProvider, public SceneProvider
{
Q_OBJECT

public:
    DiaScene(WebSocketServer *ws, AudioProcessor *jackP, SceneBuilder *builder,
             MonitorIO* monitorIoP,
             PlanedTriggerHandler *triggerHandlerP,
             QString name, QJsonObject serialized = QJsonObject());
    QList<Device> getLights();
    QList<Device> getUsedLights();
    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
    void stop();
    void start();
    QJsonObject serialize();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();

    void addScene(QSharedPointer<Scene>scene, QString name, QString desc, float fadeInDuration);
    void addScene(QSharedPointer<Dia>dia);
    void loadSerialized(QJsonObject serialized);
public slots:
    void music();
private:

    QJsonObject getState(bool addScenes);

    QMap<int, QSharedPointer<Dia> > scenes;
    QList<int> order;
    int current;
    int fadingTo;

    bool nextOnMusic;
    QTime fadeTimer;
    FusionScene fusion;
    WebSocketServer *wss;
    SceneBuilder builder;

    AudioProcessor *jack;

    void next();

    void setNextOnMusic(bool enable);

    MonitorIO *monitorIo;

    int sceneIdCountr;

    PlanedTriggerHandler *triggerHandler;


    // SceneProvider interface
    void prev();
    
public:
    QList<QSharedPointer<Scene> > getScenes();

    // SceneProvider interface
signals:
   void scenesChanged();
   void scenesInitialized();

   // Scene interface
public:
   QList<QSharedPointer<Scene> > getSubScenes();
};

#endif // DIASCENE_H
