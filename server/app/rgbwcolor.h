#ifndef RGBWCOLOR_H
#define RGBWCOLOR_H

#include "serializable.h"
#include <QColor>
#include "devices/devicestate.h"

class RGBWColor : public Serializable
{
public:
    RGBWColor();
    RGBWColor(QColor color, float alpha = 1.0);
    RGBWColor(float rp, float gp, float bp, float wp, float alpha = 1.0);
    RGBWColor(QJsonObject serialized);
    RGBWColor(const RGBWColor& copy);

    RGBWColor fusionWith(RGBWColor other, float opacity, DeviceState::FusionType fusionType = DeviceState::AV);

    static RGBWColor create(int r, int g, int b, int w);

    bool operator ==(RGBWColor const& other);
    bool operator !=(RGBWColor const& b);

    void multiply(float factor);
    void multiplyAlpha(float factor);
    QColor getRGB();

private:
    float r,g,b,w,a;
    float multiplyChannel(float original, float factor);

    // Serializable interface
public:
    QJsonObject serialize();
    float getR() const;
    void setR(float value);
    float getG() const;
    void setG(float value);
    float getB() const;
    void setB(float value);
    float getW() const;
    void setW(float value);
    float getA() const;
    void setA(float value);
};

#endif // RGBWCOLOR_H
