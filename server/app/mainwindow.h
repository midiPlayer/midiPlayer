#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QObject>
#include <QList>
#include <QMap>
#include "scene.h"
#include "audioprocessor.h"
#include "devices/device.h"
#include "websocketserver.h"
#include "oladeviceprovider.h"
#include <QList>
#include "outputdevice.h"
#include <QTimer>
#include "remotebeat.h"
#include "discoscene.h"
#include <QSharedPointer>
#include "scenebuilder.h"
#include <QSettings>
#include "diascene.h"
#include "fileioprovider.h"
#include "monitorio.h"
#include "channeltester.h"
#include "dynamicvirtualdevicemanager.h"
#include "ungroupvirtualdevicemanager.h"
#include "oscconnector.h"
#include "beamer/beamereffecthandler.h"
#include "beamer/beamerdispatcher.h"
#include "beamer/beameroutdevice.h"
#include "savehandler.h"

class MainWindow : public QObject, public WebSocketServerProvider, public SaveHandler
{
    Q_OBJECT

public:
    explicit MainWindow(WebSocketServer *ws);
    ~MainWindow();
    void getChanges();

    void loadScenes(QJsonObject data);
public slots:
    void trigger();
    void save();

private:
    void resetFadeStart();
    float getTimeSinceFadePercentage(int duration);
    QJsonObject getSettings();


    WebSocketServer *wss;
    OscConnector oscc;
    QMap<QString, QSharedPointer<DeviceState> > status;
    bool offsetRequested;
    QSettings settings;
    AudioProcessor jackProcessor;
    QList<OutputDevice*> outDevices;
    QTimer timer;
    bool getChangesRunning;
    DynamicVirtualDeviceManager virtualDevManager;
    UngroupVirtualDeviceManager ungroupedDevs;
    MonitorIO monitorIO;
    BeamerEffectHandler beamerEffectHandler;
    SceneBuilder sceneBuilder;
    OlaDeviceProvider olaDeviceProvider;
    RemoteBeat remoteBeat;
    QSharedPointer<DiaScene> mainScene;
    FileIOProvider filieIoProv;
    ChannelTester channelTester;
    BeamerDispatcher beamerDispatcher;
    BeamerOutDevice beamerOutDevice;




    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject msg, int clientId);

    QJsonObject getSceneJson(QSharedPointer<Scene> scene);
};

#endif // MAINWINDOW_H
