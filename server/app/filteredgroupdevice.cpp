#include "filteredgroupdevice.h"

FilteredGroupDevice::FilteredGroupDevice(QSharedPointer<GroupDevice> dev, QList<QString> filter) :
    GroupDevice(dev->getGroupId()),
    devices(),
    groups(),
    groupType(dev->getGroupType()),
    builder(dev->getSelectorBuilder()),
    displayName(dev->getDisplayName())
{
    QHash<QString, QSharedPointer<Device> >  avDevs = dev.data()->getChildren();
    QHash<QString, QSharedPointer<GroupDevice> >  avGroups = dev.data()->getChildGroups();
    foreach(QString f, filter){
        if(avDevs.contains(f)){
            QSharedPointer<Device> d = avDevs.value(f);
            devices.insert(f,d);
        }
    }
    foreach (QString gid, avGroups.keys()) {
        if(filter.contains(gid))
            groups.insert(gid,avGroups.value(gid));
        else
            groups.insert(gid,QSharedPointer<FilteredGroupDevice>(new FilteredGroupDevice(avGroups.value(gid),filter)));
    }
}

bool FilteredGroupDevice::isSerializable()
{
    return false;
}

QHash<QString, QSharedPointer<Device> > FilteredGroupDevice::getChildren()
{
    return devices;
}

QHash<QString, QSharedPointer<GroupDevice> > FilteredGroupDevice::getChildGroups()
{
    return groups;
}

QString FilteredGroupDevice::getGroupType()
{
    return groupType;
}

QSharedPointer<SelectorBuilder> FilteredGroupDevice::getSelectorBuilder()
{
    return builder;
}

QString FilteredGroupDevice::getDisplayName()
{
    return displayName;
}

