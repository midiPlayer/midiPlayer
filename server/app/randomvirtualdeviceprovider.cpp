#include "randomvirtualdeviceprovider.h"
#include "devices/groupdevice.h"

RandomDeviceChooser::RandomDeviceChooser(VirtualDeviceManager *parent) :
    manager(parent)
{

}

/*
 * returns, if possible, <num> random Devices
*/
QList<QString> RandomDeviceChooser::choose(int num)
{
    QList<QString> freeDevs = manager->getDevices().keys();
    QHash<QString, QSharedPointer<GroupDevice> > groupDevs = manager->getGroupDevices();
    QList<QString> groupKeys = groupDevs.keys();

    QList<QString> chosenDevs;
    if(num == 0){
        chosenDevs = freeDevs;
        foreach(QSharedPointer<GroupDevice> gdev, groupDevs){
            chosenDevs += gdev.data()->getChildren().keys();
        }
    }
    else{
        for (int i = 0; i < num && (freeDevs.count() + groupKeys.count()) > 0 ; i++) {
            int index = rand() % (freeDevs.count() + groupKeys.count());
            if(index < freeDevs.count()){
                chosenDevs.append(freeDevs.at(index));
                freeDevs.removeAt(index);
            }
            else{
                index -= freeDevs.count();
                chosenDevs += groupDevs.value(groupKeys.at(index)).data()->getChildren().keys();
                groupKeys.removeAt(i);
            }
        }
    }
    return chosenDevs;
}
