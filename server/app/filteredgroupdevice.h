#ifndef FILTEREDGROUPDEVICE_H
#define FILTEREDGROUPDEVICE_H
#include "devices/groupdevice.h"

class FilteredGroupDevice  : public GroupDevice
{
public:
    FilteredGroupDevice(QSharedPointer<GroupDevice> dev, QList<QString> filter);
    // Device interface
public:
    bool isSerializable();

    // GroupDevice interface
public:
    QHash<QString, QSharedPointer<Device> > getChildren();
    QHash<QString, QSharedPointer<GroupDevice> > getChildGroups();
    QString getGroupType();
private:
    QHash<QString, QSharedPointer<Device> > devices;
    QHash<QString, QSharedPointer<GroupDevice> > groups;
    QString groupType;
    QSharedPointer<SelectorBuilder> builder;
    QString displayName;
    // GroupDevice interface
public:
    QSharedPointer<SelectorBuilder> getSelectorBuilder();

    // GroupDevice interface
public:
    QString getDisplayName();

};

#endif // FILTEREDGROUPDEVICE_H
