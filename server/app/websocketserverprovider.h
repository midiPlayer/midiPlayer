#ifndef WEBSOCKETSERVERPROVIDER_H
#define WEBSOCKETSERVERPROVIDER_H

#include <QList>
#include <QString>
#include <QJsonObject>
#include <QWebSocket>
#include "websocketserverprovideridprovider.h"

class WebSocketServer;

class WebSocketServerProvider
{
public:
    WebSocketServerProvider(WebSocketServer* s, WebSocketServerProvider *parent = 0);
    virtual ~WebSocketServerProvider();
    virtual QString getRequestType() = 0;
    void registerClient(QJsonObject msg, QWebSocket *client);
    void unregisterClient(QJsonObject msg, QWebSocket *client);
    void unregisterClient(QWebSocket *client);
    void onMessage(QJsonObject msg, QWebSocket *client);
    int providerId;

private:
    virtual void clientRegistered(QJsonObject,int) {}
    virtual void clientUnregistered(QJsonObject,int){}
    virtual void clientMessage(QJsonObject,int) {}
    void lazyRegister();
    void lazyUnregister();

protected:
    void sendMsg(QJsonObject msg, int client_id, bool onlyForProvierID = false);
    void sendMsg(QJsonObject msg, bool onlyForProvierID = false);
    void sendMsgButNotTo(QJsonObject msg, int client_id, bool onlyForProvierID = false);
    bool isSomeoneListening();

    int clientIdCounter;
    QMap<QWebSocket*,int> connectedSockets;
    WebSocketServer* server;
    QList<WebSocketServerProvider*> children;
};

#endif // WEBSOCKETSERVERPROVIDER_H
