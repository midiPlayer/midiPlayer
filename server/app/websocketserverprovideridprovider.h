#ifndef WEBSOCKETSERVERPROVIDERIDPROVIDER_H
#define WEBSOCKETSERVERPROVIDERIDPROVIDER_H

#include <QString>

class WebSocketServerProviderIdProvider
{
public:
    WebSocketServerProviderIdProvider();
    virtual ~WebSocketServerProviderIdProvider(){}
    virtual int getProviderId() const = 0;
    virtual QString getRequestType() = 0;
};

#endif // WEBSOCKETSERVERPROVIDERIDPROVIDER_H
