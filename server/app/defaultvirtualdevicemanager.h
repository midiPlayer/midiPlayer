#ifndef DEFAULTVIRTUALDEVICEMANAGER_H
#define DEFAULTVIRTUALDEVICEMANAGER_H
#include "virtualDeviceManager/virtualdevicemanager.h"
class DefaultVirtualDeviceManager : public VirtualDeviceManager
{
public:
    DefaultVirtualDeviceManager();
    QHash<QString,QSharedPointer<Device> > getDevices();
    void addDevice(QSharedPointer<Device> dev);
private:
    QHash<QString,QSharedPointer<Device> > devices;
};


#endif // DEFAULTVIRTUALDEVICEMANAGER_H
