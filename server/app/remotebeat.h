#ifndef REMOTEBEAT_H
#define REMOTEBEAT_H

#include "websocketserverprovider.h"
#include "audioprocessor.h"
#include <QObject>
class RemoteBeat :  public QObject, public WebSocketServerProvider
{
Q_OBJECT
public:
    RemoteBeat(WebSocketServer* ws,AudioProcessor*jp,QObject * parent = 0);
    void clientRegistered(QJsonObject,int){}
    void clientUnregistered(QJsonObject,int){}
    void clientMessage(QJsonObject,int){}
    QString getRequestType();
public slots:
    void onOnset();
    void onBeat();
};

#endif // REMOTEBEAT_H
