#ifndef MUSICSCENE_H
#define MUSICSCENE_H
#include "scene.h"
#include "devices/musicdevice.h"
#include "websocketserverprovider.h"

class MusicScene : public Scene, public WebSocketServerProvider
{
public:
    MusicScene(QString name, WebSocketServer *ws, QJsonObject serialized = QJsonObject());
    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject, int){}
    QString getRequestType();
    void stop();
    void start();
    QJsonObject serialize();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();

private:
    MusicDevice player;

};

#endif // MUSICSCENE_H
