#ifndef RANDOMVIRTUALDEVICEPROVIDER_H
#define RANDOMVIRTUALDEVICEPROVIDER_H

#include "virtualDeviceManager/virtualdevicemanager.h"

class RandomDeviceChooser
{
public:
    RandomDeviceChooser(VirtualDeviceManager *parent);
private:
    VirtualDeviceManager* manager;

    // VirtualDeviceManager interface
public:
    QList<QString> choose(int num);
};

#endif // RANDOMVIRTUALDEVICEPROVIDER_H
