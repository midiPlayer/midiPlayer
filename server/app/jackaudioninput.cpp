#include "jackaudioninput.h"
#include "audioprocessor.h"

JackAudionInput::JackAudionInput(AudioProcessor *parent):
    audioP(parent)
{
    ibuf = new jack_sample_t[1025];

    if ((jackHandle = jack_client_open("MidiPlayerInput", JackNullOption, NULL)) == 0) {
      qDebug() << "JACK server not running ?";
      exit(1);
    }
    jackAudioIn = jack_port_register(jackHandle,"Audio Trigger in",JACK_DEFAULT_AUDIO_TYPE,JackPortIsInput,0);
    samplerate = jack_get_sample_rate (jackHandle);
}

JackAudionInput::~JackAudionInput()
{
    jack_client_close (jackHandle);
}

int JackAudionInput::jack_static_callback(jack_nframes_t nframes, void *arg)
{
    return ((JackAudionInput *) arg)->jack_callback(nframes);
}

int JackAudionInput::jack_callback(jack_nframes_t nframes)
{
    audioP->pushAudio((jack_sample_t *)jack_port_get_buffer (jackAudioIn, nframes),nframes);
    return 0;
}

void JackAudionInput::enable()
{
    jack_set_process_callback(jackHandle, jack_static_callback, (void *)this);
    if (jack_activate(jackHandle)) {
        qDebug() << "Can't activate JACK.";
        exit(1);
    }
}
