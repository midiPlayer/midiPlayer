#ifndef JACKAUDIONINPUT_H
#define JACKAUDIONINPUT_H
#include <jack/jack.h>
#include "audioinput.h"

class AudioProcessor;

class JackAudionInput : public AudioInput
{
public:
    typedef jack_default_audio_sample_t jack_sample_t;
    JackAudionInput(AudioProcessor* parent);
    ~JackAudionInput();

private:
    jack_client_t *jackHandle;
  jack_port_t *jackAudioIn;
  jack_sample_t *ibuf;

  AudioProcessor* audioP;

  static int jack_static_callback(jack_nframes_t nframes, void *arg);
  int jack_callback(jack_nframes_t nframes);


  // AudioInput interface
public:
  void enable();
};

#endif // JACKAUDIONINPUT_H
