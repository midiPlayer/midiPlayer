#include "screencolorscene.h"
#include "devices/channeldevicestate.h"
#include "websocketserver.h"

#define KEY_SELECTED_DEVS "selected_devices"

ScreenColorScene::ScreenColorScene(VirtualDeviceManager *manager, WebSocketServer *ws, QString name, QJsonObject serialized) :
    Scene(name,serialized),
    WebSocketServerProvider(ws),
    scanner(ws),
    filterDevManager(manager,{Device::RGB, Device::RGBW}),
    selectDevManager(&filterDevManager,ws,serialized.value(KEY_SELECTED_DEVS).toObject())
{
    
    connect(&filterDevManager,SIGNAL(virtualDevicesChanged()),&selectDevManager,SLOT(updateDevices()));
    connect(&selectDevManager,SIGNAL(virtualDevicesChanged()),this,SLOT(updateDevices()));

    connect(&scanner,SIGNAL(recievedColors(QList<QColor>)),this,SLOT(setColors(QList<QColor>)));
}

QJsonObject ScreenColorScene::serialize()
{
    return serializeScene();
}

QMap<QString, QSharedPointer<DeviceState> > ScreenColorScene::getDeviceState()
{
    return state;
}

QString ScreenColorScene::getSceneTypeString()
{
    return getSceneTypeStringStaticaly();
}

QString ScreenColorScene::getSceneTypeStringStaticaly()
{
    return "screenColor";
}

void ScreenColorScene::clientRegistered(QJsonObject, int id)
{
    QJsonObject config;
    config.insert("devManager",selectDevManager.providerId);
    sendMsg(config,id,true);
}

QString ScreenColorScene::getRequestType()
{
    return "screenColor";
}


void ScreenColorScene::updateDevices()
{
    state.clear();
    foreach(QSharedPointer<Device> dev, selectDevManager.getDevices().values() ){
        state.insert(dev.data()->getDeviceId(),dev.data()->createEmptyState());
    }
    updateColor();
}

void ScreenColorScene::setColors(QList<QColor> colors)
{
    this->colors = colors;
    updateColor();
}

void ScreenColorScene::updateColor()
{
    if(colors.length() > 1){
        qDebug() << "WARNING: currently only one color is supported!";
        return;
    }
    QColor c(0,0,0);

    if(colors.length() > 0){
        c = colors.first();
    }

    foreach(QSharedPointer<DeviceState> s, state){
        QSharedPointer<ChannelDeviceState> cDevState = s.dynamicCast<ChannelDeviceState>();
        cDevState.data()->setRGB(c);
    }
}
