#ifndef SCALAINPUT_H
#define SCALAINPUT_H

#include "websocketserverprovider.h"
#include "serializable.h"
#include "audioprocessor.h"
#include "RemoteTypes/remotenumber.h"

class ScalaInput : public QObject, public WebSocketServerProvider, public Serializable
{
    Q_OBJECT

public:
    ScalaInput(WebSocketServer *ws, AudioProcessor* audioP, QJsonObject serialized);
    void start();
    void stop();

    // Serializable interface
public:
    QJsonObject serialize();

    // WebSocketServerProvider interface
public:
    QString getRequestType();

    float getLoudness();

public slots:
    void setLoudness(float newLoudness);

private:
    void clientRegistered(QJsonObject, int clientId);
    AudioProcessor* audio;
    RemoteNumber amplification;
    RemoteNumber inSmoothness, outSmoothness;
    int loudnessCtr;
    float loudnessSum;
    float loudness;
};

#endif // SCALAINPUT_H
