#include "rgbwcolor.h"
#include "math.h"

RGBWColor::RGBWColor():
  r(0),
  g(0),
  b(0),
  w(0),
  a(1.0)
{}

RGBWColor::RGBWColor(QColor color, float alpha):
    r(color.redF()),
    g(color.greenF()),
    b(color.blueF()),
    w(0),
    a(color.alphaF())
{

}

RGBWColor::RGBWColor(float rp, float gp, float bp, float wp, float alpha):
    r(rp),
    g(gp),
    b(bp),
    w(wp),
    a(alpha)
{
    if(r > 1 || g > 1 || b > 1 || w > 1)
        throw("color is invalid");
}

RGBWColor::RGBWColor(QJsonObject serialized) :
    r(serialized.value("r").toDouble()),
    g(serialized.value("g").toDouble()),
    b(serialized.value("b").toDouble()),
    w(serialized.value("w").toDouble()),
    a(serialized.value("a").toDouble())
{

}

RGBWColor::RGBWColor(const RGBWColor &copy):
    r(copy.r),
    g(copy.g),
    b(copy.b),
    w(copy.w),
    a(copy.a)
{

}

RGBWColor RGBWColor::fusionWith(RGBWColor other, float opacity, DeviceState::FusionType fusionType)
{
    if(fusionType == DeviceState::OVERRIDE){
            fusionType = DeviceState::AV;
            opacity = other.a;
    }
    if(fusionType == DeviceState::MAX){
        return RGBWColor((r*a> other.r * other.a ? r : other.r),
                         (g*a > other.g * other.a ? g : other.g),
                         (b*a > other.b * other.a ? b : other.b),
                         (w*a > other.w * other.a ? w : other.w),
                         (a > other.a ? a : other.a));
    }
    else if(fusionType == DeviceState::MIN){
        return RGBWColor((r*a < other.r * other.a ? r : other.r),
                         (g*a < other.g * other.a ? g : other.g),
                         (b*a < other.b * other.a ? b : other.b),
                         (w*a < other.w * other.a ? w : other.w),
                         (a < other.a ? a : other.a));
    }
    else if(fusionType == DeviceState::Multiply){
        return RGBWColor(multiplyChannel(r,other.r),
                         multiplyChannel(g,other.g),
                         multiplyChannel(b,other.b),
                         multiplyChannel(w,other.w),
                         multiplyChannel(a,other.a));
    }
    else{//AVG
        return RGBWColor(r * (1.0-opacity) + other.r * opacity,
                         g * (1.0-opacity) + other.g * opacity,
                         b * (1.0-opacity) + other.b * opacity,
                         w * (1.0-opacity) + other.w * opacity,
                         a * (1.0-opacity) + other.a * opacity);
    }
}

RGBWColor RGBWColor::create(int r, int g, int b, int w)
{
    return RGBWColor(r / 255.0, g / 255.0, b / 255.0, w / 255.0);
}

bool RGBWColor::operator ==(const RGBWColor &other)
{
    if(r != other.r) return false;
    if(g != other.g) return false;
    if(b != other.b) return false;
    if(w != other.w) return false;
    if(a != other.a) return false;
    return true;
}

bool RGBWColor::operator !=(const RGBWColor &b)
{
    return !(operator ==(b));
}

void RGBWColor::multiply(float factor)
{
    r = multiplyChannel(r,factor);
    g = multiplyChannel(g,factor);
    b = multiplyChannel(b,factor);
    w = multiplyChannel(w,factor);
}

void RGBWColor::multiplyAlpha(float factor)
{
    a = multiplyChannel(a, factor);
}

QColor RGBWColor::getRGB()
{
    QColor c = QColor::fromRgbF(r,g,b);
    c.setAlphaF(a);
    return c;
}

float RGBWColor::getA() const
{
    return a;
}

void RGBWColor::setA(float value)
{
    a = value;
}

float RGBWColor::multiplyChannel(float original, float factor)
{
    original *= factor;
    if(original > 1.0) original = 1;
    else if(original < 0) original = 0;
    return original;
}

float RGBWColor::getW() const
{
    return w;
}

void RGBWColor::setW(float value)
{
    w = value;
}

float RGBWColor::getB() const
{
    return b;
}

void RGBWColor::setB(float value)
{
    b = value;
}

float RGBWColor::getG() const
{
    return g;
}

void RGBWColor::setG(float value)
{
    g = value;
}

float RGBWColor::getR() const
{
    return r;
}

void RGBWColor::setR(float value)
{
    r = value;
}

QJsonObject RGBWColor::serialize()
{
    QJsonObject ret;
    ret.insert("r",r);
    ret.insert("g",g);
    ret.insert("b",b);
    ret.insert("w",w);
    ret.insert("a",a);
    return ret;
}
