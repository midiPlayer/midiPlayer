#include "remotefloat.h"

RemoteFloat::RemoteFloat(WebSocketServer *ws, WebSocketServerProvider *parent, float value):
    RemoteType(ws,parent),
    floatValue(value)
{

}

float RemoteFloat::val() const
{
    return floatValue;
}

void RemoteFloat::setFloatValue(float value)
{
    floatValue = value;
    dataChanged();
}

QJsonValue RemoteFloat::getValue()
{
    return QJsonValue(floatValue);
}

void RemoteFloat::setValue(QJsonValue val)
{
    floatValue = val.toDouble();
}
