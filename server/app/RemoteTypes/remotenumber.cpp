#include "remotenumber.h"

RemoteNumber::RemoteNumber(WebSocketServer *ws, WebSocketServerProvider* parent, int num):
    RemoteType(ws, parent),
    number(num)
{

}

int RemoteNumber::getNumber()
{
 return number;
}

void RemoteNumber::setNumber(int value)
{
    number = value;
    dataChanged();
}

QJsonValue RemoteNumber::getValue()
{
    return QJsonValue(number);
}

void RemoteNumber::setValue(QJsonValue val)
{
    number = val.toInt(number);
}
