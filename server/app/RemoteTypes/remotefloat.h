#ifndef REMOTEFLOAT_H
#define REMOTEFLOAT_H

#include "remotetype.h"
class RemoteFloat : public RemoteType
{
public:
    RemoteFloat(WebSocketServer* ws, WebSocketServerProvider* parent, float value);

    float val() const;
    void setFloatValue(float value);

private:
    float floatValue;

    // RemoteType interface
private:
    QJsonValue getValue();
    void setValue(QJsonValue);
};

#endif // REMOTEFLOAT_H
