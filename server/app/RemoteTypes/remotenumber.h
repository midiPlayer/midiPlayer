#ifndef REMOTENUMBER_H
#define REMOTENUMBER_H

#include "remotetype.h"

class RemoteNumber : public RemoteType
{
public:
    RemoteNumber(WebSocketServer *ws, WebSocketServerProvider* parent, int num = 0);

    int getNumber();
    void setNumber(int value);

private:
    int number;

    // RemoteType interface
private:
    QJsonValue getValue();
    void setValue(QJsonValue val);
};

#endif // REMOTENUMBER_H
