#ifndef REMOTESTRING_H
#define REMOTESTRING_H

#include "remotetype.h"

class RemoteString : public RemoteType
{
public:
    RemoteString(WebSocketServer *ws, WebSocketServerProvider *parent, QString val = "");

    QString getString() const;
    void setString(const QString &value);

private:
    QString string;


    // RemoteType interface
private:
    QJsonValue getValue();
    void setValue(QJsonValue val);
};

#endif // REMOTESTRING_H
