#include "remotetype.h"
#include "../websocketserver.h"
#include <QJsonObject>

RemoteType::RemoteType(WebSocketServer *ws, WebSocketServerProvider* parent) :
    QObject(),
    WebSocketServerProvider(ws,parent)
{

}

QString RemoteType::getRequestType()
{
    return "remoteType";
}

void RemoteType::clientRegistered(QJsonObject, int)
{
    dataChanged();
}

void RemoteType::clientMessage(QJsonObject msg, int)
{
    if(msg.contains("v")){
        setValue(msg.value("v"));
        emit valueChanged();
    }
}

void RemoteType::dataChanged()
{
    QJsonObject hello;
    hello.insert("v",getValue());
    sendMsg(hello,true);
}
