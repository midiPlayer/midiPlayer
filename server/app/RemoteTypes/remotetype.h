#ifndef REMOTETYPE_H
#define REMOTETYPE_H

#include "../websocketserverprovider.h"
#include <QJsonValue>

class RemoteType : public QObject, public WebSocketServerProvider
{
Q_OBJECT

public:
    RemoteType(WebSocketServer *ws, WebSocketServerProvider *parent);

    // WebSocketServerProvider interface
public:
    QString getRequestType();

signals:
    void valueChanged();

private:
    void clientRegistered(QJsonObject, int);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject msg, int);
protected:
    void dataChanged();
    virtual QJsonValue getValue() = 0;
    virtual void setValue(QJsonValue) = 0;
};

#endif // REMOTETYPE_H
