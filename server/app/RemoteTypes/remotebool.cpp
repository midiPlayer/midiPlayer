#include "remotebool.h"

RemoteBool::RemoteBool(WebSocketServer *ws, WebSocketServerProvider *parent, bool initValue) :
    RemoteType(ws, parent),
    boolValue(initValue)
{

}

bool RemoteBool::getBoolValue() const
{
    return boolValue;
}

void RemoteBool::setBoolValue(bool value)
{
    boolValue = value;
    dataChanged();
}

QJsonValue RemoteBool::getValue()
{
    return QJsonValue(boolValue);
}

void RemoteBool::setValue(QJsonValue value)
{
    boolValue = value.toBool(boolValue);
}
