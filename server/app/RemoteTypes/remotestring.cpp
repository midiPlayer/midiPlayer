#include "remotestring.h"

RemoteString::RemoteString(WebSocketServer *ws, WebSocketServerProvider* parent, QString val):
    RemoteType(ws, parent),
    string(val)
{

}

QString RemoteString::getString() const
{
    return string;
}

void RemoteString::setString(const QString &value)
{
    string = value;
    dataChanged();
}

QJsonValue RemoteString::getValue()
{
    return QJsonValue(string);
}

void RemoteString::setValue(QJsonValue val)
{
    string = val.toString(string);
}
