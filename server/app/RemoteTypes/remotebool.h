#ifndef REMOTEBOOL_H
#define REMOTEBOOL_H

#include "remotetype.h"

class RemoteBool : public RemoteType
{
public:
    RemoteBool(WebSocketServer *ws, WebSocketServerProvider* parent, bool initValue = false);

    bool getBoolValue() const;
    void setBoolValue(bool value);

private:
    bool boolValue;

    // RemoteType interface
protected:
    QJsonValue getValue();
    void setValue(QJsonValue value);
};

#endif // REMOTEBOOL_H
