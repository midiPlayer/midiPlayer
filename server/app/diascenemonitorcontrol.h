#ifndef DIASCENEMONITORCONTROL_H
#define DIASCENEMONITORCONTROL_H
#include "audioprocessor.h"
#include "trigger.h"
#include "websocketserverprovider.h"
#include <QString>
#include "monitorio.h"
#include <QObject>
#include "scene.h"
#include "scenes/planedtriggerhandler.h"

class DiaSceneMonitorControl: public QObject, public WebSocketServerProvider
{
Q_OBJECT

public:
    DiaSceneMonitorControl(WebSocketServer *ws, AudioProcessor *jackP, MonitorIO *monitorP, float* fadeInDurP, PlanedTriggerHandler *trigerHandler, QJsonObject serialized = QJsonObject());
    void start();
    void stop();

    void clientRegistered(QJsonObject,int clientId);
    void clientUnregistered(QJsonObject,int){}
    void clientMessage(QJsonObject msg,int clientId);
    QString getRequestType();

    void loadSerialized(QJsonObject serialized);
public slots:
    void triggered();
    QJsonObject serialize(bool includeTrigger = true);
private:
    Trigger trigger;
    bool random;
    MonitorIO *monitor;
    int currentRes;
    float* fadeInDur;
    double beatFadeTime;
    QList<QString> resources;

};

#endif // DIASCENEMONITORCONTROL_H
