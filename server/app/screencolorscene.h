#ifndef SCREENCOLORSCENE_H
#define SCREENCOLORSCENE_H

#include "scene.h"
#include "screencolorscenescanner.h"
#include "virtualDeviceManager/virtualdevicemanager.h"
#include "filtervirtualdevicemanager.h"
#include "virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include <QMap>

class ScreenColorScene : public Scene, public WebSocketServerProvider
{
Q_OBJECT

public:
    ScreenColorScene(VirtualDeviceManager *manager, WebSocketServer *ws, QString name, QJsonObject serialized);
    QJsonObject serialize();
    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject,int){}
    void clientMessage(QJsonObject,int) {}
    QString getRequestType();

public slots:
    void setColors(QList<QColor> colors);
    void updateDevices();

private:
    ScreenColorSceneScanner scanner;
    FilterVirtualDeviceManager filterDevManager;
    SelectVirtualDeviceManager selectDevManager;
    QMap<QString, QSharedPointer<DeviceState> > state;
    QList<QColor> colors;
    void updateColor();
};

#endif // SCREENCOLORSCENE_H
