#ifndef COLORBUTTON_H
#define COLORBUTTON_H
#include "websocketserverprovider.h"
#include <QColor>
#include <QList>
#include "RemoteTypes/remotestring.h"
#include "scenes/previewinjektor.h"

class ColorButton: public QObject, public WebSocketServerProvider
{
Q_OBJECT
public:
    ColorButton(WebSocketServer *ws, QJsonObject serialized = QJsonObject());

    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject,int clientId);
    void clientMessage(QJsonObject msg,int clientId);
    QString getRequestType();

    QList<QColor> getColors() const;
    void setColors(const QList<QColor> &value);

    void loadSerialized(QJsonObject serialized);
    QJsonObject serialize();

    QColor getRandomColor();
    QColor getRandomDiffrentColor();
    QColor getRandomDiffrentColor(int last);
    QColor getRandomDiffrentColor(QColor last);
    void setPreviewInj(PreviewInjektor *value);

signals:
    void colorChanged();
private:
    QList<QColor>colors;
    QString getColorString();
    void loadColosFromString(QString colorString);

    int lastColorIndex;
    RemoteString selectionMethod;

    PreviewInjektor* previewInj;
    int previewingClient;

    bool hasPreview();
};

#endif // COLORBUTTON_H
