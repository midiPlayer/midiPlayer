#ifndef CHANNELTESTER_H
#define CHANNELTESTER_H

#include "websocketserverprovider.h"
#include "oladeviceprovider.h"
#include <QSet>

class ChannelTester : public WebSocketServerProvider
{
public:
    ChannelTester(WebSocketServer *ws, OlaDeviceProvider *olaD);
    void clientRegistered(QJsonObject, int){}
    void clientUnregistered(QJsonObject, int id);
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
private:
    QSet<int> activeClients;
    OlaDeviceProvider *ola;
};

#endif // CHANNELTESTER_H
