#include "websocketserverprovider.h"
#include "websocketserver.h"
/*
 * If a parent is set, the provider id is generated lacy
*/
WebSocketServerProvider::WebSocketServerProvider(WebSocketServer *s, WebSocketServerProvider* parent) :
    providerId(-1),
    clientIdCounter(0),
    connectedSockets(),
    server(s),
    children()
{
    if(parent != 0){
        parent->children.append(this);
    }
    else{
        server->registerProvider(this);
    }
}

WebSocketServerProvider::~WebSocketServerProvider()
{
    server->unregisterProvider(this);
}

void WebSocketServerProvider::registerClient(QJsonObject msg, QWebSocket *client)
{
    if(connectedSockets.contains(client)){
        clientRegistered(msg,connectedSockets.value(client));
        return; //allreaddy registered
    }
    connectedSockets.insert(client,clientIdCounter);

    if(connectedSockets.count() == 1){//this is the forst child
        foreach (WebSocketServerProvider* child, children) {
            child->lazyRegister();//register all children
        }
    }

    clientRegistered(msg,clientIdCounter);
    clientIdCounter++;
}

void WebSocketServerProvider::unregisterClient(QJsonObject msg, QWebSocket *client)
{
    if(!connectedSockets.contains(client))
        return; //we don't know him
    int  id = connectedSockets.value(client);
    clientUnregistered(msg,id);
    connectedSockets.remove(client);

    if(connectedSockets.count() == 0){
        foreach (WebSocketServerProvider* child, children) {
            child->lazyUnregister();
        }
    }
}

void WebSocketServerProvider::unregisterClient(QWebSocket *client)
{
    unregisterClient(QJsonObject(),client);
}

void WebSocketServerProvider::onMessage(QJsonObject msg, QWebSocket *client)
{
    if(!connectedSockets.contains(client))
        return;
    int id = connectedSockets.value(client);
    clientMessage(msg,id);
}

void WebSocketServerProvider::lazyRegister()
{
    server->registerProvider(this);
}

void WebSocketServerProvider::lazyUnregister()
{
    server->unregisterProvider(this);
}

/**
 * @brief WebSocketServerProvider::sendMsg
 * @param msg
 * @param client_id
 * @param onlyForProvierID false: send message to request type --> all instances of this class; true--> only for this instance
 * @return
 */
void WebSocketServerProvider::sendMsg(QJsonObject msg, int client_id,bool onlyForProvierID)
{
    QWebSocket* ws = connectedSockets.key(client_id);
    server->sendData(msg,ws,this,onlyForProvierID);
}

void WebSocketServerProvider::sendMsg(QJsonObject msg,bool onlyForProvierID)
{
    foreach(QWebSocket* ws, connectedSockets.keys()){
        server->sendData(msg,ws,this,onlyForProvierID);
    }
}

void WebSocketServerProvider::sendMsgButNotTo(QJsonObject msg, int client_id,bool onlyForProvierID)
{
    QWebSocket* notTo = connectedSockets.key(client_id);
    foreach(QWebSocket* ws, connectedSockets.keys()){
        if(ws != notTo)
            server->sendData(msg,ws,this,onlyForProvierID);
    }
}

bool WebSocketServerProvider::isSomeoneListening()
{
    return !connectedSockets.isEmpty();
}


