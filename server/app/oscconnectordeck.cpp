#include "oscconnectordeck.h"
#include <QDebug>

OscConnectorDeck::OscConnectorDeck(int nrP) :
    nr(nrP),
    title(""),
    positionRel(0),
    duration(1),
    volume(0),
    play(false),
    speed(1.0)
{

}

int OscConnectorDeck::getNr() const
{
    return nr;
}

QString OscConnectorDeck::getTitle() const
{
    return title;
}

float OscConnectorDeck::getPosition() const
{
    return positionRel * duration;
}

float OscConnectorDeck::getVolume() const
{
    return volume;
}

void OscConnectorDeck::message(QString path, QString params, lo_arg **argv, int argc)
{
    //qDebug() << "deck" + QString::number(nr) + ":"<<path << ":" << params << argc << argv[0]->i;

    if(path == "/mixxx/deck/title" && params == "is"){
        QString title_new = QString((char*)argv[1]);
        if(title != title_new){
            title = title_new;
            emit titleChnaged(title);
            emit somethingChanged();
        }
    }

    if(path == "/mixxx/deck/pos" && params == "if"){
        float position_new = argv[1]->f;
        if(positionRel != position_new){
            positionRel = position_new;
            emit positionChanged(positionRel * duration);
            emit somethingChanged();
        }
    }

    if(path == "/mixxx/deck/duration" && params == "if"){
        float duration_new = argv[1]->f;
        if(duration != duration_new){
            duration = duration_new;
            emit somethingChanged();
        }
    }


    if(path == "/mixxx/deck/volume" && params == "if"){
        float volume_new = argv[1]->f;
        if(volume != volume_new){
            volume = volume_new;
            emit volumeChanged(volume);
            emit somethingChanged();
        }
    }

    if(path == "/mixxx/deck/playing" && params == "ii"){
        bool play_new = argv[1]->i != 0;
        if(play != play_new){
            play = play_new;
            emit playChanged(play);
            emit somethingChanged();
        }
    }

    if(path == "/mixxx/deck/speed" && params == "if"){
        float speed_new = argv[1]->f;
        if(speed != speed_new){
            speed = speed_new;
            emit speedChanged(speed);
            emit somethingChanged();
        }
    }
}

bool OscConnectorDeck::getPlay() const
{
    return play;
}

float OscConnectorDeck::getPositionRel() const
{
    return positionRel;
}

float OscConnectorDeck::getDuration() const
{
    return duration;
}

float OscConnectorDeck::getSpeed() const
{
    return speed;
}
