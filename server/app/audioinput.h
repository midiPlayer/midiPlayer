#ifndef AUDIOINPUT_H
#define AUDIOINPUT_H


class AudioInput
{
public:
    AudioInput();
    virtual ~AudioInput(){}
    int getSamplerate() const;
    virtual void enable() = 0;

protected:
    int samplerate;
};

#endif // AUDIOINPUT_H
