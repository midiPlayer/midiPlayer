#ifndef MONITORIO_H
#define MONITORIO_H

#include "websocketserverprovider.h"

class MonitorIO : public WebSocketServerProvider
{
public:
    MonitorIO(WebSocketServer *wss);
    void clientRegistered(QJsonObject, int){}
    void clientUnregistered(QJsonObject,int){}
    void clientMessage(QJsonObject msg, int);
    QString getRequestType();
    void showRessource(QString res, double fadeTime);
};

#endif // MONITORIO_H
