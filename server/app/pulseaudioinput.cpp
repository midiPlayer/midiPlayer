#include "pulseaudioinput.h"
#include <QDebug>

#include "audioprocessor.h"

PulseAudioInput::PulseAudioInput(AudioProcessor *parent):
    audioP(parent),
    s(NULL)
{

    static const pa_sample_spec ss = {
            .format = PA_SAMPLE_FLOAT32LE,
            .rate = SAMPLERATE,
            .channels = 2
        };

    /* Create the recording stream */
    if (!(s = pa_simple_new(NULL, "midiPlayer", PA_STREAM_RECORD, NULL, "record", &ss, NULL, NULL, &error))) {
        fprintf(stderr, __FILE__": pa_simple_new() failed: %s\n", pa_strerror(error));
        exit(1);
    }

    AudioInput::samplerate = SAMPLERATE;
}

void PulseAudioInput::run()
{
    qDebug() << "run";
    while(true){
        if (pa_simple_read(s, buf, sizeof(buf), &error) < 0) {
                fprintf(stderr, __FILE__": pa_simple_read() failed: %s\n", pa_strerror(error));
                exit(1);
        }
        audioP->pushAudio(&buf[0],BUFSIZE);
    }
}

void PulseAudioInput::enable()
{
    start();
}
