#include "colorbutton.h"
#include "websocketserver.h"
#define KEY_COLORS "colors"

ColorButton::ColorButton(WebSocketServer*ws,QJsonObject serialized):
    QObject(0),
    WebSocketServerProvider(ws),
    lastColorIndex(0),
    selectionMethod(ws,this,serialized.value("selectionMethod").toString("PlaneColorPicker.qml")),
    previewInj(NULL),
    previewingClient(-1)
{

    if(serialized.length() != 0)
    loadSerialized(serialized);
}

void ColorButton::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject message;
    message.insert("colorChanged", getColorString());
    message.insert("selectionMethod",selectionMethod.providerId);
    message.insert("hasPreview",hasPreview());

    sendMsg(message,clientId,true);
}

void ColorButton::clientUnregistered(QJsonObject, int clientId)
{
    if(previewingClient == clientId) {
        previewingClient = -1;
        previewInj->dissablePreview();
    }
}

void ColorButton::loadSerialized(QJsonObject serialized)
{
    if(serialized.contains(KEY_COLORS)){
        colors.clear();
        loadColosFromString(serialized.value(KEY_COLORS).toString(""));
    }
    emit colorChanged();
}

void ColorButton::loadColosFromString(QString colorString)
{
    colors.clear();
    QStringList colorsRead = colorString.split(",", QString::SkipEmptyParts);
    foreach (QString colorString, colorsRead) {
        colors.append(QColor(colorString));
    }
}

bool ColorButton::hasPreview()
{
    return previewInj != NULL;
}

void ColorButton::setPreviewInj(PreviewInjektor *value)
{
    previewInj = value;
}

void ColorButton::clientMessage(QJsonObject msg, int clientId)
{
    if(msg.contains("colorChanged")){
        QString colorString = msg.value("colorChanged").toString("");
        loadColosFromString(colorString);
        emit colorChanged();
    }
    if(msg.contains("preview")){
        if(!hasPreview())
            return; //we can't preview

        if(previewingClient != clientId && previewingClient != -1)
            return; //reject another client is previewing

        previewingClient = clientId;
        previewInj->enablePreview(RGBWColor(msg.value("preview").toObject()));
    }

    if(msg.contains("previewOff")){
        if(previewInj == NULL)
            return;
        previewInj->dissablePreview();
        previewingClient = -1;
    }
    sendMsgButNotTo(msg, clientId, true);
}

QString ColorButton::getRequestType()
{
    return "colorButton";
}

QList<QColor> ColorButton::getColors() const
{
    return colors;
}

void ColorButton::setColors(const QList<QColor> &value)
{
    colors = value;
}

QString ColorButton::getColorString()
{
    QString colorString;
    bool isFirst = true;
    foreach (QColor color, colors) {
        if(!isFirst){
            colorString+=",";
        }
        isFirst = false;
        colorString+=color.name();
    }

    return colorString;
}


QJsonObject ColorButton::serialize()
{
    QJsonObject ret;
    ret.insert(KEY_COLORS,getColorString());
    ret.insert("selectionMethod",selectionMethod.getString());
    return ret;
}

QColor ColorButton::getRandomColor()
{
    int r = rand() % colors.size();
    lastColorIndex = r;
    return colors.at(r);
}

QColor ColorButton::getRandomDiffrentColor()
{
    return getRandomDiffrentColor(lastColorIndex);
}

QColor ColorButton::getRandomDiffrentColor(int last)
{
    int r = 0;
    if(colors.size() > 1){
        r = rand() % (colors.size() - 1);
        if(r >= last)
            r++;
    }
    lastColorIndex = r;
    return colors.at(r);
}

QColor ColorButton::getRandomDiffrentColor(QColor last)
{
    int index = colors.indexOf(last);
    if(index != -1)
        return getRandomDiffrentColor(colors.indexOf(last));
    return getRandomDiffrentColor(lastColorIndex);
}




