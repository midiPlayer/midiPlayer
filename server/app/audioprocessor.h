#ifndef AUDIOPROCESSOR_H
#define AUDIOPROCESSOR_H

#include <QtCore/QObject>
#include <QMap>
#include <aubio/aubio.h>
#include "websocketserverprovider.h"
#include "audioinput.h"
#include "RemoteTypes/remotefloat.h"
class MainWindow;

class AudioProcessor : public QObject, public WebSocketServerProvider
{
  Q_OBJECT 
    
  private:
    AudioInput *audioIn;

    bool musicNotificationRequested;
    bool beatRequested;

    aubio_onset_t *o;
    aubio_tempo_t * tempo;
    fvec_t * tempo_out;
    uint_t buffer_size;
    uint_t hop_size;
    fvec_t *onset;
    cvec_t * fftgrain;
    aubio_fft_t * fft;

    /** internal fvec */
     fvec_t *smpl;
     int pos;
     float minLevel;
     RemoteFloat onsetLevel;

  public:
    AudioProcessor(WebSocketServer *ws, QJsonObject serialized, QObject* parent=0);
    ~AudioProcessor();
    int init(MainWindow *m);

    void pushAudio(float* audio, int nframes);

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject,int) {}
    void clientMessage(QJsonObject msg, int clientId);
    QString getRequestType();
    QJsonObject serialize();

public slots:
    void requestMusicNotification();
    void setOnsetLevel();

  signals:
    void musicNotification();
    void beatNotification();
    void onsetNotification();
    void loudness(float);
};
  
#endif
