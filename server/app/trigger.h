#ifndef TRIGGER_H
#define TRIGGER_H
#include "websocketserverprovider.h"
#include "audioprocessor.h"
#include <QSet>
#include <QObject>
#include "serializable.h"
#include <QTimer>
#include "scenes/planedtriggerhandler.h"

class Trigger : public QObject, public WebSocketServerProvider, public Serializable
{
Q_OBJECT

public:
    Trigger(WebSocketServer *ws, AudioProcessor *jackP, PlanedTriggerHandler *handler, QJsonObject serialized = QJsonObject());
    enum TriggerType{BEAT,TIMER,ONSET,PLANED};
    void start();
    void stop();
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject,int){}
    void clientMessage(QJsonObject msg,int clientId);
    QString getRequestType();
    QSet<TriggerType> triggerConfig;
    QSet<int> handlerTriggerNums;
    QJsonObject serialize();
    QJsonObject getTriggerSourceJson();
    void loadSerialized(QJsonObject serialized);
public slots:
    void beat();
    void onset();
    void onTimer();
    void hanlderTriggered(int num);
signals:
    void trigger();
private:
    AudioProcessor* jack;
    QJsonObject getState();
    void setState(QJsonObject fgt);
    int numBeats;
    int beatCount;
    void triggerInt();

    int interval;
    float randomness;

    QTimer timer;
    void startTimer();
    void stopTimer();
    bool running;
    QJsonArray serializePlanedTriggers();
};

#endif // TRIGGER_H
