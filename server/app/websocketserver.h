#ifndef WEBSOCKETSERVER_H
#define WEBSOCKETSERVER_H

#include "websocketserverprovider.h"
#include <QString>
#include <QMap>
#include <QJsonObject>
#include <QObject>
#include <QJsonDocument>
#include <QDebug>
#include <QWebSocket>
#include <QWebSocketServer>

class WebSocketServer : public QObject
{
Q_OBJECT

public:
    WebSocketServer(){}
    virtual void registerProvider(WebSocketServerProvider *me) = 0;
    virtual void unregisterProvider(WebSocketServerProvider *me) = 0;
    virtual void sendData(QJsonObject data, QWebSocket *reciever, WebSocketServerProvider *provider,bool onlyToProviderId = 0) = 0;

    virtual void disconnectAllClients() = 0;

signals:
    void clientClosed();
};

#endif // WEBSOCKETSERVER_H

