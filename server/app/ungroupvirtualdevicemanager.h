#ifndef UNGROUPVIRTUALDEVICEMANAGER_H
#define UNGROUPVIRTUALDEVICEMANAGER_H

#include "virtualDeviceManager/virtualdevicemanager.h"
class UngroupVirtualDeviceManager : public VirtualDeviceManager
{
Q_OBJECT

public:
    UngroupVirtualDeviceManager(VirtualDeviceManager *parent);

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<Device> > getDevices();
public slots:
    void updateDevices();
private:
    VirtualDeviceManager *parentVdMgr;

    QHash<QString, QSharedPointer<Device> > ungrouped;
    void ungroupGroup(QSharedPointer<GroupDevice> group);
};

#endif // UNGROUPVIRTUALDEVICEMANAGER_H
