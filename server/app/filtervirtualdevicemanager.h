#ifndef FILTERVIRTUALDEVICEMANAGER_H
#define FILTERVIRTUALDEVICEMANAGER_H
#include "virtualDeviceManager/virtualdevicemanager.h"
class FilterVirtualDeviceManager : public VirtualDeviceManager
{
Q_OBJECT

public:
    FilterVirtualDeviceManager(VirtualDeviceManager *manager,  std::vector<Device::DeviceType> accepted = std::vector<Device::DeviceType>());

    QHash<QString,QSharedPointer<Device> > getDevices();
    void addAcceptedType(Device::DeviceType type);
public slots:
    void onVirtualDevsChanged();
private:
    QHash<QString,QSharedPointer<Device> > myDevs;
    QHash<QString,QSharedPointer<GroupDevice> > myGroups;
    VirtualDeviceManager *vDevManager;
    QList<Device::DeviceType> acceptedTypes;
    void generateFilter(QSharedPointer<GroupDevice> group, QList<QString> *filter);

    // VirtualDeviceManager interface
public:
    QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();
};

#endif // FILTERVIRTUALDEVICEMANAGER_H
