#include "planedtriggerhandler.h"

PlanedTriggerHandler::PlanedTriggerHandler()
{


}

PlanedTriggerHandler::PlanedTriggerHandler(PlanedTriggerHandler *parent)
{
    if(parent != NULL)
        connect(parent,SIGNAL(triggered(int)),this,SLOT(trigger(int)));
}

void PlanedTriggerHandler::trigger(int num)
{
    emit triggered(num);
}
