#ifndef BEATSCENE1_H
#define BEATSCENE1_H
#include "../scene.h"
#include "../audioprocessor.h"
#include "../devices/device.h"
#include <QList>
#include "../devices/device.h"
#include <QColor>
#include "../websocketserverprovider.h"
#include "../trigger.h"
#include "../fusionscene.h"
#include "../colorbutton.h"
#include "../filtervirtualdevicemanager.h"
#include "../virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "../ungroupvirtualdevicemanager.h"
#include "../randomvirtualdeviceprovider.h"
#include "../RemoteTypes/remotebool.h"
#include "../scenes/planedtriggerhandler.h"
#include "multiplyer/spheremultiplyer.h"
#include "multiplyer/loudnessmultiplyer.h"
#include "multiplyer/muliplyerwrapper.h"
#include "previewinjektor.h"

class BeatScene1 : public Scene, public WebSocketServerProvider
{
    Q_OBJECT

public:
    BeatScene1(VirtualDeviceManager *manager, AudioProcessor* p, WebSocketServer* ws,QString name,PlanedTriggerHandler* planedTrigger, QJsonObject serialized = QJsonObject());
    QList<Device>getLights();
    QList<Device> getUsedLights();
    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();

    void stop();
    void start();
    void clientRegistered(QJsonObject, int clientIdCounter);
    void clientUnregistered(QJsonObject,int) {}
    void clientMessage(QJsonObject msg,int clientIdCounter);
    QString getRequestType();
    virtual QJsonObject serialize();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();
public slots:
    void devicesChanged();
    void generateNextScene();
private:
    FilterVirtualDeviceManager filterDeviceManager;
    SelectVirtualDeviceManager selectDevManager;
    UngroupVirtualDeviceManager ungrouped;
    RandomDeviceChooser randomDevChooser;
    Trigger backgroundTrigger;
    int smoothDuration;
    QTime smoothTimer;
    FusionScene prev;
    FusionScene next;
    ColorButton colorButton;
    int numChangingDevices;
    bool sameColor;
    RemoteBool inactiveBlack;
    QMap<QString,QColor> deviceColors;
    SphereMultiplyer sphereMulti;
    MuliplyerWrapper sphereMultiWrapper;
    LoudnessMultiplyer loudnessMulti;
    MuliplyerWrapper loudnessMultiWrapper;
    PreviewInjektor prevInjekt;

    QColor getNextColor();


};

#endif // BEATSCENE1_H
