#ifndef PLANEDTRIGGERHANDLER_H
#define PLANEDTRIGGERHANDLER_H


#include <QObject>
class PlanedTriggerHandler : public QObject
{
Q_OBJECT

public:
    PlanedTriggerHandler();
    PlanedTriggerHandler(PlanedTriggerHandler* parent);
public slots:
    void trigger(int num);
signals:
    void triggered(int num);
};

#endif // PLANEDTRIGGERHANDLER_H
