#include "subscenehandler.h"

#define KEY_SCENE "scene"

SubSceneHandler::SubSceneHandler(WebSocketServer* ws, SceneBuilder *builderP, QJsonObject serialized):
    QObject(),
    WebSocketServerProvider(ws),
    Serializable(),
    myScene(),
    builder(builderP)
{
    if(serialized.contains(KEY_SCENE)){
        myScene = builder->rebuild(serialized.value(KEY_SCENE).toObject());
        emit sceneChanged();
    }
}

QSharedPointer<Scene> SubSceneHandler::getScene() const
{
    return myScene;
}

void SubSceneHandler::setScene(QSharedPointer<Scene> newScene)
{
    myScene = newScene;

    emit sceneChanged();

    QJsonObject ret = getSceneMsg();
    sendMsg(ret,true);
}

QString SubSceneHandler::getRequestType()
{
    return "subscenehandler";
}

QJsonObject SubSceneHandler::getSceneMsg()
{
    QJsonObject ret;
    if(myScene.isNull()){
        ret.insert("scene","");
    }else{
        QJsonObject sceneJson;
        sceneJson.insert("name",myScene.data()->getName());
        QSharedPointer<WebSocketServerProvider> wsScene = myScene.dynamicCast<WebSocketServerProvider>();
        int provId = -1;
        if(!wsScene.isNull())
            provId = wsScene.data()->providerId;
        sceneJson.insert("provId",provId);
        sceneJson.insert("type",myScene.data()->getSceneTypeString());
        ret.insert("scene",sceneJson);
    }

    return ret;
}

void SubSceneHandler::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject ret = getSceneMsg();
    sendMsg(ret,clientId,true);
}

void SubSceneHandler::clientMessage(QJsonObject msg, int)
{
    if(msg.contains("addScene")){
        QJsonObject addCmd = msg.value("addScene").toObject();
        QSharedPointer<Scene> newScene = builder->buildNew(addCmd.value("type").toString(),addCmd.value("name").toString());
        if(newScene.isNull()){
           qDebug("subsceneHandler: ERROR: unknown scene type");
           return;
        }
        setScene(newScene);

    }
    if(msg.contains("delScene")){
        setScene(QSharedPointer<Scene>());
    }
}

QJsonObject SubSceneHandler::serialize()
{
    QJsonObject ret;
    if(!myScene.isNull())
    ret.insert(KEY_SCENE,builder->serializeScene(myScene));
    return ret;
}
