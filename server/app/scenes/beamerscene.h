#ifndef BEAMERSCENE_H
#define BEAMERSCENE_H

#include "../scene.h"
#include "../beamer/beamereffectconfig.h"
#include "../filtervirtualdevicemanager.h"
#include "../virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "../ungroupvirtualdevicemanager.h"
#include "../trigger.h"
#include "../virtualDeviceManager/beamercolorvirtualdevicemanager.h"
#include "../virtualDeviceManager/injectvirtualdevicemanager.h"

class BeamerScene: public Scene, public WebSocketServerProvider
{
Q_OBJECT

public:
    BeamerScene(QString name,
                VirtualDeviceManager *devMgr,
                BeamerEffectHandler *beamerEffects,
                WebSocketServer *ws,
                AudioProcessor *jackP,
                PlanedTriggerHandler *planedTriggerHandler,
                InjectVirtualDeviceManager* injektorP,
                QJsonObject serialized);

    ~BeamerScene();

    QSharedPointer<BeamerEffect> getEffect();


    // Serializable interface
public:
    QJsonObject serialize();

    // Scene interface
public:
    QMap<QString, QSharedPointer<DeviceState> > getDeviceState();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticly();

public slots:
    void triggered();

private:
    BeamerEffectConfig effect;
    FilterVirtualDeviceManager filterDevmgr;
    SelectVirtualDeviceManager selectDevMgr;
    UngroupVirtualDeviceManager ungroupDevMgr;
    Trigger trigger;
    bool isTriggered;
    BeamerEffectHandler *myBeamerEffects;
    BeamerColorVirtualDeviceManager colorDevMgr;
    InjectVirtualDeviceManager* injektor;



    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int);
    void sendEffect(int clientId);
    void sendEffect();
    QJsonObject getEffectMsg();

    // Scene interface
public:
    void stop();
    void start();
};

#endif // BEAMERSCENE_H
