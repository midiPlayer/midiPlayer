#ifndef PREVIEWINJEKTOR_H
#define PREVIEWINJEKTOR_H

#include "../virtualDeviceManager/virtualdevicemanager.h"
#include "../rgbwcolor.h"

class PreviewInjektor
{
public:
    PreviewInjektor(VirtualDeviceManager* devs);
    QMap<QString,QSharedPointer<DeviceState> > injektDeviceState(QMap<QString, QSharedPointer<DeviceState> > originalDeviceState);

    void enablePreview(RGBWColor color);
    void dissablePreview();

private:
    bool previewOn;
    RGBWColor previewColor;
    VirtualDeviceManager* devMgr;
};

#endif // PREVIEWINJEKTOR_H
