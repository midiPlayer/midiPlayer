#include "triggerframe.h"
#include <QDebug>

#define KEY_TIME "time"
#define KEY_TRIGGER_NUM "num"

Triggerframe::Triggerframe(QJsonObject serialized, PlanedTriggerHandler *handlerP):
    QObject(),
    time(serialized.value(KEY_TIME).toDouble()),
    triggerNum(serialized.value(KEY_TRIGGER_NUM).toInt()),
    handler(handlerP),
    lastTime(-1)
{

}

Triggerframe::Triggerframe(float timeP, int triggerNumP, PlanedTriggerHandler *handlerP):
    QObject(),
    time(timeP),
    triggerNum(triggerNumP),
    handler(handlerP),
    lastTime(-1)
{

}

void Triggerframe::timeChanged(float now)
{
       if(lastTime != -1 && lastTime < time && time < now){
           qDebug() << "triggered" << triggerNum;
        handler->trigger(triggerNum);
       }
       lastTime = now;
}

int Triggerframe::getTriggerNum() const
{
    return triggerNum;
}

float Triggerframe::getTime() const
{
    return time;
}

QJsonObject Triggerframe::serialize()
{
    QJsonObject ret;
    ret.insert(KEY_TIME,time);
    ret.insert(KEY_TRIGGER_NUM,triggerNum);
    return ret;
}
