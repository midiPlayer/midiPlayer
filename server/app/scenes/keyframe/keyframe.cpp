#include "keyframe.h"
#include "../../websocketserver.h"
#include <QJsonArray>
#include "../../devices/channeldevice.h"


/**
 * @brief Keyframe::Keyframe
 * A keyframe ist a point that stores the state of a lamp at a time
 */

#define KEY_KEYFRAME_TIME "keyframe_time"
#define KEY_KEYFRAME_STATE "keyframe_state"

Keyframe::Keyframe(double timeP, RGBWColor stateP, WebSocketServer *ws) :
    WebSocketServerProvider(ws),
    color(stateP),
    time(timeP),
    liveEditing(false),
    wss(ws),
    liveEditor(-1)
{

}

Keyframe::Keyframe(QJsonObject serialized, WebSocketServer *ws) :
    WebSocketServerProvider(ws),
    color(serialized.value(KEY_KEYFRAME_STATE).toObject()),
    time(serialized.value(KEY_KEYFRAME_TIME).toDouble()),
    liveEditing(false),
    wss(ws),
    liveEditor(-1)
{

}


RGBWColor Keyframe::fusionWith(QSharedPointer<Keyframe> later,double now)
{
    double per = (now-time) / (later.data()->time - time);
    return color.fusionWith(later.data()->color,per);
}

QJsonObject Keyframe::serialize()
{
    QJsonObject ret;
    ret.insert(KEY_KEYFRAME_TIME,time);
    ret.insert(KEY_KEYFRAME_STATE,color.serialize());
    return ret;
}

void Keyframe::clientRegistered(QJsonObject, int id)
{
    QJsonObject ret;
    ret.insert("time",time);
    ret.insert("color",color.serialize());
    sendMsg(ret,id,true);
}

void Keyframe::clientUnregistered(QJsonObject, int id)
{

    if(liveEditor == id){
        liveEditing = false;
    }
}

void Keyframe::clientMessage(QJsonObject msg, int id)
{
    if(msg.contains("valueChnaged")){

    }
    if(msg.contains("deleteKeyframe")){
        sendMsgButNotTo(msg,id,true);
        wss->unregisterProvider(this);
        emit deleteRequested(this);
    }

    if(msg.contains("time")){
        time = msg.value("time").toDouble();
        sendMsgButNotTo(msg,id,true);
    }

    if(msg.contains("color")){
        color = RGBWColor(msg.value("color").toObject());
        sendMsgButNotTo(msg,id,true);
    }

    if(msg.contains("liveEditing")){
        liveEditing = msg.value("liveEditing").toBool(false);
        liveEditor = id;
    }
}

QString Keyframe::getRequestType()
{
    return "keyframe";
}

bool Keyframe::isLiveEditing()
{
    return liveEditing;
}

void Keyframe::beforeDelete()
{
    QJsonObject del;
    del.insert("deleteKeyframe",true);
    sendMsg(del,true);
}
