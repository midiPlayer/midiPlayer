#ifndef TRACK_H
#define TRACK_H

#include "../../virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "../../ungroupvirtualdevicemanager.h"
#include "keyframe.h"
#include "../../scene.h"
#include "../../scenes/subscenehandler.h"
#include "triggerframe.h"
#include "../../scenes/planedtriggerhandler.h"

class Track : public QObject, public Serializable, public WebSocketServerProvider
{
Q_OBJECT

public:
    Track(QString nameP, VirtualDeviceManager *devMgr, WebSocketServer *ws,SceneBuilder *builder);
    Track(QJsonObject serialized, VirtualDeviceManager *devMgr, WebSocketServer *ws, SceneBuilder *builder, bool);
    ~Track();
    QMap<QString, QSharedPointer<DeviceState> > getDeviceStates(float elapsed);

    QString getName() const;
    int getDevSelectId();
    int getSceneSelectId();

    QSharedPointer<Scene> getScene();

    void start();
    void stop();
public slots:
    void removeKeyframe(Keyframe * which);

private:
    QString name;
    SelectVirtualDeviceManager select;
    UngroupVirtualDeviceManager ungroup;
    QList<QSharedPointer<Keyframe> > keyframes;
    QList<QSharedPointer<Triggerframe> > triggerFrames;
    WebSocketServer* wss;

    void addKeyframe(double time, RGBWColor color, bool notify);


    // Serializable interface
public:
    QJsonObject serialize();

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject msg, int);
    RGBWColor getColorForTime(float elapsed);
    SceneBuilder myBuilder;
    PlanedTriggerHandler triggerHandler;
    SubSceneHandler *sceneH;
};

#endif // TRACK_H
