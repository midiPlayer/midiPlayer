#ifndef KEYFRAME_H
#define KEYFRAME_H

#include "../../devices/channeldevicestate.h"
#include "../../serializable.h"
#include "../../websocketserverprovider.h"
#include <QObject>
#include <QSharedPointer>
#include "../../rgbwcolor.h"

class Keyframe  : public QObject, public Serializable, public WebSocketServerProvider
{
Q_OBJECT

public:
    Keyframe(double timeP, RGBWColor stateP, WebSocketServer *ws);
    Keyframe(QJsonObject serialized, WebSocketServer *ws);
    RGBWColor color;
    double time;
    bool liveEditing;

    RGBWColor fusionWith(QSharedPointer <Keyframe> later, double now);
    QJsonObject serialize();
    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int id);
    void clientMessage(QJsonObject msg, int id);
    QString getRequestType();
    bool isLiveEditing();
    void beforeDelete();

signals:
    void deleteRequested(Keyframe *self);
private:
    WebSocketServer *wss;
    int liveEditor;
};

#endif // KEYFRAME_H
