#include "keyframescene.h"
#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include "../../websocketserver.h"
#include "../../devices/whitedevice.h"
#include "../../devices/devicenotfoundexception.h"
#include "../../scenes/planedtriggerhandler.h"

#define KEY_MUSIC_PLAYER "music_player"
#define KEY_TRACKS "tracks"
#define KEY_TRIGGER "trigger"

void KeyFrameScene::init()
{
    connect(&watch,SIGNAL(resumed()),this,SLOT(handleResume()));
    connect(&watch,SIGNAL(started()),this,SLOT(handleResume()));
    connect(&watch,SIGNAL(stoped()),&musicPlayer,SLOT(stop()));
    connect(&watch,SIGNAL(timeSet()),this,SLOT(handleTimeChanged()));

    connect(&trigger,SIGNAL(trigger()),this,SLOT(reset()));

    if(deck != NULL){
        connect(deck,SIGNAL(positionChanged(float)),this,SLOT(setPosition(float)));
        connect(deck,SIGNAL(playChanged(bool)),this,SLOT(startStop(bool)));
        connect(deck,SIGNAL(speedChanged(float)),&watch,SLOT(setSpeed(float)));
        watch.setSpeed(deck->getSpeed());
    }
}

QList<QSharedPointer<Scene> > KeyFrameScene::getSubScenes()
{
    QList<QSharedPointer<Scene> > ret;
    foreach (QSharedPointer<Track> track, tracks.values()) {
        QSharedPointer<Scene> s = track.data()->getScene();
        if(!s.isNull())
            ret.append(s);
    }
    return ret;
}

void KeyFrameScene::start()
{
    start(true);
}

KeyFrameScene::KeyFrameScene(VirtualDeviceManager *manager, QString name, WebSocketServer *ws,
                             AudioProcessor *jack, OscConnectorDeck* deckP,SceneBuilder* builderP, PlanedTriggerHandler *triggerHandler,  QJsonObject serialized) :
    Scene(name,serialized),
    WebSocketServerProvider(ws),
    filterVDevManager(manager,{Device::Beamer,Device::RGB, Device::RGBW, Device::White}),
    wss(ws),
    watch(ws),
    musicPlayer(ws,serialized.value(KEY_MUSIC_PLAYER).toObject()),
    trigger(ws, jack, triggerHandler, serialized.value(KEY_TRIGGER).toObject()),
    deck(deckP),
    builder(builderP)
{


    init();

    //load serialized
    foreach(QJsonValue trackVal,serialized.value(KEY_TRACKS).toArray()){
        QSharedPointer<Track> track(new Track(trackVal.toObject(),&filterVDevManager,wss,builder,false));
        tracks.insert(track.data()->getName(),track);
    }
}

QMap<QString, QSharedPointer<DeviceState> > KeyFrameScene::getDeviceState()
{
    QMap<QString, QSharedPointer<DeviceState> > ret;
    float elapsed = (double)watch.getMSecs() / 1000.0;

    foreach (QSharedPointer<Track> track,tracks.values()) {
        QMap<QString, QSharedPointer<DeviceState> > trackRet = track.data()->getDeviceStates(elapsed);
        foreach(QString devId, trackRet.keys()){
            if(!ret.contains(devId)){
                ret.insert(devId,trackRet.value(devId));
            }
            else{
                QSharedPointer<DeviceState> retVal  =ret.value(devId);
                ret.remove(devId);
                QSharedPointer<DeviceState> res = retVal.data()->fusionWith(trackRet.value(devId),DeviceState::MAX,0.5);
                ret.insert(devId,res);
            }
        }
    }

    ret.insert(musicPlayer.getDeviceId(),musicPlayer.createEmptyState());

    return ret;
}


void KeyFrameScene::start(bool reset)
{
    qDebug() << "key started";
    if(reset)
        watch.start();
    else if(!watch.getRunning())
        watch.resume(true);
    foreach(QSharedPointer<Track> t, tracks){
        t.data()->start();
    }

    trigger.start();
    musicPlayer.play();
}

void KeyFrameScene::stop()
{
    trigger.stop();

    watch.stop();
    foreach(QSharedPointer<Track> t, tracks){
        t.data()->stop();
    }
    musicPlayer.stop();
}

QJsonObject KeyFrameScene::serialize()
{
    QJsonObject ret;
    QJsonArray tracksJson;
    foreach (QSharedPointer<Track> track, tracks) {
        tracksJson.append(track.data()->serialize());
    }
    ret.insert(KEY_TRACKS,tracksJson);

    ret.insert(KEY_MUSIC_PLAYER,musicPlayer.serialize());

    ret.insert(KEY_TRIGGER,trigger.serialize());
    return serializeScene(ret);
}



void KeyFrameScene::clientRegistered(QJsonObject, int id)
{
    QJsonObject ret = getTracksJosn();
    ret.insert("cursorWatch",watch.providerId);
    ret.insert("musicPlayer",musicPlayer.providerId);
    ret.insert("trigger",trigger.providerId);
    sendMsg(ret,id,true);
}

void KeyFrameScene::clientMessage(QJsonObject msg, int)
{

    if(msg.contains("add_track")){
        QString name = msg.value("add_track").toString();
        if(name != "" && !tracks.contains(name)){
            addTrack(name,true);
        }
        else{
            QJsonObject ret;
            ret.insert("adderr","trackname exists");
        }
    }
    else if(msg.contains("rm_track")){
        QString name = msg.value("rm_track").toString();
        rmTrack(name,true);
    }
}

QString KeyFrameScene::getRequestType()
{
    return "keyFrameScene";
}

QString KeyFrameScene::getSceneTypeString()
{
    return getSceneTypeStringStaticaly();
}

QString KeyFrameScene::getSceneTypeStringStaticaly()
{
    return "keyFrameScene";
}

void KeyFrameScene::handleResume()
{
    musicPlayer.play(watch.getMSecs());
}

void KeyFrameScene::handleTimeChanged()
{
    musicPlayer.setPosition(watch.getMSecs());
}

void KeyFrameScene::devicesChanged()
{

}

void KeyFrameScene::reset()
{
    stop();
    start();
}

void KeyFrameScene::setPosition(float pos)
{
    if(trigger.triggerConfig.isEmpty())
        watch.setTo(pos * 1000);
}

void KeyFrameScene::startStop(bool start)
{
    if(start)
        this->start(false);
    else
        stop();
}


void KeyFrameScene::addTrack(QString name, bool notifyClients)
{
    tracks.insert(name,QSharedPointer<Track>(new Track(name,&filterVDevManager,wss,builder)));
    if(notifyClients){
        QJsonObject ret = getTracksJosn();
        sendMsg(ret,true);
    }
}

void KeyFrameScene::rmTrack(QString name, bool notifyClients)
{
    tracks.remove(name);
    if(notifyClients){
        QJsonObject ret = getTracksJosn();
        sendMsg(ret,true);
    }
}


QJsonObject KeyFrameScene::getTracksJosn()
{
    QJsonObject ret;
    QJsonArray tracksJson;
    foreach (QString trackName, tracks.keys()) {
        QJsonObject trackJson;
        trackJson.insert("name",trackName);
        QSharedPointer<Track> track = tracks.value(trackName);
        trackJson.insert("devs",track.data()->getDevSelectId());
        trackJson.insert("scene",track.data()->getSceneSelectId());
        trackJson.insert("requestId",track.data()->providerId);
        tracksJson.append(trackJson);
    }
    ret.insert("tracks",tracksJson);

    return ret;
}
