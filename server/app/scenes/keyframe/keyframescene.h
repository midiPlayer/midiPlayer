#ifndef KEYFRAMESCENE_H
#define KEYFRAMESCENE_H
#include "../../scene.h"
#include "../../stopwatch.h"
#include <QList>
#include "../../devices/device.h"
#include "keyframe.h"
#include "../../websocketserverprovider.h"
#include <QSharedPointer>
#include "../../devices/musicdevice.h"
#include "../../filtervirtualdevicemanager.h"
#include "track.h"
#include "../../trigger.h"
#include "../../oscconnectordeck.h"

class KeyFrameScene : public Scene, public WebSocketServerProvider
{
Q_OBJECT

public:
    KeyFrameScene(VirtualDeviceManager *manager, QString name, WebSocketServer *ws,
                  AudioProcessor* jack, OscConnectorDeck *deckP, SceneBuilder *builderP, PlanedTriggerHandler *triggerHandler,  QJsonObject serialized = QJsonObject());
    QMap<QString,QSharedPointer<DeviceState> > getDeviceState();
    void start(bool reset);
    void stop();
    QJsonObject serialize();

    void clientRegistered(QJsonObject, int id);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int);
    QString getRequestType();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticaly();

public slots:
    void handleResume();
    void handleTimeChanged();
    void devicesChanged();
    void reset();
    void setPosition(float pos);
    void startStop(bool start);

private:
    FilterVirtualDeviceManager filterVDevManager;
    WebSocketServer* wss;
    Stopwatch watch;
    MusicDevice musicPlayer;
    QMap<QString,QSharedPointer<Track> > tracks;
    Trigger trigger;
    OscConnectorDeck* deck;
    SceneBuilder* builder;
    float speed;

    QJsonArray getLampsJson();
    QJsonObject getLampJson(QSharedPointer<Device> dev);

    void addTrack(QString name, bool notifyClients);
    void rmTrack(QString name, bool notifyClients);

    QJsonObject getTracksJosn();

    // Scene interface
    void init();

public:
    Scene *clone();

    // Scene interface
public:
    QList<QSharedPointer<Scene> > getSubScenes();

    // Scene interface
public:
    void start();
};

#endif // KEYFRAMESCENE_H
