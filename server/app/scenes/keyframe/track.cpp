#include "track.h"
#include <QJsonArray>
#include "../../websocketserver.h"
#include "../../devices/channeldevice.h"
#include "../../devices/channeldevicestate.h"

#define KEY_KEYFRAMES "keyframes"
#define KEY_TRIGGERFRAMES "triggerframes"
#define KEY_DEVICES "devices"
#define KEY_NAME "name"
#define KEY_SUBSCENE "subScene"

Track::Track(QString nameP,VirtualDeviceManager* devMgr, WebSocketServer* ws, SceneBuilder *builder) :
    QObject(),
    Serializable(),
    WebSocketServerProvider(ws),
    name(nameP),
    select(devMgr,ws,QJsonObject()),
    ungroup(&select),
    wss(ws),
    myBuilder(builder),
    triggerHandler(builder->getTriggerHandler())
{
    myBuilder.setTriggerHandler(&triggerHandler);
    sceneH = new SubSceneHandler(ws,&myBuilder,QJsonObject());
}

Track::Track(QJsonObject serialized, VirtualDeviceManager *devMgr, WebSocketServer *ws,SceneBuilder *builder, bool):
    QObject(),
    Serializable(),
    WebSocketServerProvider(ws),
    name(serialized.value(KEY_NAME).toString()),
    select(devMgr,ws,serialized.value(KEY_DEVICES).toObject()),
    ungroup(&select),
    wss(ws),
    myBuilder(builder),
    triggerHandler(builder->getTriggerHandler())
{
    myBuilder.setTriggerHandler(&triggerHandler);
    sceneH = new SubSceneHandler(ws,&myBuilder,serialized.value(KEY_SUBSCENE).toObject());
    foreach (QJsonValue kfJson, serialized.value(KEY_KEYFRAMES).toArray()) {
        QSharedPointer<Keyframe> kf(new Keyframe(kfJson.toObject(),wss));
        connect(kf.data(),SIGNAL(deleteRequested(Keyframe*)),this,SLOT(removeKeyframe(Keyframe*)));
        keyframes.append(kf);
    }

    foreach (QJsonValue tfJson, serialized.value(KEY_TRIGGERFRAMES).toArray()) {
        triggerFrames.append(QSharedPointer<Triggerframe>(new Triggerframe(tfJson.toObject(),&triggerHandler)));
    }
}

Track::~Track()
{
    delete sceneH;
}

RGBWColor Track::getColorForTime(float elapsed)
{
    QSharedPointer<Keyframe> min;
    QSharedPointer<Keyframe> max;
    foreach (QSharedPointer<Keyframe> frame, keyframes) {
        if(frame.data()->isLiveEditing()){//override
            min = frame;
            max = QSharedPointer<Keyframe>();
            break;
        }
        if(frame.data()->time < elapsed && (min.isNull() || frame.data()->time > min.data()->time)){
            min = frame;
        }
        if(frame.data()->time > elapsed && (max.isNull() || frame.data()->time < max.data()->time)){
            max = frame;
        }
    }

    RGBWColor currentColor;
    if(!min.isNull() && !max.isNull()){
         currentColor = min.data()->fusionWith(max,elapsed);
    }
    else if(!min.isNull()){
        currentColor = min.data()->color;
    }
    else if(!max.isNull()){
        currentColor = max.data()->color;
    }

    return currentColor;
}

QMap<QString, QSharedPointer<DeviceState> > Track::getDeviceStates(float elapsed)
{

    //run triggers:
    foreach (QSharedPointer<Triggerframe> triggerF, triggerFrames) {
        triggerF.data()->timeChanged(elapsed);
    }

    //generate output
    RGBWColor currentColor = getColorForTime(elapsed);
    QMap<QString, QSharedPointer<DeviceState> > ret;
    if(sceneH->getScene().isNull()){ //classic functionality
        foreach (QSharedPointer<Device> dev, ungroup.getDevices().values()) {
            QSharedPointer<ChannelDevice> cdev = dev.dynamicCast<ChannelDevice>();
            if(cdev.isNull())
                continue;
            QSharedPointer<ChannelDeviceState> state = cdev.data()->createEmptyChannelState();
            state.data()->setRGBW(currentColor);
            ret.insert(dev.data()->getDeviceId(),state);
        }
    }
    else{ //use scene and scale by white value
        QMap<QString, QSharedPointer<DeviceState> > sceneData = sceneH->getScene().data()->getDeviceState();
        foreach(QString devId, sceneData.keys()){
            QSharedPointer<DeviceState> newState = sceneData.value(devId).data()->fusionAlone(DeviceState::AV,1-currentColor.getW());
            ret.insert(devId,newState);
        }
    }
    return ret;
}

void Track::removeKeyframe(Keyframe *which)
{
    for(int i = 0; i < keyframes.length();i++){
        if(keyframes.at(i).data() == which){
            keyframes.removeAt(i);
            break;
        }
    }
}

void Track::addKeyframe(double time,RGBWColor color, bool notify)
{
    QSharedPointer<Keyframe> kf(new Keyframe(time, color,wss));
    connect(kf.data(),SIGNAL(deleteRequested(Keyframe*)),this,SLOT(removeKeyframe(Keyframe*)));
    keyframes.append(kf);
    if(notify){
        QJsonObject msg;
        msg.insert("new_keyframe",kf.data()->providerId);
        sendMsg(msg,true);
    }
}

QJsonObject Track::serialize()
{
    QJsonObject ret;
    QJsonArray keyframesJson;
    foreach (QSharedPointer<Keyframe> k, keyframes) {
        keyframesJson.append(k.data()->serialize());
    }
    ret.insert(KEY_KEYFRAMES,keyframesJson);

    QJsonArray triggerFramesJson;
    foreach (QSharedPointer<Triggerframe> triggerF, triggerFrames) {
        triggerFramesJson.append(triggerF.data()->serialize());
    }
    ret.insert(KEY_TRIGGERFRAMES,triggerFramesJson);


    ret.insert(KEY_NAME,name);
    ret.insert(KEY_DEVICES,select.serialize());
    ret.insert(KEY_SUBSCENE,sceneH->serialize());
    return ret;
}

QString Track::getName() const
{
    return name;
}

int Track::getDevSelectId()
{
    return select.providerId;
}

int Track::getSceneSelectId()
{
    return sceneH->providerId;
}

QSharedPointer<Scene> Track::getScene()
{
    return sceneH->getScene();
}

void Track::start()
{
    foreach(QSharedPointer<Keyframe> kf, keyframes){
        kf.data()->liveEditing = false;
    }

    if(!sceneH->getScene().isNull()){
        sceneH->getScene().data()->start();
    }
}

void Track::stop()
{
    if(!sceneH->getScene().isNull()){
        sceneH->getScene().data()->stop();
    }
}

QString Track::getRequestType()
{
    return "keyframescene_track";
}

void Track::clientRegistered(QJsonObject, int id)
{
    QJsonObject ret;
    QJsonArray keyframesJson;
    foreach(QSharedPointer<Keyframe> kf, keyframes){
        keyframesJson.append(kf.data()->providerId);
    }
    ret.insert("keyframes",keyframesJson);

    QJsonArray triggersJson;
    foreach(QSharedPointer<Triggerframe> tf, triggerFrames){
        triggersJson.append(tf.data()->serialize());
    }
    ret.insert(KEY_TRIGGERFRAMES,triggersJson);

    sendMsg(ret,id,true);
}

void Track::clientMessage(QJsonObject msg, int)
{
    if(msg.contains("add_keyframe")){
        QJsonObject addObj = msg.value("add_keyframe").toObject();
        float time = addObj.value("time").toDouble();
        addKeyframe(time,getColorForTime(time),true);
    }
    if(msg.contains(KEY_TRIGGERFRAMES)){
        triggerFrames.clear();
        foreach (QJsonValue tfJson, msg.value(KEY_TRIGGERFRAMES).toArray()) {
            triggerFrames.append(QSharedPointer<Triggerframe>(new Triggerframe(tfJson.toObject(),&triggerHandler)));
        }
    }
}
