#ifndef TRIGGERFRAME_H
#define TRIGGERFRAME_H

#include <QObject>
#include "../../serializable.h"
#include "../../scenes/planedtriggerhandler.h"

class Triggerframe : public QObject, public Serializable
{
Q_OBJECT

public:
    Triggerframe(QJsonObject serialized, PlanedTriggerHandler* handlerP);
    Triggerframe(float timeP,int triggerNumP, PlanedTriggerHandler* handlerP);

    void timeChanged(float now);
private:
    float time;
    int triggerNum;
    PlanedTriggerHandler* handler;
    float lastTime;

    // Serializable interface
public:
    QJsonObject serialize();
    float getTime() const;
    int getTriggerNum() const;
};

#endif // TRIGGERFRAME_H
