#include "beatscene1.h"
#include <QDebug>
#include "../websocketserver.h"
#include "math.h"
#include "../devices/groupdevice.h"

#define KEY_SMOOTHNESS "smoothness"
#define KEY_BACKGROUNDTRIGGER "backgroundTrigger"
#define KEY_COLOR "color"
#define KEY_SELECT_DEV_MANAGER "selectDevManager"
#define KEY_NUM_CHANGING_DEVS "changingDevices"
#define KEY_SAME_COLOR "sameColor"
#define KEY_INACTIVE_BLACK "inactiveBlack"

#define l1 12
#define l2 13
#define l3 14

#define MAX_SMOOTHNESS_DUR 6000


BeatScene1::BeatScene1(VirtualDeviceManager *manager, AudioProcessor *p, WebSocketServer *ws, QString name, PlanedTriggerHandler *planedTrigger, QJsonObject serialized) :
    Scene(name,serialized),WebSocketServerProvider(ws),
    filterDeviceManager(manager,{Device::RGB,Device::RGBW, Device::BeamerColor,Device::White}),
    selectDevManager(&filterDeviceManager,ws,serialized.value(KEY_SELECT_DEV_MANAGER).toObject()),
    ungrouped(&selectDevManager),
    randomDevChooser(&selectDevManager),
    backgroundTrigger(ws,p, planedTrigger),
    smoothDuration(serialized.value(KEY_SMOOTHNESS).toInt(200)),
    smoothTimer(),
    prev("prev"),
    next("next"),
    colorButton(ws,serialized.value(KEY_COLOR).toObject()),
    numChangingDevices(serialized.value(KEY_NUM_CHANGING_DEVS).toInt(0)),
    sameColor(serialized.value(KEY_SAME_COLOR).toBool(true)),
    inactiveBlack(ws,this,serialized.value(KEY_INACTIVE_BLACK).toBool(false)),
    sphereMulti(&selectDevManager,&filterDeviceManager,ws,p,serialized.value("sphereMulti").toObject()),
    sphereMultiWrapper(&sphereMulti,ws,serialized.value("sphereMultiWrapper").toObject()),
    loudnessMulti(ws, p, &selectDevManager,serialized.value("loudnessMulti").toObject()),
    loudnessMultiWrapper(&loudnessMulti,ws,serialized.value("loudnessMultiWrapper").toObject()),
    prevInjekt(&ungrouped)
{


    backgroundTrigger.loadSerialized(serialized.value(KEY_BACKGROUNDTRIGGER).toObject());

    colorButton.setPreviewInj(&prevInjekt);

    connect(&ungrouped,SIGNAL(virtualDevicesChanged()),this,SLOT(devicesChanged()));

    connect(&backgroundTrigger,SIGNAL(trigger()),this,SLOT(generateNextScene()));
    connect(&colorButton,SIGNAL(colorChanged()),this,SLOT(generateNextScene()));

    smoothTimer.start();
}


QMap<QString, QSharedPointer<DeviceState> > BeatScene1::getDeviceState()
{
    QMap<QString, QSharedPointer<DeviceState> > ret;
    if(smoothDuration == 0 || smoothTimer.elapsed() > smoothDuration){
        ret = next.getDeviceState();
    }
    else{
        float per = float(smoothTimer.elapsed()) / float(smoothDuration);
        if(per>1.0)
            per = 1;
        ret= prev.passiveFusion(next.getDeviceState(),DeviceState::AV,per);
    }

    sphereMultiWrapper.applyOn(&ret);
    loudnessMultiWrapper.applyOn(&ret);
    return prevInjekt.injektDeviceState(ret);
}


void BeatScene1::stop()
{
    backgroundTrigger.stop();
    sphereMulti.stop();
    loudnessMulti.stop();
}

void BeatScene1::start()
{
    qDebug() << "start";
    backgroundTrigger.start();
    sphereMulti.start();
    loudnessMulti.start();
}

void BeatScene1::clientRegistered(QJsonObject, int id)
{
    QJsonObject reply;
    QJsonObject config;
    config.insert("backgroundTrigger",backgroundTrigger.providerId);
    config.insert("smoothnessChanged",double(smoothDuration)/double(MAX_SMOOTHNESS_DUR));
    config.insert("colorButton", colorButton.providerId);
    config.insert("selectDevManager", selectDevManager.providerId);
    config.insert("numDevs",selectDevManager.getGroupDevices().count() + selectDevManager.getDevices().count());
    config.insert(KEY_NUM_CHANGING_DEVS,numChangingDevices);
    config.insert(KEY_SAME_COLOR,sameColor);
    config.insert("sphereMulti",sphereMultiWrapper.providerId);
    config.insert("loudnessMulti",loudnessMultiWrapper.providerId);
    reply.insert(KEY_INACTIVE_BLACK,inactiveBlack.providerId);
    reply.insert("config",config);
    sendMsg(reply,id,true);
}


void BeatScene1::clientMessage(QJsonObject msg, int id)
{
    if(msg.contains("smoothnessChanged")){
        smoothDuration = msg.value("smoothnessChanged").toDouble(0)*MAX_SMOOTHNESS_DUR;
    }
    if(msg.contains(KEY_NUM_CHANGING_DEVS)){
        numChangingDevices =  round(msg.value(KEY_NUM_CHANGING_DEVS).toDouble(numChangingDevices));
    }
    if(msg.contains(KEY_SAME_COLOR)){
        sameColor = msg.value(KEY_SAME_COLOR).toBool(sameColor);
    }
    sendMsgButNotTo(msg,id,true);
}

QString BeatScene1::getRequestType()
{
    return "beatScene1";
}

QJsonObject BeatScene1::serialize()
{
    QJsonObject ret;
    ret.insert(KEY_BACKGROUNDTRIGGER,backgroundTrigger.serialize());
    ret.insert(KEY_COLOR,colorButton.serialize());
    ret.insert(KEY_SMOOTHNESS,smoothDuration);
    ret.insert(KEY_SELECT_DEV_MANAGER,selectDevManager.serialize());
    ret.insert(KEY_NUM_CHANGING_DEVS,numChangingDevices);
    ret.insert(KEY_SAME_COLOR,sameColor);
    ret.insert(KEY_INACTIVE_BLACK,inactiveBlack.getBoolValue());
    ret.insert("sphereMulti",sphereMulti.serialize());
    ret.insert("sphereMultiWrapper",sphereMultiWrapper.serialize());
    ret.insert("loudnessMulti",loudnessMulti.serialize());
    ret.insert("loudnessMultiWrapper",loudnessMultiWrapper.serialize());
    return serializeScene(ret);
}

QString BeatScene1::getSceneTypeString()
{
    return getSceneTypeStringStaticaly();
}

QString BeatScene1::getSceneTypeStringStaticaly()
{
    return "beatScene1";
}

void BeatScene1::devicesChanged()
{
    QJsonObject config;
    config.insert("numDevs",selectDevManager.getGroupDevices().count() + selectDevManager.getDevices().count());
    QJsonObject msg;
    msg.insert("config",config);
    sendMsg(msg,true);

    prev.reset();
    next.reset();

    generateNextScene();
}

void BeatScene1::generateNextScene()
{
    QMap<QString, QSharedPointer<DeviceState> > ret;
    QHash<QString, QSharedPointer<Device> > avDevs = ungrouped.getDevices();

    //HACK!!!
    if(colorButton.getColors().count() == 0)
        return;


    //generate Device Colors
    if(!sameColor){ //if we use the same color for all devices there is no point in this
        QMap<QString, QColor> newDeviceColors;
        foreach(QString devId, selectDevManager.getDevices().keys()){
            if(deviceColors.contains(devId))
                newDeviceColors.insert(devId,colorButton.getRandomDiffrentColor(deviceColors.value(devId)));
            else
                newDeviceColors.insert(devId,colorButton.getRandomDiffrentColor());
        }

        foreach(QSharedPointer<GroupDevice> group, selectDevManager.getGroupDevices()){
            QColor nextc;
            if(deviceColors.contains(group.data()->getGroupId()))
                nextc = colorButton.getRandomDiffrentColor(deviceColors.value(group.data()->getGroupId()));
            else
                nextc = colorButton.getRandomDiffrentColor();
            foreach(QString cid, group.data()->getAllChildren().keys())
                newDeviceColors.insert(cid,nextc);
        }
        deviceColors = newDeviceColors;//override
    }

    //choose devices
    QList<QString> chosenDevs = randomDevChooser.choose(numChangingDevices);

    QColor commonColor = getNextColor();
    foreach (QString devId, avDevs.keys()) {
        QSharedPointer<Device> dev = avDevs.value(devId);
        //not chosen devices keep their old state
        if(!chosenDevs.contains(devId) && next.getDeviceState().contains(devId)){
            if(inactiveBlack.getBoolValue())
                ret.insert(devId,dev.data()->createEmptyState());
            else
                ret.insert(devId,next.getDeviceState().value(devId));
            continue;
        }

        QSharedPointer<DeviceState> state = dev.data()->createEmptyState();
        QSharedPointer<ColorfulDeviceState> d = state.dynamicCast<ColorfulDeviceState>();
        if(sameColor){
            d.data()->setRGB(commonColor);
        }
        else{
            d.data()->setRGB(deviceColors.value(devId));
        }

        ret.insert(devId,d);
    }
    prev.import(next.getDeviceState());
    next.reset();
    next.import(ret);

    smoothTimer.restart();
}

QColor BeatScene1::getNextColor()
{
    return colorButton.getRandomDiffrentColor();
}



