#include "linescene.h"
#include <iterator>
#include "../devices/colorfuldevicestate.h"
#include <QDebug>

LineScene::LineScene(QString name,
                     WebSocketServer *ws,
                     AudioProcessor *audio,
                     PlanedTriggerHandler *handler,
                     VirtualDeviceManager *vDev,
                     QJsonObject serialized):
    Scene(name,serialized),
    WebSocketServerProvider(ws),
    trigger(ws,audio,handler,serialized.value("trigger").toObject()),
    colors(ws,serialized.value("colors").toObject()),
    devices(vDev, ws, serialized.value("devices").toObject()),
    startDevices(&devices,ws,serialized.value("startDevices").toObject()),
    speed(ws,this,serialized.value("speed").toDouble(0.1)),
    acceleration(ws, this, serialized.value("acceleration").toDouble(0.0)),
    blockSize(ws, this, serialized.value("blockSize").toInt(3))
{
    connect(&startDevices,SIGNAL(virtualDevicesChanged()),this,SLOT(regenerateLines()));
    connect(&trigger,SIGNAL(trigger()),this,SLOT(triggered()));
    regenerateLines();
}

LineScene::~LineScene()
{
    clearLines();
}

QJsonObject LineScene::serialize()
{
    QJsonObject serialized;
    serialized.insert("trigger",trigger.serialize());
    serialized.insert("colors",colors.serialize());
    serialized.insert("devices",devices.serialize());
    serialized.insert("startDevices",startDevices.serialize());
    serialized.insert("speed",speed.val());
    serialized.insert("acceleration",acceleration.val());
    serialized.insert("blockSize",blockSize.getNumber());
    return serializeScene(serialized);
}

QString LineScene::getRequestType()
{
    return "lineScene";
}

QMap<QString, QSharedPointer<DeviceState> > LineScene::getDeviceState()
{
    QMap<QString, RGBWColor> opacitys;
    QList<Block*> oldBlocks;
    foreach(Block* block, blocks){
        float pos = block->getPos(speed.val(),acceleration.val());
        bool isOnAnyLine = false;
        foreach(Line* line, lines){
            if(line->isOnLine(pos))
                isOnAnyLine = true;
            line->getCurrent(block,pos,&opacitys);
        }
        if(!isOnAnyLine){
            oldBlocks.append(block);
        }
    }

    foreach(Block* oldBlock, oldBlocks){
        blocks.removeAll(oldBlock);
        delete oldBlock;
    }

    QMap<QString, QSharedPointer<DeviceState> > ret;
    foreach (QSharedPointer<Device> dev, devices.getDevices().values()) {
        QSharedPointer<DeviceState> devState = dev.data()->createEmptyState();

        QSharedPointer<ColorfulDeviceState> colorful = devState.dynamicCast<ColorfulDeviceState>();
        if(!colorful.isNull()){
            colorful.data()->setRGBW(opacitys.value(dev.data()->getDeviceId(),RGBWColor()));
        }

        ret.insert(dev.data()->getDeviceId(),devState);
    }
    return ret;
}

void LineScene::stop()
{
    trigger.stop();
}

void LineScene::start()
{
    trigger.start();
}

QString LineScene::getSceneTypeString()
{
    return getSceneTypeStringStyticly();
}

QString LineScene::getSceneTypeStringStyticly()
{
    return "lineScene";
}

void LineScene::clearLines()
{
    foreach(Line* line, lines){
        delete line;
    }
    lines.clear();
}

void LineScene::regenerateLines()
{
    clearLines();
    QMap<QString, QVector3D> devsPos;
    foreach(QSharedPointer<Device> dev,devices.getDevices().values()){
        devsPos.insert(dev.data()->getDeviceId(),dev.data()->getPosition());
    }

    if(devsPos.empty())
        return;

    QMap<QString, QVector3D> starts;
    foreach(QSharedPointer<Device> dev,startDevices.getDevices().values()){
        starts.insert(dev.data()->getDeviceId(),dev.data()->getPosition());
    }
    if(starts.empty()){
        starts.insert(devsPos.firstKey(),devsPos.first());
    }

    foreach(QString startName, starts.keys()){
        QVector3D currentPoint = starts.value(startName);
        Line *line = new Line();
        QMap<QString, QVector3D> free = devsPos;
        while(true){
            //find next
            float minDist = -1;
            QString min;
            QVector3D minPos;
            for(QMap<QString, QVector3D>::iterator i = free.begin(); i != free.end(); ++i){
                float dist = i.value().distanceToPoint(currentPoint);
                if(minDist == -1 || dist < minDist){
                    minDist = dist;
                    min = i.key();
                    minPos = i.value();
                }
            }
            if(minDist == -1)
                break;
            free.remove(min);
            line->points.append(min);
            currentPoint = minPos;
        }
        lines.append(line);
    }
}

void LineScene::triggered()
{
    RGBWColor color;
    if(colors.getColors().size() > 0)
     color= RGBWColor(colors.getRandomDiffrentColor());
    blocks.append(new Block(blockSize.getNumber(),color));
}

void LineScene::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject ret;
    ret.insert("devices",devices.providerId);
    ret.insert("startDevices",startDevices.providerId);
    ret.insert("colors",colors.providerId);
    ret.insert("trigger",trigger.providerId);
    ret.insert("blockSize", blockSize.providerId);
    ret.insert("speed", speed.providerId);
    ret.insert("acceleration", acceleration.providerId);
    sendMsg(ret,clientId,true);
}

void LineScene::Line::getCurrent(LineScene::Block* block, float pos, QMap<QString,RGBWColor> *dest)
{
    int num = block->getSize();
    for(int i = 0; i <= num && int(pos) + i < points.length(); i++){
        float opacity = 1.0;
        if(i == 0){
            opacity = 1 - (pos - float(int(pos)));
        }
        if(i == num){
            opacity = pos - float(int(pos));
        }


        RGBWColor myColor = block->getColor();
        myColor.multiply(opacity);
        QString devId = points.at(int(pos) + i);
        if(dest->contains(devId)){
            RGBWColor otherCol = dest->value(devId);
            dest->remove(devId);
            dest->insert(devId,otherCol.fusionWith(myColor,1,DeviceState::MAX));
        }
        else{
            dest->insert(devId,myColor);
        }
    }
}

bool LineScene::Line::isOnLine(int pos)
{
    return pos < points.size();
}

LineScene::Block::Block(int sizeP, RGBWColor colorP):
    stopwatch(),
    size(sizeP),
    color(colorP)
{
    stopwatch.start();
}

float LineScene::Block::getPos(float speed, float acceleration)
{
    return float(stopwatch.elapsed()) * 0.1 * (speed + (acceleration * stopwatch.elapsed() * 0.001));
}

int LineScene::Block::getSize() const
{
    return size;
}

RGBWColor LineScene::Block::getColor() const
{
    return color;
}
