#include "mousescene.h"
#include "../devices/colorfuldevicestate.h"

MouseScene::MouseScene(QString name, VirtualDeviceManager *mgr, WebSocketServer *ws, QJsonObject serialized):
    Scene(name, serialized),
    WebSocketServerProvider(ws),
    filter(mgr,{Device::White}),
    xDev(&filter, ws, serialized.value("xDev").toObject()),
    yDev(&filter, ws, serialized.value("yDev").toObject()),
    x(ws,this,serialized.value("x").toDouble(0)),
    y(ws,this,serialized.value("y").toDouble(0)),
    savedX(x.val()),
    savedY(y.val())
{
}

QJsonObject MouseScene::serialize()
{
    QJsonObject ret;
    ret.insert("xDev",xDev.serialize());
    ret.insert("yDev",yDev.serialize());
    ret.insert("x",savedX);
    ret.insert("y",savedY);
    return Scene::serializeScene(ret);
}

QMap<QString, QSharedPointer<DeviceState> > MouseScene::getDeviceState()
{
    QMap<QString, QSharedPointer<DeviceState> > ret;
    foreach(QSharedPointer<Device> dev, xDev.getDevices()){
        QSharedPointer<DeviceState> devSatate = dev.data()->createEmptyState();
        QSharedPointer<ColorfulDeviceState> colState = devSatate.dynamicCast<ColorfulDeviceState>();
        colState.data()->setRGBW(RGBWColor(x.val(),x.val(),x.val(),x.val()));
        ret.insert(dev.data()->getDeviceId(),colState);
    }
    foreach(QSharedPointer<Device> dev, yDev.getDevices()){
        QSharedPointer<DeviceState> devSatate = dev.data()->createEmptyState();
        QSharedPointer<ColorfulDeviceState> colState = devSatate.dynamicCast<ColorfulDeviceState>();
        colState.data()->setRGBW(RGBWColor(y.val(),y.val(),y.val(),y.val()));
        ret.insert(dev.data()->getDeviceId(),colState);
    }
    return ret;
}

QString MouseScene::getSceneTypeString()
{
    return getSceneTypeStringStaticly();
}

QString MouseScene::getSceneTypeStringStaticly()
{
    return "mouseScene";
}

QString MouseScene::getRequestType()
{
    return "mouseScene";
}

void MouseScene::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject msg;
    msg.insert("xDev",xDev.providerId);
    msg.insert("yDev",yDev.providerId);
    msg.insert("x",x.providerId);
    msg.insert("y",y.providerId);
    sendMsg(msg,clientId,true);
}

void MouseScene::clientMessage(QJsonObject msg, int)
{
    if(msg.contains("savePos")){
        savedX = x.val();
        savedY = y.val();
    }
}

void MouseScene::start()
{
    x.setFloatValue(savedX);
    y.setFloatValue(savedY);
}
