#ifndef MULTIPLYER_H
#define MULTIPLYER_H

#include "../../websocketserverprovider.h"
#include "../../serializable.h"
class Multiplyer : public WebSocketServerProvider, public Serializable
{
public:
    Multiplyer(WebSocketServer* ws);
    virtual ~Multiplyer(){}
    virtual QMap<QString,float> getMultiplicators() = 0;
};

#endif // MULTIPLYER_H
