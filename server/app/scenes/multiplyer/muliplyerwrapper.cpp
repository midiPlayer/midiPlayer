#include "muliplyerwrapper.h"
#include "../../devices/colorfuldevicestate.h"

MuliplyerWrapper::MuliplyerWrapper(Multiplyer *multiplyerP, WebSocketServer *ws, QJsonObject serialized):
    WebSocketServerProvider(ws),
    multiplyer(multiplyerP),
    active(ws,this,serialized.value("active").toBool(false))
{

}

void MuliplyerWrapper::applyOn(QMap<QString, QSharedPointer<DeviceState> > *on)
{
    //QMap copy(on);
    if(!active.getBoolValue())
        return;
    QMap<QString,float> multiplicators = multiplyer->getMultiplicators();
    foreach (QString devid, on->keys()) {
        float multiplicator = multiplicators.value(devid,1.0);
        QSharedPointer<DeviceState> state = on->value(devid);
        on->remove(devid);
        on->insert(devid,state.data()->clone().data()->fusionAlone(DeviceState::AV,1- multiplicator));
    }
}

QString MuliplyerWrapper::getRequestType()
{
    return "multiplyerWrapper";
}

void MuliplyerWrapper::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject msg;
    msg.insert("active",active.providerId);
    msg.insert("multiplyer",multiplyer->providerId);
    sendMsg(msg,clientId,true);
}

QJsonObject MuliplyerWrapper::serialize()
{
    QJsonObject serialized;
    serialized.insert("active",active.getBoolValue());
    return serialized;
}
