#ifndef SPHEREMULTIPLYER_H
#define SPHEREMULTIPLYER_H
#include "multiplyer.h"
#include <QObject>
#include "../../virtualDeviceManager/virtualdevicemanager.h"
#include "../../virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "../../ungroupvirtualdevicemanager.h"
#include "../../audioprocessor.h"
#include "../../scalainput.h"
#include "../../RemoteTypes/remotefloat.h"
#include "../../devices/groupdevice.h"

class SphereMultiplyer : public QObject, public Multiplyer
{
Q_OBJECT

public:
    SphereMultiplyer(VirtualDeviceManager* paretnVdev,
                     VirtualDeviceManager* vdev,
                     WebSocketServer* ws,
                     AudioProcessor *audioP,
                     QJsonObject serialized);

    QMap<QString, float> getMultiplicators();
    QJsonObject serialize();
    QString getRequestType();
    void start();
    void stop();

public slots:
    void recalcCenters();

private:
    void clientRegistered(QJsonObject, int clientId);
    float getMinDistanceToNextCenter(QVector3D devPos);

    VirtualDeviceManager* selectDevices;
    SelectVirtualDeviceManager selectCenters;
    UngroupVirtualDeviceManager ungroupDevices;

    QList<QVector3D> centers;
    ScalaInput scala;
    RemoteFloat fadeOverLength;
    float maxDistanceToCenter;

    void findCentersFor(QSharedPointer<GroupDevice> group);
    void findCentersFor(QList<QSharedPointer<Device> > devices);
    void findCentersFor(QList<QSharedPointer<GroupDevice> > groups);
};

#endif // SPHEREMULTIPLYER_H
