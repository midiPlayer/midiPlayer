#ifndef LOUDNESSMULTIPLYER_H
#define LOUDNESSMULTIPLYER_H

#include "multiplyer.h"
#include "../../websocketserverprovider.h"
#include "../../audioprocessor.h"
#include "../../virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "../../ungroupvirtualdevicemanager.h"
#include "../../RemoteTypes/remotenumber.h"
#include "../../scalainput.h"

class LoudnessMultiplyer : public Multiplyer
{

public:
    LoudnessMultiplyer(WebSocketServer* ws,
                       AudioProcessor *jackP,
                       VirtualDeviceManager* devMgr,
                       QJsonObject serialized = QJsonObject());
    void stop();
    void start();
    QJsonObject serialize();
    QString getRequestType();
    QMap<QString, float> getMultiplicators();


private:
    void clientRegistered(QJsonObject, int clientId);

    QMap<QString,QSharedPointer<ChannelDeviceState> > whiteState;
    UngroupVirtualDeviceManager ungroup;
    ScalaInput scala;
};

#endif // LOUDNESSMULTIPLYER_H
