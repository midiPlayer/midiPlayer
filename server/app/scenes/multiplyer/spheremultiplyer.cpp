#include "spheremultiplyer.h"

SphereMultiplyer::SphereMultiplyer(VirtualDeviceManager *paretnVdev,
                                   VirtualDeviceManager *vdev,
                                   WebSocketServer *ws,
                                   AudioProcessor* audioP,
                                   QJsonObject serialized):
    Multiplyer(ws),
    selectDevices(paretnVdev),
    selectCenters(vdev,ws,serialized.value("selectCenters").toObject()),
    ungroupDevices(selectDevices),
    scala(ws, audioP, serialized.value("scala").toObject()),
    fadeOverLength(ws,this,serialized.value("fadeOverLength").toDouble(3.0)),
    maxDistanceToCenter(1.0)
{
    connect(&selectCenters,SIGNAL(virtualDevicesChanged()),this,SLOT(recalcCenters()));
    connect(selectDevices,SIGNAL(virtualDevicesChanged()),this,SLOT(recalcCenters()));
    recalcCenters();
}

QMap<QString, float> SphereMultiplyer::getMultiplicators()
{
    float loudness = scala.getLoudness();
    float fadeOverLength_f = fadeOverLength.val();
    QMap<QString, float> ret;
    foreach(QSharedPointer<Device> dev,ungroupDevices.getDevices()){
        float pos = getMinDistanceToNextCenter(dev.data()->getPosition()) /maxDistanceToCenter;
        float per = 0;
        if(loudness >= pos){
            per = loudness - pos;
            per *= fadeOverLength_f;
            if(per > 1)
                per = 1;
        }
        ret.insert(dev.data()->getDeviceId(),per);
    }

    return ret;
}

QJsonObject SphereMultiplyer::serialize()
{
    QJsonObject serialized;
    serialized.insert("selectCenters",selectCenters.serialize());
    serialized.insert("scala",scala.serialize());
    serialized.insert("fadeOverLength",fadeOverLength.val());
    return serialized;
}

QString SphereMultiplyer::getRequestType()
{
    return "sphereMultiplyer";
}

void SphereMultiplyer::start()
{
    scala.start();
}

void SphereMultiplyer::stop()
{
    scala.stop();
}

void SphereMultiplyer::findCentersFor(QList< QSharedPointer<GroupDevice> > groups){
   foreach(QSharedPointer<GroupDevice> group, groups){
       findCentersFor(group);
   }
}

void SphereMultiplyer::findCentersFor(QSharedPointer<GroupDevice> group){
   findCentersFor(group.data()->getChildren().values());
   foreach(QSharedPointer<GroupDevice> subGroup, group.data()->getChildGroups()){
        findCentersFor(subGroup);
   }
}

void SphereMultiplyer::findCentersFor(QList<QSharedPointer<Device> > devices)
{
    if(devices.isEmpty())
        return;
    QVector3D sum;
    int numDev = 0;
    foreach (QSharedPointer<Device> dev, devices) {
        sum += dev.data()->getPosition();
        numDev++;
    }
    centers.append(sum / numDev);
}

void SphereMultiplyer::recalcCenters()
{

    centers.clear();

    foreach(QSharedPointer<Device> dev, selectCenters.getDevices().values()){
        centers.append(dev.data()->getPosition());
    }

    if(centers.length() == 0){//we can't work with no center so we add in the middle
        findCentersFor(selectDevices->getDevices().values());
        QHash<QString, QSharedPointer<GroupDevice> >  groups = selectDevices->getGroupDevices();
        findCentersFor(groups.values());
    }

    //calculate max distance to a center:
    maxDistanceToCenter = 0;
    foreach (QSharedPointer<Device> dev, selectDevices->getDevices().values()) {
        float minDistance = getMinDistanceToNextCenter(dev.data()->getPosition());
        if(minDistance > maxDistanceToCenter){
            maxDistanceToCenter = minDistance;
        }
    }
    if(maxDistanceToCenter == 0) maxDistanceToCenter = 1;//catch divison by zero
}

void SphereMultiplyer::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject msg;
    msg.insert("centers",selectCenters.providerId);
    msg.insert("scala",scala.providerId);
    msg.insert("fadeOverLength",fadeOverLength.providerId);
    sendMsg(msg,clientId,true);
}

float SphereMultiplyer::getMinDistanceToNextCenter(QVector3D devPos)
{
    float minDistance = -1;
    foreach(QVector3D center, centers){
        float dist = center.distanceToPoint(devPos);
        if(minDistance == -1 || minDistance > dist){
            minDistance = dist;
        }
    }
    return minDistance;
}

