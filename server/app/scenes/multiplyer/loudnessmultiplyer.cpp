#include "loudnessmultiplyer.h"

LoudnessMultiplyer::LoudnessMultiplyer(WebSocketServer *ws,
                                       AudioProcessor *jackP,
                                       VirtualDeviceManager *devMgr,
                                       QJsonObject serialized):
        Multiplyer(ws),
        ungroup(devMgr),
        scala(ws,jackP,serialized.value("sala").toObject())
{
}

void LoudnessMultiplyer::stop()
{
    scala.stop();
}

void LoudnessMultiplyer::start()
{
    scala.start();
}

QJsonObject LoudnessMultiplyer::serialize()
{
    QJsonObject ret;
    ret.insert("scala",scala.serialize());
    return ret;
}

QString LoudnessMultiplyer::getRequestType()
{
    return "loudnessMultiplyer";
}

QMap<QString, float> LoudnessMultiplyer::getMultiplicators()
{
    float l = scala.getLoudness();
    if(l > 1)
        l = 1;
    QMap<QString, float > ret;
    foreach (QString devId, ungroup.getDevices().keys()) {
        ret.insert(devId,l);
    }
    return ret;
}

void LoudnessMultiplyer::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject ret;
    ret.insert("scala",scala.providerId);
    sendMsg(ret,clientId,true);
}
