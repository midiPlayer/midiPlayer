#ifndef MULIPLYERWRAPPER_H
#define MULIPLYERWRAPPER_H

#include "multiplyer.h"
#include "../../RemoteTypes/remotebool.h"
#include "../../devices/devicestate.h"

class MuliplyerWrapper : public WebSocketServerProvider, public Serializable
{
public:
    MuliplyerWrapper(Multiplyer *multiplyerP, WebSocketServer *ws, QJsonObject serialized);
    void applyOn(QMap<QString, QSharedPointer<DeviceState> > *on);

private:
    Multiplyer* multiplyer;
    RemoteBool active;

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientId);

    // Serializable interface
public:
    QJsonObject serialize();
};

#endif // MULIPLYERWRAPPER_H
