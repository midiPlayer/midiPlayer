#include "beamerscene.h"
#include "../devices/beamerdevicestate.h"

BeamerScene::BeamerScene(QString name,
        VirtualDeviceManager *devMgr,
        BeamerEffectHandler *beamerEffects,
        WebSocketServer* ws,
        AudioProcessor *jackP,
        PlanedTriggerHandler *planedTriggerHandler, InjectVirtualDeviceManager *injektorP,
        QJsonObject serialized):
    Scene(name,serialized),
    WebSocketServerProvider(ws),
    effect(serialized.value("effect").toObject(),beamerEffects,providerId),
    filterDevmgr(devMgr,{Device::Beamer}),
    selectDevMgr(&filterDevmgr,ws,serialized.value("selectDevMgr").toObject()),
    ungroupDevMgr(&selectDevMgr),
    trigger(ws, jackP, planedTriggerHandler, serialized.value("trigger").toObject()),
    isTriggered(false),
    myBeamerEffects(beamerEffects),
    colorDevMgr(&selectDevMgr,this),
    injektor(injektorP)
{
    connect(&trigger,SIGNAL(trigger()),this,SLOT(triggered()));
    injektor->addInjektor(&colorDevMgr);
}

BeamerScene::~BeamerScene()
{
    injektor->rmInjektor(&colorDevMgr);
}

QJsonObject BeamerScene::serialize()
{
    QJsonObject ser;
    ser.insert("effect",effect.serialize());
    ser.insert("selectDevMgr",selectDevMgr.serialize());
    ser.insert("trigger",trigger.serialize());
    return Scene::serializeScene(ser);
}

QMap<QString, QSharedPointer<DeviceState> > BeamerScene::getDeviceState()
{
    BeamerEffectConfig preparedEffect = effect;
    if(isTriggered)
        preparedEffect.trigger();

    QMap<QString, QSharedPointer<DeviceState> > ret;
    foreach(QSharedPointer<Device> dev, ungroupDevMgr.getDevices().values()){
        QSharedPointer<BeamerDeviceState> state = dev.data()->createEmptyState().dynamicCast<BeamerDeviceState>();
        if(state.isNull())
            continue;
        state.data()->addEffect(preparedEffect);
        ret.insert(dev.data()->getDeviceId(),state);
    }

    isTriggered = false;
    return ret;
}

QString BeamerScene::getSceneTypeString()
{
    return getSceneTypeStringStaticly();
}

QString BeamerScene::getSceneTypeStringStaticly()
{
    return "beamerScene";
}

void BeamerScene::triggered()
{
    isTriggered = true;
}

QString BeamerScene::getRequestType()
{
    return getSceneTypeStringStaticly();
}

QJsonObject BeamerScene::getEffectMsg()
{
    QJsonObject resp;
    if(effect.getEffect().isNull()){
        resp.insert("effect",-1);
    }
    else{
        resp.insert("config",effect.getConfig());
        resp.insert("effect",effect.getEffect().data()->providerId);
    }

    resp.insert("trigger",trigger.providerId);
    resp.insert("devs",selectDevMgr.providerId);

    return resp;
}

QSharedPointer<BeamerEffect> BeamerScene::getEffect()
{
    return effect.getEffect();
}

void BeamerScene::stop()
{
    trigger.stop();
}

void BeamerScene::start()
{
    trigger.start();
}

void BeamerScene::sendEffect(int clientId)
{
    QJsonObject resp = getEffectMsg();
    sendMsg(resp,clientId,true);
}

void BeamerScene::sendEffect()
{
    QJsonObject resp = getEffectMsg();
    sendMsg(resp,true);    }

void BeamerScene::clientRegistered(QJsonObject, int clientId)
{
    sendEffect(clientId);
}

void BeamerScene::clientMessage(QJsonObject msg, int)
{
    bool changed = false;

    if(msg.contains("effect")){
        if(effect.getEffect().isNull() || effect.getEffect().data()->getName() != msg.value("effect").toString())
        {
            effect.setEffect(myBeamerEffects->getBeamerEffectByName(msg.value("effect").toString()));
            effect.setConfig(QJsonObject());//clear
            colorDevMgr.reloadDevices();//effect changed
            changed = true;
        }
    }
    if(msg.contains("config") && effect.getConfig() != msg.value("config").toObject()){
        effect.setConfig(msg.value("config").toObject());
        changed = true;
    }
    if(changed){
        sendEffect();
        qDebug() << "effectChanged";
    }
}
