#ifndef SUBSCENEHANDLER_H
#define SUBSCENEHANDLER_H

#include <QSharedPointer>
#include "../scene.h"
#include "../websocketserverprovider.h"
#include "../scenebuilder.h"

class SubSceneHandler : public QObject, public WebSocketServerProvider, public Serializable
{
    Q_OBJECT

public:
    SubSceneHandler(WebSocketServer *ws, SceneBuilder* builderP, QJsonObject serialized = QJsonObject());

    QSharedPointer<Scene> getScene() const;
    void setScene(QSharedPointer<Scene> newScene);

signals:
    void sceneChanged();

private:
    QSharedPointer<Scene> myScene;
    SceneBuilder* builder;

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int) {}
    void clientMessage(QJsonObject msg, int);

    // Serializable interface
    QJsonObject getSceneMsg();

public:
    QJsonObject serialize();
};

#endif // SUBSCENEHANDLER_H
