#include "djdeck.h"

DjDeck::DjDeck(OscConnectorDeck *oscDeckP, QMap<QString,QSharedPointer<SubSceneHandler> >  *song_scenesP, WebSocketServer *ws,
               SceneBuilder* builderP):
    QObject(),
    WebSocketServerProvider(ws),
    oscDeck(oscDeckP),
    song_scenes(song_scenesP),
    sendTimer(),
    builder(builderP),
    myScene()
{
    builder.setOsccDeck(oscDeck);

    connect(oscDeck,SIGNAL(somethingChanged()),this,SLOT(requestSend()));
    connect(oscDeck,SIGNAL(playChanged(bool)),this,SLOT(startStopScene(bool)));
    connect(oscDeck,SIGNAL(titleChnaged(QString)),this,SLOT(updateScene()));
    connect(&sendTimer,SIGNAL(timeout()),this,SLOT(sendState()));
    sendTimer.setSingleShot(true);

    updateScene();
}

DjDeck::~DjDeck()
{
}

float DjDeck::getOpacity()
{
    if(!oscDeck->getPlay())
        return false;
    return oscDeck->getVolume();
}

QSharedPointer<Scene> DjDeck::getScene()
{
    return myScene;
}

QString DjDeck::getSong()
{
    return oscDeck->getTitle();
}

QString DjDeck::getRequestType()
{
    return "djDeck";
}

void DjDeck::requestSend()
{
    if(sendTimer.isActive())
        return;
    sendTimer.start(5);
}

void DjDeck::sendState()
{
    QJsonObject ret;
    ret.insert("title",oscDeck->getTitle());
    ret.insert("playing",oscDeck->getPlay());
    ret.insert("pos",oscDeck->getPositionRel());
    ret.insert("volume",oscDeck->getVolume());
    QString  sceneName = "";
    QSharedPointer<Scene> scene = getScene();
    if(!scene.isNull()){
     sceneName = scene.data()->getName();
     QSharedPointer<WebSocketServerProvider> wsScene = scene.dynamicCast<WebSocketServerProvider>();
     if(!wsScene.isNull()){
         ret.insert("sceneProviderId",wsScene.data()->providerId);
         ret.insert("sceneRequestType",wsScene.data()->getRequestType());
     }
    }
    ret.insert("scene",sceneName);
    sendMsg(ret,true);
}

void DjDeck::startStopScene(bool start)
{
    if(myScene.isNull())
        return;
    if(start)
        myScene.data()->start();
    else
        myScene.data()->stop();
}

void DjDeck::updateScene()
{
    if(!myScene.isNull())
        myScene.data()->stop();
    myScene = QSharedPointer<Scene>();
    if(song_scenes->contains(getSong())){
        QSharedPointer<Scene> scene = song_scenes->value(getSong()).data()->getScene();
        if(!scene.isNull()){
            myScene = builder.copyScene(scene);
            if(oscDeck->getPlay())
                myScene.data()->start();
        }
    }
    emit sceneChanged();
    sendState();
}

void DjDeck::clientRegistered(QJsonObject, int)
{
    sendState();
}

void DjDeck::clientMessage(QJsonObject msg, int clientId)
{
    if(msg.contains("save")){
        if(song_scenes->contains(getSong())){
            QSharedPointer<SubSceneHandler> handl = song_scenes->value(getSong());
            handl.data()->setScene(myScene);

        }
    }
}
