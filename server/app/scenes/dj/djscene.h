#ifndef DJSCENE_H
#define DJSCENE_H

#include "../../scene.h"
#include "../../websocketserverprovider.h"
#include "../../oscconnector.h"
#include "../../oscconnectordeck.h"
#include "../../scenes/sceneprovider.h"
#include <QMap>
#include <QString>
#include "djdeck.h"
#include "../../virtualDeviceManager/virtualdevicemanager.h"
#include "../../ungroupvirtualdevicemanager.h"
#include "../../scenes/subscenehandler.h"

class DjScene : public Scene, public WebSocketServerProvider
{
    Q_OBJECT

public:
    DjScene(QString name,
            OscConnector *oscc,
            WebSocketServer* ws,
            SceneProvider* sceneProviderP,
            VirtualDeviceManager* devMgrP,
            SceneBuilder *builderP,
            QJsonObject serialized = QJsonObject());
    ~DjScene();

public slots:
    void deserializeScenes();
    void sendSongScenes();
    void updateScenes();
    void updateDecks();

    // Serializable interface

public:
    QJsonObject serialize();

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int);

    // Scene interface
public:
    QMap<QString, QSharedPointer<DeviceState> > getDeviceState();
    QString getSceneTypeString();

    static QString getSceneTypeStringStaticaly();

private:
    QMap<QString,QSharedPointer<SubSceneHandler> > songs_scenes;
    QList<DjDeck*> decks;
    SceneProvider* sceneProvider;
    UngroupVirtualDeviceManager ungrouped;
    QSharedPointer<Scene> getSceneById(int id);
    QJsonArray clientJsonSongs_scenes();
    QJsonObject m_serialized;
    WebSocketServer *wss;
    SceneBuilder* builder;

    // Scene interface
public:
    QList<QSharedPointer<Scene> > getSubScenes();
};

#endif // DJSCENE_H
