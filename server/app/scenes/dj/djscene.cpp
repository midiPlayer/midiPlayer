#include "djscene.h"

#include <QJsonArray>
#include <QJsonObject>
#include "../../devices/device.h"

#define KEY_SONGS_SCENES "songs_scenes"


DjScene::DjScene(QString name, OscConnector *oscc, WebSocketServer *ws, SceneProvider *sceneProviderP,
                 VirtualDeviceManager *devMgrP, SceneBuilder *builderP, QJsonObject serialized):
    Scene(name, serialized),
    WebSocketServerProvider(ws),
    sceneProvider(sceneProviderP),
    ungrouped(devMgrP),
    m_serialized(serialized),
    wss(ws),
    builder(builderP)
{
    for (int deckNr = 0; deckNr < oscc->getNumDecks();deckNr++) {
        DjDeck *deck = new DjDeck(oscc->getDeck(deckNr), &songs_scenes,ws,builderP);
        connect(deck,SIGNAL(sceneChanged()),this,SLOT(updateScenes()));
        decks.append(deck);
    }

    deserializeScenes();
}

DjScene::~DjScene()
{
    foreach (DjDeck* deck, decks) {
       delete deck;
    }
}

QJsonObject DjScene::serialize()
{
    QJsonObject ret;

    QJsonArray songs_sceneJson;
    foreach (QString song, songs_scenes.keys()) {
        QJsonObject song_sceneJson;
        song_sceneJson.insert("song",song);
        song_sceneJson.insert("scene", songs_scenes.value(song).data()->serialize());
        songs_sceneJson.append(song_sceneJson);
    }

    ret.insert(KEY_SONGS_SCENES,songs_sceneJson);

    return serializeScene(ret);
}

void DjScene::deserializeScenes()
{
    foreach(QJsonValue song_sceneVal, m_serialized.value(KEY_SONGS_SCENES).toArray()){
        QJsonObject song_sceneObj = song_sceneVal.toObject();
        QSharedPointer<SubSceneHandler> prov = QSharedPointer<SubSceneHandler>(new SubSceneHandler(wss,builder,song_sceneObj.value("scene").toObject()));
        connect(prov.data(),SIGNAL(sceneChanged()),this,SLOT(updateDecks()));
        songs_scenes.insert(song_sceneObj.value("song").toString(),prov);
    }

    foreach(DjDeck *deck,decks){
        deck->updateScene();
    }
}

void DjScene::sendSongScenes()
{
    QJsonArray songs_scenesJson = clientJsonSongs_scenes();
    QJsonObject ret;
    ret.insert("songs_scenes",songs_scenesJson);
    sendMsg(ret,true);
}

void DjScene::updateScenes()
{
    //remove unused
    QStringList rm;
    foreach(QString song,songs_scenes.keys()){
        if(songs_scenes.value(song).data()->getScene().isNull())
            rm.append(song);
    }
    foreach(QString song,rm){
        songs_scenes.remove(song);
    }

    //insert empty for decks
    foreach(DjDeck* deck, decks){
        if(deck->getSong() != "" && !songs_scenes.contains(deck->getSong())){
            QSharedPointer<SubSceneHandler> handl(new SubSceneHandler(wss,builder,QJsonObject()));
            connect(handl.data(),SIGNAL(sceneChanged()),this,SLOT(updateDecks()));
            songs_scenes.insert(songs_scenes.begin(),deck->getSong(),handl);
        }
    }

    sendSongScenes();
}

void DjScene::updateDecks()
{
    foreach(DjDeck* deck, decks){
        deck->updateScene();
    }
}

QJsonArray DjScene::clientJsonSongs_scenes()
{
    QJsonArray songs_scenesJson;
    foreach (QString song, songs_scenes.keys()) {
        QJsonObject song_sceneJson;
        song_sceneJson.insert("song",song);
        int sceneProv = songs_scenes.value(song).data()->providerId;
        song_sceneJson.insert("sceneProv",sceneProv);
        songs_scenesJson.append(song_sceneJson);
    }

    return songs_scenesJson;
}

QList<QSharedPointer<Scene> > DjScene::getSubScenes()
{
    QList<QSharedPointer<Scene> > ret;
    foreach (QSharedPointer<SubSceneHandler> sceneH, songs_scenes.values()) {
        QSharedPointer<Scene> scene = sceneH.data()->getScene();
        if(!scene.isNull())
            ret.append(scene);
    }
    return ret;
}

QString DjScene::getRequestType()
{
    return "djScene";
}

void DjScene::clientRegistered(QJsonObject, int clientId)
{
    QJsonObject ret;

    //decks
    QJsonArray decksJson;
    foreach (DjDeck* deck, decks) {
        decksJson.append(deck->providerId);
    }
    ret.insert("decks",decksJson);

    //songs_scenes
    QJsonArray songs_scenesJson = clientJsonSongs_scenes();
    ret.insert("songs_scenes",songs_scenesJson);

    sendMsg(ret,clientId,true);

}

QSharedPointer<Scene> DjScene::getSceneById(int id)
{
    QSharedPointer<Scene> s;
    QList<QSharedPointer<Scene> > avScenes = sceneProvider->getScenes();
    foreach (QSharedPointer<Scene> scene, avScenes) {
        if(scene.data()->getId() == id)
            s = scene;
    }
    return s;
}

void DjScene::clientMessage(QJsonObject msg, int)
{

}

QMap<QString, QSharedPointer<DeviceState> > DjScene::getDeviceState()
{
    QMap<QString, QSharedPointer<DeviceState> > ret;

    //create empty state
    QHash<QString, QSharedPointer<Device> >  avDev = ungrouped.getDevices();
    foreach(QSharedPointer<Device> dev, avDev.values()){
        ret.insert(dev.data()->getDeviceId(),dev.data()->createEmptyState());
    }

    //fusion scenes:
    foreach (DjDeck* deck, decks) {
        QSharedPointer<Scene> deckScene = deck->getScene();
        if(deckScene.isNull())
            continue;
        QMap<QString,QSharedPointer<DeviceState> > states = deckScene.data()->getDeviceState();
        foreach(QString devId, states.keys()){
            float opaciry = deck->getOpacity();
            QSharedPointer<DeviceState> state = states.value(devId).data()->fusionAlone(DeviceState::AV,1 - opaciry);
            if(ret.contains(devId)){
                state = ret.value(devId).data()->fusionWith(state,DeviceState::MAX,0);//opacity not used in Fusiontype MAX
                ret.remove(devId);
            }
            ret.insert(devId,state);
        }
    }

    return ret;
}

QString DjScene::getSceneTypeString()
{
    return DjScene::getSceneTypeStringStaticaly();
}

QString DjScene::getSceneTypeStringStaticaly()
{
 return "djScene";
}
