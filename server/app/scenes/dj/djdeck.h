#ifndef DJDECK_H
#define DJDECK_H

#include "../../websocketserverprovider.h"
#include "../../oscconnectordeck.h"
#include "../../scene.h"
#include <QTimer>
#include "../../scenebuilder.h"
#include "../../scenes/subscenehandler.h"

class DjDeck : public QObject, public WebSocketServerProvider
{
    Q_OBJECT

public:

    DjDeck(OscConnectorDeck* oscDeckP, QMap<QString,QSharedPointer<SubSceneHandler> > *song_scenesP, WebSocketServer* ws, SceneBuilder *builderP);
    ~DjDeck();

    float getOpacity();
    QSharedPointer<Scene> getScene();

    QString getSong();

signals:
    void sceneChanged();


private:
    OscConnectorDeck* oscDeck;
    QMap<QString,QSharedPointer<SubSceneHandler> > *song_scenes;

    // WebSocketServerProvider interface
public:
    QString getRequestType();

public slots:
    void requestSend();
    void sendState();
    void startStopScene(bool start);
    void updateScene();

private:
    void clientRegistered(QJsonObject, int);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int clientId);
    QTimer sendTimer;

    SceneBuilder builder;
    QSharedPointer<Scene> myScene;
};

#endif // DJDECK_H
