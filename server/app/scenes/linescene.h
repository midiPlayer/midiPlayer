#ifndef LINESCENE_H
#define LINESCENE_H

#include "../scene.h"
#include "../websocketserverprovider.h"
#include "../trigger.h"
#include "../colorbutton.h"
#include "../virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "../filtervirtualdevicemanager.h"
#include "../audioprocessor.h"
#include "../RemoteTypes/remotefloat.h"
#include "../RemoteTypes/remotenumber.h"
#include "../rgbwcolor.h"

class LineScene : public Scene, public WebSocketServerProvider
{
Q_OBJECT

public:
    LineScene(QString name,
              WebSocketServer* ws,
              AudioProcessor* audio, PlanedTriggerHandler *handler,
              VirtualDeviceManager *vDev,
              QJsonObject serialized);

    ~LineScene();

    QJsonObject serialize();
    QString getRequestType();
    QMap<QString, QSharedPointer<DeviceState> > getDeviceState();
    void stop();
    void start();
    QString getSceneTypeString();
    static QString getSceneTypeStringStyticly();

    void clearLines();

public slots:
    void regenerateLines();
    void triggered();

private:
    void clientRegistered(QJsonObject, int clientId);
    Trigger trigger;
    ColorButton colors;
    SelectVirtualDeviceManager devices;
    SelectVirtualDeviceManager startDevices;
    RemoteFloat speed;
    RemoteFloat acceleration;
    RemoteNumber blockSize;

    class Block{
    public:
        Block(int sizeP, RGBWColor colorP);
        float getPos(float speed, float acceleration);
        int getSize() const;

        RGBWColor getColor() const;

    private:
        QTime stopwatch;
        int size;
        RGBWColor color;
    };

    class Line{
    public:
        Line(){}
        QList<QString> points;
        void getCurrent(LineScene::Block *block, float pos, QMap<QString, RGBWColor> *dest);
        bool isOnLine(int pos);
    };


    QList<Line*> lines;
    QList<Block*> blocks;

};

#endif // LINESCENE_H
