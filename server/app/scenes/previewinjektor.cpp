#include "previewinjektor.h"
#include "../devices/colorfuldevicestate.h"

PreviewInjektor::PreviewInjektor(VirtualDeviceManager *devs):
    previewOn(false),
    previewColor(0,0,0,0),
    devMgr(devs)
{

}

QMap<QString, QSharedPointer<DeviceState> > PreviewInjektor::injektDeviceState(QMap<QString, QSharedPointer<DeviceState> > originalDeviceState)
{
    if(!previewOn)
        return originalDeviceState;

    QMap<QString, QSharedPointer<DeviceState> > ret = originalDeviceState;

    foreach (QSharedPointer<Device> dev, devMgr->getDevices()) {
        QSharedPointer<DeviceState> state = dev.data()->createEmptyState();
        QSharedPointer<ColorfulDeviceState> colState = state.dynamicCast<ColorfulDeviceState>();
        if(colState.isNull())
            continue;//we only want to preview colorful devices

        colState.data()->setRGBW(previewColor);

        if(ret.contains(dev.data()->getDeviceId())){
            ret.remove(dev.data()->getDeviceId());
        }

        ret.insert(dev.data()->getDeviceId(),colState);
    }
    return ret;
}

void PreviewInjektor::enablePreview(RGBWColor color)
{
    previewColor = color;
    previewOn = true;
}

void PreviewInjektor::dissablePreview()
{
    previewOn = false;
}
