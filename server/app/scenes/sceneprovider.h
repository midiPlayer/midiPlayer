#ifndef SCENEPROVIDER_H
#define SCENEPROVIDER_H

#include  "../scene.h"

class SceneProvider
{

public:
    SceneProvider();
    virtual ~SceneProvider() {}
    virtual QList<QSharedPointer<Scene>> getScenes() = 0;

//signals:
    virtual void scenesChanged()  = 0;
    virtual void scenesInitialized()  = 0;
};

#endif // SCENEPROVIDER_H
