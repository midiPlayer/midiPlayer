#ifndef MOUSESCENE_H
#define MOUSESCENE_H

#include "../scene.h"
#include "../filtervirtualdevicemanager.h"
#include "../virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include "../RemoteTypes/remotefloat.h"

class MouseScene : public Scene, public WebSocketServerProvider
{
public:
    MouseScene(QString name,VirtualDeviceManager* mgr, WebSocketServer* ws, QJsonObject serialized);

private:
    FilterVirtualDeviceManager filter;
    SelectVirtualDeviceManager xDev;
    SelectVirtualDeviceManager yDev;
    RemoteFloat x,y;
    float savedX, savedY;
    // Serializable interface
public:
    QJsonObject serialize();

    // Scene interface
public:
    QMap<QString, QSharedPointer<DeviceState> > getDeviceState();
    QString getSceneTypeString();
    static QString getSceneTypeStringStaticly();
    void start();

    // WebSocketServerProvider interface
public:
    QString getRequestType();

private:
    void clientRegistered(QJsonObject, int clientId);
    void clientUnregistered(QJsonObject, int){}
    void clientMessage(QJsonObject msg, int);

};

#endif // MOUSESCENE_H
