#ifndef BEAMERCOLORDEVICESTATETEST_HPP
#define BEAMERCOLORDEVICESTATETEST_HPP

#include <QtTest>
#include "../app/devices/beamercolordevice.h"
#include "../app/devices/beamercolordevicestate.h"
#include <QSharedPointer>

class BeamerColorDeviceStateTest : public QObject
{
Q_OBJECT

public:
    BeamerColorDeviceStateTest(){

    }

    ~BeamerColorDeviceStateTest(){

    }


private slots:
    void multiply();

private:
    void checkColor(QSharedPointer<DeviceState> state,RGBWColor expected);
};

#endif // BEAMERCOLORDEVICESTATETEST_HPP
