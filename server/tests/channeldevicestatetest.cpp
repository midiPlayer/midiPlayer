#include "channeldevicestatetest.hpp"

#define CHECK_OPACIY(actual, expected) \
{\
    QSharedPointer<ChannelDeviceState> channelState = actual.dynamicCast<ChannelDeviceState>();\
    if(channelState.isNull())\
        throw("test error");\
    QCOMPARE(int(channelState.data()->getOpacity() * 100.0),int(expected*100.0));\
}

#define CHECK_COLOR(state, expected, noOpacity)\
{\
    QSharedPointer<ChannelDeviceState> channelState = state.dynamicCast<ChannelDeviceState>();\
    if(channelState.isNull())\
        throw("test error");\
    QCOMPARE(int(channelState.data()->getChannelValue(0,noOpacity) * 255.0), int(expected.getR() * 255.0));\
    QCOMPARE(int(channelState.data()->getChannelValue(1,noOpacity) * 255.0), int(expected.getG() * 255.0));\
    QCOMPARE(int(channelState.data()->getChannelValue(2,noOpacity) * 255.0), int(expected.getB() * 255.0));\
    QCOMPARE(int(channelState.data()->getChannelValue(3,noOpacity) * 255.0), int(expected.getW() * 255.0));\
}

#define CHECK_COLOR_OP(state, expected)\
    CHECK_COLOR(state, expected, false)

#define CHECK_COLOR_NO_OP(state, expected)\
    CHECK_COLOR(state, expected, true)

void ChannelDeviceStateTest::setRgb(){
    //single
    ChannelDevice whiteDev(0,1,0,"testDevSingle",Device::White,false);
    QSharedPointer<ChannelDeviceState> state2 = whiteDev.createEmptyChannelState();
    state2.data()->setRGB(QColor(90,0,0));
    QCOMPARE(int(state2.data()->getChannelValue(0) * 255.0), 30);

    //rgb
    ChannelDevice rgbDev(0,3,0,"testDevRGB",Device::RGB,false);
    QSharedPointer<ChannelDeviceState> state = rgbDev.createEmptyChannelState();
    state.data()->setRGB(QColor(100,80,70));
    QCOMPARE(int(state.data()->getChannelValue(0) * 255.0), 100);
    QCOMPARE(int(state.data()->getChannelValue(1) * 255.0), 80);
    QCOMPARE(int(state.data()->getChannelValue(2) * 255.0), 70);

    //on rgbw
    ChannelDevice rgbwDev(0,4,0,"testDevRGBW",Device::RGBW,false);
    QSharedPointer<ChannelDeviceState> state1 = rgbwDev.createEmptyChannelState();
    state1.data()->setRGB(QColor(100,80,70));
    QCOMPARE(int(state1.data()->getChannelValue(0) * 255.0), 100);
    QCOMPARE(int(state1.data()->getChannelValue(1) * 255.0), 80);
    QCOMPARE(int(state1.data()->getChannelValue(2) * 255.0), 70);
    QCOMPARE(int(state1.data()->getChannelValue(3) * 255.0), 0);
}

void ChannelDeviceStateTest::multiply()
{
    ChannelDevice dev(0,3,0,"testDev1",Device::RGB,false);
    QSharedPointer<ChannelDeviceState> state = dev.createEmptyChannelState();
    state.data()->setRGB(QColor(100,50,0));
    state.data()->multiply(0.5);
    QCOMPARE(int(state.data()->getChannelValue(0) * 255.0), 100);
    QCOMPARE(int(state.data()->getChannelValue(1) * 255.0), 50);
    QCOMPARE(int(state.data()->getChannelValue(2) * 255.0), 0);
    QCOMPARE(state.data()->getOpacity(), 0.5);
    QCOMPARE(int(state.data()->getChannelValue(0,true) * 255.0), 50);
    QCOMPARE(int(state.data()->getChannelValue(1,true) * 255.0), 25);
    QCOMPARE(int(state.data()->getChannelValue(2,true) * 255.0), 0);
}

void ChannelDeviceStateTest::fusion(){
    ChannelDevice dev(0,4,0,"testDev1",Device::RGBW,false);
    QSharedPointer<ChannelDeviceState> stateRed = dev.createEmptyChannelState();
    QSharedPointer<ChannelDeviceState> stateBlue = dev.createEmptyChannelState();
    stateRed.data()->setRGBW(RGBWColor::create(255, 10,   8, 5));
    stateBlue.data()->setRGBW(RGBWColor::create(30,  25 ,255,10));

    CHECK_COLOR_NO_OP(stateRed,RGBWColor::create(255,10,8,5));

    QSharedPointer<DeviceState> maxFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::MAX,0);
    CHECK_COLOR_NO_OP(maxFusion,RGBWColor::create(255,25,255,10));

    QSharedPointer<DeviceState> minFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::MIN,0);
    CHECK_COLOR_NO_OP(minFusion,RGBWColor::create(30,10,8,5));

    QSharedPointer<DeviceState> avFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::AV,0.5);
    CHECK_COLOR_NO_OP(avFusion,RGBWColor::create(142,17,131,7));

    QSharedPointer<DeviceState> av2Fusion = stateRed.data()->fusionWith(stateBlue,DeviceState::AV,0);
    CHECK_COLOR_NO_OP(av2Fusion,RGBWColor::create(255,10,8,5));

    QSharedPointer<DeviceState> av3Fusion = stateRed.data()->fusionWith(stateBlue,DeviceState::AV,1);
    CHECK_COLOR_NO_OP(av3Fusion,RGBWColor::create(30,25,255,10));

    QSharedPointer<DeviceState> ovFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::OVERRIDE,0);
    CHECK_COLOR_NO_OP(ovFusion,RGBWColor::create(30,25,255,10));

    QSharedPointer<DeviceState> mulFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::Multiply,0);
    CHECK_COLOR_NO_OP(mulFusion,RGBWColor::create(30,0,8,0));

    //and now with opacity
    stateBlue.data()->setOpacity(0.5);
    //blue is now 15, 12, 127, 5
    QSharedPointer<DeviceState> opMaxFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::MAX,0);
    CHECK_COLOR_NO_OP(opMaxFusion,RGBWColor::create(255,12,127,5));
    CHECK_OPACIY(opMaxFusion,1);

    QSharedPointer<DeviceState> opMinFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::MIN,0);
    CHECK_COLOR_NO_OP(opMinFusion,RGBWColor::create(15,10,8,5));
    CHECK_OPACIY(opMinFusion,1);

    QSharedPointer<DeviceState> opAvFusion = stateRed.data()->fusionWith(stateBlue,DeviceState::AV,0.5);
    CHECK_COLOR_OP(opAvFusion,RGBWColor::create(142,17,131,7));//fusions still as there was no opacity
    CHECK_OPACIY(opAvFusion,0.75);
}
