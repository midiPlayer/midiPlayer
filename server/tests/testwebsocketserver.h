#ifndef TESTWEBSOCKETSERVER_H
#define TESTWEBSOCKETSERVER_H

#include "../app/websocketserver.h"

class TestWebsocketServer : public WebSocketServer
{
public:
    TestWebsocketServer();

    // WebSocketServer interface
public:
    void registerProvider(WebSocketServerProvider *){}
    void unregisterProvider(WebSocketServerProvider *me){}
    void sendData(QJsonObject, QWebSocket *, WebSocketServerProvider *, bool){}
    void disconnectAllClients(){}
};

#endif // TESTWEBSOCKETSERVER_H
