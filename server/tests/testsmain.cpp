#include "channeldevicestatetest.hpp"
#include "beamercolordevicestatetest.hpp"
#include "VirtualDeviceManager/filtervirtualdevicemanagertest.h"
#include "VirtualDeviceManager/selectvirtualdevicemanagertest.h"
#include "VirtualDeviceManager/dynamicvirtualdevicemanagertest.h"
#include <cppunit/ui/text/TestRunner.h>

int main(int, char**)
{
   CppUnit::TextUi::TestRunner runner;
    runner.addTest( FilterVirtualDeviceManagerTest::suite() );
    runner.addTest( SelectVirtualDeviceManagerTest::suite() );
    runner.addTest( DynamicVirtualDeviceManagerTest::suite() );
   return runner.run();
}
