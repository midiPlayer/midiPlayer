QT += testlib
#QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

LIBS += -l cppunit

TEMPLATE = app
include(../app/src.pri)

SOURCES += channeldevicestatetest.cpp \
    beamercolordevicestatetest.cpp \
    testsmain.cpp \
    VirtualDeviceManager/abstractvirtualdevicemanagertest.cpp \
    VirtualDeviceManager/filtervirtualdevicemanagertest.cpp \
    VirtualDeviceManager/selectvirtualdevicemanagertest.cpp \
    VirtualDeviceManager/dynamicvirtualdevicemanagertest.cpp \
    testwebsocketserver.cpp

HEADERS += \
    channeldevicestatetest.hpp \
    beamercolordevicestatetest.hpp \
    VirtualDeviceManager/abstractvirtualdevicemanagertest.h \
    VirtualDeviceManager/filtervirtualdevicemanagertest.h \
    VirtualDeviceManager/selectvirtualdevicemanagertest.h \
    VirtualDeviceManager/dynamicvirtualdevicemanagertest.h \
    testwebsocketserver.h
