#include "beamercolordevicestatetest.hpp"
#include "testwebsocketserver.h"

//QTEST_APPLESS_MAIN(BeamerColorDeviceStateTest)

//#include "beamercolordevicestatetest.moc"

void BeamerColorDeviceStateTest::multiply(){
    TestWebsocketServer ws;
    QSharedPointer<BeamerDevice> beamer(new BeamerDevice("dev",&ws,QVector3D(0,0,0)));
    //BeamerScene scene("scene", NULL, NULL,NULL,NULL,NULL,NULL,QJsonObject());
    BeamerColorDevice dev("beamer",NULL,beamer,"FG");
    QSharedPointer<ColorfulDeviceState> state = dev.createEmptyState().dynamicCast<ColorfulDeviceState>();
    state.data()->setRGBW(RGBWColor::create(10,10,10,10));
    state.data()->multiply(0.5);
    QCOMPARE(int(state.data()->getOpacity()*100),50);
}

void BeamerColorDeviceStateTest::checkColor(QSharedPointer<DeviceState> state, RGBWColor expected){
    QSharedPointer<BeamerColorDeviceState> beamerState = state.dynamicCast<BeamerColorDeviceState>();
    if(beamerState.isNull())
        throw("test error!");
    QVERIFY(beamerState.data()->getColor() == expected);
}
