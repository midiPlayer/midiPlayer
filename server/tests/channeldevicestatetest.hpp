#ifndef CHANNELDEVICESTATETEST_HPP
#define CHANNELDEVICESTATETEST_HPP

#include <QtTest>
#include "../app/devices/channeldevice.h"
#include "../app/devices/channeldevicestate.h"
#include <QSharedPointer>

class ChannelDeviceStateTest : public QObject
{

Q_OBJECT

public:
    ChannelDeviceStateTest()
    {

    }

    ~ChannelDeviceStateTest(){

    }

private slots:
    void setRgb();

    void multiply();

    void fusion();
};

//QTEST_APPLESS_MAIN(ChannelDeviceStateTest)
//#include "channeldevicestatetest.moc"


#endif // CHANNELDEVICESTATETEST_HPP
