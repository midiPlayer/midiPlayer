#ifndef FILTERVIRTUALDEVICEMANAGERTEST_H
#define FILTERVIRTUALDEVICEMANAGERTEST_H

#include "abstractvirtualdevicemanagertest.h"

#include <cppunit/TestFixture.h>
#include <cppunit/Test.h>

class FilterVirtualDeviceManagerTest : public AbstractVirtualDeviceManagerTest
{
public:
    FilterVirtualDeviceManagerTest();
    static CppUnit::Test* suite();
    // AbstractVirtualDeviceManagerTest interface
private:
    VirtualDeviceManager *createVdevMgr(VirtualDeviceManager *parent);
};

#endif // FILTERVIRTUALDEVICEMANAGERTEST_H
