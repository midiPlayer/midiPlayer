#ifndef DYNAMICVIRTUALDEVICEMANAGERTEST_H
#define DYNAMICVIRTUALDEVICEMANAGERTEST_H

#include "../testwebsocketserver.h"
#include <cppunit/TestFixture.h>
#include <cppunit/Test.h>
#include <cppunit/extensions/HelperMacros.h>
#include <QObject>
#include "../../app/dynamicvirtualdevicemanager.h"

class DynamicVirtualDeviceManagerTest : public QObject, public CppUnit::TestFixture
{
    Q_OBJECT

public:
    DynamicVirtualDeviceManagerTest();
    void testAddingRgbDev();
    void testAddingStripe();
    void testSerialize();
    static CppUnit::Test *suite();
private:
    TestWebsocketServer ws;
    void addRgbDev(DynamicVirtualDeviceManager *d);
    void addStripeDev(DynamicVirtualDeviceManager *d);
    int calls;

public slots:
    void countCall();
};

#endif // DYNAMICVIRTUALDEVICEMANAGERTEST_H
