#ifndef SELECTVIRTUALDEVICEMANAGERTEST_H
#define SELECTVIRTUALDEVICEMANAGERTEST_H

#include "abstractvirtualdevicemanagertest.h"
#include "../testwebsocketserver.h"

class SelectVirtualDeviceManagerTest : public AbstractVirtualDeviceManagerTest
{
public:
    SelectVirtualDeviceManagerTest();
    static CppUnit::Test* suite();
    void testSerialize();
    // AbstractVirtualDeviceManagerTest interface
private:
    VirtualDeviceManager *createVdevMgr(VirtualDeviceManager *parent);
    TestWebsocketServer ws;
};

#endif // SELECTVIRTUALDEVICEMANAGERTEST_H
