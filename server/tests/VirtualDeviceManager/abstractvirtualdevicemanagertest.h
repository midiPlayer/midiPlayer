#ifndef ABSTRACTVIRTUALDEVICEMANAGERTEST_H
#define ABSTRACTVIRTUALDEVICEMANAGERTEST_H

#include "../../app/devices/beamercolordevice.h"
#include "../../app/devices/beamercolordevicestate.h"
#include <cppunit/TestFixture.h>
#include <cppunit/Test.h>
#include <cppunit/extensions/HelperMacros.h>
#include <QObject>

class AbstractVirtualDeviceManagerTest : public QObject, public CppUnit::TestFixture
{
Q_OBJECT

public:
    AbstractVirtualDeviceManagerTest();

    virtual ~AbstractVirtualDeviceManagerTest() {}
    void testParentChanges();
    template<class T> static CppUnit::TestSuite *suite(){
        CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite( "AbstractVdevTest" );
        suiteOfTests->addTest( new CppUnit::TestCaller<T>(
                                       "testPartentChanges",
                                       &T::testParentChanges ) );
        return suiteOfTests;
    }

protected:
    class TestDevMgr : public VirtualDeviceManager{
        // VirtualDeviceManager interface
    QHash<QString, QSharedPointer<Device> > devices;
    QHash<QString, QSharedPointer<GroupDevice> > groups;
    int nameCtr;
    public:
        QHash<QString, QSharedPointer<Device> > getDevices();
        QHash<QString, QSharedPointer<GroupDevice> > getGroupDevices();
        void addDevice();
        void rmDevice();
        void addGroup(int nr = 0);
        void rmGroup(int nr = 0);

    };

private:
    virtual VirtualDeviceManager* createVdevMgr(VirtualDeviceManager* parent) = 0;
    int devChangedCount;

private slots:
    void devChanged();
};

#endif // ABSTRACTVIRTUALDEVICEMANAGERTEST_H
