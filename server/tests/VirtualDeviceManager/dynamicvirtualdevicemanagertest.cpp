#include "dynamicvirtualdevicemanagertest.h"
#include "../../app/devices/channeldevice.h"
#include "QJsonDocument"

DynamicVirtualDeviceManagerTest::DynamicVirtualDeviceManagerTest()
{

}

void DynamicVirtualDeviceManagerTest::testAddingRgbDev()
{
    /*//asdd rgb dev
    calls = 0;
    DynamicVirtualDeviceManager d(&ws,QJsonObject());
    connect(&d,SIGNAL(virtualDevicesChanged()),this,SLOT(countCall()));
    addRgbDev(&d);
    CPPUNIT_ASSERT_EQUAL(1, d.getDevices().size());
    CPPUNIT_ASSERT("testrgb" == d.getDevices().firstKey());
    QSharedPointer<Device> dev = d.getDevices().first();
    CPPUNIT_ASSERT("testrgb" == dev.data()->getDeviceId());
    CPPUNIT_ASSERT("rgb" == dev.data()->getTypeString());
    QSharedPointer<ChannelDevice> cdev = dev.dynamicCast<ChannelDevice>();
    CPPUNIT_ASSERT(!cdev.isNull());
    CPPUNIT_ASSERT_EQUAL(3,cdev.data()->getFirstChannel());
    CPPUNIT_ASSERT_EQUAL(3,cdev.data()->getNumChannels());
    CPPUNIT_ASSERT_EQUAL(10,cdev.data()->getUniverse());

    CPPUNIT_ASSERT_EQUAL(1,calls);*/
}

void DynamicVirtualDeviceManagerTest::testAddingStripe()
{
    //asdd rgb dev
    calls = 0;
    DynamicVirtualDeviceManager d(&ws,QJsonObject());
    connect(&d,SIGNAL(virtualDevicesChanged()),this,SLOT(countCall()));
    addStripeDev(&d);
    CPPUNIT_ASSERT_EQUAL(3, d.getGroupDevices().size()); //all allU + stripe
    CPPUNIT_ASSERT_EQUAL(1,calls);

}

void DynamicVirtualDeviceManagerTest::testSerialize()
{
    DynamicVirtualDeviceManager d(&ws,QJsonObject());
    connect(&d,SIGNAL(virtualDevicesChanged()),this,SLOT(countCall()));
    addRgbDev(&d);
    addStripeDev(&d);

    calls = 0;
    DynamicVirtualDeviceManager d2(&ws,d.serialize());
    CPPUNIT_ASSERT_EQUAL(1, d2.getDevices().size());
    CPPUNIT_ASSERT_EQUAL(3, d2.getGroupDevices().size());
    CPPUNIT_ASSERT_EQUAL(0,calls);
}

CppUnit::Test *DynamicVirtualDeviceManagerTest::suite()
{
    CppUnit::TestSuite *suiteOfTests = new CppUnit::TestSuite( "DynamicVirtualDeviceManagerTest" );
    suiteOfTests->addTest( new CppUnit::TestCaller<DynamicVirtualDeviceManagerTest>(
                                   "testAddingRgbDev",
                                   &DynamicVirtualDeviceManagerTest::testAddingRgbDev ) );

    suiteOfTests->addTest( new CppUnit::TestCaller<DynamicVirtualDeviceManagerTest>(
                                   "testAddingStripe",
                                   &DynamicVirtualDeviceManagerTest::testAddingStripe ) );

    suiteOfTests->addTest( new CppUnit::TestCaller<DynamicVirtualDeviceManagerTest>(
                                   "testSerialize",
                                   &DynamicVirtualDeviceManagerTest::testSerialize ) );
    return suiteOfTests;
}

void DynamicVirtualDeviceManagerTest::addRgbDev(DynamicVirtualDeviceManager *d)
{
    d->clientMessage(QJsonDocument::fromJson("{\"add\":{\"channel\":3,\"devId\":\"testrgb\",\"devsType\":\"rgb\",\"moduloMode\":false,\"numDev\":10,\"positions\":[],\"type\":\"rgb\",\"universe\":10,\"whiteColor\":\"\",\"x\":1,\"y\":2,\"z\":3}}").object(),0);
}

void DynamicVirtualDeviceManagerTest::addStripeDev(DynamicVirtualDeviceManager *d)
{
    d->clientMessage(QJsonDocument::fromJson("{\"add\":{\"channel\":1,\"devId\":\"stripeTest\",\"devsType\":\"rgb\",\"moduloMode\":false,\"numDev\":100,\"positions\":[],\"type\":\"group-stripe\",\"universe\":11,\"whiteColor\":\"\",\"x\":null,\"y\":null,\"z\":null}}").object(),0);
}

void DynamicVirtualDeviceManagerTest::countCall()
{
    calls++;
}
