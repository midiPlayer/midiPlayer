#include "filtervirtualdevicemanagertest.h"
#include "../../app/filtervirtualdevicemanager.h"

#include <cppunit/TestSuite.h>
#include <cppunit/TestCaller.h>

FilterVirtualDeviceManagerTest::FilterVirtualDeviceManagerTest()
{

}

VirtualDeviceManager *FilterVirtualDeviceManagerTest::createVdevMgr(VirtualDeviceManager *parent)
{
    return new FilterVirtualDeviceManager(parent);
}

CppUnit::Test *FilterVirtualDeviceManagerTest::suite()
{
    return AbstractVirtualDeviceManagerTest::suite <FilterVirtualDeviceManagerTest> ();
}
