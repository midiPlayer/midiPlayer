#include "selectvirtualdevicemanagertest.h"
#include "../../app/virtualDeviceManager/select/selectvirtualdevicemanager.h"
#include <QJsonDocument>

SelectVirtualDeviceManagerTest::SelectVirtualDeviceManagerTest()
{

}

CppUnit::Test *SelectVirtualDeviceManagerTest::suite()
{
    CppUnit::TestSuite * s =  AbstractVirtualDeviceManagerTest::suite<SelectVirtualDeviceManagerTest>();
    s->addTest( new CppUnit::TestCaller<SelectVirtualDeviceManagerTest>(
                                   "testPartentChanges",
                                   &SelectVirtualDeviceManagerTest::testSerialize ) );
    return s;
}

void SelectVirtualDeviceManagerTest::testSerialize()
{
    AbstractVirtualDeviceManagerTest::TestDevMgr tm;
    tm.addDevice();
    tm.addGroup(1);
    tm.addGroup(2);
    SelectVirtualDeviceManager * sdm1 = new SelectVirtualDeviceManager(&tm,
                                                                    &ws,QJsonObject());
    tm.rmGroup(2);

    QJsonObject s = sdm1->serialize();



    QJsonDocument json;
    json.setObject(s);
    qDebug() << json.toJson();
    CPPUNIT_ASSERT(QString(json.toJson()).contains("stripe1"));
    CPPUNIT_ASSERT(!QString(json.toJson()).contains("stripe2"));

}

VirtualDeviceManager *SelectVirtualDeviceManagerTest::createVdevMgr(VirtualDeviceManager *parent)
{
    return new SelectVirtualDeviceManager(parent,&ws,QJsonObject());
}
