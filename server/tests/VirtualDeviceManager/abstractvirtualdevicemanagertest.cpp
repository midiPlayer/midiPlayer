#include "abstractvirtualdevicemanagertest.h"
#include "../../app/devices/channeldevice.h"
#include "../../app/devices/stripegroupdevice.h"


AbstractVirtualDeviceManagerTest::AbstractVirtualDeviceManagerTest()
{

}


void AbstractVirtualDeviceManagerTest::testParentChanges()
{
    devChangedCount = 0;
    TestDevMgr testMgr;
    VirtualDeviceManager* devMgr = createVdevMgr(&testMgr);
    connect(devMgr,SIGNAL(virtualDevicesChanged()),this,SLOT(devChanged()));
    CPPUNIT_ASSERT_EQUAL(0,devChangedCount);//nothing changed

    devChangedCount = 0;
    testMgr.addDevice();
    CPPUNIT_ASSERT(devChangedCount == 1);

    devChangedCount = 0;
    testMgr.addDevice();
    CPPUNIT_ASSERT(devChangedCount <= 1);

    devChangedCount = 0;
    testMgr.rmDevice();
    CPPUNIT_ASSERT(devChangedCount <= 1);

    devChangedCount = 0;
    testMgr.rmDevice();
    CPPUNIT_ASSERT(devChangedCount <= 1);

    delete devMgr;
}

void AbstractVirtualDeviceManagerTest::devChanged()
{
    devChangedCount++;
}

QHash<QString, QSharedPointer<Device> > AbstractVirtualDeviceManagerTest::TestDevMgr::getDevices()
{
    return devices;
}

QHash<QString, QSharedPointer<GroupDevice> > AbstractVirtualDeviceManagerTest::TestDevMgr::getGroupDevices()
{
    return groups;
}

void AbstractVirtualDeviceManagerTest::TestDevMgr::addDevice()
{
    QString name = "dev" + QString::number(nameCtr++);
    devices.insert(name,QSharedPointer<ChannelDevice>(new ChannelDevice(0,3,0,name,Device::RGB,false)));
    emit virtualDevicesChanged();
}

void AbstractVirtualDeviceManagerTest::TestDevMgr::rmDevice()
{
    QString name = "dev" + QString::number(nameCtr--);
    devices.remove(name);
    emit virtualDevicesChanged();
}

void AbstractVirtualDeviceManagerTest::TestDevMgr::addGroup(int nr)
{
    QString devId = "stripe" + QString::number(nr);
    groups.insert(devId,QSharedPointer<StripeGroupDevice>(new StripeGroupDevice(devId,100,0,nr,Device::RGB,QMap<int,QVector3D>())));
    emit virtualDevicesChanged();
}

void AbstractVirtualDeviceManagerTest::TestDevMgr::rmGroup(int nr)
{
    QString devId = "stripe" + QString::number(nr);
    groups.remove(devId);
    emit virtualDevicesChanged();
}
