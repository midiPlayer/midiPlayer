#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlComponent>

#include "curvegenerator.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

     qmlRegisterType<CurveGenerator>("CurveGenerator", 1, 0, "CurveGenerator");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
