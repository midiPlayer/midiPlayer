import QtQuick 2.4
import QtQuick.Window 2.2
import CurveGenerator 1.0

Window {
    visible: true
    width: 400
    height: 300
    CurveGenerator{
        id:curveGen;
        Component.onCompleted: {
            points = [{x:0,y:0},{x:0.5,y:0.7},{x:1,y:1}];
        }
    }
    Canvas{
        property int activeElement : -1;
        anchors.fill: parent;
        onPaint: {
            console.log("paint");
            var ctx = getContext('2d');
            ctx.reset();
            ctx.beginPath();
            ctx.moveTo(0,0);
            curveGen.curvePoints.forEach(function(e){
                ctx.lineTo(e.x*width,(1-e.y)*height);
            });
            ctx.stroke();

            for(var i = 0;i < curveGen.points.length;i++){
                var e = curveGen.points[i];
                ctx.beginPath();
                var radius = 5;
                if(activeElement == i)
                    radius = 7;
                ctx.arc(e.x*width,(1-e.y)*height,radius,0,Math.PI*2,false);
                ctx.fill();
            };


        }
        MouseArea{
            anchors.fill: parent;
            onPressed: {
                var x = mouseX/width;
                var y = 1 - mouseY/height;

                var minDst = 1000;
                var min = -1;
                do{
                    for(var i = 0;i < curveGen.points.length;i++){
                        var e = curveGen.points[i];
                        var dst = (x-e.x)*(x-e.x) + (y-e.y)*(y-e.y);
                        if(dst < 8*8/(width*height) && dst < minDst) {
                            min = i;
                            minDst = dst;
                        }
                    };

                    if(min == -1){//create new point
                        curveGen.addPoint({"x" : x, "y" : y});
                        min = -2;
                    }
                }while(min == -2);
                parent.activeElement = min;

                parent.requestPaint();
            }
            onReleased: {
                parent.activeElement = -1;
                parent.requestPaint();
            }

            onPositionChanged: {
                if(parent.activeElement == -1)
                    return;
                var ps = curveGen.points;

                ps[parent.activeElement].x = Math.max(0,Math.min(1,mouseX/width));
                ps[parent.activeElement].y = Math.max(0,Math.min(1,1 - mouseY/height));

                //remove if dublikate
                for(var i = 0;i < ps.length;i++){
                    var e = ps[i];
                    if(parent.activeElement != -1
                            && Math.abs(e.x - ps[parent.activeElement].x)  < 10/width
                            && i != parent.activeElement){
                        console.log("rm");
                        ps.splice(i,1);
                        curveGen.points = ps;
                        parent.activeElement = -1;
                        parent.requestPaint();
                        return;
                    }
                }


                curveGen.points = ps;
                parent.requestPaint();
            }
        }
    }
}
