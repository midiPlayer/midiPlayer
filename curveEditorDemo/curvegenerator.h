#ifndef CURVEGENERATOR_H
#define CURVEGENERATOR_H
#include <QObject>
#include <QJsonObject>
#include <QJsonArray>
#include <QVector2D>

class CurveGenerator : public QObject
{
 Q_OBJECT

 Q_PROPERTY(QJsonArray points READ getPoints WRITE setPoints)
 Q_PROPERTY(QJsonArray curvePoints READ getCurvePoints)
public:
    CurveGenerator();
    QJsonArray getPoints();
    QJsonArray getCurvePoints();
    void setPoints(QJsonArray ps);
public slots:
    void addPoint(QJsonObject p);
private:
    QList<QVector2D> points;
    void calcCurvePoints();

    QJsonArray curvePoints;

};

#endif // CURVEGENERATOR_H
