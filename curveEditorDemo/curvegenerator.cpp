#include "curvegenerator.h"
#include "spline.h"
#include <QDebug>

CurveGenerator::CurveGenerator() : points(){
}

QJsonArray CurveGenerator::getPoints()
{
    QJsonArray ret;
    foreach (QVector2D vect,points) {
        QJsonObject pJson;
        pJson.insert("x",vect.x());
        pJson.insert("y",vect.y());
        ret.append(pJson);
    }
    return ret;
}

QJsonArray CurveGenerator::getCurvePoints()
{
    return curvePoints;
}

void CurveGenerator::setPoints(QJsonArray ps)
{
    points.clear();
    foreach(QJsonValue v, ps){
        QJsonObject pJson = v.toObject();
        if(!pJson.contains("x") || ! pJson.contains("y"))
            continue;
        points.append(QVector2D(pJson.value("x").toDouble(),pJson.value("y").toDouble()));
    }
    calcCurvePoints();
}

void CurveGenerator::addPoint(QJsonObject p)
{
    qDebug() << "new x" << p.value("x").toDouble();
    points.append(QVector2D(p.value("x").toDouble(),p.value("y").toDouble()));
    calcCurvePoints();
}

bool vectorXComp(const QVector2D &v1, const QVector2D &v2)
 {
     return v1.x() < v2.x();
 }

void CurveGenerator::calcCurvePoints()
{
        std::sort(points.begin(),points.end(),vectorXComp);
        qDebug() << getPoints();

       std::vector<double> X(points.size() + 2), Y(points.size() + 2);

       int c = 0;

       foreach(QVector2D v, points){
           X[c + 1] = v.x();
           Y[c + 1] = v.y();
           c++;
       }

       X[0] = -100;
       if(points.length() > 0)
           Y[0] = Y[1];
       else
           Y[0] = 0;

       X[c + 1] = 100;
       if(points.length() > 0)
           Y[c + 1] = Y[c];
       else
           Y[c+1] = 1;



       tk::spline s;
       s.set_points(X,Y);    // currently it is required that X is already sorted


      QJsonArray ret;
      double resolution = 50;
      for (int i = 0; i < resolution; i++) {
        double r = s(i/resolution);
        if(r < 0)
            r = 0;
        if(r > 1)
            r = 1;
        QJsonObject pJson;
        pJson.insert("x",i/resolution);
        pJson.insert("y",r);
        ret.append(pJson);
      }
      curvePoints = ret;
}
