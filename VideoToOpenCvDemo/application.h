#ifndef APPLICATION_H
#define APPLICATION_H
#include <QObject>
#include <QCamera>
#include "cameraframegrabber.h"

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

class Application : public QObject
{
Q_OBJECT

public:
    Application();
    ~Application();
public slots:
    void gotFrame(QImage img);
private:
    QCamera cam;
    CameraFrameGrabber grabber;
};

#endif // APPLICATION_H
