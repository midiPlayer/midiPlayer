#include "application.h"
#include <QDebug>
#include <QPixmap>

Application::Application()
{
    cam.setViewfinder(&grabber);
    connect(&grabber,SIGNAL(frameAvailable(QImage)),this,SLOT(gotFrame(QImage)));
    cam.start();
    namedWindow("out",CV_WINDOW_AUTOSIZE);
}

Application::~Application(){
    cam.stop();
}

void Application::gotFrame(QImage img)
{
    QImage src = img.convertToFormat(QImage::Format_RGB888);
    qDebug() << "got Frame!";
    cv::Mat tmp(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine());
     cv::Mat result; // deep copy just in case (my lack of knowledge with open cv)
     cvtColor(tmp, result,CV_BGR2RGB);
    cv::imshow("out",result);
}
