#include <QCoreApplication>

#include "application.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    new Application();
    return a.exec();
}
