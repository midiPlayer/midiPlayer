QT += core
QT -= gui
QT += multimedia

CONFIG += c++11
CONFIG += link_pkgconfig

PKGCONFIG += opencv

TARGET = VideoToOpenCvDemo
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    application.cpp \
    cameraframegrabber.cpp

HEADERS += \
    application.h \
    cameraframegrabber.h
