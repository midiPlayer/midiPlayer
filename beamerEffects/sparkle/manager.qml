import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
Item{
    id: myMgr
    GridLayout{
        columns:2
        anchors.fill: parent;
        
        Text{
         text: "Ball Size";
         color: "#fff"
        }

        Slider{
            id: ballSizeSlider
         minimumValue:5
         maximumValue:200
         onValueChanged:{
          send();
         }
        }
        
        Text{
         text: "Delay";
         color: "#fff"
        }

        Slider{
            id: delaySlider
         minimumValue:0
         maximumValue:1000
         onValueChanged:{
          send();
         }
        }
        
        
        Text{
         text: "Fading";
         color: "#fff"
        }
        Slider{
            id: fadingSlider
         minimumValue:0
         maximumValue:1000
         onValueChanged:{
            send(); 
         }
        }
        
        Text{
         text: "Motion distance";
         color: "#fff"
        }
        Slider{
            id: motionDistSlider
         minimumValue:0
         maximumValue:100
         onValueChanged:{
            send(); 
         }
        }
        
        Text{
         text: "Motion duration";
         color: "#fff"
        }
        Slider{
            id: motionDurSlider
         minimumValue:0
         maximumValue:200
         onValueChanged:{
            send(); 
         }
        }
        
    }
    
    function send(){
        myMgr.parent.send({"fading":fadingSlider.value,
            "delay":delaySlider.value,
            "motionDist":motionDistSlider.value,
            "motionDur":motionDurSlider.value,
            "ballSize": ballSizeSlider.value
        });
    }

 function recieve(msg){
     console.log(JSON.stringify(msg));
     if(msg.hasOwnProperty("delay"))
         delaySlider.value = msg.delay;
     if(msg.hasOwnProperty("fading"))
         fadingSlider.value = msg.fading;
     if(msg.hasOwnProperty("motionDist"))
         motionDistSlider.value = msg.motionDist;
     if(msg.hasOwnProperty("motionDur"))
         motionDurSlider.value = msg.motionDur;
     if(msg.hasOwnProperty("ballSize"))
         ballSizeSlider.value = msg.ballSize;
 }
    
} 
 
