//Fg
import QtQuick 2.7
Item{
    id: effect
    anchors.fill: parent;
    /*Canvas{
        anchors.fill: parent;
        onPaint: {
            var ctx = getContext("2d");
            ctx.fillStyle = Qt.rgba(1, 1, 1, 1);
            ctx.fillRect(0, 0, width/2, height);
        }
    }*/
    
    property string sparkleColor: "#ff0000";
    property int sparkleDelay: 200;
    property int sparkleFading: 0;
    property int motionDist:0;
    property int motionDur:100;
    property int ballSize:20;
    
    function color(name,value){
        sparkleColor = value;
    }

    Repeater{
        id: rep
        delegate: Rectangle{
            id: deleg
            color:effect.sparkleColor;
            width:ballSize;
            height: width
            radius: width/2
            
            y: effect.height * Math.random();

            opacity: 0;
            Timer{
                interval: sparkleDelay*Math.random();
               running: true
               onTriggered: {
                   runMoveEffect();
                  deleg.opacity = 1;
               }
            }

            Behavior on opacity {
                NumberAnimation{
                    duration: sparkleFading;
                }
            }
            
            Behavior on x {
                id:animX
                enabled:false
                NumberAnimation{
                    duration: motionDur;
                }
            }
            
            Behavior on y {
                id:animY
                enabled:false
                NumberAnimation{
                    duration: motionDur;
                }
            }
        
            function runMoveEffect(){
                x = effect.width * Math.random();
                animX.enabled = true;
                animY.enabled = true;
                var angle = Math.random()*360;
                 x+= motionDist * Math.sin(angle);
                 y+= motionDist * Math.cos(angle);
                 
                 console.log("anchor"+angle);
            }
            
            
            
        }
        model:10
    }

    function recieve(msg){
        if(msg.hasOwnProperty("delay"))
            sparkleDelay = msg.delay;
        if(msg.hasOwnProperty("fading"))
            sparkleFading = msg.fading;
        if(msg.hasOwnProperty("motionDist"))
            motionDist = msg.motionDist;
        if(msg.hasOwnProperty("motionDur"))
            motionDur = msg.motionDur;
        if(msg.hasOwnProperty("ballSize"))
            ballSize = msg.ballSize;
    }

    function trigger(){
        rep.model = 0
        rep.model = 10
    }


}
