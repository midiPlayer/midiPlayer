//Size, Color
import QtQuick 2.7
import RGBWColor 1.1;
Item{
    id: effect
    anchors.fill: parent;
    
    function color(name,value){
        if(name === "Size"){
            sizeColor.argbString = value;
            console.log(value)
        }
        else if(name === "Color"){
            canvas.waveColor = value;
        }
    }
    

    RGBWColor{
     id: sizeColor;
    }

    Canvas{
        id: canvas
        anchors.fill: parent;
        renderStrategy: Canvas.Cooperative;
        renderTarget: Canvas.FramebufferObject;
        property string waveColor : "#1188ff";
        onPaint: {
            var ctx = getContext("2d");
            //ctx.reset();
            ctx.clearRect(0,0, width, height);
            ctx.resetTransform();
            ctx.moveTo(0,0);
            ctx.strokeStyle = waveColor;
            ctx.beginPath();
            ctx.lineWidth = 30;
            for(var i = 0; i < 100; i++){
             ctx.lineTo(i/100*width,wave(i/100.0)*height + height/2);
            }
            ctx.stroke();
        }
        
    


        function wave(x){
            //return 0.3;
           //console.log(sizeColor.brightness);
            return Math.sin((x)*20 )/2 * sizeColor.brightness * sizeColor.a;
        }
    }

    Timer{
        interval:10;
       onTriggered: {
           canvas.requestPaint();
       }
       repeat: true;
       running: true;
    }
    

    function recieve(msg){

    }

    function trigger(){
    }


}
