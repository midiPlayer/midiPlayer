import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
Item{
    id: myMgr
    GridLayout{
        columns:2
        anchors.fill: parent;
        Text{
         text: "Größe";
         color: "#fff"
        }

        Slider{
            id: maxSizeSlider
         minimumValue:0
         maximumValue:1
         onValueChanged:{
          myMgr.parent.send({"maxSize":value});   
         }
        }
        
    }

 function recieve(msg){
     console.log(JSON.stringify(msg));
     if(msg.hasOwnProperty("maxSize"))
         maxSizeSlider.value = msg.maxSize;
 }
    
} 
 
