//Bg,Fg
import QtQuick 2.5
Item{
 anchors.fill: parent;

 
 function color(name,value){
     if(name === "Fg")
         ballColor = value;
 }

 function trigger(){
     widthAnim.enabled = false;
     circle.width = height * maxSize;
     widthAnim.enabled = true;
     circle.width = height * 0.1;
 }
 
 property double maxSize: 0.9;
 property var ballColor: "#fff";
 
 function recieve(msg){
     console.log(JSON.stringify(msg));
     if(msg.hasOwnProperty("maxSize"))
         maxSize = msg.maxSize;
 }

 Rectangle{
     Behavior on width{
         id: widthAnim
         NumberAnimation{

             duration: 500

         }
     }

     id:circle
    width: parent.height * 0.1;
    height: width
    radius: width/2
    x: parent.width / 2 - width/2
    y: parent.height / 2 - height/2
    color:parent.ballColor;
 }
    
} 
