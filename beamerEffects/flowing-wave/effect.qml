//Wave
import QtQuick 2.7
Item{
    id: effect
    anchors.fill: parent;
    
    function color(name,value){
            canvas.waveColor = value;
    }

    Canvas{
        id: canvas
        anchors.fill: parent;
        renderStrategy: Canvas.Cooperative;
        renderTarget: Canvas.FramebufferObject;
        property string waveColor : "#1188ff";
        onPaint: {
            var ctx = getContext("2d");
            //ctx.reset();
            ctx.clearRect(0,0, width, height);
            ctx.resetTransform();
            ctx.moveTo(0,0);
            ctx.strokeStyle = waveColor;
            ctx.beginPath();
            ctx.lineWidth = 30;
            for(var i = 0; i < 100; i++){
             ctx.lineTo(i/100*width,getWaves(i/100.0)*height + height/2);
            }
            ctx.stroke();
        }
        
        

        property var waves : [];

        function getWaves(x){
            var ret = 0;
            var newWaves = [];
            for(var i = 0; i < waves.length; i++){
                var now = new Date().getTime();
                if(now - waves[i]  < 10000){
                    newWaves.push( waves[i]);
                    ret += wave(x,waves[i]);
                }
            }
            waves = newWaves;

            return ret;
        }


        function wave(x,start){
            x += 1;
            var delta = new Date().getTime() - start;
            delta /= 2000;
            delta = 0 - delta;;
            x += delta;
            var mul = x / 0.8;
            mul = Math.pow(4,mul) /4;
            if(x > 0.8){
                mul = (x - 0.8)/ 0.2;
                mul = 1 - mul;
                mul = Math.max(0,mul);
            }
            return (Math.sin((x*3)*Math.PI*2))/2 * (mul) * 0.9;
        }
    }

    Timer{
        interval:10;
       onTriggered: {
           canvas.requestPaint();
       }
       repeat: true;
       running: true;
    }
    

    function recieve(msg){

    }

    function trigger(){
        canvas.waves.push(new Date().getTime());
    }


}
